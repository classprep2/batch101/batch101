// Variable to store session number for printing out the grade for the session
let sessionNumber = 23;
const { performance } = require('perf_hooks');

const assert = require('assert'); 
const expect = require("chai").expect;

const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

if(!jsonData.gradedTest.s23){

	try {

		// Requires the path to the index file inside the activity folder
		const activity = require(`../sessions/backend/s${sessionNumber}/activity/index`);

		// Variable to store passing unit test scores for the session
		let passingTests = 0;
		// Variable to store total number of session unit tests
		let totalTests = 0;

		describe(`s${sessionNumber}`, function () {

			let startTime;
			let errors = [];
			before(async function() {
			  startTime = performance.now();
			});

			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})
			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}

				if (this.currentTest.state === 'failed') {
					errors.push({
						test: this.currentTest.title,
						feedback: this.currentTest.err.message
					});
				}
			})



			// Unit tests for the activity
			it(`Incomplete inputs to match appropriate login message.`, function () {
					expect(activity.login("kagome123", "admin")).to.match(/empty/,"Incomplete input does not match instructed login message.");
			});
		    it(`admin to match appropriate login message.`, function () {
		    		expect(activity.login("Kagome", "kagome123", "admin")).to.match(/admin!/,"Admin input does not match instructed login message.");
		    });

		    it(`teacher to match appropriate login message.`, function () {
		    		expect(activity.login("Kagome", "kagome123", "teacher")).to.match(/teacher!/,"Teach input does not match instructed login message.");
		    });

		    it(`student to match appropriate login message.`, function () {
		    		expect(activity.login("Kagome", "kagome123", "student")).to.match(/student!/,"Student input does not match instructed login message");
		    });

		    it(`undefined role to match appropriate login message.`, function () {
		    		expect(activity.login("Kagome", "kagome123", "carpenter")).to.match(/range./,"Out of Range input does not match instructed login message");
		    });

		    it(`< 75 should return a grade of F`, function () {
		    		expect(activity.checkAverage(74, 74, 74, 74)).to.match(/F/,"< 75 grade does not output correct equivalent");                   
		    });

		    it(`> 75 and < 85 should return a grade of C`, function () {  
		    		expect(activity.checkAverage(83, 83, 83, 83)).to.match(/C/,"> 75 and < 85 grade does not output correct equivalent");                   
		    });

		    it(`> 95 and < 85 should return a grade of A+`, function () {  
		    		expect(activity.checkAverage(96, 96, 99, 97)).to.match(/A+/,"> 95 and < 85 grade does not output correct equivalent");
		    });


			// Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
			// Use of objects with the "session number" as keys to allow for ease of automation
			after(function () {

				const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);
				
				global.gradesObject[`s${sessionNumber}`] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
					feedback: errors.length ? errors : "No Errors"
				}

				jsonData.gradedTest.s23 = true;
				fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
			});

		});
		
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		  console.log(err)
		}
	}
} else {
	console.log(`S${sessionNumber} - Tested`);
}


