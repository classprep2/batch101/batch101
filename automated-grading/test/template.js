// //template: Copy this template to get started.
// //Note: Add only the unit tests and imports from chatGPT.//Sample

// let sessionNumber = 27;

// const assert = require('assert'); 
// const expect = require("chai").expect;

// // Requires the path to the index file inside the activity folder
// const activity = require(`../backend/s${sessionNumber}/activity/index`);
// // Variable to store passing unit test scores for the session
// let passingTests = 0;
// // Variable to store total number of session unit tests
// let totalTests = 0;

// describe(`s${sessionNumber}`, function () {

// 	// Increments the totalTests variable to use in computation of session score
// 	beforeEach(function () {
// 		totalTests += 1;
// 	})

// 	afterEach(function () {
// 		// this.currentTest.state allows to access the state of each test after it is run
// 		// returns either "passed" or "failed" for the "state" property
// 		if (this.currentTest.state == "passed") {
// 			passingTests += 1;
// 		}
// 	})


// 	//Add unit tests here
// 	it("testname1",()=>{

// 	})

// 	// Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
// 	// Use of objects with the "session number" as keys to allow for ease of automation
// 	after(function () {
// 		global.gradesObject[sessionNumber] = {
// 			grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
// 			score: `${passingTests}/${totalTests}`
// 		}

// 		// jsonData.gradedTest.s25 = true;
// 		// fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData));
// 	});

// });
