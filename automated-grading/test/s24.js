// Variable to store session number for printing out the grade for the session
let sessionNumber = 24;

const assert = require('assert'); 
const expect = require("chai").expect;
const { performance } = require('perf_hooks');

const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

if(!jsonData.gradedTest.s24){

	try {
		// Requires the path to the index file inside the activity folder
		const activity = require(`../sessions/backend/s${sessionNumber}/activity/index`);

		// Variable to store passing unit test scores for the session
		let passingTests = 0;
		// Variable to store total number of session unit tests
		let totalTests = 0;

		

		describe(`s${sessionNumber}`, function () {

			let startTime;
			let errors = [];
			const {numberLooper} = activity;

			before(async function() {
			  startTime = performance.now();
			  originalConsoleLog = console.log;
			});

			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})
			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}

				if (this.currentTest.state === 'failed') {
					errors.push({
						test: this.currentTest.title,
						feedback: this.currentTest.err.message
					});
				}
			})


			it('should terminate the loop when number is less than or equal to 50', () => {
				const result = numberLooper(40);
				expect(result).to.include('40',"Does not return the correct message if number is less than 50");
			  });
			
			  it('should skip printing numbers divisible by 10', () => {
				const consoleOutput = [];
				console.log = (message) => consoleOutput.push(message);
			
				numberLooper(75);
				expect(consoleOutput).to.include('The number is divisible by 10. Skipping the number.',"Does not print instructed message if a number is divisible by 10.");
			
				// console.log = originalConsoleLog;
			  });
			
			  it('should print numbers divisible by 5', () => {
				const consoleOutput = [];
				console.log = (message) => consoleOutput.push(message);
			
				numberLooper(75);
				console.log = originalConsoleLog;

				expect(consoleOutput).to.include(75,"Does not print instructed message if a number is divisible by 5.");
				expect(consoleOutput).to.include(65,"Does not print instructed message if a number is divisible by 5.");
				expect(consoleOutput).to.include(55,"Does notprint instructed message if a number is divisible by 5.");
			
				
			  });
			
			  it('should return the termination message', () => {
				const result = numberLooper(100);
				expect(result).to.include('50',"Does not return the correct message if number is 50");
			  });


		    it('should not contain any vowels in the string', () => {
		    	expect(activity.filteredString).to.equal('sprclfrglstcxpldcs');        
		    })

			// Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
			// Use of objects with the "session number" as keys to allow for ease of automation
			after(function () {
				
				console.log = originalConsoleLog;
				const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);

				global.gradesObject[`s${sessionNumber}`] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
					feedback: errors.length ? errors : "No Errors"
				}
				
				jsonData.gradedTest.s24 = true;
				fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
			});

		});
		
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		  console.log(err)
		}
	}
} else {
	console.log(`S${sessionNumber} - Tested`);
}


