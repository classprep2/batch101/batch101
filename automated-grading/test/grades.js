const path = require("path");

// Used to access the currently working directory to grab the name of the student from the root directory of the cloned project for automation
let pwd = process.cwd();
let base =  path.basename(pwd);

// Global variable to store the grade equivalent and the numerical score per session
global.gradesObject = {};

// Used to create the "grades.txt" file containing the scores of all unit tests
let fs = require('fs');

// Function to import test files for modularity in updating
function importTest(path) {
	require(path);
}

describe("Activity Error Reference", function () {
	
	// importTest function added to allow modularity when updating test files for specific sessions
	importTest('./s03');
	importTest('./s04');
	importTest('./s05');
	// importTest('./s06');
	// importTest('./s07');
	importTest('./s07');
	importTest('./s08');
	importTest('./s09');
	importTest('./s10');
	importTest('./s11');
	importTest('./s12');
	importTest('./s13');
	// importTest('./s14');
	importTest('./s15');
	importTest('./s19');
	importTest('./s20');
	importTest('./s21');
	importTest('./s22');
	importTest('./s23');
	importTest('./s24');
	importTest('./s25');
	importTest('./s26');
	importTest('./s27');
	importTest('./s28');
	importTest('./s29');
	importTest('./s30');
	importTest('./s31');
	// importTest('./s32');
	importTest('./s33');
	importTest('./s34');
	importTest('./s35');
	importTest('./s36');
	importTest('./s37');
	importTest('./s38');
	importTest('./s39');
	importTest('./s40');
	importTest('./s41');
	importTest('./s42');
	importTest('./s43');
	importTest('./s44');
	importTest('./s45');
	importTest('./s46');
	importTest('./s47');
	importTest('./s16-s17');
	// importTest('./s49-s53');

	// Executes the file generation for the compiled students grades after all test groups have been completed
	after(function () {

		// Prints out the formatted result of all tests in the console
		console.log(" ");
		console.log("============================");
		// Uses the current directory's name as the header for the list (e.g. to be the student's name)
		console.log(base);
		console.log("============================");
		// Prints out the formatted string of the session number, the grade equivalent and the score count for each session/test
		let gradesObjectKeys = Object.keys(gradesObject);
		gradesObjectKeys.forEach((gradeKey) => { console.log(`${gradeKey} - ${gradesObject[gradeKey].grade} - ${gradesObject[gradeKey].score}`) });
		console.log("============================");
		
		gradesObject.name = base;
		const sortedGrades = Object.keys(gradesObject)
		.sort()
		.reduce((accumulator, key) => {
			accumulator[key] = gradesObject[key];
			return accumulator;
		}, {});

		if(fs.existsSync('./grades.json')){

			const data = fs.readFileSync('./grades.json');
			const grades = JSON.parse(data);

			gradesObjectKeys.forEach((gradeKey)=> {
				grades[gradeKey] = sortedGrades[gradeKey];
			});

			grades.name = base;
			fs.writeFileSync('./grades.json', JSON.stringify(grades,null,4), function (err) {
				if (err) throw err;
				console.log('grades.json successfully created.');
			});

		} else {
			fs.writeFileSync('./grades.json', JSON.stringify(sortedGrades,null,4), function (err) {
				if (err) throw err;
				console.log('grades.json successfully created.');
			});

		}

	});
});