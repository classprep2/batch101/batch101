// Variable to store session number for printing out the grade for the session
let sessionNumber = 37;

const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

const assert = require('assert');

const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

const { performance } = require('perf_hooks');

if(!jsonData.gradedTest.s37){

  try {

    // Requires the path to the index file inside the activity folder
    let {app} = require(`../sessions/backend/s37/activity/index`)

    // Variable to store passing unit test scores for the session
    let passingTests = 0;
    // Variable to store total number of session unit tests
    let totalTests = 0;

    describe(`s37`, function () {

      let startTime;
      let errors = [];
			before(async function() {
			  startTime = performance.now();
			});

			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})
			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}
        if (this.currentTest.state === 'failed') {
          errors.push({
              test: this.currentTest.title,
              feedback: this.currentTest.err.message
          });
        }
			})


      it('should export an http.createServer function', function() {
        expect(typeof app).to.equal('object',"app must be an object");
        expect(typeof app.listen).to.equal('function',"app.listen must be a function");
      });

      it('should run on port 3000', function() {
        expect(app._connectionKey).to.include('3000');
      });

      it('should have a "/login"', (done)=>{
        chai.request(app)
        .get('/login')
        .end((err, res) => {
          expect(res).to.have.status(200, 'Expecting status code should be 200');
          done();
        })
      })


      it('should have a "/login" with response that includes the word "login"', (done)=>{
        chai.request(app)
        .get('/login')
        .end((err, res) => {
          expect(res.text.toLowerCase()).to.include("login","response text does not include 'login'")
          done();
        })
      })

     
      it('should have a checker for all other routes', (done)=>{
        chai.request(app)
        .get('/*')
        .end((err, res) => {
          expect(res).to.have.status(404, 'Expecting status code 404 for all other routes');
          done();
        })
      })

      it('should have a checker for all other routes with response that includes the word "cannot be found"', (done)=>{
        chai.request(app)
        .get('/*')
        .end((err, res) => {
          expect(res.text.toLowerCase(),"response text does not include 'cannot be found'").to.have.include("cannot").include("be").include("found");
          done();
        })
      })
      
      // Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
      // Use of objects with the "session number" as keys to allow for ease of automation
      after(function () {

        const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);
        
				global.gradesObject[`s${sessionNumber}`] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
          feedback: errors.length ? errors : "No Errors"
				}

        jsonData.gradedTest.s37 = true;
        fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));

      });

    });
    
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		  console.log(err)
		}
	}
} else {
  console.log(`S${sessionNumber} - Tested`);
}



