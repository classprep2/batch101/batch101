// Variable to store session number for printing out the grade for the session
let sessionNumber = 42;

const chai = require('chai');
// Chai http package allows for use of request method to process HTTP requests on API
const http = require('chai-http');
// Stores expect method from chai module to use for sending HTTP requests
const expect = chai.expect;
// Allows use of chai methods like "request", "post", etc.
chai.use(http);

const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

if(!jsonData.gradedTest.s42){

	try {

		// Requires the path to the index file inside the activity folder
		const app = require(`../sessions/backend/s${sessionNumber}/index`);

		// Variable to store passing unit test scores for the session
		let passingTests = 0;
		// Variable to store total number of session unit tests
		let totalTests = 0;
		// Variable to store the task_id to be checked

		describe(`s${sessionNumber}`, function () {

			this.timeout(30000);


			let task_id;
			// Runs before all tests in the test group to retrieve the first id of the task
			// Used to allow testing for dynamic routes with the task_id
			// before(function () {

			// })
			let errors = [];
			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})

			// // Tests whether a task in the database is successfully
			it("test_api_tasks_update_to_complete_status", (done)=>{
				// let link = `/tasks/6108e03b2b2c815964d95c63/complete`
				// console.log(link);
				// The "request" method accepts base url for the request
				// The "put" method accepts the specified route to be added on top of the base url
				// The "end" method is used to add the different test assertions/expectations from the request
				// The "res" object has access to a "body" property which contains the return value of the API
				// The dynamic route does not work with a variable as part of the URL. Bug fix for the test needed
				// Needs bugfix, task_id is hardcoded due to possible error with mocha chai environment unable to capture updated variable value from "before" method
				// getId((taskId)=>{
				chai.request(app)
				.get(`/tasks/`)
				.end((err, res) => {
					// task_id = res.body[0]._id
					task_id = res.body[0]._id;

					chai.request(app)
					.put(`/tasks/${task_id}/complete`)
					.end((err,res)=>{

						expect(res.body.status.toLowerCase(),"response status not equal to complete").to.equal('complete');
						done();
					})
					
				})
			})

			// Increases the amount of passing tests if test succeeds to generate final score
			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}
				if (this.currentTest.state === 'failed') {
                    errors.push({
                        test: this.currentTest.title,
                        feedback: this.currentTest.err.message
                    });
                }
			})

			// Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
			// Use of objects with the "session number" as keys to allow for ease of automation
			after(async function () {
				global.gradesObject[`s${sessionNumber}`] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
                    feedback: errors.length ? errors : "No Errors"
				}

				jsonData.gradedTest.s42 = true;
				await fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
			});

		});
		
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		  console.log(err)
		}
	}
} else {
	console.log(`S${sessionNumber} - Tested`);
}



