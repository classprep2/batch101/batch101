# WDC028v1.5b - Automated Grading

## How to use Automated Grading
This document serves as a guide for the WDC028v1.5b MCP sessions automated grading.

| Topic                                  | Link             |
|----------------------------------------|------------------|
| New Folder Structure                   | [Link](#folders) |
| Steps to Use Automated Grading         | [Link](#howto)   |

<a name="folders"></a>
## New Folder Structure
  
### Trainee Folder Structure Update 

1. Create the trainee's named folder in their documents as lastname-batchNumber
2. Create a folder each course (frontend/backend/fullstack) in the trainee's named folder which will be where to add session folders.
3. Add the session folder in its appropriate session folder.
   - Add discussion/activity folders in session folders avoid creating d1 or a1.
  ```
      lastname-batchNumber
          --- frontend
            -- s1
              - discussion
              - activity
         ...
          --- backend
            -- s15
                - discussion
                - activity
            -- s16
                - discussion
                - activity
            -- s17
                - discussion
                - activity
  ```
4. This folder structure must be similar in the trainee's gitlab folders.
```
      lastname-batchNumber
         --- frontend (folder)
           -- s1 (repo)
             - discussion
             - activity
         ...
         --- backend (repo)
           -- s15
               - discussion
               - activity
           -- s16
               - discussion
               - activity
           -- s17
               - discussion
               - activity
  ```
5. Since frontend sessions are not yet added in automated grading, create independent repos per session as-is.
6. However, for the backend, instead, have trainees create an online/remote repo called backend
7. Initialize the local backend folder as a local repository and add its remote link.
8. Push the backend folder to git with the defined commit message in each activity.

<a name="howto"></a>

## Steps to Use Automated Grading

### Step 1
- Create a batch folder.
- For each trainee, Create a folder with the trainee's name with their lastName-firstName.
- Open a git bash/terminal on the trainee's folder.
- Initialize an npm project with the following command:
```bash
  npm init -y
```
A package.json file must be created.

### Step 2
Install the needed node packages with the following command:
````
   npm i mocha chai chai-http selenium-webdriver geckodriver follow-redirects
```` 

Note: You can also copy the contents of the package.json from the automated-grading folder in the wdc028v1.5b repo. Then simply reinstall all the packages using npm i.

Open sublime text and check the package.json file after installation to confirm.
Then add the following in the scripts property:
   ````
   ...
   "scripts": {
   "test": "mocha --recursive"
   }
   ....
   ````

Install Firefox for the capstone 1 test.

https://www.mozilla.org/en-US/firefox/download/thanks/

Notes:
- Selenium IDE is used for the capstone 1 testing and it requires geckodriver.
- geckodriver uses Firefox to check a web page.


### Step 3

  Clone the wdc028v1.5b repo and copy the tests folder in the automated-grading folder into the trainee's named folder.

### Step 4

Clone/Copy the trainee's frontend folder (which contains the trainee's frontend activities) and backend folder (which contains the trainee's backend activities) into the trainee's named folder.

### Step 5

When checking the trainee's capstone 1, using sublime text, open tests/s13-s14.js to provide the details for the students' capstone.

- liveLink - the hosted capstone link
- name - trainee's provided name for checking
- isMulti - provide boolean to determine if capstone is multiple page or single page
- isTested - provide boolean if the capstone has already been checked. (Selenium repeatedly launches Firefox for checking)

![s13-s14.js](readme-assets/s13-s14.png)

Note:
- Update isTested value to true after test.
- This is to avoid re-checking when using npm test.

### Step 6
Open a terminal in the trainee's named folder and trigger the command "npm test" which will run all the tests inside the subfolders, check for the activities, compile the grades and generate a "grades.txt" file at the root folder.

![sample npm test run!](readme-assets/sample-npmtest.png)

- Running the tests again will append a second set of grades to the "grades.txt" file. If the tests need to be run again to recheck the grades, delete the "grades.txt" file.

### Step 7

Update the students backend folder by opening terminal in the cloned backend folder pulling updates from the trainee's backend repo using the git pull command.

![git pull in the backend folder!](readme-assets/git-pull.png)

**Repeat steps 6 and 7 when checking for each session**

**Important Notes:**

- **When** pulling expressjs activities (s34 - s41), **reinstall** node packages for each expressjs sessions when checking.
  - Open a terminal in the backend folder and input the following command for ease of use:
      ````
      git pull origin master && cd <sessionNumber> && npm i  
      ````

## Automated Grading Notes:

- s26 - s30 are mongodb sessions and currently not included in automated checking.
- s31 - s33 are nodejs/postman sessions and currently not included in automated checking.
- All entry point files must be standardized to index.js to avoid failing tests.
- The folder names should use the correct format provided in the tests (e.g. s[sessionNumber] = s31). Incorrect folder name will result in tests not running properly
