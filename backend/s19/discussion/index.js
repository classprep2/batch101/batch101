// [SECTION] Syntax, Statements and Comments

console.log("Hello World");

// [SECTION] Variables


// Declaring variables
let myVariable;
console.log(myVariable);

let hello;
console.log(hello);


// Declaring and initializing variables
let productName = 'desktop computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;


// Reassigning variable values
productName = 'Laptop';
console.log(productName);

//let variable cannot be re-declared within its scope. So while this will work:
let friend = 'Kate';

//this will return an error
let friend = 'Kate';
// let friend = 'Jane'; // error: Identifier 'greeting' has already been declared

// Reassigning variables vs initializing variables

// Declares a variable first
let supplier;

// Initialization is done after the variable has been declared
supplier = "John Smith Tradings";
console.log(supplier);

// This is considered as reassignment because it's initial value was already declared
supplier = "Zuitt Store";
console.log(supplier);


//Can you declare a const variable without initialization? No. An error will occur.
/*
Example: 

    const pi;
    pi=3.1416;
    console.log(pi)
*/



// Multiple variable declarations

let productCode = 'DC017';
const productBrand = 'Dell';
console.log(productCode, productBrand);


// [SECTION] Data Types

// Strings

let country = 'Philippines';
let province = "Metro Manila"; 

// Concatenating strings
let fullAddress = province + ', ' + country;
console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting);

let mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress);

let message = "John's employees went home early";
console.log(message);
message = 'John\'s employees went home early';
console.log(message);

// Numbers
let headcount = 26;
console.log(headcount);

// Decimal Numbers/Fractions
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text and strings
console.log("John's grade last quarter is " + grade);

// Boolean
let isMarried = false;
let inGoodConduct = true;
console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + inGoodConduct);

// Arrays
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// different data types
let details = ["John", "Smith", 32, true];
console.log(details);

// Objects
let person = {

    fullName: 'Juan Dela Cruz',
    age: 35,
    isMarried: false,
    contact: ["+63917 123 4567", "8123 4567"],
    address: {
        houseNumber: '345',
        city: 'Manila'
    }

}

console.log(person);

// They're also useful for creating abstract objects
let myGrades = {
    firstGrading: 98.7, 
    secondGrading: 92.1, 
    thirdGrading: 90.2, 
    fourthGrading: 94.6
}

console.log(myGrades);


//typeof operator is used to determine the type of data or the value of a variable. It outputs a string.
console.log(typeof myGrades);//object

console.log(typeof grades);



// Null
let spouse = null;

let myNumber = 0;
let myString = '';

// Undefined
let fullName;
console.log(fullName);

// Undefined vs Null
let varA = null;
console.log(varA);

let varB;
console.log(varB);

