/*
===============================
S34 - MongoDB - CRUD Operations
===============================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/17bWumR-t9424AwXlJAApeKWhHUeFizg7qRdmnmvpw0w/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s23

Other References:
	Intro to MongoDB
		https://docs.mongodb.com/manual/introduction/
	Databases and Collections
		https://docs.mongodb.com/manual/core/databases-and-collections/
	Mongo DB Community Server Installation
		Linux
			https://docs.mongodb.com/manual/administration/install-on-linux/
		MacOS
			https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/
		Windows
			https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/
	Mongo DB Community Server
		https://www.mongodb.com/try/download/community
	Mongo DB Account Registration
		https://account.mongodb.com/account/register
	Google Public DNS
		https://developers.google.com/speed/public-dns/docs/using
	Open DNS
		https://use.opendns.com/
	MongoDB Manage mongod process
		https://docs.mongodb.com/manual/tutorial/manage-mongodb-processes/
	MongoDB Troubleshooting Connection Issues
		https://docs.atlas.mongodb.com/troubleshoot-connection/
	Robo3t Download
		https://studio3t.com/download/?source=robomongo&medium=homepage
	MongoDB Compass
		https://docs.mongodb.com/compass/current/install/
	MongoDB insertOne Method
		https://docs.mongodb.com/manual/reference/method/db.collection.insertOne/
	MongoDB insertMany Method
		https://docs.mongodb.com/manual/reference/method/db.collection.insertMany/
	MongoDB find Method
		https://docs.mongodb.com/manual/reference/method/db.collection.find/
	MongoDB pretty Method
		https://docs.mongodb.com/manual/reference/method/cursor.pretty/
	MongoDB updateOne Method
		https://docs.mongodb.com/manual/reference/method/db.collection.updateOne/
	MongoDB updateMany Method
		https://docs.mongodb.com/manual/reference/method/db.collection.updateMany/
	MongoDB replaceOne Method
		https://docs.mongodb.com/manual/reference/method/db.collection.replaceOne/
	MongoDB deleteOne Method
		https://docs.mongodb.com/manual/reference/method/db.collection.deleteOne/
	MongoDB deleteMany Method
		https://docs.mongodb.com/manual/reference/method/db.collection.deleteMany/
	Creating Git Projects:
		GitLab
			https://gitlab.com/projects/new#blank_project
		GitHub
			https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application
*/

/*
==========
Discussion
==========
*/

/*
1. Present the slides and discuss the concepts found in our materials.
	Browser > Google Slides
*/

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentations for Discussion Slides, Intro to MongoDB and Databases and Collections.
		*/

/*
2. Install the Mongo DB Community Server
	Device > Terminal
*/

		// Linux
			wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -
		// MacOS
			brew tap mongodb/brew
			brew install mongodb-community@5.0
			brew services start mongodb/brew/mongodb-community
		// Windows
			https://www.mongodb.com/try/download/community

		/*
		Important Note:
			- Upon creation of this material Mongo DB Community Server 5.0 is the latest version.
			- MongoDB installation in Linux and MacOS systems are done via terminal commands.
			- MongoDB installation in windows is done by downloading the installer and double clicking on the downloaded file.
			- The MongoDB Community Server will allow us to use MongoDB locally within our devices.
			- The use of this will also give the students an idea that backend development is possible without access to the internet.
			- Installing the MongoDB Community Server is best done due to students encountering issues with connecting to MongoDB Atlas, which is the online database for MongoDB, for a number of reasons.
			- This will be used as a backup in case the students encounter connection issues to MongoDB Atlas.
			- The instructor may have the students use this while troubleshooting issues encountered by students in preparation for future sessions.
			- MongoDB Atlas is the online version of the MongoDB database while the MongoDB Community Server is the local counterpart of MongoDB Atlas.
			- Refer to "references" section of this file to find the documentations for Mongo DB Community Server Installation and Mongo DB Community Server Download Link.
		*/

/*
3. Create a MongoDB account.
	Browser > MongoDB
*/

		// https://account.mongodb.com/account/register

		/*
		Important Note:
			- It is recommended that the students register an account using their professional gmail accounts requested by the CAs upon registration for the bootcamp.
			- Make sure that registration is done through this page to ensure that the students may be able to follow along with the registration.
			- MongoDB has several pages for creating accounts and some of them redirect the students to adding an organization to their MongoDB accounts.
			- Creating an account using their gmail accounts will simplify the process of logging in using the Single Sign-On (SSO) approach where as long as their Google accounts are logged in their devices, they can easily just click on the "Log In With Google" button.
			- If this occurs, have the students register any organization name. After this is done, guide the students in by requesting them to share their screen until they reach the cluster setup procedure after creating their accounts.
			- Upon setting up of the students' accounts for "Database Access", have the students create an account with the password "admin123" which is equivalent to an 8 letter alphanumeric combination which is easy to remember providing the username as the prefix to the password and numbers starting from 1 to the nth number to complete an 8 character combination.
			- Doing this will ease troubleshooting steps ensuring that the students have the same username and password which is easy to remember.
			- The Database Access password may be changed anytime upon completion of the bootcamp to provide more security to their accounts.
			- Upon setting up of the students' accounts for "Network Access", ensure that they create access to all devices by allowing access to anywhere using the 0.0.0.0/0 IP address which can be access via the following:
				- Browser > MongoDB > Network Access > IP Access List Tab > Add IP Address Button.
			- The "IP Access List" may be updated anytime and changed upon completion of the bootcamp to provide more security to their accounts.
			- You may refer to the slides provided in the S23 Discussion slides to find the step by step procedure to have this completed.
			- Refer to "references" section of this file to find the documentations for Mongo DB Acount Registration and Discussion Slides.
		*/

/*

4. Run the mongo shell application.
	Device > Terminal
*/

	// MongoDB atlas
		mongo "<MongoDB Connection String>"
		mongo "<MongoDB Connection String>" --username <MongoDB Database Username> --password <MongoDB Database Password>

	// MongoDB Connection String
		mongodb+srv://<Username>:<Password>@cluster0.7iowx.mongodb.net/<Database Name>?retryWrites=true&w=majority

	// local mongoDB
		mongo "mongodb://localhost:27017/<Database Name>"

		/*
		Important Note:
			- The discussion maybe accomplished by using the mongo shell or by using robo3t.
			- All operating systems connect to the Mongo DB Atlas using the same command.
			- The syntax provided above with angle braces (<>) means that the information need to be replaced with corresponding information established in the user's MongoDB account.
			- The following information may be found via the following:
				MongoDB Connection String
					Browser > MongoDB > Databases > Connect > Connect Your Application
				MongoDB Username and MongoDB Password
					Browser > MongoDB > Database Access
			- Students may encounter issues when connecting to MongoDB Atlas and running MongoDB locally in their devices.
			- Several reasons may prevent them from connecting to MongoDB Atlas or running the MongoDB Community Server and the following are commonly encountered issues and the corresponding solutions:
				- Service provider security
					- Changing the DNS of their internet connection will mostly solve the issue.
					- Refer to "references" section of this file to find the documentations for Google Public DNS and Open DNS.
				- MongoDB processes
					- This occurs because the MongoDB service is not running properly in their devices. Common troubleshooting steps to resolve this issue is to start/restart the services manually.
					- Refer to "references" section of this file to find the documentation for MongoDB Manage mongod process.
				- Incorrect username/password provided for the MongoDB Account
					- Check if the correct username and password was provided in the MongoDB Connection String.
					- The username may be found by accessing the following:
						- Browser > MongoDB > Database Access
				- Network access was not successfully established in the MongoDB account
					- Make sure that the students register an IP address of 0.0.0.0/0 that allows access from any device during the bootcamp to prevent any issues from accessing their databases even if they switch to a different device.
		*/

/*
5. Create the MongoDB syntax in a text editor for code readability and demonstrate them using the Mongo Shell terminal.
	Device > Terminal
*/

		// CRUD Operations
		/*
		    - CRUD operations are the heart of any backend application.
		    - Mastering the CRUD operations is essential for any developer.
		    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
		    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
		*/

		// [Section] Inserting documents (Create)

		// Insert one document
		/*
		    - Since mongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods.
		    - The mongo shell also uses JavaScript for it's syntax which makes it convenient for us to understand it's code
		    - Creating MongoDB syntax in a text editor makes it easy for us to modify and create our code as opposed to typing it directly in the terminal where the whole code is only visible in one line.
		    - By using a text editor it allows us to type the syntax using multiple lines and simply copying and pasting the code in terminal will make it work.
		    - Syntax
		        - db.collectionName.insertOne({object});
		    - JavaScript syntax comparison
		        - object.object.method({object});
		*/
		db.users.insertOne({
		    firstName: "Jane",
		    lastName: "Doe",
		    age: 21,
		    contact: {
		        phone: "87654321",
		        email: "janedoe@gmail.com"
		    },
		    courses: [ "CSS", "Javascript", "Python" ],
		    department: "none"
		});

		// Insert Many
		/*
		    - Syntax
		        - db.collectionName.insertMany([ {objectA}, {objectB} ]]);
		*/
		db.users.insertMany([
		    {
		        firstName: "Stephen",
		        lastName: "Hawking",
		        age: 76,
		        contact: {
		            phone: "87654321",
		            email: "stephenhawking@gmail.com"
		        },
		        courses: [ "Python", "React", "PHP" ],
		        department: "none"
		    },
		    {
		        firstName: "Neil",
		        lastName: "Armstrong",
		        age: 82,
		        contact: {
		            phone: "87654321",
		            email: "neilarmstrong@gmail.com"
		        },
		        courses: [ "React", "Laravel", "Sass" ],
		        department: "none"
		    }
		]);

		// [Section] Finding documents (Read)
		// Find
		/*
		    - If multiple documents match the criteria for finding a document only the FIRST document that matches the search term will be returned
		    - This is based from the order that documents are stored in a collection
		    - If a document is not found, the terminal will respond with a blank line
		    - Syntax
		        - db.collectionName.find();
		        - db.collectionName.find({ field: value });
		*/

		// Finding a single document
		// Leaving the search criteria empty will retrieve ALL the documents
		db.users.find();

		db.users.find({ firstName: "Stephen" });

		// The "pretty" method allows us to be able to view the documents returned by our terminals in a better format
		db.users.find({ firstName: "Stephen" }).pretty();

		// Finding documents with multiple parameters
		/*
		    - Syntax
		        - db.collectionName.find({ fieldA: valueA, fieldB: valueB });
		*/
		db.users.find({ lastName: "Armstrong", age: 82 }).pretty();

		// [Section] Updating documents (Update)

		// Updating a single document

		// Creating a document to update
		db.users.insertOne({
		    firstName: "Test",
		    lastName: "Test",
		    age: 0,
		    contact: {
		        phone: "00000000",
		        email: "test@gmail.com"
		    },
		    courses: [],
		    department: "none"
		});

		/*
		    - Just like the "find" method, methods that only manipulate a single document will only update the FIRST document that matches the search criteria.
		    - Syntax
		        - db.collectionName.updateOne( {criteria}, {$set: {field: value}});
		*/
		db.users.updateOne(
		    { firstName: "Test" },
		    {
		        $set : {
		            firstName: "Bill",
		            lastName: "Gates",
		            age: 65,
		            contact: {
		                phone: "12345678",
		                email: "bill@gmail.com"
		            },
		            courses: ["PHP", "Laravel", "HTML"],
		            department: "Operations",
		            status: "active"
		        }
		    }
		);

		db.users.find({ firstName: "Bill" }).pretty();

		// Updating multiple documents
		/*
		    - Syntax
		        - db.collectionName.updateMany( {criteria}, {$set: {field: value}});
		*/
		db.users.updateMany(
		   { department: "none" },
		   {
		     $set: { department: "HR" }
		   }
		);

		db.users.find().pretty();

		// Replace One
		/*
		    - Can be used if replacing the whole document is necessary.
		    - Syntax
		        - db.collectionName.replaceOne( {criteria}, {$set: {field: value}});
		*/
		db.users.replaceOne(
		    { firstName: "Bill" },
		    {
		        firstName: "Bill",
		        lastName: "Gates",
		        age: 65,
		        contact: {
		            phone: "12345678",
		            email: "bill@gmail.com"
		        },
		        courses: ["PHP", "Laravel", "HTML"],
		        department: "Operations"
		    }
		);

		db.users.find({ firstName: "Bill" }).pretty();

		// [Section] Deleting documents (Delete)

		// Creating a document to delete
		db.users.insert({
		    firstName: "test"
		});

		// Deleting a single document
		/*
		    - Syntax
		        - db.collectionName.deleteOne({criteria});
		*/
		db.users.deleteOne({
		    firstName: "test"
		});

		db.users.find().pretty();

		// Delete Many
		/*
		    - Be careful when using the "deleteMany" method. If no search criteria is provided, it will delete all documents in a database.
		    - DO NOT USE: databaseName.collectionName.deleteMany()
		    - Syntax
		        - db.collectionName.deleteMany({criteria});
		*/
		db.users.deleteMany({ 
		    firstName: "Bill"
		});

		db.users.find().pretty();



//************** To be removed in discussion, to add in Take Home Quiz

		// [Section] Advanced queries
		/*
		    - Retrieving data with complex data structures is also a good skill for any developer to have.
		    - Real world examples of data can be as complex as having two or more layers of nested objects and arrays.
		    - Learning to query these kinds of data is also essential to ensure that we are able to retrieve any information that we would need in our application
		*/

		// Query an embedded document
		db.users.find({
		    contact: {
		        phone: "87654321",
		        email: "stephenhawking@gmail.com"
		    }
		}).pretty();

		// Query on nested field
		db.users.find(
		    {"contact.email": "janedoe@gmail.com"}
		).pretty();

		// Querying an Array with Exact Elements
		db.users.find( { courses: [ "CSS", "Javascript", "Python" ] } ).pretty();

		// Querying an Array without regard to order
		db.users.find( { courses: { $all: ["React", "Python"] } } ).pretty();

		// Querying an Embedded Array
		db.users.insert({
		    namearr: [
		        {
		            namea: "juan"
		        },
		        {
		            nameb: "tamad"
		        }
		    ]
		});

		db.users.find({
		    namearr: 
		        {
		            namea: "juan"
		        }
		}).pretty();

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentations for MongoDB insertOne Method, MongoDB insertMany Method, MongoDB find Method, MongoDB pretty Method, MongoDB updateOne Method, MongoDB updateMany Method, MongoDB replaceOne Method, MongoDB deleteOne Method and MongoDB deleteMany Method.
		*/

/*
========
Activity
========
*/

/*
1. Create an index.js file and copy the contents from template.js. Read and understand the additional instructions from the template.

2. Create a new database called hotel (MongoDB Compass)

3. In the addOneFunc(), copy and paste your query to insert a single room (insertOne method) in the rooms collection with the following details:
*/

		// Inserting a single document
		db.rooms.insert({
		    name: "single",
		    accomodates: 2,
		    price: 1000,
		    description: "A simple room with all the basic necessities",
		    rooms_available: 10,
		    isAvailable: false
		});

/*
4. In the addManyFunc(), copy and paste your query to insert multiple rooms (insertMany method)  in the rooms collection with the following details:

*/

		// Inserting multiple documents
		db.rooms.insertMany([
		    {
		        name: "double",
		        accomodates: 3,
		        price: 2000,
		        description: "A room fit for a small family going on a vacation",
		        rooms_available: 5,
		        isAvailable: false
		    },
		    {
		        name: "queen",
		        accomodates: 4,
		        price: 4000,
		        description: "A room with a queen sized bed perfect for a simple getaway",
		        rooms_available: 15,
		        isAvailable: false
		    }
		]);

/*
5. In the findRoom(), copy and paste your query to use the findOne method to search for a room with the name double.
*/

		// Find a document
		db.rooms.find({ name: "double" }).pretty();

/*
6. In the updateOneFunc(), copy and paste your query to use the updateOne method to update the queen room and set the available rooms to 0
*/

		// Updating a document
		db.rooms.updateOne(
		    { name: "queen" },
		    {
		        $set : {
		            rooms_available: 0
		        }
		    }
		);

		db.rooms.find({ name: "queen" }).pretty();

/*
7. In the deleteManyFunc(), copy and paste your robo3T query to use the deleteMany method to delete all rooms that have 0 rooms available
*/

		// Deleting multiple documents
		db.rooms.deleteMany({
		    rooms_available: 0
		})

		db.rooms.find().pretty();

/*

8. Once done with your solution, remove all the comments as these will cause error in checking the output.

9. Update your local backend git repository and push to git with the commit message of Add activity code s34.

10. Add the link in Boodle for s34
*/