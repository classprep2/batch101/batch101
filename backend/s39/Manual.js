/*
======================================
S39 - Introduction to Postman and REST
======================================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1UhizSK9gnWNFmA1ddALIQdTc9ZeJVBHqnUSVGqJyG7M/edit#slide=id.gaaf8d9a761_0_603
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s28

Other References:
	JavaScript Synchronous and Asynchronous
		https://betterprogramming.pub/is-javascript-synchronous-or-asynchronous-what-the-hell-is-a-promise-7aa9dd8f3bfb
	Execution of Synchronous and Asynchronous codes
		https://siddharthac6.medium.com/javascript-execution-of-synchronous-and-asynchronous-codes-40f3a199e687
	Programming Jokes API
		https://official-joke-api.appspot.com/jokes/programming/random
	What Is An API
		https://apifriends.com/api-management/what-is-an-api/
	JSON Placeholder
		https://jsonplaceholder.typicode.com/guide/
	Request Response Cycle
		https://iq.opengenus.org/content/images/2019/09/Untitled-1-.png
	Fetch API - JavaScript Tutorial
		https://www.javascripttutorial.net/javascript-fetch-api
	Fetch API - MDN Web Docs
		https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API
	Using Fetch
		https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
	Promise Diagram
		https://wtcindia.files.wordpress.com/2016/06/promises.png?w=605
	What is a Promise
		https://medium.com/javascript-scene/master-the-javascript-interview-what-is-a-promise-27fc71e77261
	Promise - MDN Web Docs
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise
	Using Promise
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises
	then Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/then
	How To Use Fetch With Async Await
		https://dmitripavlutin.com/javascript-fetch-async-await/
	Async and Await
		https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Asynchronous/Async_await
	REST API Diagram
		https://usercontent.one/wp/www.kennethlange.com/wp-content/uploads/2020/04/customer_rest_api.png
	REST API - MDN Web Docs
		https://developer.mozilla.org/en-US/docs/Glossary/REST
	What Is The Difference Between PUT and PATCH
		https://rapidapi.com/blog/put-vs-patch/
	Creating Git Projects:
		GitLab
			https://gitlab.com/projects/new#blank_project
		GitHub
			https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
==========
Discussion
==========
*/

// 1. Create a "discussion" folder and create an "index.html" file inside of it.
	// Batch Folder > S28 > discussion > index.html

		/*
		<!DOCTYPE html>
		<html>
		<head>
			<title>Postman and REST API</title>
		</head>
		<body>
			<script src="index.js"></script>
		</body>
		</html>
		*/


// 2. Create an "index.js" file and discuss the concepts of synchronous and asynchronous.
	// Application > index.js

		// [Section] Javascript Synchronous vs Asynchronous
		// Javascript is by default is synchronous meaning that only one statement is executed at a time

		// This can be proven when a statement has an error, javascript will not proceed with the next statement
		console.log("Hello World");
		//conosle.log("Hello Again");
		console.log("Goodbye");

		// When certain statements take a lot of time to process, this slows down our code
		// An example of this are when loops are used on a large amount of information or when fetching data from databases
		// When an action will take some time to process, this results in code  "blocking"
		console.log("Hello World");
		// We might not notice it due to the fast processing power of our devices
		// This is the reason why some websites don't instantly load and we only see a white screen at times while the application is still waiting for all the code to be executed
		// for(let i = 0; i <= 1500; i++){
		// 	console.log(i);
		// }
		// Only after the loop ends will this statement execute
		console.log("Hello Again");

		// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for JavaScript Synchronous and Asynchronous and a document on Execution of Synchronous and Asynchronous codes.
		*/

// 3. Access the API endpoint for programming jokes and give the students a brief overview about APIs.
	// Browser > Programming Jokes

		/*
		Important Note:
			- Refer to "references" section of this file to find the link to the Programming Jokes API and a documentation of What Is An API.
		*/

// 4. Create a simple fetch request.
	// Application > index.js

		/*...*/
		console.log("Hello Again");

		// [Section] Getting all posts

		// The Fetch API allows you to asynchronously request for a resource (data)
		// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value
		// Syntax
			// fetch('URL')
		console.log(fetch('https://jsonplaceholder.typicode.com/posts')
		);

		/*
		Important Note:
			- Refer to "references" section of this file to find several documentation and diagrams regarding the Request Response Cycle, Fetch API and Promises.
		*/

// 5. Check the status of the request.
	// Application > index.js

		/*...*/
		console.log(fetch('https://jsonplaceholder.typicode.com/posts')
		);

		// Syntax
			// fetch('URL')
			// .then((response)=>{})

		// Retrieves all posts following the Rest API (retrieve, /posts, GET)
		// By using the then method we can now check for the status of the promise
		fetch('https://jsonplaceholder.typicode.com/posts')
		// The "fetch" method will return a "promise" that resolves to a "Response" object
		// The "then" method captures the "Reponse" object and returns another "promise" which will eventually be "resolved" or "rejected"
		.then(response => console.log(response.status));

		/*
		Important Note:
			- Refer to "references" section of this file to find a documentation on the then Method.
		*/

// 6. Retrieve the contents/data from the "Response" object.
	// Application > index.js

		/*...*/
		.then(response => console.log(response.status));

		fetch('https://jsonplaceholder.typicode.com/posts')
		// Use the "json" method from the "Response" object to convert the data retrieved into JSON format to be used in our application
		.then((response) => response.json())
		// Print the converted JSON value from the "fetch" request
		// Using multiple "then" methods creates a "promise chain"
		.then((json) => console.log(json));

		/*
		Important Note:
			- The contents/data of the response object cannot be accessed directly from the response object.
			- We would need to convert it into a JSON format in order for us to be able to access the data response that is sent back by the API.
		*/

// 7. Create a function that will demonstrate using the "async" and "await" keywords.
	// Application > index.js

		/*...*/
		.then((json) => console.log(json));

		// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
		// Used in functions to indicate which portions of code should be waited for
		// Creates an asynchronous function
		async function fetchData(){

			// waits for the "fetch" method to complete then stores the value in the "result" variable
			let result = await fetch('https://jsonplaceholder.typicode.com/posts');
			// Result returned by fetch is a returns a promise
			console.log(result);
			// The returned "Response" is an object
			console.log(typeof result);
			// We cannot access the content of the "Response" by directly accessing it's body property
			console.log(result.body);

			// Converts the data from the "Response" object as JSON
			let json = await result.json();
			// Print out the content of the "Response" object
			console.log(json);

		};

		fetchData();

		/*
		Important Note:
			- Refer to "references" section of this file to find a documentation on How To Use Fetch With Async Await and Async and Await.
		*/

// 8. Process a GET request using postman to get all the posts.
	// Postman

		url: https://jsonplaceholder.typicode.com/posts
		method: GET

// 9. Retrieve a specific post.
	// Application > index.js

		/*...*/
		fetchData();

		// [Section] Getting a specific post

		// Retrieves a specific post following the Rest API (retrieve, /posts/:id, GET)
		fetch('https://jsonplaceholder.typicode.com/posts/1')
		.then((response) => response.json())
		.then((json) => console.log(json));

		/*
		Important Note:
			- Refer to "references" section of this file to find a diagram and a documentation on Rest API.
		*/

// 10. Process a GET request using postman to get an individual post.
	// Postman

		url: https://jsonplaceholder.typicode.com/posts/1
		method: GET

// 11. Create a post.
	// Application > index.js

		/*...*/
		.then((json) => console.log(json));

		// [Section] Creating a post

		// Syntax
			// fetch('URL', options)
			// .then((response)=>{})
			// .then((response)=>{})

		// Creates a new post following the Rest API (create, /posts/:id, POST)
		fetch('https://jsonplaceholder.typicode.com/posts', {
			// Sets the method of the "Request" object to "POST" following REST API
			// Default method is GET
			method: 'POST',
			// Sets the header data of the "Request" object to be sent to the backend
			// Specified that the content will be in a JSON structure
			headers: {
				'Content-type': 'application/json',
			},
			// Sets the content/body data of the "Request" object to be sent to the backend
			// JSON.stringify converts the object data into a stringified JSON
			body: JSON.stringify({
				title: 'New post',
				body: 'Hello world!',
				userId: 1
			}),
			
		})
		.then((response) => response.json())
		.then((json) => console.log(json));

// 12. Process a POST request using postman to create a post.
	// Postman

		url: https://jsonplaceholder.typicode.com/posts
		method: POST
		body: raw + JSON
			{
			    "title": "My First Blog Post",
			    "body": "Hello world!"
			    "userId": 1
			}

// 13. Update a post using the PUT method.
	// Application > index.js

		/*...*/
		.then((json) => console.log(json));

		// [Section] Updating a post

		// Updates a specific post following the Rest API (update, /posts/:id, PUT)
		fetch('https://jsonplaceholder.typicode.com/posts/1', {
			method: 'PUT',
			headers: {
		  	'Content-type': 'application/json',
			},
			body: JSON.stringify({
			  	id: 1,
			  	title: 'Updated post',
			  	body: 'Hello again!',
			  	userId: 1
			})
		})
		.then((response) => response.json())
		.then((json) => console.log(json));

// 14. Process a PUT request using postman to update a post.
	// Postman

		url: https://jsonplaceholder.typicode.com/posts/1
		method: PUT
		body: raw + JSON
			{
			    "title": "My First Revised Blog Post",
			    "body": "Hello there! I revised this a bit.",
			    "userId": 1
			}

// 15. Update a post using the PATCH method.
	// Application > index.js

		/*...*/
		.then((json) => console.log(json));

		// [Section] Updating a post using PATCH method

		// Updates a specific post following the Rest API (update, /posts/:id, Patch)
		// The difference between PUT and PATCH is the number of properties being changed
		// PATCH is used to update the whole object
		// PUT is used to update a single/several properties
		fetch('https://jsonplaceholder.typicode.com/posts/1', {
			method: 'PUT',
			headers: {
		  	'Content-type': 'application/json',
			},
			body: JSON.stringify({
			  	title: 'Corrected post',
			})
		})
		.then((response) => response.json())
		.then((json) => console.log(json));

		/*
		Important Note:
			- The PUT is a method of modifying resource where the client sends data that updates the entire resource. It is used to set an entity’s information completely.
			- The PATCH method applies a partial update to the resource.
			- Refer to "references" section of this file to find a diagram and a documentation on What Is The Difference Between PUT and PATCH.
		*/

// 16. Process a PATCH request using postman to update a post.
	// Postman

		url: https://jsonplaceholder.typicode.com/posts/1
		method: PATCH
		body: raw + JSON
			{
			    "title": "This is my final title."
			}

// 17. Delete a post.
	// Application > index.js

		/*...*/
		.then((json) => console.log(json));

		// [Section] Deleting a post

		// Deleting a specific post following the Rest API (delete, /posts/:id, DELETE)
		fetch('https://jsonplaceholder.typicode.com/posts/1', {
		  method: 'DELETE'
		});

// 18. Process a DELETE request using postman to delete a post.
	// Postman

		url: https://jsonplaceholder.typicode.com/posts/1
		method: DELETE

// 21. Filter the posts.
	// Application > index.js

		/*...*/
		fetch('https://jsonplaceholder.typicode.com/posts/1', {
		  method: 'DELETE',
		});

		// [Section] Filtering posts

		// The data can be filtered by sending the userId along with the URL
		// Information sent vie the url can be done by adding the question mark symbol (?)
		// Syntax
			// Individual Parameters
				// 'url?parameterName=value'
			// Multiple Parameters
				// 'url?paramA=valueA&paramB=valueB'
		fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
		.then((response) => response.json())
		.then((json) => console.log(json));

		/*
		Important Note:
			- Information can be sent via the URL via parameters.
			- The "?" symbol at the end of a URL indicates that parameters will be sent to the endpoint.
			- The format for sending parameters via a URL is as follows:
				- Single Parameter
					- url?parameterName=value
				- Multiple Parameters
					- url?paramA=valueA&paramB=valueB
			- Sending data via the URL does not only apply to JavaScript.
			- This can also be achieved when sending information via a form in HTML.
		*/

// 22. Retrieve comments of a specific post.
	// Application > index.js

		/*...*/
		.then((json) => console.log(json));

		// [Section] Retreving nested/related comments to posts

		// Retrieving comments for a specific post following the Rest API (retrieve, /posts/:id/comments, GET)
		fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
		.then((response) => response.json())
		.then((json) => console.log(json));

/*
========
Activity
========
*/

// Provide activity-template.js code to student in Boodle Notes.

/*
## Instructions that can be provided to the students for reference:

1. In the s39 folder, create an activity folder and an index.html and a index.js file inside of it.
2. Link the index.js file to the index.html file.
	a. Copy the activity code and instructions from your Boodle Notes into your index.js.
3. In the getAllToDo() function, add a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
	a. Using the data retrieved, create an array using the map method to return just the title of every item.
	b. Then, return the array.
4. In the getSpecificToDo() function, add a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
	a. In another then(), Return the retrieved json.
5. In the createToDo() function, add a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
	a. In another then(), Return the json response.
	b. The object returned must contain the values of the item created.
6. In the updateToDo() function, add a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
	a. Update a to do list item by changing the data structure to contain the following properties:
		i. Title
		ii. Description
		iii. Status
		iv. Date Completed
		v. User ID
7. In the deleteToDo() function, add a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.

Note: The next objectives are to be done with Postman
8. In Postman, Create a new collection called “s39-activity”.
9. Create a request via Postman to retrieve all the to do list items. 
	a. GET HTTP method
	b. https://jsonplaceholder.typicode.com/todos URI endpoint
	c. Save this request in the s33-activity collection with name as getAllToDo
10. Create a request via Postman to retrieve an individual to do list item. 
	a. GET HTTP method
	b. https://jsonplaceholder.typicode.com/todos/1 URI endpoint
	c. Save this request in the s33-activity collection with name as getSpecificToDo
11. Create a request via Postman to create a to do list item.
	a. POST HTTP method
	b. https://jsonplaceholder.typicode.com/todos URI endpoint
	c. Save this request in the s33-activity collection with name as createToDo
12. Create a request via Postman to update a to do list item.
	a. PUT HTTP method
	b. https://jsonplaceholder.typicode.com/todos/1 URI endpoint
	c. Save this request in the s33-activity collection with name as updateToDo
	d. Update the to do list item to mirror the data structure used in the PUT fetch request
13. Create a request via Postman to delete a to do list item.
	a. DELETE HTTP method
	b. https://jsonplaceholder.typicode.com/todos/1 URI endpoint
	c. Save this request in the s33-activity collection with name as deleteToDo
14. Export the s39-activity Postman collection and save it in the activity folder.
15. Update your local backend git repository and push to git with the commit message of Add activity code s39.
16. Add the link in Boodle for s39.

*/

// Solution:

// 1. Create an "activity" folder and create an "index.html" file inside of it.
	// Batch Folder > s33 > activity > index.html

		/*
			<!DOCTYPE html>
			<html>
			<head>
				<title>Postman and REST API Activity</title>
			</head>
			<body>
				<script src="index.js"></script>
			</body>
			</html>
		*/

// 2. Copy the template given and add the correct fetch request for each function:
	// Application > index.js

		//Note: don't add a semicolon at the end of then().
		//Fetch answers must be inside the await () for each function.
		//Each function will be used to check the fetch answers.
		//Don't console log the result/json, return it.

		// Get Single To Do [Sample]
		async function getSingleToDo(){

			return await (

			//add fetch here.
			
			fetch('<urlSample>')
			.then((response) => response.json())
			.then((json) => json)
		
		
			);

		}



		// Getting all to do list item
		async function getAllToDo(){

			return await (

				//add fetch here.
				
				fetch('https://jsonplaceholder.typicode.com/todos')
				.then((response) => response.json())
				.then((json) => {

					let list = json.map((todo => {
						return todo.title;
					}))

					return list;

				})


			);

		}

		// [Section] Getting a specific to do list item
		async function getSpecificToDo(){
		
			return await (

				//Add fetch here.


				fetch('https://jsonplaceholder.typicode.com/todos/1')
				.then((response) => response.json())
				.then((json) => json)


			);

		}

		// [Section] Creating a to do list item using POST method
		async function createToDo(){
		
			return await (

				//Add fetch here.


					fetch('https://jsonplaceholder.typicode.com/todos', {

						method: 'POST',
						headers: {
							'Content-type': 'application/json',
						},
						body: JSON.stringify({
							title: 'Created To Do List Item',
							completed: false,
							userId: 1
						})

					})
					.then((response) => response.json())
					.then((json) => json)


			);

		}

		// [Section] Updating a to do list item using PUT method
		async function updateToDo(){
			
			return await (

				//Add fetch here.


				fetch('https://jsonplaceholder.typicode.com/todos/1', {
					method: 'PUT',
					headers: {
						'Content-type': 'application/json',
					},
					body: JSON.stringify({
							title: 'Updated To Do List Item',
							description: 'To update the my to do list with a different data structure.',
							status: 'Pending',
							dateCompleted: 'Pending',
							userId: 1
					})
				})
				.then((response) => response.json())
				.then((json) => json)


			);

		}

		// [Section] Deleting a to do list item
		async function deleteToDo(){
			
			return await (

				//Add fetch here.
				fetch('https://jsonplaceholder.typicode.com/todos/1', {
						method: 'DELETE',
				})

			);

		}




		//Do not modify
		//For exporting to test.js
		try{
		module.exports = {
			getSingleToDo: typeof getSingleToDo !== 'undefined' ? getSingleToDo : null,
			getAllToDo: typeof getAllToDo !== 'undefined' ? getAllToDo : null,
			getSpecificToDo: typeof getSpecificToDo !== 'undefined' ? getSpecificToDo : null,
			createToDo: typeof createToDo !== 'undefined' ? createToDo : null,
			updateToDo: typeof updateToDo !== 'undefined' ? updateToDo : null,
			deleteToDo: typeof deleteToDo !== 'undefined' ? deleteToDo : null,
		}
		} catch(err){

		}

