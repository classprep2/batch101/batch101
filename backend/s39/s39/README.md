# S28 - Introduction to Postman and REST

## Topics
* What is an API?
* What is REST?
* Postman API Client

## Presentation
[Google Slides](https://docs.google.com/presentation/d/1UhizSK9gnWNFmA1ddALIQdTc9ZeJVBHqnUSVGqJyG7M/edit?usp=sharing)

## Instructions
1. Import this collection in Postman (in Postman, go to file -> import).
2. Follow the presentation slides.
3. Use this imported Postman collection as reference.
