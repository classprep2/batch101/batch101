/*
================================
S40 - Express.js - Introduction
================================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1GpL82WuYAuuy90OFNa3vtrgR9OEatt_KVlP8TxR5qQo/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		hhttps://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s29

Other References:
	npm init documentation
		https://docs.npmjs.com/cli/v7/commands/npm-init
	Express JS
		https://expressjs.com/
	What Is A Middleware
		https://www.redhat.com/en/topics/middleware/what-is-middleware
	Express JS json Method
		http://expressjs.com/en/resources/middleware/body-parser.html#bodyparserjsonoptions
	Express JS urlencoded Method
		http://expressjs.com/en/resources/middleware/body-parser.html#bodyparserurlencodedoptions
	Array splice Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice
	Creating Git Projects:
		GitLab
			https://gitlab.com/projects/new#blank_project
		GitHub
			https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
==========
Discussion
==========
*/

// 1. Create a "package.json" file
	// Batch Folder > S29 > Terminal

		npm init

		/*
		Important Note:
			- npm stands for "node package manager" which is responsible for assisting us developers with managing dependencies within a project.
			- Triggering this command will prompt the user for the different settings that will define the application.
			- No changes will be added to the package.json file, just keep on pressing "enter" key on the keyboard to create the default package.json file.
			- To prevent pressing the "enter" button upon creation of this file, the command "npm init -y" can be used to easily do this and skip the questionnaire.
			- You may highlight the questions asked after inputting the command then showing the students the package.json file to find the same items asked for are available in it.
			- Refer to "references" section of this file to find the documentation for npm init.
		*/

// 2. Install express JS.
	// Application > Terminal

		npm install express

		/*
		Important Note:
			- After triggering the command, express will now be listed under the "dependencies" property of the package.json file.
			- This will create a "node_modules" folder that will contain all the necessary files downloaded for the package.
			- The "package.json" file is responsible for listing all packages and the versions used in creating the application.
			- The version of the package installed is also indicated in the file which prevents applications from breaking if ever new updates are introduced to packages.
			- If the students incorrectly install a different package, the command "npm uninstall <package_name>" may be used.
			- A specific version of a package may also be installed by using the command "npm install@<version number>" (e.g. npm install@4.17.1).
			- This is useful for instances where an older version of the package is preferred to be used or when a current version of a package has bugs that tend to break an application.
			- Refer to "references" section of this file to find the link to the Express JS website.
		*/

// 3. Create a ".gitignore" file.
	// Application > index.js

		node_modules

		/*
		Important Note:
			- The "node_modules" directory should be left on the local machine to keep our git commits lightweight.
			- When pushing files in a git repository, normally the "node_modules" folder is added in the ".gitignore" file.
			- This is because packages installed in a project will create thousands of files that will take a very long time to upload to a git repository.
			- Upon successful cloning of a repository using the node package manager, triggering the command "npm install" will install all the missing dependencies with the specified versions in the package.json file to allow the application to work again as intended.
			- If a newer version of the package would like to be installed instead, you may trigger the command "npm install <package_name>" to overwrite the current version installed in an application.
		*/

// 5. Create an "index.js" file that will serve as our app's entry point and setup a simple server.
	// Application > index.js

		// Use the "require" directive to load the express module/package
		// A "module" is a software component or part of a program that contains one or more routines
		// This is used to get the contents of the express package to be used by our application
		// It also allows us access to methods and functions that will allow us to easily create a server
		const express = require("express");

		// Create an application using express
		// This creates an express application and stores this in a constant called app
		// In layman's terms, app is our server
		const app = express(); 

		// For our application server to run, we need a port to listen to
		const port = 4000;

		// Setup for allowing the server to handle data from requests
		// Allows your app to read json data
		// Methods used from express JS are middlewares
		// Middleware is software that provides common services and capabilities to applications outside of what’s offered by the operating system
		// API management is one of the common application of middlewares.
		app.use(express.json());
		// Allows your app to read data from forms
		// By default, information received from the url can only be received as a string or an array
		// By applying the option of "extended:true" this allows us to receive information in other data types such as an object which we will use throughout our application
		app.use(express.urlencoded({extended:true}));

		// Tells our server to listen to the port
		// If the port is accessed, we can run the server
		// Returns a message to confirm that the server is running in the terminal

		//if(require.main) would allow us to listen to the app directly if it is not imported to another module, it will run the app directly.
		//else, if it is needed to be imported, it will not run the app and instead export it to be used in another file.
		if(require.main === module){
		    app.listen(port, () => console.log(`Server running at port ${port}`));
		}

		module.exports = app;

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentations for What Is A Middleware, Express JS json Method and Express JS urlencoded Method.
			- require.main === module is used to avoid the server to be run immediately when tested.
		*/

// 6. Create a GET route.
	// Application > index.js

		/*...*/
		app.use(express.urlencoded({extended:true}));

		// [Section] Routes
		// Express has methods corresponding to each HTTP method
		// This route expects to receive a GET request at the base URI "/"
		// The full base URI for our local application for this route will be at "http://localhost:4000"
		// This route will return a simple message back to the client
		app.get("/", (req, res) => {

		    // Once the route is accessed it will in send a string response containing "Hello World"
		    // Compared to the previous session, res.end uses the node JS module's method
		    // res.send uses the express JS module's method instead to send a response back to the client
			res.send("Hello World");

		});

		//An if statement is added here to be able to export the following app/server for checking.
		if(require.main === module){
		    app.listen(port, () => console.log(`Server running at port ${port}`));
		}

		module.exports = app;

// 7. Process a GET request using postman.
	// Postman

		url: http://localhost:4000
		method: GET

// 8. Create another GET route.
	// Application > index.js

		app.get("/", (req, res) => {
			/*...*/
		});

		// This route expects to receive a GET request at the URI "/hello"
		app.get("/hello", (req, res) => {
			res.send("Hello from the /hello endpoint!");
		});

		//An if statement is added here to be able to export the following app/server for checking.
		if(require.main === module){
		    app.listen(port, () => console.log(`Server running at port ${port}`));
		}

		module.exports = app;

// 9. Process a GET request using postman.
	// Postman

		url: http://localhost:4000/hello
		method: GET

// 10. Create a POST route
	// Application > index.js

		/*...*/

		app.get("/hello", (req, res) => {
			/*...*/
		});

		// This route expects to receive a POST request at the URI "/hello"
		app.post("/hello", (req, res) => {
		    // req.body contains the contents/data of the request body
		    // All the properties defined in our Postman request will be accessible here as properties with the same names
		    res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
		});

		//An if statement is added here to be able to export the following app/server for checking.
		if(require.main === module){
		    app.listen(port, () => console.log(`Server running at port ${port}`));
		}

		module.exports = app;
		 

// 11. Process a POST request using postman.
	// Postman

		url: http://localhost:4000/hello
		method: POST
		body: raw + JSON
			{
			    "firstName": "John",
			    "lastName": "Doe"
			}

// 12. Create an array to use as our mock databse.
	// Application > index.js

		/*...*/

		// This route expects to receive a POST request at the URI "/hello"
		app.post("/hello", (req, res) => {
		    /*...*/
		});

		// An array that will store user objects when the "/signup" route is accessed
		// This will serve as our mock database
		let users = [];

		//An if statement is added here to be able to export the following app/server for checking.
		if(require.main === module){
		    app.listen(port, () => console.log(`Server running at port ${port}`));
		}

		module.exports = app;

// 13. Create a POST route to register a user.
	// Application > index.js

		/*...*/
		let users = [];

		// This route expects to receive a POST request at the URI "/signup"
		// This will create a user object in the "users" variable that mirrors a real world registration process
		app.post("/signup", (req, res) => {

		    console.log(req.body);

		    // If contents of the "request body" with the property "username" and "password" is not empty
		    if(req.body.username !== '' && req.body.password !== ''){

		        // This will store the user object sent via Postman to the users array created above
		        users.push(req.body);

		        // This will send a response back to the client/Postman after the request has been processed
		        res.send(`User ${req.body.username} successfully registered!`);

		    // If the username and password are not complete an error message will be sent back to the client/Postman
		    } else {

		        res.send("Please input BOTH username and password.");

		    }

		})

		//An if statement is added here to be able to export the following app/server for checking.
		if(require.main === module){
		    app.listen(port, () => console.log(`Server running at port ${port}`));
		}

		module.exports = app;

// 14. Process a POST request using postman.
	// Postman

		url: http://localhost:4000/signup
		method: POST
		body: raw + JSON
			{
			    "username": "johndoe",
			    "password": "john1234"
			}

// 15. Create a PUT route to change the password of a specific user.
	// Application > index.js

		/*...*/
		app.post("/signup", (req, res) => {
			/*...*/
		})

		// This route expects to receive a PUT request at the URI "/change-password"
		// This will update the password of a user that matches the information provided in the client/Postman
		app.put("/change-password", (req, res) => {

		    // Creates a variable to store the message to be sent back to the client/Postman 
		    let message;

		    // Creates a for loop that will loop through the elements of the "users" array
		    for(let i = 0; i < users.length; i++){

		        // If the username provided in the client/Postman and the username of the current object in the loop is the same
		        if (req.body.username == users[i].username) {

		            // Changes the password of the user found by the loop into the password provided in the client/Postman
		            users[i].password = req.body.password;

		            // Changes the message to be sent back by the response
		            message = `User ${req.body.username}'s password has been updated.`;

		            // Breaks out of the loop once a user that matches the username provided in the client/Postman is found
		            break;

		        // If no user was found
		        } else {

		            // Changes the message to be sent back by the response
		            message = "User does not exist.";

		        }

		    }

		    // Sends a response back to the client/Postman once the password has been updated or if a user is not found
		    res.send(message);

		})

		//An if statement is added here to be able to export the following app/server for checking.
		if(require.main === module){
		    app.listen(port, () => console.log(`Server running at port ${port}`));
		}

		module.exports = app;

// 14. Process a PUT request using postman.
	// Postman

		url: http://localhost:4000/change-password
		method: PUT
		body: raw + JSON
			{
			    "username": "johndoe",
			    "password": "johndoe1234"
			}

/*
========
Activity
========
*/

/*
## Instructions that can be provided to the students for reference:

1. Create a GET route that will access the /home route that will send a response with a simple message:
	a. "Welcome to the home page"
2. Process a GET request at the /home route using postman.
3. Create a GET route that will access the /users route that will send a the users array as a response.
4. Process a GET request at the /users route using postman.
5. Create a DELETE route that will access the /delete-user route to remove a user from the mock database.
6. The delete-user route logic is as follows:
	a. Create a variable to store the message to be sent back to the client/Postman 
	b. Create a condition to check if there are users found in the array
		i. If there is, create a for loop that will loop through the elements of the "users" array
			1. Add an if statement that if the username provided in the request body and the username of the current object in the loop is the same
				a. If it is, remove the current object from the array. You can use splice array method.
				b. Change the message to be sent back by the response:
					i. ”User <username> has been deleted.”
				c. Break the loop.

			2.	Outside the loop, add an if statement that if the message variable is undefined, update the message variable with the following message:
				a. "User does not exist."
	c. Add an else statement if there are no users in the array, update the message variable with the following message:
		a. "No users found."
	d. Send a the message variable as a response back to the client/Postman once the user has been deleted or if a user is not found
7. Process a DELETE request at the /delete-user route using postman.
8. Export the Postman collection and save it inside the root folder of our application.
9. Update your local backend git repository and push to git with the commit message of Add activity code s40.
10. Add the link in Boodle for s40.


*/

// 1. Create a GET route that will access the "/home" route that will print out a simple message.
	// Application > index.js

		/*...*/
		app.put("/change-password", (req, res) => {
			/*...*/
		})

		// This route expects to receive a GET request at the URI "/home"
		app.get("/home", (req, res) => {
		    res.send("Welcome to the home page");
		})

		app.listen(port, () => console.log(`Server running at port ${port}`));

// 2. Process a GET request at the "/home" route using postman.
	// Postman

		url: http://localhost:4000/home
		method: GET

// 3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
	// Application > index.js

		/*...*/
		app.get("/home", (req, res) => {
		    /*...*/
		})

		// This route expects to receive a GET request at the URI "/users"
		// This will retrieve all the users stored in the variable created above
		app.get("/users", (req, res) => {
		    res.send(users);
		})

		app.listen(port, () => console.log(`Server running at port ${port}`));

// 4. Process a GET request at the "/users" route using postman.
	// Postman

		url: http://localhost:4000/users
		method: GET

// 5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
	// Application > index.js

		/*...*/
		app.get("/users", (req, res) => {
		    /*...*/
		})

		// This route expects to receive a DELETE request at the URI "/delete-user"
		// This will remove the user from the array for deletion
		// This route expects to receive a DELETE request at the URI "/delete-user"
		// This will a user from the array for deletion
		app.delete("/delete-user", (req, res) => {

		    // Creates a variable to store the message to be sent back to the client/Postman 
		    let message;

		    // Creates a condition if there are users found in the array
		    if (users.length != 0){

		        // Creates a for loop that will loop through the elements of the "users" array
		        for(let i = 0; i < users.length; i++){

		            // If the username provided in the client/Postman and the username of the current object in the loop is the same
		            if (req.body.username == users[i].username) {

		                // The splice method manipulates the array and removes the user object from the "users" array based on it's index
		                // users[i] is used here to indicate the start of the index number in the array for the element to be removed
		                // The number 1 defines the number of elements to be removed from the array
		                users.splice(users[i], 1);
		                // Changes the message to be sent back by the response
		                message = `User ${req.body.username} has been deleted.`;

		                break;

		            } 

		        }

		        if(message === undefined){
		            message = "User does not exist."
		        }

		    // If no user was found
		    } else {
		        // Changes the message to be sent back by the response
		        message = "No users found.";

		    }
		    
		    // Sends a response back to the client/Postman once the user has been deleted or if a user is not found
		    res.send(message);

		})


		// Tells our server to listen to the port
		// If the port is accessed, we can run the server
		// Returns a message to confirm that the server is running in the terminal
		// Listen to the port, meaning, if the port is accessed, we run the server

		//An if statement is added here to be able to export the following app/server for checking.
		if(require.main === module){
		    app.listen(port, () => console.log(`Server running at port ${port}`));
		}

		module.exports = app;
		 

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for Array splice Method.
			- You may provide this link to the students for reference.
		*/

// 6. Process a DELETE request at the "/delete-user" route using postman.
	// Postman

		url: http://localhost:4000/users
		method: Delete
		body: raw + JSON
			{
			    "username": "johndoe"
			}