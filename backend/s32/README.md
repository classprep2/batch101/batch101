# Session Objectives

At the end of the session, the students are expected to:

- learn how data is stored in a computer system by discussing the concepts related to a database.

# Resources

## Instructional Materials

- [Google Slide Presentation](https://docs.google.com/presentation/d/1opuYhkyDrolEp99JwNMXmi-3hH_sHjgkM68pRZGtZn4/edit#slide=id.g53aad6d9a4_0_728)
- [Google Form](https://docs.google.com/forms/d/1F8K1zq_eIMPP1OIcGnmqinokzVhKawfYUuD-fjHGHUY/edit)
- [Lesson Plan](lesson-plan.md)

## Supplemental Materials

- [What is a database? (Oracle Philippines)](https://www.oracle.com/ph/database/what-is-database/)
- [Intro to MongoDB (MongoDB Docs)](https://docs.mongodb.com/manual/introduction/)
- [Databases and Collections (MongoDB Docs)](https://docs.mongodb.com/manual/core/databases-and-collections/)

# Lesson Proper

## What is a database?

A database is an organized collection of information or data. Databases typically refer to information stored in a computer system but it can also refer to physical databases. One such example is a library which is a database of books.

Data is different from information. Data is raw and does not carry any specific meaning. Information on the other hand is a group of organized data that contains logical meaning.

A database is managed by what is called a database management system.

## What is a database management system?

A database management system (or DBMS) is a system specifically designed to manage the storage, retrieval and modification of data in a database. Continuing on the example earlier, the DBMS of a physical library is the Dewey Decimal System.

The Dewey Decimal System allows a librarian to organize the content of the library based on the division of all knowledge there is. [[reference](https://www.britannica.com/science/Dewey-Decimal-Classification#:~:text=Dewey%20Decimal%20Classification%2C%20also%20called,each%20group%20assigned%20100%20numbers.)]

Database systems allows four types of operations when it comes to handling data, namely create (insert), read (select), update and delete. Collectively it is called the CRUD operations.

Relational database is a type of database where data is stored as a set of tables with rows and columns (pretty much like a table printed on a piece of paper). For unstructured data (data that cannot fit into a strict tabular format), NoSQL databases are commonly used.

## What is SQL?

SQL stands for structured query language. It is the language used typically in relational DBMS to store, retrieve and modify data. A code in SQL looks like this:

```sql
SELECT id, first_name, last_name FROM students WHERE batch_number = 1;
```

The code above instructs the DBMS to retrieve the ID and name of the students that belongs to Batch 1.

An alternative to this approach is NoSQL. We can write the SQL code earlier into something like this:

```jsx
db.students.find({ batchNumber: 1 }, {id: 1, firstName: 1, lastName: 1});
```

## What is NoSQL?

NoSQL means Not only SQL. NoSQL was conceptualized when capturing complex, unstructured data became more difficult. One of the popular NoSQL databases is MongoDB.

## What is MongoDB?

MongoDB is an open-source database and the leading NoSQL database. Its language is highly expressive, and generally friendly to those already familiar with the JSON structure.

"Mongo" in MongoDB is a part of the word "humongous" which then means huge or enormous.

## What does data stored in MongoDB look like?

Since MongoDB is a database that uses key-value pairs, our data looks like this:

```json
{
    "_id" : ObjectId("5d3007ad8a427f3fbd6f2fd3"),
    "firstName" : "Sylvan",
    "lastName" : "Cahilog",
    "position" : "Instructor",
    "createdAt" : ISODate("2019-07-18T05:46:21.028Z"),
    "updatedAt" : ISODate("2019-07-18T05:46:21.028Z")
}
```

The main advantage of this data structure is that it is stored in MongoDB using the JSON format. A developer does not need to keep in mind the difference between writing code for the program itself and data access in a DBMS.

Also, in MongoDB, some terms in relational databases will be changed:

- from tables to **collections**;
- from rows to **documents**; and
- from columns to **fields**

# Activity

## Instructions

Copy the quiz template from this [link](https://docs.google.com/forms/d/1F8K1zq_eIMPP1OIcGnmqinokzVhKawfYUuD-fjHGHUY/edit) to your batch kit.

Answer the quiz form provided by your instructor.

## Solution

What is a table in MongoDB?

- collection, collections

What is a row in MongoDB?

- document, documents

What is a column in MongoDB?

- field, fields

Mongo in MongoDB means?

- humongous, huge, enormous

NoSQL means?

- Not only SQL, No SQL

SQL stands for?

- Structured Query Language

An organized collection of information or data.

- database

Raw and does not carry any specific meaning.

- data

Data that contains logical meaning.

- information

A type of database where data is stored as a set of tables with rows and columns.

- relational, relational database