# WDC028 - S26 - MongoDB - Intro to Database and NoSQL

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1opuYhkyDrolEp99JwNMXmi-3hH_sHjgkM68pRZGtZn4/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1opuYhkyDrolEp99JwNMXmi-3hH_sHjgkM68pRZGtZn4/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s21) |
| Manual                  | Link                                                         |
| Google Form             | [Link](https://docs.google.com/forms/d/1F8K1zq_eIMPP1OIcGnmqinokzVhKawfYUuD-fjHGHUY/edit) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                           | Link                                                         |
| ----------------------------------------------- | ------------------------------------------------------------ |
| Relational Database                             | [Link](https://www.oracle.com/ph/database/what-is-a-relational-database/) |
| Introduction to SQL                             | [Link](https://www.w3schools.com/sql/sql_intro.asp)          |
| Non-Relational Database and NoSQL               | [Link](https://docs.microsoft.com/en-us/azure/architecture/data-guide/big-data/non-relational-data) |
| Intro to MongoDB                                | [Link](https://docs.mongodb.com/manual/introduction/)        |
| Databases and Collections                       | [Link](https://docs.mongodb.com/manual/core/databases-and-collections/) |
| MongoDB Community Server Installation - Linux   | [Link](https://docs.mongodb.com/manual/administration/install-on-linux/) |
| MongoDB Community Server Installation - MacOS   | [Link](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/) |
| MongoDB Community Server Installation - Windows | [Link](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/) |
| MongoDB Community Server                        | [Link](https://www.mongodb.com/try/download/community)       |
| MongoDB Account Registration                    | [Link](https://account.mongodb.com/account/register)         |
| Google Public DNS                               | [Link](https://developers.google.com/speed/public-dns/docs/using) |
| Open DNS                                        | [Link](https://use.opendns.com/)                             |
| MongoDB Manage mongod Process                   | [Link](https://docs.mongodb.com/manual/tutorial/manage-mongodb-processes/) |
| MongoDB Troubleshooting Connection Issues       | [Link](https://docs.atlas.mongodb.com/troubleshoot-connection/) |
| Robo3t Download                                 | [Link](https://studio3t.com/download/?source=robomongo&medium=homepage) |
| MongoDB Compass                                 | [Link](https://docs.mongodb.com/compass/current/install/)    |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[20 mins] - What is a database?
	2.[10 mins] - What is a database management system?
		- CRUD Operations
	3.[20 mins] - What is SQL?
	4.[20 mins] - What is NoSQL?
	5.[20 mins] - What is MongoDB?
	6.[30 mins] - What does data stored in MongoDB look like?
	7.[1 hr] - MongoDB Environment Check
	8.[1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

​	

[Back to top](#table-of-contents)
