/*
==========================
S25 - JavaScript - Objects
==========================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1P25QHleQpcd6xXCFNX_ZhFR8KAviByiNsExz2McHydo/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s18

Other References:
	Working With Objects
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Working_with_Objects
	JavaScript Objects
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object
	Object Literal Vs. Constructor In JavaScript
		https://medium.com/@mandeepkaur1/object-literal-vs-constructor-in-javascript-df143296b816
	JavaScript this Keyword
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/this
	Instance Definition
		https://www.codemotion.com/magazine/Glossary/instance-computer-science/
	JavaScript new Operator
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/new
	Difference Between Object Literal and Instance Object
		https://blog.kevinchisholm.com/javascript/difference-between-object-literal-and-instance-object/
	JavaScript Property Accessors
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Property_accessors
	JavaScript delete Operator
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/delete
	General Method Definition
		https://developer.mozilla.org/en-US/docs/Glossary/Method
	JavaScript Method Definition
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Method_definitions
	Creating Git Projects:
		GitLab
			https://gitlab.com/projects/new#blank_project
		GitHub
			https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
==========
Discussion
==========
*/

/*
1. Create an "index.html" file.
	Batch Folder > S18  > Discussion > index.html
*/

		/*
		<!DOCTYPE HTML>
		<html>
		    <head>
		        <title>JavaScript Objects</title>
		    </head>
		    <body>
		    </body>
		</html>
		*/

/*
2. Create a "script.js" file and to test if the script is properly linked to the HTML file.
	Application > script.js
*/

		console.log("Hello World!");

/*
3. Link the "script.js" file to our HTML file.
	Application > script.html
*/

		/*
		<!DOCTYPE HTML>
		<html>
		    <!-- ... -->
		    <body>
		    	<script src="./script.js"></script>
		    </body>
		</html>
		*/

		/*
		Important Note:
			- The "script" tag is commonly placed at the bottom of the HTML file right before the closing "body" tag.
			- The reason for this is because Javascript's main function in frontend development is to make our websites and applications interactive.
			- In order to achieve this, JavaScript selects/targets specific HTML elements in our application and performs a certain output.
			- It is added last to allow all HTML and CSS resources to load first before applying any JavaScript code to run.
			- Placing the "script" tag at the top the the file might result in errors because since the HTML elements have not yet been loaded when the JavaScript loads, it does not have any valid HTML elements to target/select.
		*/

/*
4. Add code to the "script.js" file to demonstrate and discuss Objects.
	Application > script.js
*/

		// console.log('Hello World');

		// [Section] Objects
		/*
		    - An object is a data type that is used to represent real world objects
		    - It is a collection of related data and/or functionalities
		    - In JavaScript, most core JavaScript features like strings and arrays are objects (Strings are a collection of characters and arrays are a collection of data)
		    - Information stored in objects are represented in a "key:value" pair
		    - A "key" is also mostly referred to as a "property" of an object
		    - Different data types may be stored in an object's property creating complex data structures
		*/

		// Creating objects using object initializers/literal notation
		/*
		    - This creates/declares an object and also initializes/assigns it's properties upon creation
		    - A cellphone is an example of a real world object
		    - It has it's own properties such as name, color, weight, unit model and a lot of other things
		    - Syntax
		        let objectName = {
		            keyA: valueA,
		            keyB: valueB
		        }
		*/
		let cellphone = {
		    name: 'Nokia 3210',
		    manufactureDate: 1999
		};

		console.log('Result from creating objects using initializers/literal notation:');
		console.log(cellphone);
		console.log(typeof cellphone);

		// Creating objects using a constructor function
		/*
		    - Creates a reusable function to create several objects that have the same data structure
		    - This is useful for creating multiple instances/copies of an object
		    - An instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it
		    - Syntax
		        function ObjectName(keyA, keyB) {
		            this.keyA = keyA;
		            this.keyB = keyB;
		        }
		*/
		// This is an object
		// The "this" keyword allows to assign a new object's properties by associating them with values received from a constructor function's parameters
		function Laptop(name, manufactureDate){
		    this.name = name;
		    this.manufactureDate = manufactureDate;
		}

		// This is a unique instance of the Laptop object
		/*
		    - The "new" operator creates an instance of an object
		    - Objects and instances are often interchanged because object literals (let object = {}) and instances (let object = new object) are distinct/unique objects
		*/
		let laptop = new Laptop('Lenovo', 2008);
		console.log('Result from creating objects using object constructors:');
		console.log(laptop);

		// This is another unique instance of the Laptop object
		let myLaptop = new Laptop('MacBook Air', 2020);
		console.log('Result from creating objects using object constructors:');
		console.log(myLaptop);

		let oldLaptop = Laptop('Portal R2E CCMC', 1980);
		/*
		    - The example above invokes/calls the "Laptop" function instead of creating a new object instance
		    - Returns "undefined" without the "new" operator because the "Laptop" function does not have a return statement
		*/
		console.log('Result from creating objects without the new keyword:');
		console.log(oldLaptop);

		// Creating empty objects
		let computer = {}
		let myComputer = new Object();

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentations for Working With Objects, JavaScript Objects, Object Literal Vs. Constructor In JavaScript, JavaScript this Keyword, Instance Definition, JavaScript new Operator and Difference Between Object Literal and Instance Object.
		*/

/*
5. Add more code to demonstrate and discuss Accessing Object Properties.
	Application > script.js
*/

		/*...*/
		let myComputer = new Object();

		// [Section] Accessing Object Properties

		// Using the dot notation
		console.log('Result from dot notation: ' + myLaptop.name);

		// Using the square bracket notation
		console.log('Result from square bracket notation: ' + myLaptop['name']);

		// Accessing array objects
		/*
		    - Accessing array elements can be also be done using square brackets
		    - Accessing object properties using the square bracket notation and array indexes can cause confusion
		    - By using the dot notation, this easily helps us differentiate accessing elements from arrays and properties from objects
		    - Object properties have names that makes it easier to associate pieces of information
		*/

		let array = [laptop, myLaptop];

		// May be confused for accessing array indexes
		console.log(array[0]['name']);
		// Differentiation between accessing arrays and object properties
		// This tells us that array[0] is an object by using the dot notation
		console.log(array[0].name);

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for JavaScript Property Accessors.
		*/

/*
6. Add more code to demonstrate and discuss Initializing/Adding/Deleting/Reassigning Object Properties.
	Application > script.js
*/

		/*...*/
		console.log(array[0].name);

		// [Section] Initializing/Adding/Deleting/Reassigning Object Properties
		/*
		    - Like any other variable in JavaScript, objects may have their properties initialized/added after the object was created/declared
		    - This is useful for times when an object's properties are undetermined at the time of creating them
		*/

		let car = {};

		// Initializing/adding object properties using dot notation
		car.name = 'Honda Civic';
		console.log('Result from adding properties using dot notation:');
		console.log(car);

		// Initializing/adding object properties using bracket notation
		/*
		    - While using the square bracket will allow access to spaces when assigning property names to make it easier to read, this also makes it so that object properties can only be accesssed using the square bracket notation
		    - This also makes names of object properties to not follow commonly used naming conventions for them
		*/
		car['manufacture date'] = 2019;
		console.log(car['manufacture date']);
		console.log(car['Manufacture Date']);
		console.log(car.manufactureDate);
		console.log('Result from adding properties using square bracket notation:');
		console.log(car);

		// Deleting object properties
		delete car['manufacture date'];
		console.log('Result from deleting properties:');
		console.log(car);

		// Reassigning object properties
		car.name = 'Dodge Charger R/T';
		console.log('Result from reassigning properties:');
		console.log(car);

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for JavaScript delete Operator.
		*/

/*
7. Add more code to demonstrate and discuss Object Methods.
	Application > script.js
*/

		/*...*/
		console.log(car);

		// [Section] Object Methods
		/*
		    - A method is a function which is a property of an object
		    - They are also functions and one of the key differences they have is that methods are functions related to a specific object
		    - Methods are useful for creating object specific functions which are used to perform tasks on them
		    - Similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how it should work
		*/

		let person = {
		    name: 'John',
		    talk: function() {
		        console.log('Hello my name is ' + this.name);
		    }
		}

		console.log(person);
		console.log('Result from object methods:');
		person.talk();

		// Adding methods to objects
		person.walk = function() { 
		    console.log(this.name + ' walked 25 steps forward.');
		};
		person.walk();

		// Methods are useful for creating reusable functions that perform tasks related to objects
		let friend = {
		    firstName: 'Joe',
		    lastName: 'Smith',
		    address: {
		        city: 'Austin',
		        country: 'Texas'
		    },
		    emails: ['joe@mail.com', 'joesmith@email.xyz'],
		    introduce: function() {
		        console.log('Hello my name is ' + this.firstName + ' ' + this.lastName);
		    }
		};
		friend.introduce();

/*
8. Add more code to demonstrate and discuss Real World Application Of Objects.
	Application > script.js
*/

		// [Section] Real World Application Of Objects
		/*
		    - Scenario
		        1. We would like to create a game that would have several pokemon interact with each other
		        2. Every pokemon would have the same set of stats, properties and functions
		*/

		// Using object literals to create multiple kinds of pokemon would be time consuming
		let myPokemon = {
		    name: "Pikachu",
		    level: 3,
		    health: 100,
		    attack: 50,
		    tackle: function(){
		        console.log( "This Pokemon tackled targetPokemon");
		        console.log( "targetPokemon's health is now reduced to _targetPokemonhealth_");
		    },
		    faint : function() {
		     console.log("Pokemon fainted");
		    }
		}

		console.log(myPokemon);

		// Creating an object constructor instead will help with this process
		function Pokemon(name, level) {

		    // Properties
		    this.name = name;
		    this.level = level;
		    this.health = 2 * level;
		    this.attack = level;

		    //Methods
		    this.tackle = function(target) {
		        console.log(this.name + ' tackled ' + target.name);
		        console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
		        };
		    this.faint = function(){
		        console.log(this.name + 'fainted.');
		    }

		}

		// Creates new instances of the "Pokemon" object each with their unique properties
		let pikachu = new Pokemon("Pikachu", 16);
		let rattata = new Pokemon('Rattata', 8);

		// Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects
		pikachu.tackle(rattata);

/*
========
Activity
========
*/

/*
Instructions that can be provided to the students for reference:

============================
Activity References
============================
JS Objects
https://www.w3schools.com/js/js_objects.asp
JS Object Constructor
https://www.w3schools.com/js/js_object_constructors.asp
JS Methods
https://www.w3schools.com/js/js_object_methods.asp
JS Assignment Operators
https://www.w3schools.com/js/js_assignment.asp

============================
Activity:
=============================
Member 1:
1. In the S25 folder, create an activity folder and an index.html and index.js file inside of it.
- Create an index.html file to attach our index.js file
- Copy the template from boodle notes and paste it in an index.js file.
- Update your local sessions git repository and push to git with the commit message of Add template code s25.
- Console log the message Hello World to ensure that the script file is properly associated with the html file.
2. Create a trainer object using object literals.
- Initialize/add the following trainer object properties:
- Name (String)
- Age (Number)
- Pokemon (Array)
- Friends (Object with Array values for properties)
Member 2:
3. Initialize/add the trainer object method named talk that returns the message Pikachu! I choose you!
4. Access the trainer object properties using dot and square bracket notation.
5. Invoke/call the trainer talk object method in a console.log() to display the value returned by the method.

Member 3:
6. Create a constructor function called Pokemon for creating a pokemon with the following properties:
 - Name (Provided as an argument to the contructor)
- Level (Provided as an argument to the contructor)
- Health (Create an equation that uses the level property)
- Attack (Create an equation that uses the level property)
7. Create/instantiate several pokemon object from the constructor with varying name and level properties.

Member 4:
8. Add and Create a tackle method for the pokemon constructor that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
9. Add and Create a faint method for the pokemon constructor that will print out a message of targetPokemon has fainted.

Member 5:
10. Add a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
11. Invoke the tackle method of one pokemon object to see if it works as intended.

All Members:
12. Check out to your own git branch with git checkout -b <branchName>
13. Update your local sessions git repository and push to git with the commit message of Add activity code s25.
14. Add the sessions repo link in Boodle for s25.

*/