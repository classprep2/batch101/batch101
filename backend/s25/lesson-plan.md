# WDC028 - S23 - JavaScript - Objects

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1P25QHleQpcd6xXCFNX_ZhFR8KAviByiNsExz2McHydo/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1P25QHleQpcd6xXCFNX_ZhFR8KAviByiNsExz2McHydo/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s18) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/backend/s18/Manual.js) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                                 | Link                                                         |
| ----------------------------------------------------- | ------------------------------------------------------------ |
| Working With Objects                                  | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Working_with_Objects) |
| JavaScript Objects                                    | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object) |
| Object Literal Vs. Constructor In JavaScript          | [Link](https://medium.com/@mandeepkaur1/object-literal-vs-constructor-in-javascript-df143296b816) |
| JavaScript this Keyword                               | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/this) |
| Instance Definition                                   | [Link](https://www.codemotion.com/magazine/Glossary/instance-computer-science/) |
| JavaScript new Operator                               | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/new) |
| Difference Between Object Literal and Instance Object | [Link](https://blog.kevinchisholm.com/javascript/difference-between-object-literal-and-instance-object/) |
| JavaScript Property Accessors                         | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Property_accessors) |
| JavaScript delete Operator                            | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/delete) |
| General Method Definition                             | [Link](https://developer.mozilla.org/en-US/docs/Glossary/Method) |
| JavaScript Method Definition                          | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Method_definitions) |
| Object Oriented Programming (OOP) - Free Code Camp    | [Link](https://www.freecodecamp.org/news/four-pillars-of-object-oriented-programming/) |
| Object Oriented Programming (OOP) - Educative         | [Link](https://www.educative.io/blog/object-oriented-programming) |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1. [30 mins] - JavaScript Objects
	2. [30 mins] - Accessing Object Properties
	3. [30 mins] - Initializing/Adding/Deleting/Reassigning Object Properties
	4. [30 mins] - Object Methods
	5. [1 hr] - Real World Application Of Objects
	6. [1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

	1. Object Oriented Programming (OOP)

[Back to top](#table-of-contents)