# Session Objectives

At the end of the session, the students are expected to:

- learn about a JavaScript object and its difference with an array by capturing real-world objects into code.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5/s18)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1P25QHleQpcd6xXCFNX_ZhFR8KAviByiNsExz2McHydo)
- [Lesson Plan](lesson-plan.md)

## Supplemental Materials

- [JavaScript Objects (w3schools)](https://www.w3schools.com/js/js_objects.asp)
- [Working with Objects (MDN Web Docs)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Working_with_Objects)

# Lesson Proper

## Limitation of an Array

Previously, we have learned how to put **multiple values** in a single variable using **arrays**.

However, the elements in an array can **only be accessed** through **indexes**.

This is a **limitation** with arrays since the elements inside does not have **context** other than the name of the array.

Let's look at an example:

```jsx
let grades = [98.5, 94.3, 89.2, 90.1];
```

We previously had an array of grades with four elements.

Even though there are four elements in the array, we are unable to determine which grade belongs to the a specific grading period.

That is where **objects** will come in!

## Introduction to Objects

An **object** is **similar** to an **array** since it also contains **multiple** values.

However, unlike an **array** that uses **indexes**, an **object** uses **properties**.

Let's look at an example:

```jsx
let grade = {
    firstGrading: 89.2,
    secondGrading: 98.5,
    thirdGrading: 90.1,
    fourthGrading: 94.3
};
```

With an object, we can **easily** give a **label** to each value.

# Code Discussion

## Folder and File Preparation

Create a folder named **s18**, a folder named **discussion** inside the **s18** folder, then a file named **index.html** and **index.js** inside the **discussion** folder.

In the **index.html**, add the preliminary code:

```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>JavaScript Objects</title>
    </head>
    <body>
        <script src="./index.js"></script>
    </body>
</html>
```

## Structure of an Object

An **object** is created through **curly brackets** and **key-value pairs**.

Add the following code in the **index.js** file.

```jsx
let person = {
    firstName: 'John',
    lastName: 'Smith'
};
```

The **key** is also called a **property** of an object and its value is found after the right side of the colon character.

An object can also contain another object.

```jsx
let person = {
    firstName: 'John',
    lastName: 'Smith',
    address: {
        city: 'Tokyo',
        country: 'Japan'
    }
};
```

It can also contain arrays.

```jsx
let person = {
    firstName: 'John',
    lastName: 'Smith',
    address: {
        city: 'Tokyo',
        country: 'Japan'
    },
    emails: ['john@mail.com', 'johnsmith@email.xyz']
};
```

It can even contain functions!

```jsx
let person = {
    firstName: 'John',
    lastName: 'Smith',
    address: {
        city: 'Tokyo',
        country: 'Japan'
    },
    emails: ['john@mail.com', 'johnsmith@email.xyz'],
    fullName: function() {
        return this.firstName + ' ' + this.lastName;
    }
};
```

You might notice that there is a **this** before the firstName and lastName of the `fullName()` function.

The **this** keyword refers to the properties from within the person object.

## Accessing Object Properties

An object's properties can be accessed using the **dot notation**.

```jsx
console.log(person.firstName);
```

Alternatively, a property can also be accessed using the **square brackets**.

```jsx
console.log(person['firstName']);
```

However, to prevent confusion, we shall stick to using **dot notation** for **object properties** and **square brackets** for **array indexes**.

## Objects From Blueprints

Now that we are able to create an object variable. What can we do to allow object creation from a blueprint?

When we want to create multiple objects based on a blueprint, we can create what is called a **class**.

Classes are **templates** used to create objects. It allows us to make objects with a **predefined** set of properties and functions.

Classes can be created by using a function in JavaScript.

Let's take a look at an example:

```jsx
function Person() {
    this.firstName;
    this.lastName;
}

let personA = new Person();
```

The **this** keyword is again used to indicate that the variables are to be an object's properties once a new object has been created.

To create a new object from the template, we use the **new** keyword then **call** the function.

The output on the console should look like this.

![readme-images/Untitled.png](readme-images/Untitled.png)

At the moment, the object is empty even though we declared its properties. That is because we did not give the values upon its creation.

Let's update the class by **including parameters** in the function and **providing arguments** when the function is called.

```jsx
function Person(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
}

let personA = new Person('John', 'Smith');
```

We should now have the following output in the console.

![readme-images/Untitled%201.png](readme-images/Untitled%201.png)

# Activity

## Instructions

Create an activity folder and create the index.html and index.js files inside the created folder.

Given the following object array:

```jsx
const studentGrades = [
    { studentId: 1, Q1: 89.3, Q2: 91.2, Q3: 93.3, Q4: 89.8 },
    { studentId: 2, Q1: 69.2, Q2: 71.3, Q3: 76.5, Q4: 81.9 },
    { studentId: 3, Q1: 95.7, Q2: 91.4, Q3: 90.7, Q4: 85.6 },
    { studentId: 4, Q1: 86.9, Q2: 74.5, Q3: 83.3, Q4: 86.1 },
    { studentId: 5, Q1: 70.9, Q2: 73.8, Q3: 80.2, Q4: 81.8 }
];
```

- Compute for the average grade of each student object.
- Round off the average into a single decimal number.
- Put the average of the student as a property.
- Log the modified object array to the console.

## Expected Output

![readme-images/Untitled%202.png](readme-images/Untitled%202.png)

## Solution

**activity/index.html**

```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>Activity: JavaScript Objects</title>
    </head>
    <body>
        <script src="./index.js"></script>
    </body>
</html>
```

**activity/index.js**

```jsx
for (let i = 0; i < studentGrades.length; i++) {
    let student = studentGrades[i];
    let average = (student.Q1 + student.Q2 + student.Q3 + student.Q4) / 4;

    studentGrades[i].average = parseFloat(average).toFixed(1);
}

console.log(studentGrades);
```