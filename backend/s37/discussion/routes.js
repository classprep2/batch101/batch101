const http = require('http')

// Creates a variable 'port' to store the port number
const port = 4000

// Creates a variable 'app' that stores the output of the 'createServer' method
const app = http.createServer((request, response) => {

	// Accessing the 'greeting' route returns a message of 'Hello World'
    if (request.url == '/greeting') {

        response.writeHead(200, {'Content-Type': 'text/plain'})
        response.end('Hello World')

    // Accessing the 'homepage' route returns a message of 'This is the homepage'
    } else if (request.url == '/homepage') {

        response.writeHead(200, {'Content-Type': 'text/plain'})
        response.end('This is the homepage')

    // All other routes will return a message of 'Page not available'
    } else {

    	// Set a status code for the response - a 404 means Not Found
        response.writeHead(404, {'Content-Type': 'text/plain'})
        response.end('Page not available')

    }

})

// Uses the "app" and "port" variables created above.
app.listen(port)

// When server is running, console will print the message:
console.log(`Server now accessible at localhost:${port}.`)