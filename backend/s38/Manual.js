/*
=====================================
S38 - Node.js Routing w/ HTTP Methods
=====================================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1vMbhXjIE2pSKSR39idsQiyNCuf80LN48YL74BCGz-uk/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s27

Other References:
	What Does Module Mean
		https://www.techopedia.com/definition/3843/module
	An overview of HTTP
		https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview
	Node JS Documentation createServer
		https://nodejs.org/api/http.html#http_http_createserver_options_requestlistener
	Request Object
		https://developer.mozilla.org/en-US/docs/Web/API/Request
	Response Object
		https://developer.mozilla.org/en-US/docs/Web/API/Response
	What is a Computer Port
		https://www.cloudflare.com/learning/network-layer/what-is-a-computer-port/
	HTTP Response Status Codes
		https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
	Rest API Table
		https://miro.medium.com/max/1838/1*77_S1ANTdgFDmRBD5_Jf7w.jpeg
	Request Response Cycle
		https://iq.opengenus.org/content/images/2019/09/Untitled-1-.png
	Postman Download Link
		https://www.postman.com/downloads/
	JSON.stringify method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify
	What is a Stream
		https://study.com/academy/lesson/streams-in-computer-programming-definition-examples.html
	Node JS Stream Documentation
		https://nodejs.org/api/stream.html#stream_event_data
	JSON.parse method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/parse
	Creating Git Projects:
		GitLab
			https://gitlab.com/projects/new#blank_project
		GitHub
			https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
==========
Discussion
==========
*/

// 1. Create a "discussion" folder and create an "index.js" file inside of it.
	// Batch Folder > S27 > discussion > index.js

		// Use the "require" directive to load Node.js modules
		// A "module" is a software component or part of a program that contains one or more routines
		// The "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol
		// The "http module" is a set of individual files that contain code to create a "component" that helps establish data transfer between applications
		// HTTP is a protocol that allows the fetching of resources such as HTML documents
		// Clients (browser) and servers (node JS/express JS applications) communicate by exchanging individual messages.
		// The messages sent by the client, usually a Web browser, are called requests
		// The messages sent by the server as an answer are called responses.
		let http = require("http");

		/*
		Important Note:
			- A number of the comments added to this manual and the final output are the same as the previous discussion's comments.
			- Duplicate comments may be removed for clarity of final output.
			- Comments were added again in this section for instructor's reference.
			- Refer to "references" section of this file to find the documentation for what a module is and an overview of the HTTP.
		*/

// 2. Create a server.
	// Application > index.js

		let http = require("http");

		// Using this module's createServer() method, we can create an HTTP server that listens to requests on a specified port and gives responses back to the client
		// The http module has a createServer() method that accepts a function as an argument and allows for a creation of a server
		// The arguments passed in the function are request and response objects (data type) that contains methods that allow us to receive requests from the client and send responses back to it
		//Save the server in variable called app for better readability.
		let app = http.createServer(function (request, response) {});

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for the Node JS documentation on the createServer method, request object and response object.
		*/

// 3. Define the port number that the server will be listening in to.
	// Application > index.js

		/*...*/

		let app = http.createServer(function (request, response) {});

		//Use the app's listen() method
		
		// A port is a virtual point where network connections start and end.
		// Each port is associated with a specific process or service
		// The server will be assigned to port 4000 via the ".listen(4000)" method where the server will listen to any requests that are sent to it eventually communicating with our server

		app.listen(port)

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for what is a computer port.
		*/

// 4. Add a console log to listen method to indicate that the server is running.
	// Application > index.js

		/*...*/

		let app = http.createServer(function (request, response) {});

		//The listen method can take 2 arguments.
		//First, the port number to assign our server to.
		//Second, a callback method/function to run when the server is running.
		// When server is running, console will print the message:
		app.listen(port,() => console.log('Server running at localhost:4000'));

		

// 5. Run the server.
	// Application > Terminal

		nodemon index.js

		/*
		Important Note:
			- Inputting the command tells our device to run node JS.
			- index.js refers to the file that will be run.
		*/

// 6. Create a condition and a response when the route "/items" is accessed with a method of "GET".
	// Application > index.js

		/*...*/

		const app = http.createServer((request, response) => {

			// The HTTP method of the incoming request can be accessed via the "method" property of the "request" parameter
			// The method "GET" means that we will be retrieving or reading information
			if(request.url == "/items" && request.method == "GET"){

				// Requests the "/items" path and "GETS" information 
				response.writeHead(200, {'Content-Type': 'text/plain'});
				// Ends the response process
				response.end('Data retrieved from the database');

			}

		})

		app.listen(port,() => console.log('Server running at localhost:4000'));

		/*
		Important Note:
			- The method "GET" comes from the concept of Restful API where certain methods are associated to a certain action.
			- The "GET" method is associated with the action of retrieving information.
			- The "response.end" method is responsible for telling our client that request response cycle has been completed. Without this, our application will encounter an error that will leave our request pending and it will never be resolved.
			- Refer to "references" section of this file to find a table for Restful API and it's associated actions, a document for HTTP Response Status Codes and a diagram of the request response cycle.
		*/

// 7. Open Postman and process a GET request.
	// Postman

		url: localhost:4000/items
		method: GET

		/*
		Important Note:
			- Since the node JS application that we are building is a backend application and there is no frontend application to process these requests, we will be using postman to process the request.
			- Refer to "references" section of this file to find the download link for Postman if the students have not yet installed them.
			- Refer to the S27.postman_collection.json for the solution set to input in postman to process the requests. You can import the file postman.
		*/

// 8. Create a condition and a response when the route "/items" is accessed with a method of "POST".
	// Application > index.js

		/*...*/

		const server = http.createServer((request, response) => {

			if(request.url == "/items" && request.method == "GET"){

				/*...*/

			}

			// The method "POST" means that we will be adding or creating information
			// In this example, we will just be sending a text response for now
			if(request.url = "/items" && request.method == "POST"){

				// Requests the "/items" path and "SENDS" information
				response.writeHead(200, {'Content-Type': 'text/plain'});
				response.end('Data to be sent to the database');

			}

		})

		app.listen(port,() => console.log('Server running at localhost:4000'));

		/*
		Important Note:
			- The POST method is associated with the action of creating a new record.
			- Refer to "references" section of this file to find a table for Restful API and it's associated actions and a document for HTTP Response Status Codes.
		*/

// 9. Process a POST request using postman.
	// Postman

		url: localhost:4000/items
		method: POST

// 10. Create a "crud.js" file.
	// Batch Folder > S27 > discussion > crud.js

		let http = require("http");

		let app = http.createServer(function (request, response) {});

		app.listen(port,() => console.log('Server running at localhost:4000'));

// 11. Stop the previously running server and rerun the crud.js file using nodemon.
	// Application > Terminal

		// Stop the server
		ctrl + c

		// Run the routes.js file
		nodemon crud.js

		/*
		Important Note:
			- It's good practice to always stop any running process in our terminals before closing them.
			- Forcefully closing the terminal when a process is still running might lead to unexpected behavior.
			- A common error that students might encounter is the error that says "The specified port is already in use.".
			- Simply restarting the device is the best solution to this.
			- An alternative solution is to use the command "npx kill-port [port-number]" (e.g npx kill-port 4000).
			- Make sure that the correct port number is being killed as this might cause problems if an incorrect port number or a port number that was running a critical operating system process is suddenly killed.
		*/

// 12. Create a "directory" variable and store an array of user objects inside of it that will serve as our mock database
	// Application > crud.js

		let http = require("http");

		// Mock database
		let directory = [
			{
				"name" : "Brandon",
				"email" : "brandon@mail.com" 
			},
			{
				"name" : "Jobert",
				"email" : "jobert@mail.com"
			}
		]

		let app = http.createServer(function (request, response) {

			/*...*/

		});

		app.listen(port,() => console.log('Server running at localhost:4000'));

		/*...*/

// 13. Create a condition and a response when the route "/users" is accessed with a method of "GET", the list of users will be retrieved.
	// Application > crud.js

		/*...*/

		const app = http.createServer((request, response) => {

		    // Route for returning all items upon receiving a GET request
			if(request.url == "/users" && request.method == "GET"){

				// Requests the "/users" path and "GETS" information 
		        // Sets the status code to 200, denoting OK
		        // Sets response output to JSON data type
				response.writeHead(200,{'Content-Type': 'application/json'});

		        // Input HAS to be data type STRING hence the JSON.stringify() method
		        // This string input will be converted to desired output data type which has been set to JSON
		        // This is done because requests and responses sent between client and a node JS server requires the information to be sent and received as a stringified JSON
			    response.write(JSON.stringify(directory));
			    // Ends the response process
			    response.end();

			}

		})

		app.listen(port,() => console.log('Server running at localhost:4000'));

		/*...*/

		/*
		Important Note:
			- When information is sent to the client, it is sent in the format of a stringified JSON.
			- After the client received the stringified JSON, then it is converted back into a JSON object to be consumed.
			- Refer to "references" section of this file to find a documentation for the JSON.stringify method.
		*/

// 14. Process a GET request using postman to retrieve the list of users.
	// Postman

		url: localhost:4000/users
		method: GET

// 15. Create a condition and a response when the route "/users" is accessed with a method of "POST" that will add a user to the mock database.
	// Application > crud.js

		// 15a. Create a condition that will check if the url is at "/users" and the method is "POST".

			/*...*/

			const app = http.createServer((request, response) => {

				if(request.url == "/users" && request.method == "GET"){
					/*...*/
				}

				if(request.url == "/users" && request.method == "POST"){
				}

			})

			app.listen(port,() => console.log('Server running at localhost:4000'));

		// 15b. Create a variable "requestBody" that will eventually store the information received from the request.

			/*...*/

			const app = http.createServer((request, response) => {

				/*...*/

				if(request.url == "/users" && request.method == "POST"){

					// Declare and intialize a "requestBody" variable to an empty string
					// This will act as a placeholder for the resource/data to be created later on
					let requestBody = '';

				}

			})

			app.listen(port,() => console.log('Server running at localhost:4000'));

			/*...*/

		// 15c. Retrieve the information from the request and store it in the variable "requestBody".

			/*...*/

			const app = http.createServer((request, response) => {

				/*...*/

				if(request.url == "/users" && request.method == "POST"){

					let requestBody = '';

					// A stream is a sequence of data
					// Data is received from the client and is processed in the "data" stream
					// The information provided from the request object enters a sequence called "data" the code below will be triggered
				 	// data step - this reads the "data" stream and processes it as the request body
				 	request.on('data', function(data){

				 		// Assigns the data retrieved from the data stream to requestBody
				 		requestBody += data;

				 	});

				}

			})

			app.listen(port,() => console.log('Server running at localhost:4000'));

			/*...*/

			/*
			Important Note:
				- Refer to "references" section of this file to find a documentation for What is a Stream and the Node JS Stream.
			*/

		// 15d. Add a block of code that will trigger when the request enters the end stream.

			/*...*/

			const app = http.createServer((request, response) => {

				/*...*/

				if(request.url == "/users" && request.method == "POST"){

					/*...*/

				 	request.on('data', function(data){
				 		/*...*/
				 	});

				 	// response end step - only runs after the request has completely been sent
				 	request.on('end', function(){});

				}

			})

			app.listen(port,() => console.log('Server running at localhost:4000'));

			/*...*/

		// 15e. Convert the retrieved information from the "data" stream into a JSON object

			/*...*/

			const app = http.createServer((request, response) => {

				/*...*/

				if(request.url == "/users" && request.method == "POST"){

					/*...*/

				 	request.on('end', function(){

				 		// Check if at this point the requestBody is of data type STRING
				 		// We need this to be of data type JSON to access its properties
				 		console.log(typeof requestBody);

				 		// Converts the string requestBody to JSON
				 		requestBody = JSON.parse(requestBody);

				 	});

				}

			})

			app.listen(port,() => console.log('Server running at localhost:4000'));

			/*...*/

			/*
			Important Note:
				- Refer to "references" section of this file to find a documentation for JSON.parse method.
			*/

		// 15f. Create a variable "newUser" that will store the information of the new user to be added to the mock database.

			/*...*/

			const app = http.createServer((request, response) => {

				/*...*/

				if(request.url == "/users" && request.method == "POST"){

					/*...*/

				 	request.on('end', function(){

				 		/*...*/

				 		requestBody = JSON.parse(requestBody);

	 		            // Create a new object representing the new mock database record
	 				    let newUser = {
	 				    	"name" : requestBody.name,
	 				    	"email" : requestBody.email
	 				    }

				 	});

				}

			})

			app.listen(port,() => console.log('Server running at localhost:4000'));

			/*...*/

		// 15g. Add the new user to our mock database.

			/*...*/

			const app = http.createServer((request, response) => {

				/*...*/

				if(request.url == "/users" && request.method == "POST"){

					/*...*/

				 	request.on('end', function(){

				 		/*...*/

	 				    let newUser = {
	 				    	/*...*/
	 				    }

	 				    // Add the new user into the mock database
	 				    directory.push(newUser)
	 				    console.log(directory);

				 	});

				}

			})

			app.listen(port,() => console.log('Server running at localhost:4000'));

			/*...*/

		// 15h. Complete the "response" cycle by providing a status of "200" and converting the user information into stringified JSON for consumption of the frontend application.

			/*...*/

			const app = http.createServer((request, response) => {

				/*...*/

				if(request.url == "/users" && request.method == "POST"){

					/*...*/

				 	request.on('end', function(){

				 		/*...*/
	 				    console.log(directory);

	 				    response.writeHead(200,{'Content-Type': 'application/json'});
	 				    response.write(JSON.stringify(newUser));
	 				    response.end();

				 	});

				}

			})

			app.listen(port,() => console.log('Server running at localhost:4000'));

			/*...*/

// 16. Process a POST request using postman to create a new user.
	// Postman

		url: http://localhost:4000/users
		method: POST
		body: raw + JSON
			{
			    "name": "John",
			    "email": "jdoe@mail.com"
			}

/*
========
Activity
========
*/


// Instructor Note: Copy the activity-template.js to your batch Boodle Notes.

/*
## Instructions that can be provided to the students for reference:

1. In the s38 folder, create an activity folder and an  index.js file inside of it.
2. Copy the template code in boodle and paste it in your index.js file.
3. Add a simple server and the following routes with their corresponding HTTP methods and responses:
	a. If the url is http://localhost:4000/, send a response Welcome to Booking System
	b. If the url is http://localhost:4000/profile, send a response Welcome to your profile!
	c. If the url is http://localhost:4000/courses, send a response Here’s our courses available
	d. If the url is http://localhost:4000/addcourse, send a response Add a course to our resources
4. 	Create a simple server and the following routes with their corresponding HTTP methods and responses:
	a. If the url is http://localhost:4000/updatecourse, send a response Update a course to our resources
	b. If the url is http://localhost:4000/archivecourses, send a response Archive courses to our resources
5. Test all the endpoints in Postman.
6. Update your local backend git repository and push to git with the commit message of Add activity code s38.
7. Add the link in Boodle for s38.

*/




// 1. Create an "activity" folder and create an "index.js" file inside of it.
	// backend > S32 > activity > index.js

// 2. Create a simple server.
	// Application > index.js

		let http = require("http");

		let app = http.createServer(function (request, response) {})

		app.listen(port,() => console.log('Server running at localhost:4000'));

// 3. Copy the template.js to the Boodle Notes and Follow the instructions in the Google Slides.
	// Application > index.js

	let http = require("http");

	const app = http.createServer(function (request, response) {
	
		if(request.url == "/" && request.method == "GET"){
	
			response.writeHead(200, {'Content-Type': 'text/plain'});
			response.end('Welcome to booking system');
	
		}
	
		if(request.url == "/profile" && request.method == "GET"){
	
			response.writeHead(200, {'Content-Type': 'text/plain'});
			response.end('Welcome to your profile');
	
		}
	
		// Retrieving a course
		if(request.url == "/courses" && request.method == "GET"){
	
			response.writeHead(200, {'Content-Type': 'text/plain'});
			response.end(`Here's our courses available`);
	
		}
	
		// Adding a new course
		if(request.url = "/addCourse" && request.method == "POST"){
	
			response.writeHead(200, {'Content-Type': 'text/plain'});
			response.end('Add course to our resources');
	
		}
	
		// Updating a course
		if(request.url = "/updateCourse" && request.method == "PUT"){
	
			response.writeHead(200, {'Content-Type': 'text/plain'});
			response.end('Update a course to our resources');
	
		}
	
		// Deleting a course
		if(request.url = "/archiveCourses" && request.method == "DELETE"){
	
			response.writeHead(200, {'Content-Type': 'text/plain'});
			response.end('Archive courses to our resources');
	
		}
	
	})
	
	//Do not modify
	//Make sure to save the server in variable called app
	
	if(require.main === module){
		app.listen(4000, () => console.log(`Server running at port 4000`));
	}
	
	module.exports = app;