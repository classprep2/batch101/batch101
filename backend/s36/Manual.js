/*
==================================================
S36 - MongoDB - Aggregation and Query Case Studies
==================================================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/11TTrf0pxffQWwNX2MkYh7BovKz-pdQSWeUU8MEODYRs/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s25

Other References:
	MongoDB aggregation
		https://docs.mongodb.com/manual/aggregation/
	MongoDB $match
		https://docs.mongodb.com/manual/reference/operator/aggregation/match/
	MongoDB $group
		https://docs.mongodb.com/manual/reference/operator/aggregation/group/
	MongoDB $sum
		https://docs.mongodb.com/manual/reference/operator/aggregation/sum/
	MongoDB Data Models
		https://docs.mongodb.com/manual/core/data-model-design/
	MongoDB $project
		https://docs.mongodb.com/manual/reference/operator/aggregation/project/
	MongoDB $sort
		https://docs.mongodb.com/manual/reference/operator/aggregation/sort/
	MongoDB $unwind
		https://docs.mongodb.com/manual/reference/operator/aggregation/unwind/
	MongoDB $count
		https://docs.mongodb.com/manual/reference/operator/aggregation/count/
	MongoDB $avg
		https://docs.mongodb.com/manual/reference/operator/aggregation/avg/
	MongoDB $min
		https://docs.mongodb.com/manual/reference/operator/aggregation/min/
	MongoDB $max
		https://docs.mongodb.com/manual/reference/operator/aggregation/max/
	Creating Git Projects:
		GitLab
			https://gitlab.com/projects/new#blank_project
		GitHub
			https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application
*/

/*
==========
Discussion
==========
*/

/*
1. Create the MongoDB syntax in a text editor for code readability and demonstrate them using the Mongo Shell terminal
	Device > Terminal
*/

		// Create documents to use for the discussion
		db.fruits.insertMany([
			{
				name : "Apple",
				color : "Red",
				stock : 20,
				price: 40,
				supplier_id : 1,
				onSale : true,
				origin: [ "Philippines", "US" ]
			},

			{
				name : "Banana",
				color : "Yellow",
				stock : 15,
				price: 20,
				supplier_id : 2,
				onSale : true,
				origin: [ "Philippines", "Ecuador" ]
			},

			{
				name : "Kiwi",
				color : "Green",
				stock : 25,
				price: 50,
				supplier_id : 1,
				onSale : true,
				origin: [ "US", "China" ]
			},

			{
				name : "Mango",
				color : "Yellow",
				stock : 10,
				price: 120,
				supplier_id : 2,
				onSale : false,
				origin: [ "Philippines", "India" ]
			}
		]);

		// [Section] MongoDB Aggregation
		/*
			- Used to generate manipulated data and perform operations to create filtered results that helps in analyzing data
			- Compared to doing CRUD operations on our data from previous sessions, aggregation gives us access to manipulate, filter and compute for results providing us with information to make necessary development decisions without having to create a frontend application.
		*/

		// Using the aggregate method
		/*
			- The "$match" is used to pass the documents that meet the specified condition(s) to the next pipeline stage/aggretation process.
			- Syntax
				- { $match: { field: value } }
			- The "$group" is used to group elements together and field-value pairs using the data from the grouped elements
			- Syntax
				- { $group: { _id: "value", fieldResult: "valueResult" } }
			- Using both "$match" and "$group" along with aggregation will find for products that are on sale and will group the total amount of stocks for all suppliers found.
			- Syntax
				- db.collectionName.aggregate([
					{ $match: { fieldA, valueA } },
					{ $group: { _id: "$fieldB" }, { result: { operation } } }
				])
			- The "$" symbol will refer to a field name that is available in the documents that are being aggregated on.
			- The "$sum" operator will total the values of all "stock" fields in the returned documents that are found using the "$match" criteria.
		*/
		db.fruits.aggregate([
		   { $match: { onSale: true } },
		   { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } }
		]);

		// Field projection with aggregation
		/*
			- The "$project" can be used when aggregating data to include/exclude fields from the returned results
			- Syntax
				- { $project : { field: 1/0 } }
		*/
		db.fruits.aggregate([
		   { $match: { onSale: true } },
		   { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
		   { $project: { _id: 0 } }
		]);

		// Sorting aggregated results
		/*
			- The "$sort" can be used to change the order of aggregated results
			- Providing a value of -1 will sort the aggregated results in a reverse order
			- Syntax
				- { $sort { field: 1/-1 } }
		*/
		db.fruits.aggregate([
		   { $match: { onSale: true } },
		   { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
		   { $sort: { total: -1 } }
		]);

		// Aggregating results based on array fields
		/*
			- The "$unwind" deconstructs an array field from a collection/field with an array value to output a result for each element.
			- The syntax below will return results creating separate documents for each of the countries provided per the "origin" field
			- Syntax
				- { $unwind: field }
		*/
		db.fruits.aggregate([
			{ $unwind: "$origin" }
		]);

		// Displays fruit documents by their origin and the kinds of fruits that are supplied
		db.fruits.aggregate([
			{ $unwind : "$origin" },
			{ $group : { _id : "$origin" , kinds : { $sum : 1 } } }
		]);



//************** To be removed in discussion, to add in Take Home Quiz
		// [Section] Guidelines on Schema Design
		/*
			- Schema design/data modelling is an important feature when creating databases.
			- MongoDB documents can be categorized into normalized and de-normalized/embedded data.
			- Normalized data refers to a data structure where documents are referred to each other using their ids for related pieces of information.
			- De-normalized data/embedded data refers to a data structure where related pieces of information is added to a document as an embedded object.
			- Both data structures are common practice but each of them have their pros and their cons.
			- Normalized data makes it easier to read information because separate documents can be retrieved but in terms of querying results, it performs slower compared to embedded data due to having to retrieve multiple documents at the same time.
			- This approach is recommended for data structures where pieces of information are commonly operated on/changed.
			- De-normalized data/embedded data makes it easier to query documents and has a faster performance because only one query needs to be done in order to retrieve documents. However, if the data structure becomes too complex it makes it more difficult to manipulate and access information.
			- This approach is recommended for data structures where pieces of information are commonly retrieved and rarely operated on/changed.
		*/

		// One-To-One Relationship
		// Creates an id and stores it in the variable owner for use in document creation
		var owner = ObjectId();

		// Creates an "owner" document that uses the generated id
		db.owners.insert({
			_id: owner,
			name: "John Smith",
			contact: "0987654321"
		});

		// Change the "<owner_id>" using the actual id in the previously created document
		db.suppliers.insert({
			name: "ABC fruits",
			contact: "1234567890",
			owner_id: <owner_id>
		});

		// One-To-Few Relationship
		db.suppliers.insert({
			name: "DEF Fruits",
			contact: "1234567890",
			addresses : [
				{ street: "123 San Jose St", city: "Manila"},
				{ street: "367 Gil Puyat", city: "Makati"}
			]
		});

		// One-To-Many Relationship
		var supplier = ObjectId();
		var branch1 = ObjectId();
		var branch2 = ObjectId();

		db.suppliers.insert({
			_id: supplier,
			name: "GHI Fruits",
			contact: "1234567890",
			branches: [
				branch1
			]
		});

		// Change the "<branch_id>" and "<supplier_id>" using the actual ids in the previously created document
		db.branches.insert({
			_id: <branch_id>,
		    name: "BF Homes",
		    address: "123 Arcardio Santos St",
		    city: "Paranaque",
		    supplier_id: <supplier_id>
		});

		db.branches.insert(
			{
				_id: <branch_id>,
			    name: "Rizal",
			    address: "123 San Jose St",
			    city: "Manila",
			    supplier_id: <supplier_id>
			}
		);

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentations for MongoDB aggregation, MongoDB $match, MongoDB $group, MongoDB $sum, MongoDB Data Models, MongoDB $project, MongoDB $sort and MongoDB $unwind.
		*/

/*
========
Activity
========
*/

/*
1. In the activity folder, create an index.js file and copy the contents from template.js. Read and understand the additional instructions from the template.

2. Create a new database called inventory(MongoDB Compass). Insert the mock data to be provided by your instructor.

3. Use the count operator to count the total number of fruits on sale. Copy and paste your query into the fruitsOnSale().

*/

		db.fruits.aggregate([
			   { $match: { onSale: true } },
			   { $count: "fruitsOnSale" }
			]);


		
/*

4. Use the count operator to count the total number of fruits with stock more than or equal to  20. Copy and paste your query into the fruitsInStock()

*/
		db.fruits.aggregate([
				{ $match: { stock: { $gte: 20 } } },
				{ $count: "enoughStock"}
			]);



/*

5. Use the count operator to count the total number of fruits with stock more than or equal to  20. Copy and paste your query into the fruitsInStock()

*/

		db.fruits.aggregate([
				{ $match: { onSale: true } },
				{ $group: { _id: "$supplier_id", avg_price: { $avg: "$price" } } }
			]);




/*
6. Use the max operator to get the highest price of a fruit per supplier. Copy and paste your query into the fruitsHighPrice().

*/

		db.fruits.aggregate([
				{ $match: { onSale: true } },
				{ $group: { _id: "$supplier_id", max_price: { $max: "$price" } } }
			]);




/*
7. Use the min operator to get the lowest price of a fruit per supplier. Copy and paste your query into the fruitsLowPrice().

*/

		db.fruits.aggregate([
				{ $match: { onSale: true } },
				{ $group: { _id: "$supplier_id", min_price: { $min: "$price" } } }
			]);


/*

8. Update your local backend git repository and push to git with the commit message of Add activity code s36.

9. Add the link in Boodle for s36.

*/