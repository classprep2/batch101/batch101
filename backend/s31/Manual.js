/*
=======================================
S31 - JavaScript - Introduction to JSON
=======================================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1wFgPDDrIuudyxqJ0oMYOrW1Zt6NKcpTSuUYQ06L5wQY/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s20

Other References:
	JSON object
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON
	What Is Serialization
		https://hazelcast.com/glossary/serialization/
	What Is A Byte
		https://searchstorage.techtarget.com/definition/byte
	What Are HTTP Request Methods
		https://rapidapi.com/blog/api-glossary/http-request-methods/#:~:text=An%20HTTP%20request%20is%20an,is%20assigned%20a%20specific%20purpose.
	What Is A Database
		https://www.guru99.com/introduction-to-database-sql.html
	Node JS Documentation
		https://nodejs.org/en/about/
	What is Node JS
		https://effectussoftware.com/blog/node-js-a-framework/
	Express JS Documentation
		https://expressjs.com/
	What is Parse
		https://www.techopedia.com/definition/3853/parse#:~:text=To%20parse%20is%20to%20break,each%20part's%20function%20or%20form.&text=Parsing%20is%20used%20in%20all,transformed%20into%20executable%20machine%20code.

	Creating Git Projects:
		GitLab
			https://gitlab.com/projects/new#blank_project
		GitHub
			https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
==========
Discussion
==========
*/

/*
1. Create an "index.html" file.
	Batch Folder > S19  > Discussion > index.html
*/

		/*
		<!DOCTYPE HTML>
		<html>
		    <head>
		        <title>Introduction to JSON</title>
		    </head>
		    <body>
		    </body>
		</html>
		*/

/*
2. Create an "index.js" file and to test if the script is properly linked to the HTML file.
	Application > index.js
*/

		console.log("Hello World!");

/*
3. Link the "index.js" file to our HTML file.
	Application > index.html
*/

		/*
		<!DOCTYPE HTML>
		<html>
		    <!-- ... -->
		    <body>
		    	<script src="./index.js"></script>
		    </body>
		</html>
		*/

		/*
		Important Note:
			- The "script" tag is commonly placed at the bottom of the HTML file right before the closing "body" tag.
			- The reason for this is because Javascript's main function in frontend development is to make our websites and applications interactive.
			- In order to achieve this, JavaScript selects/targets specific HTML elements in our application and performs a certain output.
			- It is added last to allow all HTML and CSS resources to load first before applying any JavaScript code to run.
			- Placing the "script" tag at the top the the file might result in errors because since the HTML elements have not yet been loaded when the JavaScript loads, it does not have any valid HTML elements to target/select.
		*/

/*
4. Add code to the "index.js" file to demonstrate and discuss JSON objects.
	Application > index.js
*/

		// console.log("Hello World!");

		// console.log('Hello World');

		// [SECTION] JSON Objects
		/*
			- JSON stands for JavaScript Object Notation
			- JSON is also used in other programming languages hence the name JavaScript Object Notation
			- Core JavaScript has a built in JSON object that contains methods for parsing JSON objects and converting strings into JavaScript objects
			- JavaScript objects are not to be confused with JSON
			- JSON is used for serializing different data types into bytes
			- Serialization is the process of converting data into a series of bytes for easier transmission/transfer of information
			- A byte is a unit of data that is eight binary digits (1 and 0) that is used to represent a character (letters, numbers or typographic symbols)
			- Bytes are information that a computer processes to perform different tasks
			- Uses double quotes for property names
			- Syntax
				{
					"propertyA": "valueA",
					"propertyB": "valueB",
				}
		*/

		// JSON Objects
		{
		    "city": "Quezon City",
		    "province": "Metro Manila",
		    "country": "Philippines"
		}

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentations for JSON object, What Is Serialization and What Is A Byte.
		*/

/*
5. Add more code to demonstrate and discuss JSON Arrays.
	Application > index.js
*/

		/*
		{
		    "city": "Quezon City",
		    "province": "Metro Manila",
		    "country": "Philippines"
		}
		*/

		// [SECTION] JSON Arrays
		/*
		"cities": [
		    { "city": "Quezon City", "province": "Metro Manila", "country": "Philippines" },
		    { "city": "Manila City", "province": "Metro Manila", "country": "Philippines" },
		    { "city": "Makati City", "province": "Metro Manila", "country": "Philippines" }
		]
		*/

/*
6. Add more code to demonstrate and discuss Converting Data Into Stringified JSON.
	Application > index.js
*/

		/*
		"cities": [
		    { "city": "Quezon City", "province": "Metro Manila", "country": "Philippines" },
		    { "city": "Manila City", "province": "Metro Manila", "country": "Philippines" },
		    { "city": "Makati City", "province": "Metro Manila", "country": "Philippines" }
		]
		*/

		// [SECTION] JSON Methods
		// - The JSON object contains methods for parsing and converting data into stringified JSON

		// [Section] Converting Data Into Stringified JSON
		/*
			- Stringified JSON is a JavaScript object converted into a string to be used in other functions of a JavaScript application
			- They are commonly used in HTTP requests where information is required to be sent and received in a stringified JSON format
			- Requests are an important part of programming where an application communicates with a backend application to perform different tasks such as getting/creating data in a database
			- A frontend application is an application that is used to interact with users to perform different visual tasks and display information while backend applications are commonly used for all the business logic and data processing
			- A database normally stores information/data that can be used in most applications
			- Commonly stored data in databases are user information, transaction records and product information
			- Node/Express JS are two of technologies that are used for creating backend applications which processes requests from frontend applications
			- Node JS is a Java Runtime Environment (JRE)/software that is made to execute other software
			- Express JS is a NodeJS framework that provides features for easily creating web and mobile applications
			- An example of where JSON is also used is on package.json files which an express JS application uses to keep track of information regarding a project
		*/
		let batchesArr = [{ batchName: 'Batch X' }, { batchName: 'Batch Y' }];

		// The "stringify" method is used to convert JavaScript objects into a string
		console.log('Result from stringify method:');
		console.log(JSON.stringify(batchesArr));

		let data = JSON.stringify({
			name: 'John',
			age: 31,
			address: {
				city: 'Manila',
				country: 'Philippines'
			}
		});

		console.log(data);

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentations for JSON object, What Are HTTP Request Methods, What Is A Database, Node JS Documentation, What is Node JS and Express JS Documentation.
		*/

/*
7. Show the students a sample of a package.json file.
	Application > index.js
*/

		/*
		Important Note:
			- Refer to the "package.json" file found in the codebase.
		*/

/*
8. Add more code to demonstrate and discuss Using Stringify Method With Variables.
	Application > index.js
*/

		/*...*/
		console.log(data);

		// [Section] Using Stringify Method With Variables
		/*
			- When information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable
			- The "property" name and "value" would have the same name which can be confusing for beginners
			- This is done on purpose for code readability meaning when an information is stored in a variable and when the object created to be stringified is created, we supply the variable name instead of a hard coded value
			- This is commonly used when the information to be stored and sent to a backend application will come from a frontend application
			- Syntax
				JSON.stringify({
					propertyA: variableA,
					propertyB: variableB
				});
			- Since we do not have a frontend application yet, we will use the prompt method in order to gather user data to be supplied to the user details
		*/
		// User details
		let firstName = prompt('What is your first name?');
		let lastName = prompt('What is your last name?');
		let age = prompt('What is your age?');
		let address = {
			city: prompt('Which city do you live in?'),
			country: prompt('Which country does your city address belong to?')
		};

		let otherData = JSON.stringify({
			firstName: firstName,
			lastName: lastName,
			age: age,
			address: address
		})

		console.log(otherData);

/*
9. Add more code to demonstrate and discuss Converting Stringified JSON Into JavaScript Objects.
	Application > index.js
*/

		/*...*/
		console.log(otherData);

		// [Section] Converting Stringified JSON Into JavaScript Objects
		/*
			- Objects are common data types used in applications because of the complex data structures that can be created out of them
			- Information is commonly sent to applications in stringified JSON and then converted back into objects
			- This happens both for sending information to a backend application and sending information back to a frontend application
		*/
		let batchesJSON = `[{ "batchName": "Batch X" }, { "batchName": "Batch Y" }]`;

		console.log('Result from parse method:');
		console.log(JSON.parse(batchesJSON));

		let stringifiedObject = `{ "name": "John", "age": "31", "address": { "city": "Manila", "country": "Philippines" } }`

		console.log(JSON.parse(stringifiedObject));

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for JSON object.
		*/

/*
========
Activity
========
*/

// Note: Copy the code from activity-template.js into the batch Boodle Notes so students can copy the template of the code for this activity.


/*

Instructions that can be provided to the students for reference:

Activity Reference:

What is JSON?
https://www.w3schools.com/js/js_json_intro.asp

JSON.parse()
https://www.w3schools.com/js/js_json_parse.asp

JSON.stringify()
https://www.w3schools.com/js/js_json_stringify.asp

Activity:

Member 1:
1. In the s31 folder, create an activity folder and an index.html and index.js file inside of it.
- Create an index.html file to attach our index.js file
- Copy the template from boodle notes and paste it in an index.js file.
- Update your local sessions git repository and push to git with the commit message of Add template code s31.
- Console log the message Hello World to ensure that the script file is properly associated with the html file.

Member 1-4:
2. Given the code provided by your instructor, fix the JSON string by looking for errors when the code tries to parse the JSON string.
3. Check and correct the JSON string if the following are missing:
- Double quotes
- Commas
- Colons
- Square/Curly brackets
- Values

Member 5:
4. Create an object into a variable called product with the following properties:
Name (String)
Category (String)
Quantity (Number)
Model (String)
5. Then, stringify the object and save it in the given stringifiedProduct variable.

All Members:
6. Check out to your own git branch with git checkout -b <branchName>
7. Update your local sessions git repository and push to git with the commit message of Add activity code s31.
8. Add the sessions repo link in Boodle for s31.
*/

/*
Solution:


1. Create an "activity" folder, an "index.html" file inside of it and link the "index.js" file.
	Batch Folder > S20 > Activity > index.html
*/

		/*
		<!DOCTYPE HTML>
		<html>
		    <head>
		        <title>Activity: Introduction to JSON</title>
		    </head>
		    <body>
		    	<script src="./index.js"></script>
		    </body>
		</html>
		*/

/*
2. Create a "index.js" file and copy the contents of the "template.js" file.
	- Add activity-template.js file in boodle.
	- Have trainees copy the content into their own index.js file
	Application > index.js
*/

		let users = `[
		    {
		      "id": ,
		      "name": "Leanne Graham",
		      "username": "Bret",
		      "email": "Sincere@april.biz",
		      "address": {
		        "street": "Kulas Light",
		        "suite": "Apt. 556",
		        "city": "Gwenborough",
		        "zipcode": "92998-3874",
		        "geo": {
		          "lat": "-37.3159",
		          "lng": "81.1496"
		        }
		      },
		      "phone": "1-770-736-8031 x56442",
		      "website": hildegard.org",
		      "company": {
		        "name": "Romaguera-Crona",
		        "catchPhrase": "Multi-layered client-server neural-net",
		        "bs": "harness real-time e-markets"
		      }
		    },
		    {
		      "id": 2,
		      "name": "Ervin Howell",
		      "username": "Antonette",
		      "email": "Shanna@melissa.tv",
		      "address": {
		        "street": "Victor Plains",
		        "suite": "Suite 879",
		        "city": "Wisokyburgh",
		        "zipcode": "90566-7771",
		        "geo": {
		          "lat": "-43.9509",
		          "lng": "-34.4618"
		        }
		      },
		      "phone": "010-692-6593 x09125",
		      "website": "anastasia.net",
		      "company": {
		        "name": "Deckow-Crist",
		        "catchPhrase": "Proactive didactic contingency",
		        "bs": synergize scalable supply-chains"
		      }
		    },
		    {
		      "id": 3,
		      "name": "Clementine Bauch",
		      "username": "Samantha",
		      "email": "Nathan@yesenia.net",
		      "address": {
		        "street": "Douglas Extension",
		        "suite": "Suite 847",
		        "city": "McKenziehaven",
		        "zipcode": "59590-4157",
		        "geo": {
		          "lat": "-68.6102",
		          "lng": "-47.0653"
		        }
		      },
		      "phone": "1-463-123-4447",
		      "website": "ramiro.info",
		      "company": 
		        "name": "Romaguera-Jacobson",
		        "catchPhrase": "Face to face bifurcated interface",
		        "bs": "e-enable strategic applications"
		      }
		    },
		    {
		      "id": 4,
		      "name": "Patricia Lebsack",
		      "username": "Karianne",
		      "email": "Julianne.OConner@kory.org",
		      "address": {
		        "street": "Hoeger Mall",
		        "suite": "Apt. 692",
		        "city": "South Elvis",
		        "zipcode": "53919-4257",
		        "geo": {
		          "lat": "29.4572",
		          "lng": "-164.2990"
		        }
		      },
		      "phone": "493-170-9623 x156",
		      "website": "kale.biz"
		      "company": {
		        "name": "Robel-Corkery",
		        "catchPhrase": "Multi-tiered zero tolerance productivity",
		        "bs": "transition cutting-edge web services"
		      }
		    },
		    {
		      "id": 5,
		      "name": "Chelsey Dietrich",
		      "username": "Kamren",
		      "email": "Lucio_Hettinger@annie.ca",
		      "address": {
		        "street": "Skiles Walks",
		        "suite": "Suite 351",
		        "city": "Roscoeview",
		        "zipcode": "33263",
		        "geo": {
		          "lat": "-31.8129",
		          "lng": "62.5342"
		        }
		      },
		      "phone": "(254)954-1289",
		      "website": "demarco.info",
		      "company": {
		        "name": "Keebler LLC",
		        "catchPhrase": "User-centric fault-tolerant solution",
		        "bs": "revolutionize end-to-end systems"
		      }
		    },
		    {
		      "id": 6,
		      "name": "Mrs. Dennis Schulist",
		      "username": "Leopoldo_Corkery",
		      "email": "Karley_Dach@jasper.info",
		      "address": 
		        "street": "Norberto Crossing",
		        "suite": "Apt. 950",
		        "city": "South Christy",
		        "zipcode": "23505-1337",
		        "geo": {
		          "lat": "-71.4197",
		          "lng": "71.7478"
		        
		      },
		      "phone": "1-477-935-8478 x6430",
		      "website": "ola.org",
		      "company": {
		        "name": "Considine-Lockman",
		        "catchPhrase": "Synchronised bottom-line interface",
		        "bs": "e-enable innovative applications"
		      }
		    },
		    {
		      "id": 7,
		      "name": "Kurtis Weissnat",
		      "username": "Elwyn.Skiles",
		      "email": "Telly.Hoeger@billy.biz",
		      "address": {
		        "street": "Rex Trail",
		        "suite": "Suite 280",
		        "city": "Howemouth",
		        "zipcode": "58804-1099",
		        "geo": 
		          "lat": "24.8918",
		          "lng": "21.8984"
		        
		      },
		      "phone": "210.067.6132",
		      "website": "elvis.io",
		      "company": {
		        "name": "Johns Group",
		        "catchPhrase": "Configurable multimedia task-force",
		        "bs": "generate enterprise e-tailers"
		      }
		    },
		    {
		      "id": 8,
		      "name": "Nicholas Runolfsdottir V",
		      "username": "Maxime_Nienow",
		      "email": "Sherwood@rosamond.me",
		      "address": {
		        "street": "Ellsworth Summit",
		        "suite": "Suite 729",
		        "city": "Aliyaview",
		        "zipcode": "45169",
		        "geo": {
		          "lat": "-14.3990",
		          "lng": "-120.7677"
		        }
		      }
		      "phone": "586.493.6943 x140",
		      "website": "jacynthe.com",
		      "company": {
		        "name": "Abernathy Group",
		        "catchPhrase": "Implemented secondary concept",
		        "bs": "e-enable extensible e-tailers"
		      }
		    },
		    {
		      "id": 9,
		      "name": "Glenna Reichert",
		      "username": "Delphine",
		      "email": "Chaim_McDermott@dana.io",
		      "address": {
		        "street": "Dayna Park",
		        "suite": "Suite 449",
		        "city": "Bartholomebury",
		        "zipcode": "76495-3109",
		        "geo": {
		          "lat": "24.6463"
		          "lng": "-168.8889"
		        }
		      },
		      "phone": "(775)976-6794 x41206",
		      "website": "conrad.com",
		      "company": {
		        "name": "Yost and Sons",
		        "catchPhrase": "Switchable contextually-based project",
		        "bs": "aggregate real-time technologies"
		      }
		    },
		    {
		      "id": 10,
		      "name": "Clementina DuBuque",
		      "username": "Moriah.Stanton",
		      "email": "Rey.Padberg@karina.biz",
		      "address": {
		        "street" "Kattie Turnpike",
		        "suite": "Suite 198",
		        "city": "Lebsackbury",
		        "zipcode": "31428-2261",
		        "geo": {
		          "lat": "-38.2386",
		          "lng": "57.2232"
		        }
		      },
		      "phone": "024-648-3804",
		      "website": "ambrose.net",
		      "company": {
		        "name": "Hoeger LLC",
		        "catchPhrase": "Centralized empowering task-force",
		        bs": "target end-to-end models"
		      }
		    }
		  ]`;
		  
		let parsedUsers = JSON.parse(users)
		console.log(parsedUsers);

/*
Lines with error:

- L003: missing value (ID: 1)
- L018: missing double quote
- L045: missing double quote
- L065: missing curly bracket (opening)
- L087: missing comma
- L122: missing curly bracket (opening)
- L130: missing curly bracket (closing)
- L150: missing curly bracket (opening)
- L150: missing curly bracket (closing)
- L177: missing comma
- L197: missing comma
- L215: missing colon
- L229: missing double quote
*/

/*
3. Find the errors in the code and correct them.
	Application > index.js
*/

		/*
		Important Note:
			- One of the outputs for the activity is to make the script work properly.
			- Once the script runs properly meaning that all errors have been found, 
			- Refer to the "index.js" file in the GitLab codebase to find the solution.
		*/

/*
4. Stringify a product object.
	Application > index.js
*/

		/*...*/
		let parsedUsers = JSON.parse(users)
		console.log(parsedUsers);

		let product = {
		  name: 'Home Desktop Table',
		  category: 'Furniture',
		  quantity: 293,
		  model: '140CMMRNBLK'
		};

		let stringifiedProduct = JSON.stringify(product)
		console.log(stringifiedProduct);



