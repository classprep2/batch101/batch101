/*
====================================================
S41 - Express.js - Data Persistence via Mongoose ODM
====================================================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1QWDh8mf_kcSBwmNSXCGuBqyWtgOyEQmLVwte_uLqSz0/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s30

Other References:
	npm init
		https://docs.npmjs.com/cli/v7/commands/npm-init
	MongoDB Atlas
		https://cloud.mongodb.com/
	Get Started With Atlas
		https://docs.atlas.mongodb.com/getting-started/
	Express JS
		https://expressjs.com/
	What Is Mongoose
		https://mongoosejs.com/
	Mongoose Documentation
		https://mongoosejs.com/docs/
	What Are Drivers
		https://www.digitalcitizen.life/what-are-drivers-why-do-you-need-them/
	Mongoose Deprecation Warnings
		https://mongoosejs.com/docs/deprecations.html
	Mongoose Connections
		https://mongoosejs.com/docs/connections.html
	MongoDB String URI Format
		https://docs.mongodb.com/manual/reference/connection-string/
	console.error.bind
		https://www.tjvantoll.com/2015/12/29/console-error-bind/
	Default MongoDB Port
		https://docs.mongodb.com/manual/reference/default-mongodb-port/#:~:text=27017,setting%20in%20a%20configuration%20file.
	Mongoose Schemas
		https://mongoosejs.com/docs/guide.html
	Mongoose Models
		https://mongoosejs.com/docs/models.html
	Mongoose findOne Method
		https://mongoosejs.com/docs/api.html#model_Model.findOne
	Mongoose save Method
		https://mongoosejs.com/docs/models.html#constructing-documents
	Mongoose 7.x Dropped Callback Functions
		https://mongoosejs.com/docs/migrating_to_7.html#dropped-callback-support
	HTTP Status Codes
		https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
	Creating Git Projects:
		GitLab
			https://gitlab.com/projects/new#blank_project
		GitHub
			https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
==========
Discussion
==========
*/

// 1. Create a MongoDB database.
	// Browser > MongoDB

	Database Name: bXXX_to-do

	/*
	Important Note:
		- This step may be skipped entirely as adding the database name in the connection string and creating documents will prompt MongoDB to create the database.
		- Refer to "references" section of this file to find the link to MongoDB Atlas.
	*/

// 2. Get the MongoDB connection string from MongoDB Atlas.
// Browser > MongoDB

	mongodb+srv://admin:<password>@cluster0.7iowx.mongodb.net/myFirstDatabase?retryWrites=true&w=majority

	/*
	Important Note:
		- If the students have not successfully setup their Mongo DB Atlas accounts, you may use the documentation below to guide the students on how to set it up.
		- The databases of the students should have been set up at this time due to previous sessions related to Mongo DB discussions.
		- Refer to "references" section of this file to find the documentation for Get Started With Atlas.
	*/

// 3. Create a "package.json" file
// Batch Folder > S30 > Terminal

	npm init

	/*
	Important Note:
		- npm stands for "node package manager" which is responsible for assisting us developers with managing dependencies within a project.
		- Triggering this command will prompt the user for the different settings that will define the application.
		- No changes will be added to the package.json file, just keep on pressing "enter" key on the keyboard to create the default package.json file.
		- To prevent pressing the "enter" button upon creation of this file, the command "npm init -y" can be used to easily do this and skip the questionnaire.
		- You may highlight the questions asked after inputting the command then showing the students the package.json file to find the same items asked for are available in it.
		- Refer to "references" section of this file to find the documentation for npm init.
	*/

// 4. Install Express JS and Mongoose.
// Application > Terminal

	// Syntax
		// npm install packageA packageB
	npm install express mongoose

	/*
	Important Note:
		- After triggering the command, express and mongoose will now be listed under the "dependencies" property of the package.json file.
		- This will create a "node_modules" folder that will contain all the necessary files downloaded for the package.
		- The "package.json" file is responsible for listing all packages and the versions used in creating the application.
		- The version of the package installed is also indicated in the file which prevents applications from breaking if ever new updates are introduced to packages.
		- If the students incorrectly install a different package, the command "npm uninstall <package_name>" may be used.
		- A specific version of a package may also be installed by using the command "npm install@<version number>" (e.g. npm install@4.17.1).
		- This is useful for instances where an older version of the package is preferred to be used or when a current version of a package has bugs that tend to break an application.
		- Refer to "references" section of this file to find the link to the Express JS website, documentations for What Is Mongoose and Mongoose Documentation.
	*/

// 5. Setup a basic Express JS server.
// Application > index.js

	const express = require("express");

	const app = express();
	const port = 3001;

	// [Section] Creation of todo list routes
	// Setup for allowing the server to handle data from requests
	// Allows your app to read json data
	app.use(express.json());
	// Allows your app to read data from forms
	app.use(express.urlencoded({extended:true})); 

	// Listen to the port, meaning, if the port is accessed, we run the server
	if(require.main === module){
		app.listen(port, () => console.log(`Server running at port ${port}`));
	}
	
	module.exports = app;

// 6. Import the Mongoose package.
// Application > index.js

	const express = require("express");
	// Mongoose is a package that allows creation of Schemas to model our data structures
	// Also has access to a number of methods for manipulating our database
	const mongoose = require("mongoose");

	const app = express();
	/*...*/

// 7. Connect to MongoDB
// Application > index.js

	/*...*/
	const port = 3001;

	// [Section] MongoDB connection

	// Connect to the database by passing in your connection string, remember to replace the password and database names with actual values
	// Due to updates in Mongo DB drivers that allow connection to it, the default connection string is being flagged as an error
	// By default a warning will be displayed in the terminal when the application is run, but this will not prevent Mongoose from being used in the application
	// { newUrlParser : true } allows us to avoid any current and future errors while connecting to Mongo DB

	// Syntax
		// mongoose.connect("<MongoDB Atlas connection string>", { useNewUrlParser : true });

	// Connecting to MongoDB Atlas
	mongoose.connect("mongodb+srv://admin:<password>@cluster0.7iowx.mongodb.net/bXX-to-do?retryWrites=true&w=majority", 
		{ 
			useNewUrlParser : true,  
			useUnifiedTopology : true
		}
	);

	// Connecting to MongoDB locally
	// 27017 is the default port on where mongo instances are run in a device
	// mongoose.connect("mongodb://localhost:27017/bXX-to-do", 
	// 	{ 
	// 		useNewUrlParser : true,  
	// 		useUnifiedTopology : true
	// 	}
	// );

	// Set notifications for connection success or failure
	// Connection to the database
	// Allows to handle errors when the initial connection is establised
	// Works with the on and once Mongoose methods
	let db = mongoose.connection; 
	// If a connection error occurred, output in the console
	// console.error.bind(console) allows us to print errors in the browser console and in the terminal
	// "connection error" is the message that will display if an error is encountered
	db.on("error", console.error.bind(console, "connection error"));

	// If the connection is successful, output in the console
	db.once("open", () => console.log("We're connected to the cloud database"));

	app.use(express.json());

	/*...*/

	/*
	Important Note:
		- Don't forget to change the MongoDB Atlas connection string to your own credentials to be able to show students the output of the discussion.
		- Refer to "references" section of this file to find documentation for What Are Drivers, Mongoose Deprecation Warnings, Mongoose Connections, MongoDB String URI Format, console.error.bind and Default MongoDB Port.
	*/

// 8. Run the backend application to test if everything is working as intended.
// Application > Terminal

	nodemon index.js

// 9. Create a Task schema
// Application > index.js

	/*...*/
	db.once("open", () => console.log("We're connected to the cloud database"));

	// [Section] Mongoose Schemas

	// Schemas determine the structure of the documents to be written in the database
	// Schemas act as blueprints to our data
	// Use the Schema() constructor of the Mongoose module to create a new Schema object
	// The "new" keyword creates a new Schema
	const taskSchema = new mongoose.Schema({ 
		// Define the fields with the corresponding data type
		// For a task, it needs a "task name" and "task status"
		// There is a field called "name" and its data type is "String"
		name : String,
		// There is a field called "status" that is a "String" and the default value is "pending"
		status : { 
			type : String,
			// Default values are the predefined values for a field if we don't put any value
			default : "pending"
		}
	})

	app.use(express.json());
	/*...*/

	/*
	Important Note:
		- Schemas are used as blueprints that define the data structure of the documents that will be stored in MongoDB
		- It allows easier validation of data that helps us focus on the business logic of our code rather than the technical side of things
		- Refer to "references" section of this file to find documentation for Mongoose Schemas.
	*/

// 10. Create a Task model
// Application > index.js

	/*...*/
	const taskSchema = new mongoose.Schema({ 
		/*...*/
	})

	// [Section] Models
	// Uses schemas and are used to create/instantiate objects that correspond to the schema
	// Models use Schemas and they act as the middleman from the server (JS code) to our database
	// Server > Schema (blueprint) > Database > Collection

	// The variable/object "Task" and "User" can now used to run commands for interacting with our database
	// "Task" and "User" are both capitalized following the MVC approach for naming conventions
	// Models must be in singular form and capitalized
	// The first parameter of the Mongoose model method indicates the collection in where to store the data
	// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
	// Using Mongoose, the package was programmed well enough that it automatically converts the singular form of the model name into a plural form when creating a collection in postman
	const Task = mongoose.model("Task", taskSchema); 

	app.use(express.json());
	/*...*/

	/*
	Important Note:
		- The Models are what allows us to gain access to methods that will allow us perform CRUD functions on our database with ease
		- So long as the singular form of the model name is in the English language, Mongoose will be able to automatically create a collection name in mongo DB of it's plural form
		- Refer to "references" section of this file to find documentation for Mongoose Models.
	*/

// 11. Create a POST route to create a new task.
// Application > index.js

	/*...*/
	app.use(express.urlencoded({extended:true})); 

	// Creating a new task

	// Business Logic
	/*
	1. Add a functionality to check if there are duplicate tasks
		- If the task already exists in the database, we return an error
		- If the task doesn't exist in the database, we add it in the database
	2. The task data will be coming from the request's body
	3. Create a new Task object with a "name" field/property
	4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
	*/

	app.post("/tasks", (req, res)=> {
		
		// Check if there are duplicate tasks
		// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
		// findOne() returns the first document that matches the search criteria as a single object.
		// findOne() can send the possible result or error in another method called then() for further processing.
		// .then() is chained to another method/promise that is able to return a value or an error.
		// .then() waits for the previous method to complete its process. It will only run once the previous method is able to return a value or error.
		// .then() method can then process the returned result or error in a callback method inside it.
		// the callback method in the then() will be able to receive the result or error returned by the previous method it is attached to.
		// the .findOne() method returns the result first and the error second as parameters.
		// Call back functions in mongoose methods are programmed this way to store the returned results in the first parameter and any errors in the second parameter
		// If there are no matches, the value of result is null
		// "err" is a shorthand naming convention for errors
		Task.findOne({name : req.body.name}).then((result, err) => {

			// If a document was found and the document's name matches the information sent via the client/Postman
			if(result != null && result.name == req.body.name){

				// Return a message to the client/Postman
				return res.send("Duplicate task found");

			// If no document was found
			} else {

				// Create a new task and save it to the database
				let newTask = new Task({
					name : req.body.name
				});

				// The "save" method will store the information to the database
				// Since the "newTask" was created/instantiated from the Mongoose Schema it will gain access to this method to save to the database
				// The "save()" method can send the result or error in another JS method called then()
				// the .save() method returns the result first and the error second as parameters.
				newTask.save().then((savedTask, saveErr) => {
					// If there are errors in saving
					if(saveErr){

						// Will print any errors found in the console
						// saveErr is an error object that will contain details about the error
						// Errors normally come as an object data type
						return console.error(saveErr);

					// No error found while creating the document
					} else {

						// Return a status code of 201 for created
						// Sends a message "New task created" on successful creation
						return res.status(201).send("New task created");

					}
				})
			}

		})
	})

	if(require.main === module){
		app.listen(port, () => console.log(`Server running at port ${port}`));
	}
	
	module.exports = app;

	/*
	Important Note:
		- Refer to "references" section of this file to find documentation for Mongoose findOne Method, Mongoose save Method and HTTP Status Codes.
		- Updated code to comply to Mongoose 7.x changes which disallows callback methods in findOne(),find(), etc.
		- Refer to "references" section for updated Mongoose Methods in Mongoose 7.x
	*/

// 12. Process a POST request at the "/tasks" route using postman to get create a task.
// Postman

	url: http://localhost:3001/tasks
	method: POST
	body: raw + JSON
		{
			"name": "Eat"
		}

// 13. Create a GET request to retrieve all the tasks.
// Application > index.js

	/*...*/
	app.post("/tasks", (req, res)=> {
		/*...*/
	})

	// Getting all the tasks

	// Business Logic
	/*
	1. Retrieve all the documents
	2. If an error is encountered, print the error
	3. If no errors are found, send a success status back to the client/Postman and return an array of documents
	*/

	app.get("/tasks", (req, res) => {

		// "find" is a Mongoose method that is similar to Mongodb "find", and an empty "{}" means it returns all the documents and stores them in the "result" parameter of the callback function
		Task.find({}).then((result, err) => {

			// If an error occurred
			if (err) {

				// Will print any errors found in the console
				return console.log(err);

			// If no errors are found
			} else {

				// Status "200" means that everything is "OK" in terms of processing
				// The "json" method allows to send a JSON format for the response
				// The returned response is purposefully returned as an object with the "data" property to mirror real world complex data structures
				return res.status(200).json({
					data : result			
				})

			}

		})
	})


	if(require.main === module){
		app.listen(port, () => console.log(`Server running at port ${port}`));
	}
	
	module.exports = app;

	/*
	Important Note:
		- Refer to "references" section of this file to find documentation for Mongoose findOne Method, Mongoose save Method and HTTP Status Codes.
		- Updated code to comply to Mongoose 7.x changes which disallows callback methods in findOne(),find(), etc.
		- Refer to "references" section for updated Mongoose Methods in Mongoose 7.x
	*/

// 14. Process a GET request at the "/tasks" route using postman to get all the tasks.
// Postman

	url: http://localhost:3001/tasks
	method: GET

/*
========
Activity
========

Activity References

ExpressJS Routing
https://expressjs.com/en/guide/routing.html

Mongoose Save()
https://mongoosejs.com/docs/models.html#constructing-documents


Activity


## Instructions that can be provided to the students for reference:

Member 1:
1. Create a User schema and a User model.
	- A user document should have username and password fields.
Member 2:
2. Create a POST route that will access the "/signup" route that will create a user.
	- Add a console.log() to check if data can be received from postman.
		- data received should contain details to be saved in the users database.
Member 3:
3. Add a functionalities to the route to check if there are duplicate tasks:
	- The user data will be coming from the request's body
	- If the user already exists in the database, we return a message to the client:
		- "Duplicate username found"
Member 4:
4. Else, If the username doesn't exist in the database, add an else statement, then inside it:
	- Check if the request username and passwords are not empty.
		- If it is not empty
			- Create a new User object with a "username" and "password" fields/properties and save it in our database.
				- If there errors in saving, return the error.
				- Else, send a message to the client:
					- "New user registered"
					- Add an 201 HTTP Status Code.
	- Else, send a message to the client with the following message:
		- "BOTH username and password must be provided."
Member 5:
5. Process a POST request at the "/signup" route using postman to register a user.
	- Save the postman collection for the requests.
	
All Members:
6. Check out to your own git branch with git checkout -b <branchName>
7. Update your local sessions git repository and push to git with the commit message of Add activity code s41.
8. Add the sessions repo link in Boodle for s41.

*/

//Solution:

// 1. Create a User schema.
// Application > index.js

	/*...*/
	app.get("/tasks", (req, res) => {
		/*...*/
	})

	// User Schema/Model
	const userSchema = new mongoose.Schema({
		username : String,
		password : String
	})

	if(require.main === module){
		app.listen(port, () => console.log(`Server running at port ${port}`));
	}
	
	module.exports = app;

// 2. Create a User model.
// Application > index.js

	/*...*/
	const userSchema = new mongoose.Schema({
		/*...*/
	})

	// The variable/object "User" can now used to run commands for interacting with our database
	const User = mongoose.model("User", userSchema);

	if(require.main === module){
		app.listen(port, () => console.log(`Server running at port ${port}`));
	}
	
	module.exports = app;

// 3. Create a POST route to register a user.
// Application > index.js

	/*...*/
	const User = mongoose.model("User", userSchema);

	// Registering a user

	// Business Logic
	/*
	1. Add a functionality to check if there are duplicate tasks
		- If the user already exists in the database, we return an error
		- If the user doesn't exist in the database, we add it in the database
	2. The user data will be coming from the request's body
	3. Create a new User object with a "username" and "password" fields/properties
	*/

	// Route for creating a user
	app.post("/signup", (req, res)=> {

		// Finds for a document with the matching username provided in the client/Postman
		User.findOne({ username : req.body.username }).then((result, err) => {

			// Check for duplicates
			if(result != null && result.username == req.body.username){

				return res.send("Duplicate username found");

			// No duplicates found
			} else {

				// If the username and password are both not blank
				if(req.body.username !== '' && req.body.password !== ''){

					// Create/instantiate a "newUser" from the "User" model
					let newUser = new User({
						username : req.body.username,
						password : req.body.password
					});
		
					// Create a document in the database
					newUser.save().then((savedTask, saveErr) => {

						// If an error occurred
						if(saveErr){

							// Return an error in the client/Postman
							return console.error(saveErr);

						// If no errors are found
						} else {

							// Send a response back to the client/Postman of "created"
							return res.status(201).send("New user registered");

						}

					})

				// If the "username" or "password" was left blank
				} else {

					/// Send a response back to the client/Postman of "created"
					return res.send("BOTH username and password must be provided.");
				}			
			}
		})
	})

	if(require.main === module){
		app.listen(port, () => console.log(`Server running at port ${port}`));
	}
	
	module.exports = app;

// 4. Process a POST request at the "/users" route using postman to register a user.
// Application > index.js

	url: http://localhost:3001/tasks
	method: POST
	{
		"username": "john",
		"password": "johndoe"
	}