/*oddOrEvenChecker -  practice if else with function*/

function oddOrEvenChecker(num){

    if(num % 2 === 0 ){
        alert(num + " is even!")
    } else {
        alert(num + " is odd!")
    }

}

oddOrEvenChecker(54);


/*ageChecker = practice if else with function and return keyword*/

function ageChecker(num){
    if(num < 18){
        alert(num + " is underaged!");
        return false;
    } else {
        alert(num + " is allowed to drink!");
        return true;
    }
}

let isAllowedToDrink = ageChecker(19);
console.log(isAllowedToDrink);



/*practice switch with function*/
function determineComputerUser(computerNumber) {
    let user;

    switch (computerNumber) {
        case 1: 
            user = 'Linus Torvalds';
            break;
        case 2:
            user = 'Steve Jobs';
            break;
        case 3:
            user = 'Sid Meier';
            break;
        case 4:
            user = 'Onel de Guzman';
            break;
        case 5:
            user = 'Christian Salvador';
            break;
        default:
            user = computerNumber + ' is out of bounds.';
            break;
    }

    return user;
}

console.log(determineComputerUser(3));

/*Longer activity- could be given as home work or after class practice*/
function playQuestions(){

    let score = 0;
    alert("Q1. Which is the largest ocean in the world? Atlantic Ocean,Pacific Ocean,Indian Ocean?")
    let answer = prompt("What is the name of the largest ocean in the world?").toLowerCase();
    switch(answer){
        case "atlantic ocean":
            alert("Wrong. The answer: Pacific Ocean!");
            break;
        case "indian ocean":
            alert("Wrong. The answer: Pacific Ocean!");
            break;
        case "pacific ocean":
            alert("Correct Answer: Pacific Ocean!");
            score++;
            break;
        default:
            alert("Wrong. The answer: Pacific Ocean!");
            break;
    }
    alert("Your current score is: "+score);
    alert("Q2. What is the capital of China? Beijing,Tokyo or Seoul?")
    answer = prompt("What is the capital of China?").toLowerCase();
    switch(answer){
        case "tokyo":
            alert("Wrong. The answer: Beijing!");
            break;
        case "seoul":
            alert("Wrong. The answer: Beijing!");
            break;
        case "beijing":
            alert("Correct Answer: Beijing!");
            score++;
            break;
        default:
            alert("Wrong. The answer: Beijing!");
            break;
    }
    alert("Your current score is: "+score);
    alert("Q3. How many planets are in the Solar System? 9,8 or 10?")
    answer = prompt("How many planets are in the Solar System?").toLowerCase();
    switch(answer){
        case "9":
            alert("Wrong. The answer: 8!");
            break;
        case "10":
            alert("Wrong. The answer: 8!");
            break;
        case "8":
            alert("Correct Answer: 8!");
            score++;
            break;
        default:
            alert("Wrong. The answer: 8!");
            break;
    }
    alert("Thank you for playing.")
    alert("Your final score is: " + score);
}


playQuestions();