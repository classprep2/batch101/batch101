# Session 27 - Array Mutator Methods
# Learning Objectives

At the end of the session, the students are expected to:

- manage sets of related data by using JavaScript arrays.

# Resources

## Instructional Materials
- [Google Slide Presentation](https://docs.google.com/presentation/d/1n6Cl7jYEM93uxqRxnKWh_FboFfiBQdKPR3tCQc8SwO8)

## Supplemental Materials

- [JavaScript Arrays (w3schools)](https://www.w3schools.com/js/js_arrays.asp)
- [JavaScript Array (MDN Web Docs)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)

# Lesson Proper

# Code Discussion

## Folder and File Preparation

Create a folder named **s27**, a folder named **discussion** inside the **s27** folder, then a file named **index.html** and **index.js** inside the **discussion** folder.

## Manipulating Arrays with Array Methods

**Javascript** has **built-in** functions and methods for arrays. This allows us to manipulate and access array items.

There are the 3 kinds of JS Array Methods.
    - Non-Mutators
    - Iterators
    - Mutators

##### Non-Mutator Methods
Non-Mutator Methods allows us to manage and perform tasks on an array without updating the contents of the original array.

##### Iterator Methods
Iterator Methods allow us to loop over elements/items in an array and perform tasks with them.

#### Mutator Methods
Mutator methods allow us to manage and perform tasks on an array by updating the contents of the original array.

For this session, we will discuss **JS Array Mutator Methods**.

### Mutator Methods

- Mutator methods are built-in JS Array methods that "mutate" or change an array after they're created
- These methods manipulate the original array performing various tasks such as adding and removing elements

Let's add the following code in our index.js file:
```js
    let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];
```
push()

- Adds an element in the end of an array AND returns the array's length
- Syntax
    - arrayName.push("itemToPush");

```js
    console.log('Current array:');
    console.log(fruits);
    let fruitsLength = fruits.push('Mango');
    console.log(fruitsLength);
    console.log('Mutated array from push method:');
    console.log(fruits);

    // Adding multiple elements to an array
    fruits.push('Avocado', 'Guava');
    console.log('Mutated array from push method:');
    console.log(fruits);

    //Let's try in a function
    function addFruit(fruit){
        //push the parameter to the array.
        fruits.push(fruit);
    }

    addFruit("Pineapple");

```
pop()

- Removes the last element in an array AND returns the removed element
- Syntax
    - arrayName.pop();

```js
    let removedFruit = fruits.pop();
    console.log(removedFruit);
    console.log('Mutated array from pop method:');
    console.log(fruits);

    //Let's try in a function
    function removeFruit(){
        //remove the last item in the array.
        fruits.pop();
    }

    removeFruit();
    console.log(fruits);

```
unshift()

- Adds one or more elements at the beginning of an array
- Syntax
    - arrayName.unshift('elementA');
    - arrayName.unshift('elementA', elementB);

```js
    fruits.unshift('Lime', 'Banana');
    console.log('Mutated array from unshift method:');
    console.log(fruits);

    //Let's try in a function
    function unshiftFruit(fruit){
        //add an item at front of the array.
        fruits.unshift(fruit);
    }

    unshiftFruit("calamansi");

    
```
shift()

- Removes an element at the beginning of an array AND returns the removed element
- Syntax
    - arrayName.shift();

```js
    let anotherFruit = fruits.shift();
    console.log(anotherFruit);
    console.log('Mutated array from shift method:');
    console.log(fruits);

    //Let's try in a function
    function shiftFruit(){
        //remove an item at front of the array.
        fruits.shift();
    }

    shiftFruit();


```
splice()

- Simultaneously removes elements from a specified index number and adds elements
- Syntax
    - arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
```js
    fruits.splice(1, 2, 'Lime', 'Cherry');
    console.log('Mutated array from splice method:');
    console.log(fruits);

    //Let's try in a function
    function spliceFruit(index,deleteCount,fruit){
        //remove and add an item at the specified point of the array.
        fruits.splice(index,deleteCount,fruit);
    }

    shiftFruit(1,1,"Avocado");
    shiftFruit(2,1,"Durian")
```
sort()

- Rearranges the array elements in alphanumeric order
- Syntax
   arrayName.sort();

```js
    fruits.sort();
    console.log('Mutated array from sort method:');
    console.log(fruits);

```
**Important Note:**
    - The "sort" method is used for more complicated sorting functions.
    - Focus the students on the basic usage of the sort method and discuss it's more advanced usage only when you are confident in the topic.
    - Discussing this might confuse the students more given the amount of topics found in the session.

reverse()

- Reverses the order of array elements
- Syntax
    - arrayName.reverse();

```js
    fruits.reverse();
    console.log('Mutated array from reverse method:');
    console.log(fruits);
```

Note:
    You can add more mini-activities to practice. However, please take note of the time left.

=================================
Activity Documentation References
=================================
JS Mutator Methods
    https://dev.to/ibrahima92/javascript-array-methods-mutator-vs-non-mutator-15e2
Add JS Object to JS Array
    https://stackabuse.com/bytes/push-an-object-to-an-array-in-javascript/
JS Function Parameters
    https://www.w3schools.com/js/js_function_parameters.asp

=============================
s27 - Activity Instructions
=============================

Member 1:
1. In the S27 folder, create an activity folder and an index.html and index.js file inside of it.
- Create an index.html file to attach our index.js file
- Copy the template from boodle notes and paste it in an index.js file.
- Update your local sessions git repository and push to git with the commit message of Add template code s27.
- Console log the message Hello World to ensure that the script file is properly associated with the html file.
2. Create a function called addPokemon which will allow us to register/add new pokemon into the registeredPokemon list.
    - this function should be able to receive a string.
    - add the received pokemon into the registeredPokemon array.

Member 2:
3. Create a function called deletePokemon which will delete the last pokemon you have added in the registeredPokemon array.
    - If the registered is not empty, delete the last pokemon in the registeredPokemon array
    - Else return a message:
        - "No pokemon registered"
    - Invoke the function.
    - Outside the function log the friendsList array.

Member 3:
4. Create a function called displayNumberOfPokemons which will display the amount of registeredPokemon in our array,
    - If the registeredPokemon array is empty return the message:
        - "No pokemon registered."
    - Else If the registeredPokemon array is not empty, return the number of registered Pokemons.

Member 4:
5. Create a function called sortPokemon which will  sort the registeredPokemon array in alphabetical order when invoked:
    - If the registeredPokemon array is empty return the message:
        - "No pokemon registered."
    - Else, sort the registeredPokemon array.

Member 5:
6. Create a function called registerTrainer which takes 3 arguments, name, level and an array of pokemons.
    - Inside the function, create an object called trainer.
        - The trainer object should have 3 properties, trainerName, trainerLevel and pokemons
        - Pass the values of the appropriate parameter to each property.
    - Add the trainer variable to the registeredTrainers array.

All Members:
7. Check out to your own git branch with git checkout -b <branchName>
8. Update your local sessions git repository and push to git with the commit message of Add activity code s27.
9. Add the sessions repo link in Boodle for s27.

## Solution

```jsx

/*
    Create functions which can manipulate our arrays.
*/

/*
    Important note: Don't pass the arrays as an argument to the function. The functions must be able to manipulate the current arrays.
*/

let registeredPokemon = [];
let registeredTrainers = [];

/*
    
   1. Create a function called addPokemon which will allow us to register/add new pokemon into the registeredPokemon list.
        - this function should be able to receive a string.
        - add the received pokemon into the registeredPokemon array.
*/
    
    function addPokemon(pokemon){
        registeredPokemon.push(pokemon);
    }
    
/*
    2. Create a function called deletePokemon which will delete the last pokemon you have added in the registeredPokemon array.
        - If the registered is not empty, delete the last pokemon in the registeredPokemon array
        - Else return a message:
            - "No registered pokemon."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/

function deletePokemon(){

    if(registeredPokemon.length > 0){

        registeredPokemon.pop();

    } else {
        return "No pokemon registered"
    }

};

/*
3. Create a function called displayNumberOfPokemons which will display the amount of registeredPokemon in our array,
        - If the registeredPokemon array is empty return the message:
            - "No pokemon registered."
        - Else If the registeredPokemon array is not empty, return the number of registered Pokemons.

*/

    function displayNumberOfPokemons(){
        if(registeredPokemon.length > 0){
            return registeredPokemon.length
        } else {
            return "No pokemon registered"
        }
    }

/*
    4. Create a function called sortPokemon which will  sort the registeredPokemon array in alphabetical order when invoked:
        - If the registeredPokemon array is empty return the message:
            - "No pokemon registered."
        - Else, sort the registeredPokemon array.

*/

    function sortPokemon(){

        if(registeredPokemon.length > 0){
            registeredPokemon.sort();
        } else {
            return "No pokemon registered"
        }

    };

/*
    5. Create a function called registerTrainer which takes 3 arguments, name, level and an array of pokemons.
        - Inside the function, create an object called trainer.
            - The trainer object should have 3 properties, trainerName, trainerLevel and pokemons
            - Pass the values of the appropriate parameter to each property.
        - Add the trainer variable to the registeredTrainers array.

*/


function registerTrainer(name,level,pokemons){


    let trainer = {
        trainerName: name,
        trainerLevel: level,
        pokemons: pokemons
    }

    registeredTrainers.push(trainer);

};

//Do not modify
//For exporting to test.js
try{
    module.exports = {

        registeredPokemon: typeof registeredPokemon !== 'undefined' ? registeredPokemon : null,
        registerTrainers: typeof registerTrainers !== 'undefined' ? registerTrainers : null,
        addPokemon: typeof addPokemon !== 'undefined' ? addPokemon : null,
        deletePokemon: typeof deletePokemon !== 'undefined' ? deletePokemon : null,
        displayNumberOfPokemons: typeof displayNumberOfPokemons !== 'undefined' ? displayNumberOfPokemons : null,
        sortPokemon: typeof sortPokemon !== 'undefined' ? sortPokemon : null,
        registerTrainer: typeof registerTrainer !== 'undefined' ? registerTrainer : null

    }
} catch(err){

}
```