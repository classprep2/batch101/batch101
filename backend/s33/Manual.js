/*
=============================================
S33 - MongoDB - Data Modeling and Translation
=============================================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1tr9pSk7H7An1O5JNq1jXBgVB0QFEfxOpydxbfIJy9cA/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s22

Other References:
	Data Model Design
		https://docs.mongodb.com/manual/core/data-model-design/
	Diagrams.net
		https://www.diagrams.net/
	Entity Relationship Diagram Symbols
		https://www.lucidchart.com/pages/ER-diagram-symbols-and-meaning
	Creating Git Projects:
		GitLab
			https://gitlab.com/projects/new#blank_project
		GitHub
			https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application
*/

/*
==========
Discussion
==========
*/

/*
1. Present the slides and discuss the concepts found in our materials.
	Browser > Google Slides
*/

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentations for Discussion Slides, Data Model Design, Diagrams.net and Entity Relationship Diagram Symbols.
		*/

/*
2. After the discussion on Identifying Relationships Between Data Models create a "users.json" file that and discuss how to translate data models into actual code.
	Application > users.json
*/

		{
		    "_id": "507f1f77bcf86cd799439011",
		    "firstName": "John",
		    "lastName": "Smith",
		    "email": "johnsmith@mail.com",
		    "password": "johnhandsome",
		    "isAdmin": false,
		    "mobileNumber": "09221231234",
		    "enrollments": ["601ccbe89bcd482ee8fa2c99", "507f191e810c19729de860ea"],
		    "datetimeRegistered": "2020-03-15T15:00:00.00Z"
		}

		{
		    "_id": "507f1f77bcf86cd799439011",
		    "firstName": "Jane",
		    "lastName": "Doe",
		    "email": "janedoe@zuitt.com",
		    "password": "johnhandsome",
		    "isAdmin": true,
		    "mobileNumber": "09277365528",
		    "enrollments": [],
		    "datetimeRegistered": "2020-03-15T15:00:00.00Z"
		}

/*
3. Create a "courses.json" file that and discuss how to translate data models into actual code.
	Application > courses.json
*/

		{
		    "_id": "601ccbe89bcd482ee8fa2c99",
		    "name": "HTML",
		    "description": "Learn the basics and how to code.",
		    "price": 1000,
		    "isActive": true,
		    "slots": "20",
		    "enrollees": ["507f1f77bcf86cd799439011"],
		    "datetimeCreated": "2020-03-10T15:00:00.00Z"
		}

		{
		    "_id": "507f191e810c19729de860ea",
		    "name": "CSS",
		    "description": "Let's make our applications look cool!",
		    "price": 2000,
		    "isActive": false,
		    "slots": "20",
		    "enrollees": ["507f1f77bcf86cd799439011"],
		    "datetimeCreated": "2021-08-15T15:00:00.00Z"
		}

		{
		    "_id": "601ccc169bcd482ee8fa2c9a",
		    "name": "JavaScript",
		    "description": "Make websites fun and interactive!",
		    "price": 4000,
		    "isActive": true,
		    "slots": "15",
		    "enrollees": [],
		    "datetimeCreated": "2022-02-01T15:00:00.00Z"
		}

/*
4. Create a "transactions.json" file that and discuss how to translate data models into actual code.
	Application > transactions.json
*/

		{
		    "_id": "6062a8eb834de22e84600edd",
		    "userId": "507f1f77bcf86cd799439011",
		    "courseId": "601ccbe89bcd482ee8fa2c99",
		    "isPaid": true,
		    "paymentMethod": "PayPal",
		    "datetimeCreated": "2020-03-12T15:00:00.00Z"
		}

		{
		    "_id": "603e10d3b30fd23b081a94eb",
		    "userId": "507f1f77bcf86cd799439011",
		    "courseId": "507f191e810c19729de860ea",
		    "isPaid": true,
		    "paymentMethod": "American Express - Credit",
		    "datetimeCreated": "2020-03-12T15:00:00.00Z"
		}

/*
========
Activity
========
*/

/*
1. In the S33 folder, create an activity folder.

2. Go to diagrams.net and link your professional gmail account.

3. Using the given set of entities and attributes, create an ERD that will show the relationships between the collections:

4. Copy the diagram from your Google Drive into the activity folder.

5. Inside the activity folder, create a mock data for each of the collection using JSON syntax. Follow the ERD structure you created and make sure to include the id property.
	a. user.json
	b. product.json
	c. order.json
	d. orderProduct.json

6. Update your local backend git repository and push to git with the commit message of Add activity code s33.

7. Add the link in Boodle for s33.


*/