# Session Objectives

At the end of the session, the students are expected to:

- put real-world data into a database system by learning to analyzing data structure and relationships; and
- translate the analyze data by creating an entity-relationship diagram.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5/s22)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1tr9pSk7H7An1O5JNq1jXBgVB0QFEfxOpydxbfIJy9cA)
- [Lesson Plan](lesson-plan.md)

## Supplemental Materials

- [Data Model Design (MongoDB Docs)](https://docs.mongodb.com/manual/core/data-model-design/)

# Lesson Proper

## What is a data model?

A data model describes how data is organized or group in a database. By creating data models, we anticipate which data will be managed by the database system and the application to be developed.

To create data model in this session, we will simulate a scenario where an organization wants to make a web app to manage the courses they offer.

## Data modelling case simulation

The web app will start with a simple login procedure; a procedure that most pre-existing apps already have.

Then, there will be two types of users: the admin of the system and their customers.

The information about the admin will be placed in the database beforehand. For the customers, they will need to register to the app before logging in.

During the registration process, they want to get the name and contact details of the customer. The process will also ask for the preferred login credentials of the customer.

The admin is then responsible in adding the details of the course that they offer. The admin should also be able to update whether the course is currently being offered or not.

Finally, the customers shall have information on which courses they enrolled to. At the same time, courses shall also contain a list of customers that enrolled to a given course.

If you are a junior developer, the task of data modelling will usually fall to the senior member or your team lead. As a junior developer, it will greatly help you understand how these things take place by completing this session.

## Creating the data models

After reading the process that the web app will follow, we then create data models. 

We will have two major data models: **users** and **courses**.

The users will contain the information of both the customers and the admin.

Next, we identify which fields are needed by each data model:

- User
    - First Name
    - Last Name
    - Email
    - Password
    - Is Admin?
    - Mobile Number
    - Enrollments
- Course
    - Name
    - Description
    - Price
    - Is Active?
    - Enrollees

## Identifying relationships between data models

Now that we have identified the data models, we then identify their relationships.

Relationships can have three types: one-to-one, one-to-many, and many-to-many. Examples are given below:

- One-to-one
    - A person can only have one employee ID on a given company.
- One-to-many
    - A person can have one or more email addresses.
- Many-to-many
    - A book can be written by multiple authors and an author can write multiple books.

With the examples given above, we can say that users and courses have a many-to-many relationship.

## Translating data models into an ERD

### What is an entity-relationship diagram?

An entity-relationship diagram (or ERD) is a diagram used to describe the structure of the database design. It aims to show what attributes (fields) an entity (collection) has. It also shows the relationship of attributes between entities.

### Drawing an ERD using diagrams.net

Given the information provided earlier, we can go to [diagrams.net](http://diagrams.net) and use it for free to create our first ERD.

Instruct the students to go to [diagrams.net](http://diagrams.net) and connect their Google Account so that they can save the ERD to their Google Drive.

When creating a diagram, select the Entity-Relationship option.

![readme-images/WDC028v1.5-S22_-_Course_Booking_ERD.png](readme-images/WDC028v1.5-S22_-_Course_Booking_ERD.png)

Finished ERD using Diagrams.net

The enrollments of users and enrollees of courses have been linked via the **user_enrollments** entity. By connecting the users and courses that way, we can implement the many-to-many relationship. 

An **associative entity** is the table that associates **two (or more) other tables** in a many-to-many relationship.

Additionally, we added the datetime fields so that we can store when the data was stored to the database.

### Converting the ERD into a JSON-like syntax

To reinforce the ERD, we can simulate the resulting JSON data that follows the given database design.

**users.json**

```json
{
    "_id": "507f1f77bcf86cd799439011",
    "firstName": "John",
    "lastName": "Smith",
    "email": "johnsmith@mail.com",
    "password": "johnhandsome",
    "isAdmin": false,
    "mobileNumber": "09221231234",
    "datetimeRegistered": "2020-03-15T15:00:00.00Z"
}
```

**courses.json**

```json
{
    "_id": "507f191e810c19729de860ea",
    "name": "Full Stack Web Dev in JavaScript",
    "description": "Learn how to do web dev using JavaScript.",
    "price": 3000,
    "isActive": true,
    "mobileNumber": "09221231234",
    "datetimeCreated": "2020-03-10T15:00:00.00Z"
}
```

**user-enrollments.json**

```json
{
    "_id": "618f192e820c19759de840ea",
    "userId": "507f1f77bcf86cd799439011",
    "courseId": "507f191e810c19729de860ea",
    "datetimeEnrolled": "2020-03-16T15:00:00.00Z"
}
```

As shown above, the **user-enrollments** holds together which course a user enrolled to and at which date and time. The collection holds reference to the IDs of both the user and the course in order to implement the many-to-many relationship.

# Activity

## Instructions

Using the given set of entities and attributes, create the ERD that will show its many-to-many relationship.

- Users
    - First Name
    - Last Name
    - Email
    - Password
    - Is Admin?
    - Mobile Number
    - Orders
        - Product ID
        - Quantity
- Products
    - Name
    - Description
    - Price
    - Is Active?

## Expected Output

![readme-images/WDC028v1.5-S22_-_Entity-Relationship_Diagram-Ecommerce.png](readme-images/WDC028v1.5-S22_-_Entity-Relationship_Diagram-Ecommerce.png)

## Solution

Open the file from [here](https://drive.google.com/file/d/18wwaeQqu5Z5xIoDR1Lnh6k7rLpK23lrg/view?usp=sharing) to check the Solution (go to the Ecommerce tab).