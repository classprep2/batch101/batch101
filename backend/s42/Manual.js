/*
================================================
S42 - Express.js - Modules, Parameterized Routes
================================================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1_VUaNcoacbzdfSh-fYYFmoE8UeRLuFd85IVCzRCElj0/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s31

Other References:
	npm init
		https://docs.npmjs.com/cli/v7/commands/npm-init
	Express JS
		https://expressjs.com/
	What Is Mongoose
		https://mongoosejs.com/
	Mongoose Documentation
		https://mongoosejs.com/docs/
	MVC Framework
		https://www.tutorialspoint.com/mvc_framework/mvc_framework_introduction.htm
	Express JS Routing
		https://expressjs.com/en/guide/routing.html
	Mongoose find Method
		https://mongoosejs.com/docs/api.html#model_Model.find
	Mongoose save Method
		https://mongoosejs.com/docs/api.html#document_Document-save
	Express JS Static and Dynamic Routes
		https://dev.to/reiallenramos/create-an-express-api-static-and-dynamic-routes-33lb
	Mongoose findByIdAndRemove
		https://mongoosejs.com/docs/api.html#model_Model.findOneAndRemove
	Mongoose findById
		https://mongoosejs.com/docs/api.html#model_Model.findById
	Creating Git Projects:
		GitLab
			https://gitlab.com/projects/new#blank_project
		GitHub
			https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
==========
Discussion
==========
*/

// 1. Create a "package.json" file
	// Batch Folder > S31 > Terminal

		npm init -y

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for npm init.
		*/

// 2. Install Express JS and Mongoose.
	// Application > Terminal

		// Syntax
			// npm install packageA packageB
		npm install express mongoose

		/*
		Important Note:
			- After triggering the command, express and mongoose will now be listed under the "dependencies" property of the package.json file.
			- This will create a "node_modules" folder that will contain all the necessary files downloaded for the package.
			- The "package.json" file is responsible for listing all packages and the versions used in creating the application.
			- The version of the package installed is also indicated in the file which prevents applications from breaking if ever new updates are introduced to packages.
			- If the students incorrectly install a different package, the command "npm uninstall <package_name>" may be used.
			- A specific version of a package may also be installed by using the command "npm install@<version number>" (e.g. npm install@4.17.1).
			- This is useful for instances where an older version of the package is preferred to be used or when a current version of a package has bugs that tend to break an application.
			- Refer to "references" section of this file to find the link to the Express JS website, documentations for What Is Mongoose and Mongoose Documentation.
		*/

// 3. Setup a basic Express JS server.
	// Application > index.js

		// Setup the dependencies
		const express = require("express");
		const mongoose = require("mongoose");

		// Server setup
		const app = express();
		const port = 4000;
		app.use(express.json());
		app.use(express.urlencoded({extended:true}));

		// Database connection
		// Connecting to MongoDB Atlas
		mongoose.connect("mongodb+srv://admin:admin123@cluster0.7iowx.mongodb.net/bXX-to-do?retryWrites=true&w=majority", 
			{ 
				useNewUrlParser : true,  
				useUnifiedTopology : true
			}
		);

		// Connecting to MongoDB locally
		// 27017 is the default port on where mongo instances are run in a device
		// mongoose.connect("mongodb://localhost:27017/bXX-to-do", 
		// 	{ 
		// 		useNewUrlParser : true,  
		// 		useUnifiedTopology : true
		// 	}
		// );

		// Server listening
		// Server listening
		if(require.main === module){
		    app.listen(port, () => console.log(Server running at port ${port}));
		}

		module.exports = app;

// 4. Create a "taskRoute.js" file that will contain all the routes for our application and a route for getting all the tasks.
	// Application > routes > taskRoute.js

		// Contains all the endpoints for our application
		// We separate the routes such that "index.js" only contains information on the server
		// We need to use express' Router() function to achieve this
		const express = require("express");
		// Creates a Router instance that functions as a middleware and routing system
		// Allows access to HTTP method middlewares that makes it easier to create routes for our application
		const router = express.Router();

		// [Section] Routes
		// The routes are responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used when a route is accessed
		// They invoke the controller functions from the controller files
		// All the business logic is done in the controller

		// Route to get all the tasks
		// This route expects to receive a GET request at the URL "/tasks"
		// The whole URL is at "http://localhost:4000/tasks" this is because of the "/tasks" defined in the "index.js" that's applied to all routes in this file
		router.get("/", (req, res) => {

			// Invokes the "getAllTasks" function from the "taskController.js" file and sends the result back to the client/Postman
			taskController.getAllTasks();

		})

		// Use "module.exports" to export the router object to use in the "index.js"
		module.exports = router;

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for MVC Framework and Express JS Routing.
		*/

// 5. Create a "task.js" file and create a schema and a model.
	// Application > models > task.js

		// Create the Schema, model and export the file
		const mongoose = require("mongoose");

		const taskSchema = new mongoose.Schema({

			name : String,
			status : {
				type : String,
				default : "pending"
			}

		});

		module.exports = mongoose.model("Task", taskSchema);
		// "module.exports" is a way for Node JS to treat this value as a "package" that can be used by other files

// 6. Create a "taskController.js" file that will contain all the business logic of our application and create a function for getting all the tasks.
	// Application > controllers > taskController.js

		// Controllers contain the functions and business logic of our Express JS application
		// Meaning all the operations it can do will be placed in this file

		// Uses the "require" directive to allow access to the "Task" model which allows us to access Mongoose methods to perform CRUD functions
		// Allows us to use the contents of the "task.js" file in the "models" folder
		const Task = require("../models/task");

		// Controller function for getting all the tasks
		// Defines the functions to be used in the "taskRoute.js" file and exports these functions
		module.exports.getAllTasks = () => {

			// The "return" statement, returns the result of the Mongoose method "find" back to the "taskRoute.js" file which invokes this function when the "/tasks" routes is accessed
			// The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client/Postman
			return Task.find({}).then(result => {

				// The "return" statement returns the result of the MongoDB query to the "result" parameter defined in the "then" method
				return result;

			})

		}

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for Mongoose find Method.
		*/

// 7. Import the "taskController.js" file.
	// Application > routes > taskRoute.js

		/*...*/
		const router = express.Router();
		// The "taskController" allows us to use the functions defined in the "taskController.js" file
		const taskController = require("../controllers/taskController");
		
		router.get("/", (req, res) => {
			/*...*/
		})

		/*...*/

// 8. Return the result back to the client/Postman.
	// Application > routes > taskRoute.js

		/*...*/

		router.get("/", (req, res) => {

			// "resultFromController" is only used here to make the code easier to understand but it's common practice to use the shorthand parameter name for a result using the parameter name "result"/"res"
			taskController.getAllTasks().then(resultFromController => res.send(resultFromController));

		})

		/*...*/

// 9. Import the "taskRoute.js" file.
	// Application > index.js

		/*...*/
		const mongoose = require("mongoose");
		// This allows us to use all the routes defined in "taskRoute.js"
		const taskRoute = require("./routes/taskRoute");

		const app = express();
		/*...*/

// 10. Create a route for base URL for the tasks.
	// Application > index.js

		/*...*/

		// mongoose.connect("mongodb://localhost:27017/bXX-to-do", 
			/*...*/
		// );

		// Add the task route
		// Allows all the task routes created in the "taskRoute.js" file to use "/tasks" route
		app.use("/tasks", taskRoute);

		// Server listening
		if(require.main === module){
		    app.listen(port, () => console.log(Server running at port ${port}));
		}

		module.exports = app;

// 11. Process a GET request at the "/tasks" route using postman to get all the tasks.
	// Postman

		url: http://localhost:4000/tasks
		method: GET

// 12. Create a route for creating a task.
	// Application > routes > taskRoutes.js

		/*...*/

		router.get("/", (req, res) => {
			/*...*/
		})

		// Route to create a new task
		// This route expects to receive a POST request at the URL "/tasks"
		// The whole URL is at "http://localhost:4000/tasks"
		router.post("/", (req, res) => {

			// The "createTask" function needs the data from the request body, so we need to supply it to the function
			// If information will be coming from the client side commonly from forms, the data can be accessed from the request "body" property
			taskController.createTask(req.body);
			
		})

		module.exports = router;

// 13. Create a controller function for creating a task.
	// Application > controllers > taskController.js

		/*...*/

		module.exports.getAllTasks = () => {
			/*...*/
		}

		// Controller function for creating a task
		// The request body coming from the client was passed from the "taskRoute.js" file via the "req.body" as an argument and is renamed as a "requestBody" parameter in the controller file
		module.exports.createTask = (requestBody) => {

			// Creates a task object based on the Mongoose model "Task"
			let newTask = new Task({
				
				// Sets the "name" property with the value received from the client/Postman
				name : requestBody.name

			})

			// Saves the newly created "newTask" object in the MongoDB database
			// The "then" method waits until the task is stored in the database or an error is encountered before returning a "true" or "false" value back to the client/Postman
			// The "then" method will accept the following 2 arguments:
				// The first parameter will store the result returned by the Mongoose "save" method
				// The second parameter will store the "error" object
			// Compared to using a callback function on Mongoose methods discussed in the previous session, the first parameter stores the result and the error is stored in the second parameter which is the other way around
				// Ex.
					// newUser.save((saveErr, savedTask) => {})
			return newTask.save().then((task, error) => {

				// If an error is encountered returns a "false" boolean back to the client/Postman
				if (error) {

					console.log(error);
					// If an error is encountered, the "return" statement will prevent any other line or code below it and within the same code block from executing
					// Since the following return statement is nested within the "then" method chained to the "save" method, they do not prevent each other from executing code
					// The else statement will no longer be evaluated
					return false;

				// Save successful, returns the new task object back to the client/Postman
				} else {

					return task; 

				}

			})

		}

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for Mongoose save Method.
		*/

// 14. Return the result back to the client/Postman.
	// Application > routes > taskRoute.js

		/*...*/

		router.post("/", (req, res) => {

			taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
			
		})

		/*...*/

// 15. Process a POST request at the "/tasks" route using postman to create a task.
	// Postman

		url: http://localhost:4000/tasks
		method: POST
		body: raw + JSON
			{
			    "name": "Eat"
			}

// 16. Create a route for deleting a task.
	// Application > routes > taskRoute.js

		/*...*/

		router.post("/", (req, res) => {
			/*...*/			
		})

		// Route to delete a task
		// This route expects to receive a DELETE request at the URL "/tasks/:id"
		// The whole URL is at "http://localhost:4000/tasks/:id"
		// The task ID is obtained from the URL is denoted by the ":id" identifier in the route
		// The colon (:) is an identifier that helps create a dynamic route which allows us to supply information in the URL
		// The word that comes after the colon (:) symbol will be the name of the URL parameter
		// ":id" is a wildcard where you can put any value, it then creates a link between "id" parameter in the URL and the value provided in the URL
			// Ex. 
				// If the route is localhost:3000/tasks/1234
				// "1234" is assigned to the "id" parameter in the URL
		router.delete("/:id", (req, res) => {

			// URL parameter values are accessed via the request object's "params" property
			// The property name of this object will match the given URL parameter name
			// In this case "id" is the name of the parameter
			// If information will be coming from the URL, the data can be accessed from the request "params" property
			taskController.deleteTask(req.params.id);

		})

		module.exports = router;

// 17. Create a controller function for deleting a task
	// Application > controllers > taskController.js

		/*...*/

		module.exports.createTask = (requestBody) => {
			/*...*/
		}

		// Controller function for deleting a task
		// "taskId" is the URL parameter passed from the "taskRoute.js" file

		// Business Logic
		/*
			1. Look for the task with the corresponding id provided in the URL/route
			2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
		*/

		// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
		module.exports.deleteTask = (taskId) => {
			
			// The "findByIdAndRemove" Mongoose method will look for a task with the same id provided from the URL and remove/delete the document from MongoDB
			// The Mongoose method "findByIdAndRemove" method looks for the document using the "_id" field
			return Task.findByIdAndRemove(taskId).then((removedTask, err) => {

				// If an error is encountered returns a "false" boolean back to the client/Postman
				if(err){

					console.log(err);
					return false;

				// Delete successful, returns the removed task object back to the client/Postman
				} else {

					return removedTask;

				}

			})

		}

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for Express JS Static and Dynamic Routes and Mongoose findByIdAndRemove method.
		*/

// 18. Return the result back to the client/Postman.
	// Application > routes > taskRoutes.js

		/*...*/

		router.delete("/:id", (req, res) => {

			taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));

		})

		/*...*/

// 19. Process a DELETE request at the "/tasks/:id" route using postman to delete a task.
	// Postman

		url: http://localhost:4000/tasks/:id
		method: DELETE

// 20. Create a route for updating a task.
	// Application > routes > taskRoutes.js

		/*...*/

		router.delete("/:id", (req, res) => {
			/*...*/
		})

		// Route to update a task
		// This route expects to receive a PUT request at the URL "/tasks/:id"
		// The whole URL is at "http://localhost:4000/tasks/:id"
		router.put("/:id", (req, res) => {

			// The "updateTask" function will accept the following 2 arguments:
				// "req.params.id" retrieves the task ID from the parameter
				// "req.body" retrieves the data of the updates that will be applied to a task from the request's "body" property
			taskController.updateTask(req.params.id, req.body);

		})

		module.exports = router;

// 21. Create a controller function for updating a task.
	// Application > controllers > taskController.js

		/*...*/

		module.exports.deleteTask = (taskId) => {
			/*...*/
		}

		// Controller function for updating a task

		// Business Logic
		/*
			1. Get the task with the id using the Mongoose method "findById"
			2. Replace the task's name returned from the database with the "name" property from the request body
			3. Save the task
		*/

		// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
		// The updates to be applied to the document retrieved from the "req.body" property coming from the client is renamed as "newContent"
		module.exports.updateTask = (taskId, newContent) => {
			
			// The "findById" Mongoose method will look for a task with the same id provided from the URL
			// "findById" is the same as "find({"_id" : value})"
			// The "return" statement, returns the result of the Mongoose method "findById" back to the "taskRoute.js" file which invokes this function 
			return Task.findById(taskId).then((result, error) => {

				// If an error is encountered returns a "false" boolean back to the client/Postman
				if(error){

					console.log(err);
					return false;

				}

				// Results of the "findById" method will be stored in the "result" parameter
				// It's "name" property will be reassigned the value of the "name" received from the request
				result.name = newContent.name;

				// Saves the updated object in the MongoDB database
				// The document already exists in the database and was stored in the "result" parameter
				// Because of Mongoose we have access to the "save" method to update the existing document with the changes we applied
				// The "return" statement returns the result of the "save" method to the "then" method chained to the "findById" method which invokes this function
				return result.save().then((updatedTask, saveErr) => {

					// If an error is encountered returns a "false" boolean back to the client/Postman
					if (saveErr){

						// The "return" statement returns a "false" boolean to the "then" method chained to the "save" method which invokes this function
						console.log(saveErr);
						return false;

					// Update successful, returns the updated task object back to the client/Postman
					} else {

						/// The "return" statement returns a "false" boolean to the "then" method chained to the "save" method which invokes this function
						return updatedTask;

					}
				})
			})
		}

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for Mongoose findById method.
		*/

// 22. Return the result back to the client/Postman.
	// Application > routes > taskRoutes.js

		/*...*/

		router.put("/:id", (req, res) => {

			taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));

		})

		/*...*/

// 23. Process a PUT request at the "/tasks/:id" route using postman to update a task.
	// Postman

		url: http://localhost:4000/tasks/:id
		method: PUT
		body: raw + JSON
			{
			    "name": "Sleep"
			}

/*
========
Activity
========
*/

/*
## Instructions that can be provided to the students for reference:

Activity References

ExpressJS
https://expressjs.com/en/guide/routing.html

Mongoose findById()
https://mongoosejs.com/docs/api/model.html#Model.findById()

Activity

Member 1:
1. Update your local sessions folder with the git commit message "Added discussion code s42"


Member 1, 2:
2. Create a route for getting a specific task.
3. Create a controller function for retrieving a specific task.
- Return the result back to the client/Postman.

Member 3,4:
4. Create a route for changing the status of a task to "complete".
5. Create a controller function for changing the status of a task to "complete".
- Return the result back to the client/Postman.

Member 5:
6. Process a POST request to create a new task.
7. Process a GET request at the "/tasks/:id" route using postman to get the new task.
8. Process a PUT request at the "/tasks/:id/complete" route using postman to update the new task.
- Save the requests as a collection.

All Members:
9. Check out to your own git branch with git checkout -b <branchName>
10. Update your local sessions git repository and push to git with the commit message of Add activity code s31.
11. Add the sessions repo link in Boodle for s42.




*/

// 1. Create a route for getting a specific task.
	// Application > routes > taskRoute.js

		/*...*/

		router.put("/:id", (req, res) => {
			/*...*/
		})

		// Route to retrieve a single task
		// This route expects to receive a GET request at the URL "/tasks/:id"
		// The whole URL is at "http://localhost:4000/tasks/:id"
		router.get("/:id", (req, res) => { 
			taskController.getTask(req.params.id);	
		})

		module.exports = router;

// 2. Create a controller function for retrieving a specific task.
	// Application > controllers > taskController.js

		/*...*/

		module.exports.updateTask = (taskId, newContent) => {
			/*...*/
		}

		// Controller function for getting a specific task

		// Business Logic
		/*
			1. Get the task with the id using the Mongoose method "findById"
		*/

		// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
		module.exports.getTask = (taskId) => {

			// The "findById" Mongoose method will look for a task with the same id provided from the URL
			return Task.findById(taskId).then((result, error) => {

				// If an error is encountered returns a "false" boolean back to the client/Postman
				if(error){

					console.log(error);
					return false;

				// Find successful, returns the task object back to the client/Postman
				} else {

					return result;	
				}
			})

		}

// 3. Return the result back to the client/Postman.
	// Application > routes > taskRoute.js

		/*...*/

		router.get("/:id", (req, res) => { 
			taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));	
		})

		/*...*/

// 4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
	// Postman

		url: http://localhost:4000/tasks/:id
		method: GET

// 5. Create a route for changing the status of a task to "complete".
	// Application > routes > taskRoute.js

		/*...*/

		router.get("/:id", (req, res) => { 
			/*...*/	
		})

		// Change the status of a task to "complete"
		// This route expects to receive a put request at the URL "/tasks/:id/complete"
		// The whole URL is at "http://localhost:4000/tasks/:id/complete"
		// We cannot use put("/tasks/:id") again because it has already been used in our route to update a task
		router.put("/:id/complete", (req, res) => {
			taskController.completeTask(req.params.id);
		}) 

		module.exports = router;

// 6. Create a controller function for changing the status of a task to "complete".
	// Application > controllers > taskController.js

		/*...*/

		module.exports.getTask = (taskId) => {
			/*...*/
		}

		// Controller function for updating a task status to "complete"

		// Business Logic
		/*
			1. Get the task with the id using the Mongoose method "findById"
			2. Change the status of the document to complete
			3. Save the task
		*/

		// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
		module.exports.completeTask = (taskId) => {

			// The "findById" Mongoose method will look for a task with the same id provided from the URL
			// The "return" statement, returns the result of the Mongoose method "findById" back to the "taskRoute.js" file which invokes this function 
			return Task.findById(taskId).then((result, err) => {

				// If an error is encountered returns a "false" boolean back to the client/Postman
				if(err){

					console.log(err);
					return err;

				}

				// Change the status of the returned document to "complete"
				result.status = "complete";

				// Saves the updated object in the MongoDB database
				// The document already exists in the database and was stored in the "result" parameter, we can use the "save" method to update the existing document with the changes we applied
				// The "return" statement returns the result of the "save" method to the "then" method chained to the "findById" method  which invokes this function
				return result.save().then((updatedTask, saveErr) => {

					// If an error is encountered returns a "false" boolean back to the client/Postman
					if (saveErr) {

						console.log(saveErr);
						// The "return" statement returns a "false" boolean to the "then" method chained to the "save" method which invokes this function
						return saveErr;

					// Update successful, returns the updated task object back to the client/Postman
					} else {

						// The "return" statement returns the result to the  "then" method chained to the "save" method which invokes this function
						return updatedTask;

					}
				})
			})

		}

// 7. Return the result back to the client/Postman.
	// Application > routes > taskRoute.js

		/*...*/

		router.put("/:id/complete", (req, res) => {
			taskController.completeTask(req.params.id).then(resultFromController => res.send(resultFromController));
		}) 

		/*...*/

// 8. Process a PUT request at the "/tasks/:id" route using postman to update a task.
	// Postman

		url: http://localhost:4000/tasks/:id/complete
		method: PUT