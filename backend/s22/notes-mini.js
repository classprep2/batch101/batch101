/*print numbers - practice arguments and parameters*/
function printNumbers(num1,num2){
	console.log("The numbers passed as arguments are:")
	console.log(num1);
	console.log(num2);
}

printNumbers(5,2);

/*print names - practice multiple arguments and parameters*/
function printFriends(friend1,friend2,friend3){
	console.log("My three friends are: " + friend1 + ", " + friend2 + ", and " + friend3);
}

printFriends("Martin","Kevin","Jerome");

/*Check divisibility by 4*/
function checkDivisibilityBy4(num){
	let remainder = num % 4;
	console.log("The remainder of " + num + " divided by 4 is: " + remainder);
	let isDivisibleBy4 = remainder === 0;
	console.log("Is " + num + " divisible by 4?");
	console.log(isDivisibleBy8);
}

/*debugging practice*/
/*correct the following function:*/

function isEven(num){
	console.log(number % 2 = 0) 
}

function isOdd(num1){
	num2/2 !== 0;
}

let numEven = isEven(20);
let numOdd = isOdd(31);

console.log(numEven);
console.log(numOdd);