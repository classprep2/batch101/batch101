# S18 - JavaScript - Function Parameters and Return Values

# Session Objectives

At the end of this session, bootcampers should be able to:

- learn how to pass data in functions using arguments.
- learn how to receive data passed in functions using parameters.
- learn how to return data from a function.
- learn the difference between a console.log() and return.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5b/-/tree/master/backend/s18)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1KAufvRBaw-HXy7d4wtfH2MQhxq5J7mzD5bSWXPPPqPM/edit#slide=id.g53aad6d9a4_0_728)


## Supplemental Materials

- [Parameters and Arguments in Javascript](https://codeburst.io/parameters-arguments-in-javascript-eb1d8bd0ef04)
- [Return statements](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/return)
- [console.log()](https://developer.mozilla.org/en-US/docs/Web/API/console/log)
- [Function Naming Conventions](https://medium.com/swlh/how-to-better-name-your-functions-and-variables-e962a4ef335b)


# Lesson Proper

# Code Discussion

## Folder and File Preparation

Create a folder named **s18**, a folder named **discussion** inside the **s18** folder.Create a file named **index.html** and **index.js** inside the **discussion** folder.

## Basic HTML Code

Inside the **index.html** file, add the following:

```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>Javascript - Function Parameters and Return Statements</title>
    </head>
    <body>
        <script src="./index.js"></script>
    </body>
</html>
```

Inside the **index.js** file, log "Hello, World!" in the console to check if the index.html file is properly connected to the index.js file.

```js
   console.log("Hello World!"); 
```

## Functions

What are functions?
- Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked.
- Functions are mostly created to create complicated tasks to run several lines of code in succession.
- They are also used to prevent repeating lines/blocks of codes that perform the same task/function.

We also learned in the previous session that we can gather data from user input using a prompt() window.
```js
	function printInput(){

		let nickname = prompt("Enter your nickname:");
		console.log("Hi, " + nickname);

	}

	printInput();
````
However, for some use cases, this may not be ideal. 
For other cases, functions can also process data directly passed into it instead of relying only on Global Variables and prompt().

Consider this function:
```js
	function printName(name){

		console.log("My name is " + name);

	};

	printName("Juana");
```
You can directly pass data into the function. The function can then call/use that data which is referred as "name" within the function.

"name" is called a **parameter**.
- A "**parameter**" acts as a named variable/container that exists only inside of a function
- It is used to store information that is provided to a function when it is called/invoked.

```js
	function printName(name){
````
"Juana", the information/data provided directly into the function, is called an **argument**.
Values **passed** when invoking a function are called **arguments**. These **arguments** are then stored as the **parameters** within the function.

```js
	printName("Juana");
```
In the following examples, "John" and "Jane" are both arguments since both of them are supplied as information that will be used to print out the full message.

When the "printName()" function is first called, it stores the value of "John" in the parameter "name" then uses it to print a message.
```js
	printName("John");
```
When the "printName()" function is called again, it stores the value of "Jane" in the parameter "name" then uses it to print a message.
```js
	printName("Jane");
```
variables can also be passed as an argument.
```js
	let sampleVariable = "Yui";
	printName(sampleVariable); 
```

Function arguments cannot be used by a function if there are no parameters provided within the function.

Now, you can create a function which can be re-used to check for a number's divisibility instead of having to manually do it every time like our previous activity!

```js
	function checkDivisibilityBy8(num){
		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is: " + remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8?");
		console.log(isDivisibleBy8);
	}

	checkDivisibilityBy8(64);
	checkDivisibilityBy8(28);
```
You can also do the same using prompt(), however, take note that prompt() outputs a string. Strings are not ideal for mathematical computations.

Functions as Arguments

- Function parameters can also accept other functions as arguments
- Some complex functions use other functions as arguments to perform more complicated results.
- This will be further seen when we discuss array methods.

```js
	function argumentFunction(){
	    console.log("This function was passed as an argument before the message was printed.")
	}

	function invokeFunction(argumentFunction){
	    argumentFunction();
	}
````
Adding and removing the parentheses "()" impacts the output of JavaScript heavily
When a function is used with parentheses "()", it denotes invoking/calling a function
A function used without a parenthesis is normally associated with using the function as an argument to another function
```js
	invokeFunction(argumentFunction);
```
finding more information about a function in the console using console.log()
```js
	console.log(argumentFunction);
```

Using Multiple Parameters

Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.

```js
	function createFullName(firstName, middleName, lastName) {

	    console.log(firstName + ' ' + middleName + ' ' + lastName);

	}

	createFullName('Juan', 'Dela', 'Cruz');
```

- "Juan" will be stored in the parameter "firstName".
- "Dela" will be stored in the parameter "middleName".
- "Cruz" will be stored in the parameter "lastName".

In JavaScript, providing more/less arguments than the expected parameters will not return an error.

Providing less arguments than the expected parameters will automatically assign an undefined value to the parameter.

In other programming languages, this will return an error stating that "the expected number of arguments do not match the number of parameters".

```js
	createFullName('Juan', 'Dela');
	createFullName('Jane', 'Dela', 'Cruz', 'Hello');
	``

	// Using variables as arguments
	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName, middleName, lastName);
```

Parameter names are just names to refer to the argument. Even if we change the name of the parameters, the arguments will be received in the same order it was passed.

The order of the argument is the same to the order of the parameters. The first argument will be stored in the first parameter, second argument will be stored in the second parameter and so on.

Consider this function:
```js
	function printFullName(middleName,firstName,lastName){

		console.log(firstName + ' ' + middleName + ' ' + lastName);

	}

	printFullName('Juan', 'Dela', 'Cruz');
````
results to "Dela Juan Cruz" because "Juan" was received as middleName, "Dela" was received as firstName.


### Using alert() and prompt()

Using alert()

alert() allows us to show a small window at the top of our browser page to show information to our users. As opposed to a console.log() which only shows the message on the console. It allows us to show a short dialog or instruction to our user. The page will wait until the user dismisses the dialog.

```js
	alert("Hello World!");//This will run immediately when the page loads.
```

alert() syntax:
```js
	//alert("<messageInString>");
````

You can use an alert() to show a message to the user from a later function invocation.
```js
	function showSampleAlert(){
		alert("Hello, User!");
	}

	showSampleAlert();
```

You will find that the page waits for the user to dismiss the dialog before proceeding. You can witness this by reloading the page while the console is open.

```js
	console.log("I will only log in the console when the alert is dismissed.");
```

Notes on using alert():

- Show only an alert() for short dialogs/messages to the user. 
- Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.

Using prompt()

prompt() allows us to show a small window at the of the browser to gather user input. It, much like alert(), will have the page wait until the user completes or enters their input. The input from the prompt() will be returned as a String once the user dismisses the window.

```js
	let samplePrompt = prompt("Enter your Name.");

	console.log("Hello, " + samplePrompt);
```
The value of the user input from a prompt is returned as a string.
```js
	console.log(typeof samplePrompt);
```
prompt() syntax:
```js
	/*
		Syntax:

		prompt("<dialogInString>");

	*/
```

prompt() returns an empty string when there is no input or null if the user cancels the prompt().
```js
	let sampleNullPrompt = prompt("Don't enter anything.");

	console.log(sampleNullPrompt);
```

prompt() can be used to gather user input and be used in our code. However, since prompt() windows will have the page wait until the user dismisses the window it must not be overused.

prompt() used globally will be run immediately, so, for better user experience, it is much better to use them accordingly or add them in a function.

```js
	// Let's create function scoped variables to store the returned data from our prompt(). This way, we can dictate when to use a prompt() window or have a reusable function to use our prompts.
		function printWelcomeMessage(){
			let firstName = prompt("Enter Your First Name");
			let lastName = prompt("Enter Your Last Name");

			console.log("Hello, " + firstName + " " + lastName + "!");
			console.log("Welcome to my page!");
		}

		printWelcomeMessage();
```

# Activity

Note: Copy the code from activity-template.js into the batch Boodle Notes so students can copy the template of the code for this activity.

## Instructions that can be provided to the students for reference:

1. In the S18 folder, create an activity folder, an index.html file inside of it and link the index.js file.

2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.

3. Create a function called addNum which will be able to add two numbers.
	- 2 Numbers must be provided as arguments.
	- function should return the sum of the 2 numbers.
4. Create a function called subNum which will be able to subtract two numbers.
	- 2 Numbers must be provided as arguments.
	- function should return result of subtraction.
5. Create a new variable called sum.
	- This sum variable should be able to receive and store the result of addNum function.
6. Create a new variable called difference.
	- This difference variable should be able to receive and store the result of subNum function.
	- Log the value of sum variable in the console.
	- Log the value of difference variable in the console.
7. Create a function which will be able to multiply two numbers.
	- Numbers must be provided as arguments.
	Return the result of the multiplication.
8. Create a function which will be able to divide two numbers.
	- Numbers must be provided as arguments.
	- Return the result of the division.
9. Create a new variable called product.
	- This product variable should be able to receive and store the result of multiplication function.
10. Create a new variable called quotient.
	- This quotient variable should be able to receive and store the result of division function.
	- Log the value of product variable in the console.
	- Log the value of quotient variable in the console.

11. Create a function which will be able to get total area of a circle from a provided radius.
	- a number should be provided as an argument.
	- look up the formula for calculating the area of a circle with a provided/given radius.
	- look up the use of the exponent operator.
	- return the result of the area calculation.
12. Create a new variable called circleArea.
	- This variable should be able to receive and store the result of the circle area calculation.
	- Log the value of the circleArea variable in the console.

13. Create a function which will be able to get total average of four numbers.
	- 4 numbers should be provided as an argument.
	- look up the formula for calculating the average of numbers.
	- return the result of the average calculation.
14. Create a new variable called averageVar.
	- This variable should be able to receive and store the result of the average calculation
	- Log the value of the averageVar variable in the console.
15. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
	- this function should take 2 numbers as an argument, your score and the total score.
	- First, get the percentage of your score against the total. You can look up the - formula to get percentage.
	- Using a relational operator, check if your score percentage is bthan 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
	- return the value of the variable isPassed.
	- This function should return a boolean.
16. Create a new variable called isPassingScore.
	- This variable should be able to receive and store the boolean result of the function.
	- Log the value of the isPassingScore variable in the console.
17. Update your local backend git repository and push to git with the commit message of Add activity code s18.
18. Add the link in Boodle for s18.

Sample Output:
![readme-images/solution.png](readme-images/solution.png)


Solution:

```js
	function addNum(num1,num2){
		return num1 + num2;
	};

	function subNum(num1,num2){
		return num1 - num2;
	};
	console.log("Displayed sum of 5 and 15");
	addNum(5,15);
	console.log("Displayed difference of 20 and 5");
	subNum(20,5);

	function multiplyNum(num1,num2){
		return num1*num2;
	};

	function divideNum(num1,num2){
		return num1/num2;
	};


	let product = multiplyNum(50,10);
	let quotient = divideNum(50,10);

	console.log("The product of 50 and 10:")
	console.log(product);
	console.log("The quotient of 50 and 10:")
	console.log(quotient);

	function getCircleArea(radius){
	/*	let area = 3.1416*(radius**2);
		return area;*/

		return 3.1416*(radius**2)
	};

	let areaCircle = getCircleArea(15);

	console.log("The result of getting the area of a circle with 15 radius:")
	console.log(areaCircle);

	function getAverage(num1,num2,num3,num4){

	/*	let average = (num1+num2+num3+num4)/4
		return average*/

		return (num1+num2+num3+num4)/4
	}

	let averageVar = getAverage(20,40,60,80);
	console.log("The average of 20,40,60 and 80: ");
	console.log(averageVar);



	function checkIfPassed(score,total){

	/*	let isPassed = (score/total)*100 > 75;
		return isPassed;*/

		return (score/total)*100 > 75;

	}

	let isPassingScore = checkIfPassed(38,50);
	console.log("Is 38/50 a passing score?")
	console.log(isPassingScore);
```