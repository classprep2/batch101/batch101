/*
====================================================
S35 - MongoDB - Query Operators and Field Projection
====================================================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1QWixmhFZkbEm_NCwipxPIsJpz1RUkAx5bXK8HLRJBno/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s24

Other References:
	Importing and Exporting Databases from RoboMongo
		https://medium.com/@salonimalhotra1ind/how-to-import-export-database-from-robomongo-ed9cec35efe
	mongoexport CLI command
		https://docs.mongodb.com/database-tools/mongoexport/
	mongoimport CLI command
		https://docs.mongodb.com/database-tools/mongoimport/
	MongoDB Greater Than Operator
		https://docs.mongodb.com/manual/reference/operator/query/gt/
	MongoDB Greater Than Or Equal To Operator
		https://docs.mongodb.com/manual/reference/operator/query/gte/
	MongoDB Less Than Operator
		https://docs.mongodb.com/manual/reference/operator/query/lt/
	MongoDB Less Than Or Equal To Operator
		https://docs.mongodb.com/manual/reference/operator/query/lte/
	MongoDB Not Equal Operator
		https://docs.mongodb.com/manual/reference/operator/query/ne/
	MongoDB In Operator
		https://docs.mongodb.com/manual/reference/operator/query/in/
	MongoDB Or Operator
		https://docs.mongodb.com/manual/reference/operator/query/or/
	MongoDB And Operator
		https://docs.mongodb.com/manual/reference/operator/query/and/
	MongoDB Slice Operator
		https://docs.mongodb.com/manual/reference/operator/projection/slice/
	MongoDB Regex Operator
		https://docs.mongodb.com/manual/reference/operator/query/regex/
	MongoDB Field Projection
		https://docs.mongodb.com/manual/tutorial/project-fields-from-query-results/
	Creating Git Projects:
		GitLab
			https://gitlab.com/projects/new#blank_project
		GitHub
			https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application
*/

/*
==========
Discussion
==========
*/

/*
1. Create the MongoDB syntax in a text editor for code readability and demonstrate them using the Mongo Shell terminal or Robo3t.
	Device > Terminal
*/

		//[Section] Comparison Query Operators

		// $gt/$gte operator
		/*
			- Allows us to find documents that have field number values greater than or equal to a specified value.
			- Syntax
				db.collectionName.find({ field : { $gt : value } });
				db.collectionName.find({ field : { $gte : value } });
		*/
		db.users.find({ age : { $gt : 50 } });
		db.users.find({ age : { $gte : 50 } });

		// $lt/$lte operator
		/*
			- Allows us to find documents that have field number values less than or equal to a specified value.
			- Syntax
				db.collectionName.find({ field : { $lt : value } });
				db.collectionName.find({ field : { $lte : value } });
		*/
		db.users.find({ age : { $lt : 50 } });
		db.users.find({ age : { $lte : 50 } });

		// $ne operator
		/*
			- Allows us to find documents that have field number values not equal to a specified value.
			- Syntax
				db.collectionName.find({ field : { $ne : value } });
		*/
		db.users.find({ age : { $ne: 82 } });

		// $in operator
		/*
			- Allows us to find documents with specific match critieria one field using different values.
			- Syntax
				db.collectionName.find({ field : { $in : value } });
		*/
		db.users.find( { lastName: { $in: [ "Hawking", "Doe" ] } } );
		db.users.find( { courses: { $in: [ "HTML", "React" ] } } );

		// [Section] Logical Query Operators

		// $or operator
		/*
			- Allows us to find documents that match a single criteria from multiple provided search criteria.
			- Syntax
				db.collectionName.find({ $or: [ { fieldA: valueB }, { fieldB: valueB } ] });
		*/
		multiple field value pairs
		db.users.find( { $or: [ { firstName: "Neil" }, { age: "25" } ] } );

		db.users.find( { $or: [ { firstName: "Neil" }, { age: { $gt: 30 } } ] } );

		// $and operator
		/*
			- Allows us to find documents matching multiple criteria in a single field.
			- Syntax
				db.collectionName.find({ $and: [ { fieldA: valueB }, { fieldB: valueB } ] });
		*/
		db.users.find({ $and: [ { age : { $ne: 82 } }, { age : { $ne: 76 } }] });

		// [Section] Field Projection
		/*
			- Retrieving documents are common operations that we do and by default MongoDB queries return the whole document as a response.
			- When dealing with complex data structures, there might be instances when fields are not useful for the query that we are trying to accomplish.
			- To help with readability of the values returned, we can include/exclude fields from the response.
		*/

		// Inclusion
		/*
			- Allows us to include/add specific fields only when retrieving documents.
			- The value provided is 1 to denote that the field is being included.
			- Syntax
				db.users.find({criteria},{field: 1})
		*/
		db.users.find( 
			{ 
				firstName: "Jane" 
			}, 
			{ 
				firstName: 1, 
				lastName: 1,
				contact: 1 
			}
		);

		// Exclusion
		/*
			- Allows us to exclude/remove specific fields only when retrieving documents.
			- The value provided is 0 to denote that the field is being included.
			- Syntax
				db.users.find({criteria},{field: 1})
		*/
		db.users.find( 
			{ 
				firstName: "Jane" 
			}, 
			{ 
				contact: 0, 
				department: 0 
			} 
		);

		// Suppressing the ID field
		/*
			- Allows us to exclude the "_id" field when retrieving documents.
			- When using field projection, field inclusion and exclusion may not be used at the same time.
			- Excluding the "_id" field is the only exception to this rule.
			- Syntax
				db.users.find({criteria},{_id: 0})
		*/
		db.users.find( 
			{ 
				firstName: "Jane" 
			}, 
			{ 
				firstName: 1, 
				lastName: 1,
				contact: 1,
				_id: 0
			}
		);



		// [Section] Evaluation Query Operators

		// $regex operator
		/*
			- Allows us to find documents that match a specific string pattern using regular expressions.
			- Syntax
				db.users.find({ field: $regex: 'pattern', $options: '$optionValue' });
		*/

		// Case sensitive query
		db.users.find({ firstName: { $regex: 'N' } }).pretty();

		// Case insensitive query
		db.users.find({ firstName: { $regex: 'j', $options: '$i' } }).pretty();

		/*
		Important Note:
			- The database used in the discussion is the same as the database accomplished during the last session.
			- If the student does not have the database available locally, you may export the database locally from your machine and provide the student with the file to import it to their device.
			- Alternatively the MongoDB Atlas database may also be exported to provide to the student.
			- Refer to "references" section of this file to find the documentations for Importing and Exporting Databases from RoboMongo, mongoexport CLI command, mongoimport CLI command, MongoDB Greater Than Operator, MongoDB Greater Than Or Equal To Operator, MongoDB Less Than Operator, MongoDB Less Than Or Equal To Operator, MongoDB Not Equal Operator, MongoDB In Operator, MongoDB Or Operator, MongoDB And Operator, MongoDB Slice Operator, MongoDB Regex Operator and MongoDB Field Projection.
		*/



//************** To be removed in discussion, to add in Take Home Quiz
// Returning Specific Fields in Embedded Documents
		db.users.find( 
			{ 
				firstName: "Jane" 
			}, 
			{ 
				firstName: 1, 
				lastName: 1,
				"contact.phone": 1
			}
		);

		// Supressing Specific Fields in embedded documents
		db.users.find( 
			{ 
				firstName: "Jane" 
			}, 
			{ 
				"contact.phone": 0
			}
		);

		// Project Specific Array Elements in the Returned Array
		// The $slice operator allows us to retrieve only 1 element that matches the search criteria.
		db.users.find(
			{ "namearr": 
				{ 
					namea: "juan" 
				} 
			}, 
			{ namearr: 
				{ $slice: 1 } 
			}
		).pretty();

/*
========
Activity
========
*/

/*
1. Create an index.js file and copy the contents from template.js. Read and understand the additional instructions from the template.

2. Create a new database called employees (MongoDB Compass). Insert the mock data to be provided by your instructor.

3. In the findName(), copy and paste your query  to find users with letter s in their first name or d in their last name.
a. Use the $or operator.
b. Show only the firstName and lastName fields and hide the _id field.

*/

		db.users.find({ 
		        $or: [
		            { firstName: { $regex: 's', $options: 'i' } },
		            { lastName: { $regex: 'd', $options: 'i' } }
		        ]
		    }, { firstName: 1, lastName: 1, _id: 0 });

/*
4. In the findDeptAge(), copy and paste your query to find users who are from the HR department and their age is greater than or equal to 70.

a. Use the $and operator

*/
		db.users.find({ 
		            $and: [
		                { department: "HR" },
		                { age: { $gte: 70 } }
		            ]
		        });

/*
5. In the findNameAge(), copy and paste your query to find users with the letter e in their last name and has an age of less than or equal to 30.

a. Use the $and, $regex and $lte operators

*/

		db.users.find({ 
		            $and: [
		                { lastName: { $regex: 'e', $options: 'i' } },
		                { age: { $lte: 30 } }
		            ]
		        });


/*
6. Update your local backend git repository and push to git with the commit message of Add activity code s35.


7. Add the link in Boodle for s35.
*/