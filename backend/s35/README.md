# Session Objectives

At the end of the session, the students are expected to:

- limit the amount of fields returned in data queries by using field projection techniques.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5/s24)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1QWixmhFZkbEm_NCwipxPIsJpz1RUkAx5bXK8HLRJBno)
- [Lesson Plan](lesson-plan.md)

## Supplemental Materials

- [Project Fields to Return from Query (MongoDB Docs)](https://docs.mongodb.com/manual/tutorial/project-fields-from-query-results/)

# Code Discussion

## Query Operators

Query operators allow for more flexible querying of data within MongoDB.

For example, we may look for record based on whether a field is greater than or less than the specified value (as opposed to an exact match).

To achieve that kind of query, we need to use the $gt (greater than) and $lt (less than) query operators.

```jsx
db.courses.find({ price: { $gt: 1000 } });
```

To use the query operator, we must enclose it as an object after specifying which field to look for (price field).

![readme-images/Untitled.png](readme-images/Untitled.png)

We can also look for a partial match within a given field by using the $regex operator.

```jsx
db.courses.find({ name: { $regex: 'O' } });
```

There will be no result in the given query since the search is case sensitive.

To make the search case insensitive, let’s add one more operator named $options.

```jsx
db.courses.find({ name: { $regex: 'O', $options: '$i' } });
```

Now the search will be case insensitive and will show all names that has letter 'o' in it.

The codes earlier can also be combined into a single query like this.

```jsx
db.courses.find({ 
    price: { $gt: 1000 },
    name: { $regex: 'O', $options: '$i' }
});
```

We will encounter some more operators in the activity later.

## Field Projection

Sometimes, you do not want to retrieve all the properties of a document.

Examples of these properties might be government ID numbers or user passwords.

To exclude these properties, we use field projection.

## Including or Excluding Fields

We simply add a second argument in the find() function.

```jsx
db.users.find({}, { '_id': 0 });
```

The second argument contains an object and specifies which properties will be excluded in the retrieval of documents.

The first argument object is empty, indicating that we are to retrieve all the documents.

![readme-images/Untitled%201.png](readme-images/Untitled%201.png)

If we change the value from 0 to 1, it changes the properties returned and only the _id property will be retrieved.

![readme-images/Untitled%202.png](readme-images/Untitled%202.png)

Including (1) and excluding (0) fields can also be done at the same time.

# Activity

## Instructions

- Find users with letter 'a' in their first or last name.
    - Use the **$or** operator
    - Show only the firstName and lastName fields and hide the _id field.
- Find users who are admins and is active.
    - Use the **$and** operator
- Find courses with letter 'u' in its name and has a price of greater than or equal to 13000.
    - Use the **$regex** and **$gte** operators

## Expected Output

![readme-images/Untitled%203.png](readme-images/Untitled%203.png)

Find users with letter 'a' in their first or last name.

![readme-images/Untitled%204.png](readme-images/Untitled%204.png)

Find users who are admins and is active.

![readme-images/Untitled%205.png](readme-images/Untitled%205.png)

Find courses with letter 'u' in its name and has a price of greater than or equal to 13000.

## Solution

```jsx
// Find users with letter 'a' in their first or last name.
// Show only the firstName and lastName fields and hide the _id field.
// https://docs.mongodb.com/manual/tutorial/query-documents/#specify-or-conditions

db.users.find({ 
    $or: [
        { firstName: { $regex: 'a', $options: '$i' } },
        { lastName: { $regex: 'a', $options: '$i' } }
    ]
}, { firstName: 1, lastName: 1, _id: 0 });

// Find users who are admins and is active.
// https://docs.mongodb.com/manual/tutorial/query-documents/#specify-and-conditions

db.users.find({ 
    $and: [
        { isAdmin: true },
        { isActive: true }
    ]
});

// Find courses with letter 'u' in its name and has a price of greather than or equal to 13000.
// https://docs.mongodb.com/manual/reference/operator/query/gte/

db.courses.find({ 
    $and: [
        { name: { $regex: 'u', $options: '$i' } },
        { price: { $gte: 13000 } }
    ]
});
```