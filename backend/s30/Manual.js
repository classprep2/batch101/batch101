/*
==============================
S30 - JavaScript - ES6 Updates
==============================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1vXGELPWYQ8K5P-WbCo6RCBiHRLNiIKIboKb19-Dq6v8/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s19

Other References:
	JavaScript ES6
		https://www.w3schools.com/js/js_es6.asp
	JavaScript Exponentation Operator
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Exponentiation
	JavaScript Template Literals
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals
	JavaScript Expressions and Statements
		https://medium.com/launch-school/javascript-expressions-and-statements-4d32ac9c0e74
	JavaScript Destructuring Assignment
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment
	JavaScript Arrow Functions
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions
	JavaScript Default Parameters
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Default_parameters
	JavaScript Classes
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes
	Creating Git Projects:
		GitLab
			https://gitlab.com/projects/new#blank_project
		GitHub
			https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
==========
Discussion
==========
*/

/*
1. Create an "index.html" file.
	Batch Folder > S19  > Discussion > index.html
*/

		/*
		<!DOCTYPE HTML>
		<html>
		    <head>
		        <title>Activity: JavaScript ES6 Updates</title>
		    </head>
		    <body>
		    </body>
		</html>
		*/

/*
2. Create an "script.js" file and to test if the script is properly linked to the HTML file.
	Application > script.js
*/

		console.log("Hello World!");

/*
3. Link the "script.js" file to our HTML file.
	Application > script.html
*/

		/*
		<!DOCTYPE HTML>
		<html>
		    <!-- ... -->
		    <body>
		    	<script src="./script.js"></script>
		    </body>
		</html>
		*/

		/*
		Important Note:
			- The "script" tag is commonly placed at the bottom of the HTML file right before the closing "body" tag.
			- The reason for this is because Javascript's main function in frontend development is to make our websites and applications interactive.
			- In order to achieve this, JavaScript selects/targets specific HTML elements in our application and performs a certain output.
			- It is added last to allow all HTML and CSS resources to load first before applying any JavaScript code to run.
			- Placing the "script" tag at the top the the file might result in errors because since the HTML elements have not yet been loaded when the JavaScript loads, it does not have any valid HTML elements to target/select.
		*/

/*
4. Add code to the "script.js" file to demonstrate and discuss Exponent Operator.
	Application > script.js
*/

		// console.log('Hello World');

		// [SECTION] Exponent Operator

		const firstNum = 8 ** 2; 
		console.log(firstNum);

		const secondNum = Math.pow(8, 2);
		console.log(secondNum);

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentations for JavaScript ES6 Updates and JavaScript Exponentation Operator.
		*/

/*
5. Add more code to demonstrate and discuss Template Literals.
	Application > script.js
*/

		/*...*/
		console.log(secondNum);

		// [SECTION] Template Literals
		/*
			- Allows to write strings without using the concatenation operator (+)
			- Greatly helps with code readability
		*/

		let name = "John";

		// Pre-Template Literal String
		// Uses single quotes ('')
		let message = 'Hello ' + name + '! Welcome to programming!';
		console.log("Message without template literals:" + message)

		// Strings Using Template Literal
		// Uses backticks (``)
		message = `Hello ${name}! Welcome to programming!`;
		console.log(`Message without template literals: ${message}`)

		// Multi-line Using Template Literals
		const anotherMessage = `
		${name} attended a math competition.
		He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.
		`

		console.log(anotherMessage);

		/*
			- Template literals allow us to write strings with embedded JavaScript expressions
			- expressions are any valid unit of code that resolves to a value
			- "${}" are used to include JavaScript expressions in strings using template literals
		*/
		const interestRate = .1;
		const principal = 1000;

		console.log(`The interest on your savings account is: ${principal * interestRate}`);

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentations for Template Literals and JavaScript Expressions and Statements.
		*/

/*
6. Add more code to demonstrate and discuss Array Destructuring.
	Application > script.js
*/

		/*...*/

		console.log(`The interest on your savings account is: ${principal * interestRate}`);

		// [SECTION] Array Destructuring
		/*
			- Allows to unpack elements in arrays into distinct variables
			- Allows us to name array elements with variables instead of using index numbers
			- Helps with code readability
			- Syntax
				let/const [variableName, variableName, variableName] = array;
		*/

		const fullName = ["Juan", "Dela", "Cruz"];

		// Pre-Array Destructuring
		console.log(fullName[0]);
		console.log(fullName[1]);
		console.log(fullName[2]);

		console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`)

		// Array Destructuring
		const [firstName, middleName, lastName] = fullName;

		console.log(firstName);
		console.log(middleName);
		console.log(lastName);

		console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`);

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for JavaScript Destructuring Assignment.
		*/

/*
7. Add more code to demonstrate and discuss Array Destructuring.
	Application > script.js
*/

		/*...*/

		console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`);

		// [SECTION] Object Destructuring
		/*
			- Allows to unpack properties of objects into distinct variables
			- Shortens the syntax for accessing properties from objects
			- Syntax
				let/const {propertyName, propertyName, propertyName} = object;
		*/
		const person = {
		    givenName: "Jane",
		    maidenName: "Dela",
		    familyName: "Cruz"
		};

		// Pre-Object Destructuring
		console.log(person.givenName);
		console.log(person.middleName);
		console.log(person.familyName);

		console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`);

		// Object Destructuring
		const { givenName, maidenName, familyName } = person;

		console.log(givenName);
		console.log(middleName);
		console.log(familyName);

		console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);

		function getFullName ({ givenName, maidenName, familyName}) {
		    console.log(`${ givenName } ${ maidenName } ${ familyName }`);
		}

		getFullName(person);

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for JavaScript Destructuring Assignment.
		*/

/*
8. Add more code to demonstrate and discuss Arrow Functions.
	Application > script.js
*/

		/*...*/

		getFullName(person);

		// [SECTION] Arrow Functions
		/*
			- Compact alternative syntax to traditional functions
			- Useful for code snippets where creating functions will not be reused in any other portion of the code
			- Adheres to the "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a name for functions that will only be used in certain code snippets
		*/

		/*
		const variableName = () => {
			console.log()
		}
		*/

		const hello = () => {
			console.log("Hello world!");
		}

		// Pre-Arrow Function and Template Literals
		/*
			- Syntax
				function functionName(parameterA, parameterB, parameterC) {
					console.log();
				}
		*/
		/*
		function printFullName (firstName, middleInitial, lastName) {
		    console.log(firstName + ' ' + middleInitial + '. ' + lastName);
		}

		printFullName("John", "D", "Smith");
		*/

		// Arrow Function
		/*
			- Syntax
				let/const variableName = (parameterA, parameterB, parameterC) => {
					console.log();
				}
		*/

		const printFullName = (firstName, middleInitial, lastName) => {
		    console.log(`${firstName} ${middleInitial}. ${lastName}`);
		}

		printFullName("John", "D", "Smith");

		const students = ["John", "Jane", "Judy"];

		// Arrow Functions with loops
		// Pre-Arrow Function
		students.forEach(function(student){
			console.log(`${student} is a student.`);
		})

		// Arrow Function
		// The function is only used in the "forEach" method to print out a text with the student's names
		students.forEach((student) => {
			console.log(`${student} is a student.`);
		})

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for JavaScript Arrow Functions.
		*/

/*
9. Add more code to demonstrate and discuss Implicit Return Statement.
	Application > script.js
*/

		/*...*/

		students.forEach((student) => {
			/*...*/
		})

		// [SECTION] Implicit Return Statement
		/*
			- There are instances when you can omit the "return" statement
			- This works because even without the "return" statement JavaScript implicitly adds it for the result of the function
		*/

		// Pre-Arrow Function
		/*
		const add = (x, y) => {
		    return x + y;
		}

		let total = add(1,2);
		console.log(total);
		*/

		// Arrow Function
		const add = (x, y) => x + y;

		let total = add(1,2);
		console.log(total);

/*
10. Add more code to demonstrate and discuss Default Argument Value.
	Application > script.js
*/

		/*...*/

		console.log(total);

		// [SECTION] Default Function Argument Value
		// Provides a default argument value if none is provided when the function is invoked

		const greet = (name = 'User') => {
		    return `Good morning, ${ name }!`;
		}

		console.log(greet());
		// console.log(greet("John"));

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for JavaScript Default Parameters.
		*/

/*
10. Add more code to demonstrate and discuss Default Argument Value.
	Application > script.js
*/
		
		/*...*/
		// console.log(greet("John"));

		// [SECTION] Class-Based Object Blueprints
		/*
			- Allows creation/instantiation of objects using classes as blueprints
		*/

		// Creating a class
		/*
			- The constructor is a special method of a class for creating/initializing an object for that class.
			- The "this" keyword refers to the properties of an object created/initialized from the class
			- By using the "this" keyword and accessing an object's property, this allows us to reassign it's values
			- Syntax
				class className {
					constructor(objectPropertyA, objectPropertyB) {
						this.objectPropertyA = objectPropertyA;
						this.objectPropertyB = objectPropertyB;
					}
				}
		*/
		class Car {
			constructor(brand, name, year) {
				this.brand = brand;
				this.name = name;
				this.year = year;
			}
		}

		// Instantiating an object
		/*
			- The "new" operator creates/instantiates a new object with the given arguments as the values of it's properties
			- No arguments provided will create an object without any values assigned to it's properties
			- let/const variableName = new ClassName();
		*/
		// let myCar = new Car();

		/*
			- Creating a constant with the "const" keyword and assigning it a value of an object makes it so we can't re-assign it with another data type
			- It does not mean that it's properties cannot be changed/immutable
		*/
		const myCar = new Car();

		console.log(myCar);

		// Values of properties may be assigned after creation/instantiation of an object
		myCar.brand = "Ford";
		myCar.name = "Ranger Raptor";
		myCar.year = 2021;

		console.log(myCar);

		// Creating/instantiating a new object from the car class with initialized values
		const myNewCar = new Car("Toyota", "Vios", 2021);

		console.log(myNewCar);

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for JavaScript Classes.
		*/

/*
========
Activity
========
*/

/*

Instructions that can be provided to the students for reference:

Activity References:

JS Array and Object Destructuring
https://www.freecodecamp.org/news/array-and-object-destructuring-in-javascript/

JS Exponent Operator
https://www.w3schools.com/howto/howto_js_exponentiation.asp

JS Template Literal
https://www.w3schools.com/js/js_string_templates.asp

JS Arrow Functions
https://www.w3schools.com/js/js_arrow_function.asp

Activity:

Member 1:
1. In the s30 folder, create an activity folder and an index.html and index.js file inside of it.
- Create an index.html file to attach our index.js file
- Copy the template from boodle notes and paste it in an index.js file.
- Update your local sessions git repository and push to git with the commit message of Add template code s30.
- Console log the message Hello World to ensure that the script file is properly associated with the html file.
2. Create a variable called getCube and use the exponent operator to compute for the cube of 2. (A cube is any number raised to 3)

Member 2:
3. Using Template Literals, print out the value of the getCube variable with a message of The cube of <inputNum> is <cubedValue>
4. Destructure the given array from into the following variables:
- houseNumber
- street
- state
- zipCode

Member 3:
5. Then, print out a message with the full address using Template Literals.
6. Destructure the given object and print out a message with the details of the animal using Template Literals.

Member 4:
7. Loop through the given array using forEach, an arrow function and using the implicit return statement to print out the numbers.
8. Create a variable called reduceNumber and using the reduce array method and an arrow function, return the sum of all the numbers in the given array.

Member 5:
9. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
10. Create/instantiate a new object from the class Dog and console log the object.

All Members:
11. Check out to your own git branch with git checkout -b <branchName>
12. Update your local sessions git repository and push to git with the commit message of Add activity code s30.
13. Add the sessions repo link in Boodle for s30.

*/

/*
Solution:


1. Create an "activity" folder, an "index.html" file inside of it and link the "index.js" file.
	Batch Folder > S19 > Activity > index.html
*/

		/*
		<!DOCTYPE HTML>
		<html>
		    <head>
		        <title>Activity: JavaScript Objects</title>
		    </head>
		    <body>
		    	<script src="./index.js"></script>
		    </body>
		</html>
		*/

/*
2. Create a "index.js" file and console log the message "Hello World" to ensure that the index.js file is properly associated with the html file.
	- Add activity-template.js file in boodle.
	- Have trainees copy the content into their own index.js file
	Application > index.js
*/

		console.log("Hello World");

/*
3. Create several statements and functions utilizing the JavaScript ES6 updates.
	Application > index.js
*/

// Exponent Operator
const getCube = 2 ** 3;


// Template Literals
console.log(`The cube of 2 is ${getCube}`);


// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];

const [ houseNumber, street, state, zipCode ] = address;

console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}`);


// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

const { name, species, weight, measurement } = animal;

console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`);


// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => console.log(number));

let reduceNumber = numbers.reduce((x, y) => x + y);

console.log(reduceNumber);


// Javascript Classes
class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog("Frankie", 5, "Miniature Dachshund");

console.log(myDog);


//Do not modify
//For exporting to test.js
//All variables and function names needed are added here.
try{
    module.exports = {

        getCube: typeof getCube !== 'undefined' ? getCube : null,
        houseNumber: typeof houseNumber !== 'undefined' ? houseNumber : null,
        street: typeof street !== 'undefined' ? street : null,
        state: typeof state !== 'undefined' ? state : null,
        zipCode: typeof zipCode !== 'undefined' ? zipCode : null,
        name: typeof name !== 'undefined' ? name : null,
        species: typeof species !== 'undefined' ? species : null,
        weight: typeof weight !== 'undefined' ? weight : null,
        measurement: typeof measurement !== 'undefined' ? measurement : null,
        reduceNumber: typeof reduceNumber !== 'undefined' ? reduceNumber : null,
        Dog: typeof Dog !== 'undefined' ? Dog : null

    }
} catch(err){

}
