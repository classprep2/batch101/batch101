# WDC028v1.5b - Grading Rubric

## How to use the Grading Rubric
This document serves as a grading rubric for the WDC028v1.5b MCP sessions.

Each session has at least 3 "item"s per session and is categorized into a topic/section during the discussion (e.g. S05 - HTML - Forms, Tables and Semantic HTML - Forms, S05 - HTML - Forms, Tables and Semantic HTML - Tables, S11 - Bootstrap - Flex - Other Projects Section).

The passing grade is 75% of the total items for each session, rounded up.

The computation for the passing grade is (# of session items x 75% = total(rounded up))

Example: 3 items x 75% is 2.25 and the rounded value is 2.

If the students are unable to reach the minimum required items or fails to submit on time without a reasonable cause, their grade for the session will be marked with a failing grade or a mark of "F".

## Capstone Projects
Capstone projects should have a 100% completion of all required features. All stretch goals are not to be included in the computation of the final score.

All projects must be hosted in their respective hosting sites (GitLab/GitHub, Heroku, Netlify, etc.) and available for online use.

Failure to complete all minimum features and hosting the projects would result in a failing grade.

At the end of the bootcamp, capstone completion with revisions to score a "P" is for the instructor's discretion.

A single failure in one of the required capstones for the bootcamp will result in an incomplete submission and would not be recommended for commencement.

## Modifications to activities
This rubric is based on the activities provided for each of the sessions. Depending on the modification on the activity the following resolutions may be applied:

### Original activity provided:
1. Use the requirements in this rubric.

### Different activity provided:
1. At a minimum, the original activity and use the grading rubric must be followed, to allow ease in checking of the materials
2. Stretch goals provided and checking them is at the discretion of the INT TM
3. Inform the instructor SVs of changes in the activity, the reason for changing the activity and a codebase output of the changed activity.
4. File a [redmine ticket and a GitLab/GitHub merge request](https://docs.google.com/presentation/d/163esoB62m8kzk837DWGGShumJBIXymmaoGJA_N_dk50/edit#slide=id.gdcd8595ae6_0_606) to inform the CDT of any modifications that need to be applied to the activities.

# Session Answer Keys

| Topic                                         | Link |
| --------------------------------------------- | ---- |
| S02 - Git Basics  | [Link](#s02) |
| S03 - HTML Introduction  | [Link](#s03) |
| S04 - HTML - Attributes and Hyperlinks  | [Link](#s04) |
| S05 - HTML - Forms, Tables and Semantic HTML  | [Link](#s05) |
| S06 - CSS - Introduction and CSS Selectors  | [Link](#s06) |
| S07 - CSS - Common Styling Properties and Positioning  | [Link](#s07) |
| S08 - CSS - Box Model  | [Link](#s08) |
| S09 - Bootstrap - Introduction and Simple Styles  | [Link](#s09) |
| S10 - Bootstrap - Grid System  | [Link](#s10) |
| S11 - Bootstrap - Flex  | [Link](#s11) |
| S12 - Bootstrap - Components  | [Link](#s12) |
| S13-S14 - Capstone 1 Development - Developer Portfolio  | [Link](#s13-s14) |
| S15 - JavaScript - Syntax, Variables, and Simple Operations  | [Link](#s15) |
| S16 - JavaScript - Operators and Truth Tables  | [Link](#s16) |
| S17 - JavaScript - Basic Functions  | [Link](#s17) |
| S18 - JavaScript - Function Parameters and Return Statements  | [Link](#s18) |
| S19 - JavaScript - Selection Control Structures  | [Link](#s19) |
| S20 - JavaScript - Repetition Control Structures  | [Link](#s20) |
| S21 - JavaScript - Array Traversal  | [Link](#s21) |
| S22 - Array Manipulation  | [Link](#s22) |
| S23 - JavaScript - Objects  | [Link](#s23) |
| S24 - JavaScript ES6 Updates  | [Link](#s24) |
| S25 - Introduction to JSON  | [Link](#s25) |
| S26 - MongoDB - Database and NoSQL Introduction  | [Link](#s26) |
| S27 - MongoDB - Data Modeling and Translation  | [Link](#s27) |
| S28 - MongoDB - CRUD Operations  | [Link](#s28) |
| S29 - MongoDB - Query Operators and Field Projection  | [Link](#s29) |
| S30 - Aggregation in MongoDB and Query Case Studies  | [Link](#s30) |
| S31 - Node.js Introduction  | [Link](#s31) |
| S32 - Node.js Routing w/ HTTP methods  | [Link](#s32) |
| S33 - Introduction to Postman and REST  | [Link](#s33) |
| S34 - Introduction to Express.js  | [Link](#s34) |
| S35 - Data Persistence via Mongoose ODM  | [Link](#s35) |
| S36 - Modules, Parameterized Routes  | [Link](#s36) |
| S37 - Booking System API - Business Use Case Translation to Model Design  | [Link](#s37) |
| S38 - Booking System API - User Registration, Authentication, and JWT's  | [Link](#s38) |
| S39 - Booking System API - Authorization via JWT, Retrieval of User Details  | [Link](#s39) |
| S40 - Booking System API - Courses CRUD  | [Link](#s40) |
| S41 - Booking System API - User Course Enrollment, API Deployment  | [Link](#s41) |
| S42-S46 - Capstone 2 - E-commerce API  | [Link](#s42-s46) |
| S47 - JavaScript - DOM Manipulation  | [Link](#s47) |
| S48 - JavaScript - Reactive DOM with JSON  | [Link](#s48) |
| S49 - JavaScript - Reactive DOM with Fetch  | [Link](#s49) |
| S50 - React.js - Component-Driven Development | [Link](#s50) |
| S51 - React.js - Props and States | [Link](#s51) |
| S52 - React.js - Effects, Events and Forms | [Link](#s52) |
| S53 - React.js - Routing and Conditional Rendering | [Link](#s53) |
| S54 - React.js - App State Management | [Link](#s54) |
| S55 - React.js - API Integration with Fetch | [Link](#s55) |
| S56 - Mock Technical Exam (Concepts and Theory + Function Coding) | [Link](#s56) |
| S57 - Mock Technical Exam (Debugging & Code Tracing + Data Structures & Algorithms) | [Link](#s57) |
| S58 - Wireframes, Mockups and Prototypes | [Link](#s58) |
| S59-S64 - Capstone 3 Development | [Link](#s59-s64) |

<a name="s02"></a>
# S02 - Git Basics
## Sample Output

![image-output](readme-images/s02-sample-output3.png)

![image-output](readme-images/s02-sample-output1.png)

![image-output](readme-images/s02-sample-output2.png)

## S02 - Item (1/3) - GitLab/Github

1. Solution

   - The students must be able to successfully create a GitLab/GitHub repository and push the text file

2. Sample Output

  ![image-output](readme-images/s02-item1-solution.png)

## S02 - Item (2/3) - GitLab/Github

1. Solution

   - The students' remote repository should have multiple commits applied for changes on the "aboutme.txt" file

2. Sample Output

   ![image-output](readme-images/s02-sample-output1.png)
   ![image-output](readme-images/s02-sample-output2.png)

## S02 - Item (3/3) - GitLab/Github

1. Solution

   - The "aboutme.txt" file should contain the students motivation in joining the bootcamp and a paragraph that briefly narrates their work experience.

2. Sample Output

   ![image-output](readme-images/s02-sample-output3.png)

[Back to top](#session-answer-keys)

<a name="s03"></a>
# S03 - HTML - Introduction
## Sample Output

![image-output](readme-images/S03-sample-output.png)

![image-output](readme-images/s03-sample-output2.png)


## S03 - Item (1/3) - HTML Elements

1. Solution

   - The html files must contain similar content as presented in the output in the correct order.

2. Sample Output

  ![image-output](readme-images/S03-sample-output.png)

  ![image-output](readme-images/s03-sample-output2.png)

## S03 - Item (2/3) - HTML Elements

1. Solution

   - The "activity-a.html" file must have the correct HTML elements applied such as headers, paragraph tags and horizontal rule elements

2. Sample Output

   ![image-output](readme-images/S03-sample-output.png)

## S03 - Item (3/3) - HTML Elements

1. Solution

   - The "activity-b.html" file must have the correct HTML elements applied such as headers, paragraph tags and horizontal rule elements

2. Sample Output

   ![image-output](readme-images/s03-sample-output2.png)

[Back to top](#session-answer-keys)

<a name="s04"></a>
# S04 - HTML - Attributes and Hyperlinks
## Sample Output

![image-output](readme-images/s04-sample-output.png)

![image-output](readme-images/s04-sample-output2.png)

![image-output](readme-images/s04-sample-output3.png)


## S04 - Item (1/3) - HTML - Attributes and Hyperlinks

1. Solution

   - The folder structure must be exactly the same as the screenshot provided below.

2. Sample Output

  ![image-output](readme-images/s04-sample-output.png)

## S04 - Item (2/3) - HTML - Attributes and Hyperlinks

1. Solution

   - All HTML pages must be linked to each other correctly. All links must be working properly and will navigate to the correct html page.

2. Sample Output

   ![image-output](readme-images/s04-sample-output2.png)

## S04 - Item (3/3) - HTML - Attributes and Hyperlinks

1. Solution

   - Each HTML page must contain a different image and a text header specifying which page they are currently on.

2. Sample Output

   ![image-output](readme-images/s04-sample-output2.png)

[Back to top](#session-answer-keys)

<a name="s05"></a>
# S05 - HTML - Forms, Tables and Semantic HTML
## Sample Output

![readme-images/sample-output1.png](readme-images/s05-sample-output1.png)

![readme-images/sample-output1.png](readme-images/s05-sample-output2.png)

## S05 - Item (1/3) - Semantic HTML

1. Solution

   - There must be a "header" element that wraps around the header element
   - The table and the form must be wrapped in "section" elements
   - The main section should wrap all "section elements"

   ![readme-images/sample-output1.png](readme-images/s05-item1-solution.png)

2. Sample Output

## S05 - Item (2/3) - HTML Tables

1. Solution

   - The table must be composed of 3 elements - thead, tbody, tfooter.
   - The table body must use HTML entities for printing out the "Pesos" symbol.
   - The "tfooter" element must have a colspan of 3.

   ![readme-images/sample-output1.png](readme-images/s05-item2-solution.png)

2. Sample Output

   ![readme-images/sample-output1.png](readme-images/s05-item2-sample-output.png)

## S05 - Item (3/3) - HTML Forms

1. Solution

   - The form should have an "action" attribute pointing to "http://google.com" that will send all form details as a query string.
   - Each "input" field must have it's own "label" element
   - Each "input" element must have the following attributes:
     - type
     - name
     - id
     - required
   - The "reset" button should clear all form field inputs

   ![readme-images/sample-output1.png](readme-images/s05-item3-solution1.png)

   ![readme-images/sample-output1.png](readme-images/s05-item3-solution2.png)

2. Sample Output

   ![readme-images/sample-output1.png](readme-images/s05-item3-sample-output.png)

[Back to top](#session-answer-keys)

<a name="s06"></a>
# S06 - CSS - Introduction and CSS Selectors
## Sample Output

![image-output](readme-images/s06-sample-output.png)

![image-output](readme-images/s06-sample-output2.png)


## S06 - Item (1/3) - CSS - Introduction and CSS Selectors

1. Solution

   - CSS must be implemented using the 3 separate methods (inline, internal and external styling) on the first 3 paragraph texts.

2. Sample Output

  ![image-output](readme-images/s06-sample-output.png)

## S06 - Item (2/3) - CSS - Introduction and CSS Selectors

1. Solution

   - The size of the header "CSS Introduction Activity" must be larger than the default header size.
   - The "The following things you will learn in this bootcamp" paragraph text must be capitalized using CSS syntax
   - The list items in the "Languages to be learned" must have a different color background

2. Sample Output

   ![image-output](readme-images/s06-sample-output.png)

## S06 - Item (3/3) - CSS - Introduction and CSS Selectors

1. Solution

   - The "Tools to be Used" section must have it's list items have a colored border
   - the "Other things to be Learned" section must have an arounded image
   - The paragraphs for the "Other things to be Learned" must have a text spacing 

2. Sample Output

   ![image-output](readme-images/s06-sample-output2.png)

[Back to top](#session-answer-keys)

<a name="s07"></a>
# S07 - CSS - Common Styling Properties and Positioning 
## Sample Output

![image-output](readme-images/s07-sample-output.png)

![image-output](readme-images/s07-sample-output2.png)


## S07 - Item (1/3) - CSS - Common Styling Properties and Positioning - Navbar

1. Solution

   - A CSS reset selector must be present in the CSS file
   - The font of the whole page must be different from the default HTML text font
   - The position of the navbar must be sticky and should be at the top of the page when the user scrolls the page
   - The navbar button should have a custom color and shape same as seen in the screenshot
   - The zuitt logo/another logo must be present at the top of the navbar

2. Sample Output

  ![image-output](readme-images/s07-sample-output.png)

## S07 - Item (2/3) - CSS - Common Styling Properties and Positioning - Body

1. Solution

   - The background color of the body should have an opaque
   - The font family of the header should ba different from the default HTML text font
   - The background color of the "Book Now" button must be different

2. Sample Output

   ![image-output](readme-images/s07-sample-output2.png)

## S07 - Item (3/3) - CSS - Common Styling Properties and Positioning - Footer

1. Solution

   - The background color of the footer should be a different color

2. Sample Output

   ![image-output](readme-images/s07-sample-output2.png)

[Back to top](#session-answer-keys)

<a name="s08"></a>
# S08 - CSS - Box Model
## Sample Output

![image-output](readme-images/s08-sample-output.png)

## S08 - Item (1/3) - CSS - Box Model

1. Solution

   - The "pre-footer" section must have a different background color
   - Each of the column divs must have a space between each of the elements using the flexbox model
   - The text must also be visible and readable against the color of the background

2. Sample Output

  ![image-output](readme-images/s08-sample-output.png)

## S08 - Item (2/3) - CSS - Box Model

1. Solution

   - The div tags inside the "pre-footer" div tag must be aligned horizontally using the flexbox model

2. Sample Output

   ![image-output](readme-images/s08-sample-output.png)

## S08 - Item (3/3) - CSS - Box Model

1. Solution

   - All the elements inside the "pre-footer" div tags columns must have equal spacing between the elements vertically using the flexbox model

2. Sample Output

   ![image-output](readme-images/s08-sample-output.png)

[Back to top](#session-answer-keys)

<a name="s09"></a>
# S09 - Bootstrap - Introduction and Simple Styles
## Sample Output

![image-output](readme-images/s09-sample-output.png)
![image-output](readme-images/s09-sample-output2.png)

## S09 - Item (1/4) - Bootstrap - Introduction and Simple Styles - Navbar

1. Solution

   - All styling should be achieved using the different Bootstrap classes
   - The background color of the navbar must be a different color
   - The navbar links should be centered with equal spacing
   - The navbar links should have a text-light color to be easily readable

2. Sample Output

  ![image-output](readme-images/s09-sample-output.png)

## S09 - Item (2/4) - Bootstrap - Introduction and Simple Styles - About Me Section

1. Solution

   - The image should have a border radius to create a circular image
   - The name of the portfolio owner should have a border bottom
   - All elements of the "About Me" section should be stacked vertically

2. Sample Output

   ![image-output](readme-images/s09-sample-output.png)

## S09 - Item (3/4) - Bootstrap - Introduction and Simple Styles - Projects Section

1. Solution

   - All the cards in the "Projects" section should be equally spaced horizontally using flexbox
   - The image sizes should be manipulated using bootstrap sizing
   - All the cards should have an ample amount of padding from the border to the content
   - All the card contents should be stacked horizontally with spaces in between

2. Sample Output

   ![image-output](readme-images/s09-sample-output2.png)

## S09 - Item (3/4) - Bootstrap - Introduction and Simple Styles - Footer

1. Solution

   - The "Footer" section should have a different background color
   - The text of the "Footer" section should be easily readable

2. Sample Output

   ![image-output](readme-images/s09-sample-output2.png)

[Back to top](#session-answer-keys)

<a name="s10"></a>
# S10 - Bootstrap - Grid System
## Sample Output

![image-output](readme-images/s10-sample-output.png)
![image-output](readme-images/s10-sample-output2.png)
![image-output](readme-images/s10-sample-output3.png)

## S10 - Item (1/3) - Bootstrap - Grid System

1. Solution

   - The elements must be aligned using Bootstrap Grid system
   - On a larger screen (e.g. desktop/laptop view) the "Projects" section images should be displayed horizontally along with the text.
   - On a larger screen the "Projects" section images and text description should displayed in an alternating fashion.
   - The first card shows text on the left and the image on the right, the second card shows the text on the right and the image on the left

2. Sample Output

  ![image-output](readme-images/s10-sample-output.png)

## S10 - Item (2/3) - Bootstrap - Grid System

1. Solution

   - The "Other Projects" section must be horizontally aligned with spaces in between on a desktop/laptop view.

2. Sample Output

   ![image-output](readme-images/s10-sample-output2.png)

## S10 - Item (3/3) - Bootstrap - Grid System

1. Solution

   - On a mobile view, all the cards/card contents must be presented stacked vertically.
   - The images should be displayed on top and the text description must be at the bottom

2. Sample Output

   ![image-output](readme-images/s10-sample-output3.png)

[Back to top](#session-answer-keys)

<a name="s11"></a>
# S11 - Bootstrap - Flex
## Sample Output

![image-output](readme-images/s11-sample-output.png)
![image-output](readme-images/s11-sample-output2.png)
![image-output](readme-images/s11-sample-output3.png)
![image-output](readme-images/s11-sample-output4.png)

## S11 - Item (1/4) - Bootstrap - Flex - Other Projects Section

1. Solution

   - All styling should be achieved using the different Bootstrap Flexbox classes
   - On a larger screen (e.g. desktop/laptop view) the "Other Projects" section cards should be displayed horizontally.

2. Sample Output

  ![image-output](readme-images/s11-sample-output.png)

## S11 - Item (2/4) - Bootstrap - Flex - Other Projects Section

1. Solution

   - On a mobile view the "Other Projects" section cards should be displayed vertically.

2. Sample Output

   ![image-output](readme-images/s11-sample-output3.png)

## S11 - Item (3/4) - Bootstrap - Flex - Core Tools and Other Tools Section

1. Solution

   - On a larger screen (e.g. desktop/laptop view) the "Core Tools" and "Other Tools" section images should be displayed similarly as seen in the screenshot both vertically and horizontally.

2. Sample Output

   ![image-output](readme-images/s11-sample-output2.png)

## S11 - Item (4/4) - Bootstrap - Flex - Core Tools and Other Tools Section

1. Solution

   - On a mobile view the "Core Tools" and "Other Tools" section cards should be displayed vertically with 2 images showing side by side.

2. Sample Output

   ![image-output](readme-images/s11-sample-output4.png)
   ![image-output](readme-images/s11-sample-output5.png)

[Back to top](#session-answer-keys)

<a name="s12"></a>
# S12 - Bootstrap - Components
## Sample Output

![image-output](readme-images/s12-sample-output.png)
![image-output](readme-images/s12-sample-output2.png)

## S12 - Item (1/3) - Bootstrap - Components - Forms

1. Solution

   - All solutions to this activity should be completed using Bootstrap components
   - The form should contain all the following input fields found in the screenshot below
   - The styling of the form should be accomplished using Bootstrap and not CSS

2. Sample Output

  ![image-output](readme-images/s12-sample-output.png)

## S12 - Item (2/3) - Bootstrap - Components - Forms

1. Solution
   - The form should be mobile responsive occupying 12 columns on small screens, 8 in medium sized screens and 4 columns in large screens
   - This should be achieved useing Bootstrap Grid
   

2. Sample Output

   ![image-output](readme-images/s12-sample-output.png)

## S12 - Item (3/3) - Bootstrap - Components - Modals

1. Solution

   - A modal should be shown when the the form button is pressed

2. Sample Output

   ![image-output](readme-images/s12-sample-output2.png)

[Back to top](#session-answer-keys)

<a name="s13-s14"></a>
# S13-S14 - Capstone 1 Development - Developer Portfolio
## Sample Output

![image-output](readme-images/s13-sample-output.png)
![image-output](readme-images/s13-sample-output2.png)
![image-output](readme-images/s13-sample-output3.png)
![image-output](readme-images/s13-sample-output4.png)
![image-output](readme-images/s13-sample-output5.png)

## S13-S14 - Item (1/5) - Capstone 1 Development - Developer Portfolio - Minimum Requirements

1. Solution

   - All CSS code must be in an external CSS file.
   - There should be no break elements in the source code.
   - There should be no inline or internal CSS code.
   - There should be no default web page fonts (Times New Roman).
   - There should be no placeholder text in the page (like the Lorem Ipsum).
   - There should be no horizontal scrolls when viewing the page.
   - There should be no dead links.
   - Do not create another website other than the required developer portfolio.
   - Do not use templates found in other sites.
   - The portfolio should contain the all 4 different pages/sections (e.g. Landing Page, About Us Page, Gallery Page and Contact Page). The students may create a single page with different sections or 4 different pages that can be navigated using the navbar link. If the student decides to omit one of the pages, a reasonable alternative approach or design should be provided to give a passing score.
   - The students must be able to push their codebase in GitLab/GitHub on a daily basis. Failure to push their outputs without an acceptable reason will result in a failing grade.

2. Sample Output

  ![image-output](readme-images/s13-sample-output.png)

## S13-S14 - Item (2/5) - Capstone 1 Development - Developer Portfolio - Landing Page/Section

1. Solution
   - The landing page/section may contain a short description about the student or simply an image with their name

2. Sample Output

   ![image-output](readme-images/s13-sample-output2.png)

## S13-S14 - Item (3/5) - Capstone 1 Development - Developer Portfolio - About Me/Us Page/Section

1. Solution

   - The about us page/section must contain information about the student. A simple text description that may describe the student's character would suffice.
   - Other means of expressing their character to their future employers/other individuals are acceptable items that can be shown in this page.

2. Sample Output

   ![image-output](readme-images/s13-sample-output3.png)

## S13-S14 - Item (4/5) - Capstone 1 Development - Developer Portfolio - Gallery Page/Section

   1. Solution

      - The gallery page/section must contain information about the student's projects or personal photos.
      - This may contain images from outputs completed in previous sessions or personal projects that the students have worked on. They can be related to software development projects or to projects they've completed in their previous work experiences which can help them to showcase their skills and talents.

   2. Sample Output

      ![image-output](readme-images/s13-sample-output4.png)

## S13-S14 - Item (5/5) - Capstone 1 Development - Developer Portfolio - Contact Page/Section

   1. Solution

      - The contact page/section must contain a contact form and details about how to contact the student through different social media and non-social media means like email, contact number, facebook, twitter, instagram, etc.

   2. Sample Output

      ![image-output](readme-images/s13-sample-output5.png)

[Back to top](#session-answer-keys)

<a name="s15"></a>
# S15 - JavaScript - Syntax, Variables, and Simple Operations
## Sample Output

![image-output](readme-images/s15-sample-output.png)

## S15 - Item (1/3) - JavaScript - Syntax, Variables, and Simple Operations - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s15-sample-output2.png)

## S15 - Item (2/3) - JavaScript - Syntax, Variables, and Simple Operations - Console Output

   1. Solution

      - The console must print out all variables declared.
      - No error should be present in the console.

   2. Sample Output

      ![image-output](readme-images/s15-sample-output.png)

## S15 - Item (3/3) - JavaScript - Syntax, Variables, and Simple Operations - Debugging Practice

   1. Solution

      - No error should be present in the console and the result should display the same as the screenshot provided below.

   2. Sample Output

      ![image-output](readme-images/s15-sample-output.png)

[Back to top](#session-answer-keys)

<a name="s16"></a>
# S16 - JavaScript - Operators and Truth Tables
## Sample Output

![image-output](readme-images/s16-sample-output.png)

## S16 - Item (1/3) - JavaScript - Operators and Truth Tables - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s16-sample-output.png)

## S16 - Item (2/3) - JavaScript - Operators and Truth Tables - Console Output

   1. Solution

      - The console must output the expected values provided by the "console.log()" outputs presented in the template.
      - Refer to screenshot below for output in console.

   2. Sample Output

      ![image-output](readme-images/s16-sample-output2.png)

## S16 - Item (3/3) - JavaScript - Operators and Truth Tables - Console Output

   1. Solution

      - Refer to screenshot below for output in console.

   2. Sample Output

      ![image-output](readme-images/s16-sample-output3.png)

[Back to top](#session-answer-keys)

<a name="s17"></a>
# S17 - JavaScript - Basic Functions
## Sample Output

![image-output](readme-images/s17-sample-output.png)
![image-output](readme-images/s17-sample-output2.png)
![image-output](readme-images/s17-sample-output3.png)
![image-output](readme-images/s17-sample-output4.png)
![image-output](readme-images/s17-sample-output5.png)
![image-output](readme-images/s17-sample-output6.png)
![image-output](readme-images/s17-sample-output7.png)

## S17 - Item (1/5) - JavaScript - Basic Functions - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s17-sample-output9.png)

## S17 - Item (2/5) - JavaScript - Basic Functions - printUserInfo() function

   1. Solution

      - All solutions must be outputted in the console using functions
      - A prompt must pop up to ask the user for their details to be used as part of the function's result
      - The user's input must be printed in the console as part of a string.

   2. Sample Output

      ![image-output](readme-images/s17-sample-output.png)
      ![image-output](readme-images/s17-sample-output8.png)

## S17 - Item (3/5) - JavaScript - Basic Functions - printMyTopFiveBands() function

   1. Solution

      - The function must be invoked that will print out a list of their favorite bands

   2. Sample Output

      ![image-output](readme-images/s17-sample-output2.png)

## S17 - Item (4/5) - JavaScript - Basic Functions - printMyTopFiveMovies() function

   1. Solution

      - The function must be invoked that will print out a list of their favorite movies

   2. Sample Output

      ![image-output](readme-images/s17-sample-output3.png)

## S17 - Item (5/5) - JavaScript - Basic Functions - printUsers() function

   1. Solution

      - A prompt must pop up to ask the user for their details to be used as part of the function's result
      - The user's input must be printed in the console as part of a string.

   2. Sample Output

      ![image-output](readme-images/s17-sample-output4.png)
      ![image-output](readme-images/s17-sample-output5.png)
      ![image-output](readme-images/s17-sample-output6.png)
      ![image-output](readme-images/s17-sample-output7.png)

[Back to top](#session-answer-keys)

<a name="s18"></a>
# S18 - JavaScript - Function Parameters and Return Statements
## Sample Output

![image-output](readme-images/s18-sample-output.png)

## S18 - Item (1/5) - JavaScript - Function Parameters and Return Statements - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s18-sample-output6.png)

## S18 - Item (2/5) - JavaScript - Function Parameters and Return Statements - addNum() and subNum() function

   1. Solution

      - All solutions must be outputted in the console using functions
      - The output of the console must perform the operation stated in the screenshot below

   2. Sample Output

      ![image-output](readme-images/s18-sample-output2.png)

## S18 - Item (3/5) - JavaScript - Function Parameters and Return Statements - multiplyNum() and divideNum function

   1. Solution

      - The output of the console must perform the operation stated in the screenshot below

   2. Sample Output

      ![image-output](readme-images/s18-sample-output3.png)

## S18 - Item (4/5) - JavaScript - Function Parameters and Return Statements - getCircleArea() function

   1. Solution

      - The output of the console must perform the operation stated in the screenshot below

   2. Sample Output

      ![image-output](readme-images/s18-sample-output4.png)

## S18 - Item (5/5) - JavaScript - Function Parameters and Return Statements - getAverage() and checkIfPassed() function

   1. Solution

      - The output of the console must perform the operation stated in the screenshot below

   2. Sample Output

      ![image-output](readme-images/s18-sample-output5.png)

[Back to top](#session-answer-keys)

<a name="s19"></a>
# S19 - JavaScript - Selection Control Structures
## Sample Output

![image-output](readme-images/s19-sample-output.png)
![image-output](readme-images/s19-sample-output2.png)
![image-output](readme-images/s19-sample-output3.png)
![image-output](readme-images/s19-sample-output4.png)
![image-output](readme-images/s19-sample-output5.png)
![image-output](readme-images/s19-sample-output6.png)

## S19 - Item (1/8) - JavaScript - Selection Control Structures - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s19-sample-output13.png)

## S19 - Item (2/8) - JavaScript - Selection Control Structures

   1. Solution

      - All solutions must be outputted in the console using selection control structures
      - If the user does not complete the information provided an alert informing the user of the incomplete details should show
      - If the user inputs the complete details a welcome alert message with all the user details must be displayed

   2. Sample Output

      ![image-output](readme-images/s19-sample-output.png)
      ![image-output](readme-images/s19-sample-output2.png)
      ![image-output](readme-images/s19-sample-output3.png)
      ![image-output](readme-images/s19-sample-output4.png)
      ![image-output](readme-images/s19-sample-output5.png)

## S19 - Item (3/8) - JavaScript - Selection Control Structures

   1. Solution

      - If the average of all numbers is less than or equal to 74 the grading equivalent is "F"

   2. Sample Output

      ![image-output](readme-images/s19-sample-output7.png)

## S19 - Item (4/8) - JavaScript - Selection Control Structures

   1. Solution

      - If the average of all numbers is between than or equal to 75 and 79 the grading equivalent is "D"

   2. Sample Output

      ![image-output](readme-images/s19-sample-output8.png)

## S19 - Item (5/8) - JavaScript - Selection Control Structures

   1. Solution

      - If the average of all numbers is between than or equal to 80 and 84 the grading equivalent is "C"

   2. Sample Output

      ![image-output](readme-images/s19-sample-output9.png)

## S19 - Item (6/8) - JavaScript - Selection Control Structures

   1. Solution

      - If the average of all numbers is between than or equal to 85 and 89 the grading equivalent is "B"

   2. Sample Output

      ![image-output](readme-images/s19-sample-output10.png)

## S19 - Item (7/8) - JavaScript - Selection Control Structures

   1. Solution

      - If the average of all numbers is between than or equal to 90 and 95 the grading equivalent is "A"

   2. Sample Output

      ![image-output](readme-images/s19-sample-output11.png)

## S19 - Item (8/8) - JavaScript - Selection Control Structures

   1. Solution

      - If the average of all numbers is greater than 96 the grading equivalent is "A+"

   2. Sample Output

      ![image-output](readme-images/s19-sample-output12.png)

[Back to top](#session-answer-keys)

<a name="s20"></a>
# S20 - JavaScript - Repetition Control Structures
## Sample Output

![image-output](readme-images/s20-sample-output.png)
![image-output](readme-images/s20-sample-output2.png)

## S20 - Item (1/3) - JavaScript - Repetition Control Structures - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s20-sample-output3.png)

## S20 - Item (2/3) - JavaScript - Repetition Control Structures - Loops

   1. Solution

      - The loop must print out a number if it's divisible by 5
      - The number will be skipped if it's divisible by 10
      - The loop will stop if the number is less than 50

   2. Sample Output

      ![image-output](readme-images/s20-sample-output.png)

## S20 - Item (3/3) - JavaScript - Repetition Control Structures - Loop Filter

   1. Solution

      - The loop must result in a console log with a whole string containing only consonants and no vowels as shown in the screenshot

   2. Sample Output

      ![image-output](readme-images/s20-sample-output2.png)

[Back to top](#session-answer-keys)

<a name="s21"></a>
# S21 - JavaScript - Array Traversal
## Sample Output

![image-output](readme-images/s21-sample-output.png)

## S21 - Item (1/7) - JavaScript - Array Traversal - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s21-sample-output9.png)

## S21 - Item (2/7) - JavaScript - Array Traversal - Add Element

   1. Solution

      - All answers must be accomplished using array traversal methods
      - A new element must be added to the list as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s21-sample-output2.png)

## S21 - Item (3/7) - JavaScript - Array Traversal - Retrieve Element

   1. Solution

      - The element must be printed in the console using it's index number

   2. Sample Output

      ![image-output](readme-images/s21-sample-output3.png)

## S21 - Item (4/7) - JavaScript - Array Traversal - Delete Element

   1. Solution

      - The name of the deleted element must be returned from the array
      - The original array must be manipulated to no longer include the deleted element

   2. Sample Output

      ![image-output](readme-images/s21-sample-output4.png)
      ![image-output](readme-images/s21-sample-output5.png)

## S21 - Item (5/7) - JavaScript - Array Traversal - Update Element

   1. Solution

      - One of the elements must be updated using an array method

   2. Sample Output

      ![image-output](readme-images/s21-sample-output6.png)

## S21 - Item (6/7) - JavaScript - Array Traversal - Clear Elements

   1. Solution

      - All array elements must be cleared

   2. Sample Output

      ![image-output](readme-images/s21-sample-output7.png)

## S21 - Item (7/7) - JavaScript - Array Traversal - Check Array Length

   1. Solution

      - The result must return "true" after checking if the array doesn't contain any elements

   2. Sample Output

      ![image-output](readme-images/s21-sample-output8.png)

[Back to top](#session-answer-keys)

<a name="s22"></a>
# S22 - Array Manipulation
## Sample Output

![image-output](readme-images/s22-sample-output.png)
![image-output](readme-images/s22-sample-output2.png)
![image-output](readme-images/s22-sample-output3.png)
![image-output](readme-images/s22-sample-output4.png)
![image-output](readme-images/s22-sample-output5.png)
![image-output](readme-images/s22-sample-output6.png)
![image-output](readme-images/s22-sample-output7.png)

## S22 - Item (1/4) - Array Manipulation - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s22-sample-output8.png)

## S22 - Item (2/4) - Array Manipulation - Adding Elements

   1. Solution

      - All answers must be accomplished using array methods
      - New elements must be added to the list as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s22-sample-output.png)
      ![image-output](readme-images/s22-sample-output2.png)
      ![image-output](readme-images/s22-sample-output3.png)

## S22 - Item (3/4) - Array Manipulation - Retrieving Elements

   1. Solution

      - New elements must be added to the list as shown in the screenshot below.
      - All elements of the array must be printed in the console as shown in the screenshot below

   2. Sample Output

      ![image-output](readme-images/s22-sample-output4.png)
      ![image-output](readme-images/s22-sample-output5.png)

## S22 - Item (4/4) - Array Manipulation - Array Length

   1. Solution

      - The number of of elements of an array must be displayed in the alert message
      - The array must contain string elements as shown in the screenshot below

   2. Sample Output

      ![image-output](readme-images/s22-sample-output6.png)
      ![image-output](readme-images/s22-sample-output7.png)

[Back to top](#session-answer-keys)

<a name="s23"></a>
# S23 - JavaScript - Objects
## Sample Output

![image-output](readme-images/s23-sample-output.png)

## S23 - Item (1/4) - JavaScript - Objects - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s23-sample-output5.png)

## S23 - Item (2/4) - JavaScript - Objects - Object Instantiation

   1. Solution

      - All answers must be accomplished using Javascript Objects
      - An object must be instantiated with the same structure as shown in the screenshot below

   2. Sample Output

      ![image-output](readme-images/s23-sample-output2.png)

## S23 - Item (3/4) - JavaScript - Objects - Class Constructors

   1. Solution

      - Several objects must be instantiated using a class constructor with the same structure as shown in the screenshot below

   2. Sample Output

      ![image-output](readme-images/s23-sample-output3.png)

## S23 - Item (4/4) - JavaScript - Objects - Object Methods

   1. Solution

      - The objects instantiated must be able to function as shown in the screenshot below by calling it's methods

   2. Sample Output

      ![image-output](readme-images/s23-sample-output4.png)

[Back to top](#session-answer-keys)

<a name="s24"></a>
# S24 - JavaScript ES6 Updates

## Sample Output

![readme-images/sample-output1.png](readme-images/s24-sample-output.png)

## S24 - Item (1/6) - Exponent Operator

1. Solution

   ![readme-images/sample-output1.png](readme-images/s24-item1-solution.png)

2. Sample Output

   ![readme-images/sample-output1.png](readme-images/s24-item1-sample-output.png)

## S24 - Item (2/6) - Template Literals

1. Solution

   ![readme-images/sample-output1.png](readme-images/s24-item2-solution.png)

2. Sample Output

   ![readme-images/sample-output1.png](readme-images/s24-item2-sample-output.png)

## S24 - Item (3/6) - Array Destructuring

1. Solution

   ![readme-images/sample-output1.png](readme-images/s24-item3-solution.png)

2. Sample Output

   ![readme-images/sample-output1.png](readme-images/s24-item3-sample-output.png)

## S24 - Item (4/6) - Object Destructuring

1. Solution

   ![readme-images/sample-output1.png](readme-images/s24-item4-solution.png)

2. Sample Output

   ![readme-images/sample-output1.png](readme-images/s24-item4-sample-output.png)

## S24 - Item (5/6) - Arrow Functions

1. Solution

   ![readme-images/sample-output1.png](readme-images/s24-item5-solution.png)

2. Sample Output

   ![readme-images/sample-output1.png](readme-images/s24-item5-sample-output.png)

## S24 - Item (6/6) - JavaScript Classes

1. Solution

   ![readme-images/sample-output1.png](readme-images/s24-item6-solution.png)

2. Sample Output

   ![readme-images/sample-output1.png](readme-images/s24-item6-sample-output.png)

[Back to top](#session-answer-keys)

<a name="s25"></a>
# S25 - Introduction to JSON
## Sample Output

![image-output](readme-images/s25-sample-output.png)

## S25 - Item (1/3) - Introduction to JSON - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s25-sample-output2.png)

## S25 - Item (2/3) - Introduction to JSON - Object Instantiation

   1. Solution

      - The trainees must be able to resolve all errors using double quotes, commas, colons, square/curly brackets and values.

   2. Sample Output

      ![image-output](readme-images/s25-sample-output.png)

## S25 - Item (3/3) - Introduction to JSON - Syntax Error Correction

   1. Solution

      - No errors should show on the console signifying that all syntax errors were captured by the trainees

   2. Sample Output

      ![image-output](readme-images/s25-sample-output.png)

[Back to top](#session-answer-keys)

<a name="s26"></a>
# S26 - MongoDB - Database and NoSQL Introduction
## Sample Output

![image-output](readme-images/s26-sample-output.png)

## S26 - Item (1/1) - MongoDB - Database and NoSQL Introduction - Code Solution

   1. Solution
      - The students must be able to answer and submit the forms on time with a passing score of 75% for the total number of items provided in the quiz form.
      - The results of the form may be found in the downloaded excel file in the INT batch folder in where the form was duplicated.

   2. Sample Output

      ![image-output](readme-images/s26-sample-output.png)

[Back to top](#session-answer-keys)

<a name="s27"></a>
# S27 - MongoDB - Data Modeling and Translation
## Sample Output

![image-output](readme-images/s27-sample-output.png)
![image-output](readme-images/s27-sample-output2.png)

## S27 - Item (1/1) - MongoDB - Data Modeling and Translation - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s27-sample-output2.png)

## S27 - Item (1/1) - MongoDB - Data Modeling and Translation - ERD

   1. Solution
      - The students should be able to create an ERD similar to the screenshot provided below.

   2. Sample Output

      ![image-output](readme-images/s27-sample-output.png)

## S27 - Item (1/1) - MongoDB - Data Modeling and Translation - Data Testing

   1. Solution
      - The students must be able to create data samples based on the ERD by creating JSON format object in a text file as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s27-sample-output2.png)

[Back to top](#session-answer-keys)

<a name="s28"></a>
# S28 - MongoDB - CRUD Operations
## Sample Output

![image-output](readme-images/s28-sample-output.png)
![image-output](readme-images/s28-sample-output2.png)
![image-output](readme-images/s28-sample-output3.png)

## S28 - Item (1/3) - MongoDB - CRUD Operations - Code Solution

   1. Solution
      - The students should have a text file pushed in the repository containing the SQL syntax for the operations requested in the activity
      - A screenshot of the student's database is not required but would help in checking the activity

   2. Sample Output

      ![image-output](readme-images/s28-sample-output2.png)

## S28 - Item (2/3) - MongoDB - CRUD Operations - Code Solution

   1. Solution
      - The students should be able to add multiple room records that follow the columns and similar values to the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s28-sample-output2.png)

## S28 - Item (3/3) - MongoDB - CRUD Operations - Code Solution

   1. Solution
      - The students should be able to perform simple database queries as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s28-sample-output3.png)

[Back to top](#session-answer-keys)

<a name="s29"></a>
# S29 - MongoDB - Query Operators and Field Projection
## Sample Output

![image-output](readme-images/s29-sample-output.png)
![image-output](readme-images/s29-sample-output2.png)
![image-output](readme-images/s29-sample-output3.png)

## S29 - Item (1/4) - MongoDB - Query Operators and Field Projection - Code Solution

   1. Solution
      - The students should have a text file pushed in the repository containing the SQL syntax for the operations requested in the activity
      - A screenshot of the student's database is not required but would help in checking the activity

   2. Sample Output

      ![image-output](readme-images/s29-sample-output.png)

## S29 - Item (2/4) - MongoDB - Query Operators and Field Projection - SQL Query with ID exclusion

   1. Solution
      - All answers should have an SQL query based on complex criteria and not an exact record match
      - The SQL query should return users with an "s" in their first name or "d" in their last name
      - The "_id" field should not be displayed

   2. Sample Output

      ![image-output](readme-images/s29-sample-output4.png)

## S29 - Item (3/4) - MongoDB - Query Operators and Field Projection - SQL Query with inclusion

   1. Solution
      - The SQL query should return users who are from the HR department and their age is greater then or equal to 70

   2. Sample Output

      ![image-output](readme-images/s29-sample-output5.png)

[Back to top](#session-answer-keys)

## S29 - Item (4/4) - MongoDB - Query Operators and Field Projection - SQL Query

   1. Solution
      - The SQL query should return users with the letter 'e' in their first name and has an age of less than or equal to 30

   2. Sample Output

      ![image-output](readme-images/s29-sample-output6.png)

[Back to top](#session-answer-keys)

<a name="s30"></a>
# S30 - Aggregation in MongoDB and Query Case Studies
## Sample Output

![image-output](readme-images/s30-sample-output.png)
![image-output](readme-images/s30-sample-output2.png)
![image-output](readme-images/s30-sample-output3.png)
![image-output](readme-images/s30-sample-output4.png)
![image-output](readme-images/s30-sample-output5.png)

## S30 - Item (1/6) - Aggregation in MongoDB and Query Case Studies - Code Solution

   1. Solution
      - The students should have a text file pushed in the repository containing the SQL syntax for the operations requested in the activity
      - A screenshot of the student's database is not required but would help in checking the activity

   2. Sample Output

      ![image-output](readme-images/s30-sample-output.png)

## S30 - Item (2/6) - Aggregation in MongoDB and Query Case Studies - Count Aggregation

   1. Solution
      - All answers should be resolved using MongoDB Aggregation
      - The result should return the count of the total number of fruits on sale

   2. Sample Output

      ![image-output](readme-images/s30-sample-output.png)
      ![image-output](readme-images/s30-sample-output6.png)

## S30 - Item (3/6) - Aggregation in MongoDB and Query Case Studies - Count Aggregation

   1. Solution
      - The result should return the count of the fruits with stock of more than 20

   2. Sample Output

      ![image-output](readme-images/s30-sample-output2.png)
      ![image-output](readme-images/s30-sample-output7.png)

## S30 - Item (4/6) - Aggregation in MongoDB and Query Case Studies - Average Aggregation

   1. Solution
      - The result should return the average price of fruits on sale per supplier

   2. Sample Output

      ![image-output](readme-images/s30-sample-output3.png)
      ![image-output](readme-images/s30-sample-output8.png)

## S30 - Item (5/6) - Aggregation in MongoDB and Query Case Studies - Max Aggregation

   1. Solution
      - The result should return the highest price of a fruit per supplier

   2. Sample Output

      ![image-output](readme-images/s30-sample-output4.png)
      ![image-output](readme-images/s30-sample-output9.png)

## S30 - Item (6/6) - Aggregation in MongoDB and Query Case Studies - Min Aggregation

   1. Solution
      - The result should return the lowest price of a fruit per supplier

   2. Sample Output

      ![image-output](readme-images/s30-sample-output5.png)
      ![image-output](readme-images/s30-sample-output10.png)

[Back to top](#session-answer-keys)

<a name="s31"></a>
# S31 - Node.js Introduction
## Sample Output

![image-output](readme-images/s31-sample-output.png)
![image-output](readme-images/s31-sample-output2.png)
![image-output](readme-images/s31-sample-output3.png)

## S31 - Item (1/4) - Node.js Introduction - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s31-sample-output4.png)

## S31 - Item (2/4) - Node.js Introduction - Short Quiz

   1. Solution
      - The Git repository should contain a text file with the answers to the following questions provided in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s31-sample-output.png)

## S31 - Item (3/4) - Node.js Introduction - Login Route

   1. Solution
      - When the route "localhost:3000/login" is accessed it should return an output in the browser same as in the screenshot.

   2. Sample Output

      ![image-output](readme-images/s31-sample-output2.png)

## S31 - Item (4/4) - Node.js Introduction - Register Route

   1. Solution
      - When the route "localhost:3000/register" is accessed it should return an output in the browser same as in the screenshot.

   2. Sample Output

      ![image-output](readme-images/s31-sample-output3.png)

[Back to top](#session-answer-keys)

<a name="s32"></a>
# S32 - Node.js Routing w/ HTTP methods
## Sample Output

![image-output](readme-images/s32-sample-output.png)
![image-output](readme-images/s32-sample-output2.png)
![image-output](readme-images/s32-sample-output3.png)
![image-output](readme-images/s32-sample-output4.png)
![image-output](readme-images/s32-sample-output5.png)
![image-output](readme-images/s32-sample-output6.png)

## S32 - Item (1/7) - Node.js Routing w/ HTTP methods - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s32-sample-output7.png)

## S32 - Item (2/7) - Node.js Routing w/ HTTP methods - Base Route

   1. Solution
      - All routes should be available in the students' node JS/express JS application and is tested using Postman to send requests
      - The postman request should access the route provided and return a similar output as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s32-sample-output.png)

## S32 - Item (3/7) - Node.js Routing w/ HTTP methods - Profile Route

   1. Solution
      - The postman request should access the route provided and return a similar output as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s32-sample-output2.png)

## S32 - Item (4/7) - Node.js Routing w/ HTTP methods - Courses Route

   1. Solution
      - The postman request should access the route provided and return a similar output as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s32-sample-output3.png)

## S32 - Item (5/7) - Node.js Routing w/ HTTP methods - Add Course Route

   1. Solution
      - The postman request should access the route provided and return a similar output as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s32-sample-output4.png)

## S32 - Item (6/7) - Node.js Routing w/ HTTP methods - Update Course

   1. Solution
      - The postman request should access the route provided and return a similar output as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s32-sample-output5.png)

## S32 - Item (7/7) - Node.js Routing w/ HTTP methods - Delete/Archive Route

   1. Solution
      - The postman request should access the route provided and return a similar output as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s32-sample-output6.png)

[Back to top](#session-answer-keys)

<a name="s33"></a>
# S33 - Introduction to Postman and REST
## Sample Output

![image-output](readme-images/s33-sample-output.png)
![image-output](readme-images/s33-sample-output2.png)

## S33 - Item (1/7) - Introduction to Postman and REST - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s33-sample-output3.png)

## S33 - Item (2/7) - Introduction to Postman and REST - Retrieving All Record Titles

   1. Solution
      - This activity should be achieved using JSON placeholder, fetch requests and Postman
      - The postman request should access the GET route using REST API and return a similar output as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s33-sample-output.png)
      ![image-output](readme-images/s33-sample-output3.png)

## S33 - Item (3/7) - Node.js Routing w/ HTTP methods - Retrieving Task Titles and Status

   1. Solution
      - The postman request should access the GET route using REST API and return a similar output as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s33-sample-output2.png)
      ![image-output](readme-images/s33-sample-output4.png)

## S33 - Item (4/7) - Node.js Routing w/ HTTP methods - Adding a record

   1. Solution
      - The postman request should access the POST route using REST API and return a similar output as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s33-sample-output5.png)

## S33 - Item (5/7) - Node.js Routing w/ HTTP methods - Updating a record

   1. Solution
      - The postman request should access the PUT route using REST API and return a similar output as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s33-sample-output6.png)

## S33 - Item (6/7) - Node.js Routing w/ HTTP methods - Updating a record

   1. Solution
      - The postman request should access the PATCH route using REST API and return a similar output as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s33-sample-output7.png)

## S33 - Item (7/7) - Node.js Routing w/ HTTP methods - Deleting a record

   1. Solution
      - The postman request should access the DELETE route using REST API and return a similar output as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s33-sample-output8.png)

[Back to top](#session-answer-keys)

<a name="s34"></a>
# S34 - Introduction to Express.js
## Sample Output

![image-output](readme-images/s34-sample-output.png)
![image-output](readme-images/s34-sample-output2.png)
![image-output](readme-images/s34-sample-output3.png)

## S34 - Item (1/4) - Introduction to Express.js - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s34-sample-output4.png)

## S34 - Item (2/4) - Introduction to Express.js - Base Route

   1. Solution
      - This activity should be achieved using the discussion codebase to be modified by the students, REST API and Postman
      - The postman request should access the GET route using REST API and return a similar output as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s34-sample-output.png)
      ![image-output](readme-images/s34-sample-output4.png)

## S34 - Item (3/4) - Introduction to Express.js - Retrieving all users

   1. Solution
      - The postman request should access the GET route using REST API and return all users a similar output as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s34-sample-output2.png)
      ![image-output](readme-images/s34-sample-output5.png)

## S34 - Item (4/4) - Introduction to Express.js - Deleting a user

   1. Solution
      - The postman request should access the DELETE route using REST API and return a single user a similar output as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s34-sample-output3.png)
      ![image-output](readme-images/s34-sample-output6.png)
      ![image-output](readme-images/s34-sample-output7.png)

[Back to top](#session-answer-keys)

<a name="s35"></a>
# S35 - Data Persistence via Mongoose ODM
## Sample Output

![image-output](readme-images/s35-sample-output.png)
![image-output](readme-images/s35-sample-output2.png)

## S35 - Item (1/3) - Data Persistence via Mongoose ODM - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s35-sample-output3.png)

## S35 - Item (2/3) - Data Persistence via Mongoose ODM - Data Model

   1. Solution
      - This activity should be achieved using the discussion codebase to be modified by the students, REST API and Postman
      - The students must have a user model with the similar structure as the one provided below

   2. Sample Output

      ![image-output](readme-images/s35-sample-output3.png)

## S35 - Item (3/3) - Data Persistence via Mongoose ODM - Adding a user

   1. Solution
      - The postman request should access the POST route using REST API and return a similar output as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s35-sample-output.png)
      ![image-output](readme-images/s35-sample-output2.png)
      ![image-output](readme-images/s35-sample-output4.png)
      ![image-output](readme-images/s35-sample-output5.png)

[Back to top](#session-answer-keys)

<a name="s36"></a>
# S36 - Modules, Parameterized Routes
## Sample Output

![image-output](readme-images/s36-sample-output.png)
![image-output](readme-images/s36-sample-output2.png)

## S36 - Item (1/3) - Modules, Parameterized Routes - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s36-sample-output3.png)

## S36 - Item (2/3) - Modules, Parameterized Routes - Parametrized GET Route

   1. Solution
      - This activity should be achieved using the discussion codebase to be modified by the students, REST API and Postman
      - The students must have a user model with the similar structure as the one provided below

   2. Sample Output

      ![image-output](readme-images/s36-sample-output.png)
      ![image-output](readme-images/s36-sample-output3.png)

## S36 - Item (3/3) - Modules, Parameterized Routes - Parametrized UPDATE Route

   1. Solution
      - The postman request should access the POST route using REST API and return a similar output as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s36-sample-output2.png)
      ![image-output](readme-images/s36-sample-output4.png)

[Back to top](#session-answer-keys)

<a name="s37"></a>
# S37 - Booking System API - Business Use Case Translation to Model Design
## Sample Output

![image-output](readme-images/s37-sample-output.png)
![image-output](readme-images/s37-sample-output2.png)

## S37 - Item (1/3) - Booking System API - Business Use Case Translation to Model Design - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s37-sample-output.png)

## S37 - Item (2/3) - Booking System API - Business Use Case Translation to Model Design - User Model

   1. Solution
      - This activity should be achieved using the discussion codebase to be modified by the students, REST API and Postman
      - The students must have a user model with the similar structure as the one provided below

   2. Sample Output

      ![image-output](readme-images/s37-sample-output.png)
      ![image-output](readme-images/s37-sample-output2.png)

## S37 - Item (3/3) - Booking System API - Business Use Case Translation to Model Design - Folder Structure

   1. Solution
      - The user model should be properly saved inside the "models" folder under the file name "user.js" for proper separation of concerns following the MVC approach.

   2. Sample Output

      ![image-output](readme-images/s37-sample-output3.png)

[Back to top](#session-answer-keys)

<a name="s38"></a>
# S38 - Booking System API - User Registration, Authentication, and JWT's
## Sample Output

![image-output](readme-images/s38-sample-output.png)

## S38 - Item (1/4) - Booking System API - User Registration, Authentication, and JWT's - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s38-sample-output2.png)

## S38 - Item (2/4) - Booking System API - User Registration, Authentication, and JWT's - Details Route

   1. Solution
      - This activity should be achieved using the discussion codebase to be modified by the students, REST API and Postman
      - The students should have a route that's authenticated using JWTs and the ID should be extracted from the JWT with the similar output as below.

   2. Sample Output

      ![image-output](readme-images/s38-sample-output2.png)

## S38 - Item (3/4) - Booking System API - User Registration, Authentication, and JWT's - getProfile() Controller Method

   1. Solution
      - The students should have a controller method retrieves the user information using the id with the similar output as below.

   2. Sample Output

      ![image-output](readme-images/s38-sample-output3.png)
      ![image-output](readme-images/s38-sample-output.png)

## S38 - Item (4/4) - Booking System API - User Registration, Authentication, and JWT's - Folder Structure

   1. Solution
      - The user model should be properly saved inside the "controllers" folder under the file name "user.js" for proper separation of concerns following the MVC approach.

   2. Sample Output

      ![image-output](readme-images/s38-sample-output4.png)

[Back to top](#session-answer-keys)

<a name="s39"></a>
# S39 - Booking System API - Authorization via JWT, Retrieval of User Details
## Sample Output

![image-output](readme-images/s39-sample-output.png)

## S39 - Item (1/3) - Booking System API - Authorization via JWT, Retrieval of User Details - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s39-sample-output2.png)

## S39 - Item (2/3) - Booking System API - Authorization via JWT, Retrieval of User Details - Adding Courses Route

   1. Solution
      - This activity should be achieved using the discussion codebase to be modified by the students, REST API and Postman
      - The route for creating a user must be refactored to include JWT authentication as seen in the screenshot below
      - A new route must not be created and for creating a new course unless stated otherwise

   2. Sample Output

      ![image-output](readme-images/s39-sample-output2.png)

## S39 - Item (3/3) - Booking System API - Authorization via JWT, Retrieval of User Details - addCourse() Controller Method

   1. Solution
      - The controller function should include validation for the user as seen in the screenshot below.
      - A new controller method must not be created and for creating a new course unless stated otherwise

   2. Sample Output

      ![image-output](readme-images/s39-sample-output3.png)
      ![image-output](readme-images/s39-sample-output4.png)

[Back to top](#session-answer-keys)

<a name="s40"></a>
# S40 - Booking System API - Courses CRUD
## Sample Output

![image-output](readme-images/s40-sample-output.png)

## S40 - Item (1/3) - Booking System API - Courses CRUD - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s40-sample-output2.png)

## S40 - Item (2/3) - Booking System API - Courses CRUD - Archiving Courses Route

   1. Solution
      - This activity should be achieved using the discussion codebase to be modified by the students, REST API and Postman
      - The route for archiving a course must be refactored to include JWT authentication as seen in the screenshot below
      - A new route must not be created and for archiving course unless stated otherwise
      - The id of the user must be retrieved from the dynamic route and not passed as part of the request body

   2. Sample Output

      ![image-output](readme-images/s40-sample-output2.png)

## S40 - Item (3/3) - Booking System API - Courses CRUD - archiveCourse() Controller Method

   1. Solution
      - The controller function should update the course based on it's previous value and include either a validation or a not (!) operator as seen in the screenshot below.
      - The controller method should only process a "soft delete" and not a "hard delete" on the course
      - A new controller method must not be created and for creating a new course unless stated otherwise

   2. Sample Output

      ![image-output](readme-images/s40-sample-output3.png)
      ![image-output](readme-images/s40-sample-output.png)

[Back to top](#session-answer-keys)

<a name="s41"></a>
# S41 - Booking System API - User Course Enrollment, API Deployment
## Sample Output

![image-output](readme-images/s41-sample-output.png)

## S41 - Item (1/3) - Booking System API - User Course Enrollment, API Deployment - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s41-sample-output2.png)

## S41 - Item (2/3) - Booking System API - User Course Enrollment, API Deployment - Enroll User Route

   1. Solution
      - This activity should be achieved using the discussion codebase to be modified by the students, REST API and Postman
      - The route for enrolling a user to a course must be refactored to include JWT authentication as seen in the screenshot below
      - A new route must be created for enrolling a user
      - The id of the user and the course must be retrieved from the JWT and not passed as part of the request body
      - The route should be created under the user route for proper separation of concerns

   2. Sample Output

      ![image-output](readme-images/s41-sample-output2.png)

## S41 - Item (3/3) - Booking System API - User Course Enrollment, API Deployment - enroll() Controller Method

   1. Solution
      - The controller function should retrieve the user and store the id of the course under the "enrollments" array as seen in the screenshot below.
      - The controller function should retrieve the course and store the id of the user under the "enrollees" array as seen in the screenshot below.
      - A new controller method must not be created and for creating a new course unless stated otherwise
      - The controller should be created under the user controller for proper separation of concerns

   2. Sample Output

      ![image-output](readme-images/s41-sample-output3.png)
      ![image-output](readme-images/s41-sample-output4.png)
      ![image-output](readme-images/s41-sample-output5.png)

[Back to top](#session-answer-keys)

<a name="s42-s46"></a>
# S42-S46 - Capstone 2 - E-commerce API
## Sample Output

![image-output](readme-images/s42-s46-sample-output.png)

## S42-S46 - Item (1/3) - Capstone 2 - E-commerce API - Requirements

   1. Solution
      - The students' capstone outputs must contain the following features
         - User registration
         - User authentication
         - Set user as admin (Admin only)
         - Retrieve all active products
         - Retrieve single product
         - Create Product (Admin only)
         - Update Product information (Admin only)
         - Archive Product (Admin only)
         - Non-admin User checkout (Create Order)
         - Retrieve authenticated user’s orders
         - Retrieve all orders (Admin only)

   2. Sample Output

      - Refer to the sample hosted [S42-S46 capstone 2 project and the postman collection file](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5b/-/tree/master/backend/s42-46) found in the materials for reference on the output

## S42-S46 - Item (2/3) - Capstone 2 - E-commerce API - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      - Refer to the sample hosted [S42-S46 capstone 2 project and the postman collection file](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5b/-/tree/master/backend/s42-46) found in the materials for reference on the output

## S42-S46 - Item (3/3) - Capstone 2 - E-commerce API - Folder Structure

   1. Solution
      - The project must be hosted in Heroku and should follow the MVC design pattern in creation of the application
      - Failure to host the capstone project without a valid reason will result in a failure
      - Inclusion of the postman collection is not required but is recommended for easier checking

   2. Sample Output

      - Refer to the sample hosted [S42-S46 capstone 2 project and the postman collection file](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5b/-/tree/master/backend/s42-46) found in the materials for reference on the output

[Back to top](#session-answer-keys)

<a name="s47"></a>
# S47 - JavaScript - DOM Manipulation
## Sample Output

![image-output](readme-images/s47-sample-output.png)
![image-output](readme-images/s47-sample-output2.png)

## S47 - Item (1/3) - JavaScript - DOM Manipulation - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s47-sample-output3.png)

## S47 - Item (2/3) - JavaScript - DOM Manipulation - index.js File

   1. Solution
      - The text output should update on each keyup of the user

   2. Sample Output

      ![image-output](readme-images/s47-sample-output3.png)

## S47 - Item (3/3) - JavaScript - DOM Manipulation - index.html File

   1. Solution
      - HTML elements should be targeted correctly using the "id" attribute and the user of proper naming convention for the ids should be observed

   2. Sample Output

      ![image-output](readme-images/s47-sample-output4.png)

[Back to top](#session-answer-keys)

<a name="s48"></a>
# S48 - JavaScript - Reactive DOM with JSON
## Sample Output

![image-output](readme-images/s48-sample-output.png)
![image-output](readme-images/s48-sample-output2.png)

## S48 - Item (1/3) - JavaScript - Reactive DOM with JSON - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s48-sample-output3.png)

## S48 - Item (2/3) - JavaScript - Reactive DOM with JSON - index.js File

   1. Solution
      - Upon clicking the "delete" button the DOM should be updated to remove the post as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s48-sample-output3.png)
      ![image-output](readme-images/s48-sample-output.png)
      ![image-output](readme-images/s48-sample-output2.png)

## S48 - Item (3/3) - JavaScript - Reactive DOM with JSON - index.html File

   1. Solution
      - HTML elements should be targeted correctly using the "id" attribute and the user of proper naming convention for the ids should be observed

   2. Sample Output

      ![image-output](readme-images/s48-sample-output4.png)

[Back to top](#session-answer-keys)

<a name="s49"></a>
# S49 - JavaScript - Reactive DOM with Fetch
## Sample Output

![image-output](readme-images/s49-sample-output.png)
![image-output](readme-images/s49-sample-output2.png)

## S49 - Item (1/3) - JavaScript - Reactive DOM with JSON - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s49-sample-output3.png)

## S49 - Item (2/3) - JavaScript - Reactive DOM with JSON - index.js File

   1. Solution
      - Upon clicking the "delete" button the DOM should be updated to remove the post as shown in the screenshot below.

   2. Sample Output

      ![image-output](readme-images/s49-sample-output3.png)
      ![image-output](readme-images/s49-sample-output.png)
      ![image-output](readme-images/s49-sample-output2.png)

## S49 - Item (3/3) - JavaScript - Reactive DOM with JSON - index.html File

   1. Solution
      - HTML elements should be targeted correctly using the "id" attribute and the user of proper naming convention for the ids should be observed

   2. Sample Output

      ![image-output](readme-images/s49-sample-output4.png)

[Back to top](#session-answer-keys)

<a name="s50"></a>
# S50 - React.js - Component-Driven Development
## Sample Output

![readme-images/sample-output1.png](readme-images/s50-sample-output.png)

## S50 - Item (1/3) - React.js - Component-Driven Development - Code Solution

1. Solution

  - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
  - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
  - The values printed in the console must be coming from variables and not hard coded values
  - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
  - Proper indentation must also be observed. A few misindented lines of code may be considered.

2. Sample Output

![readme-images/sample-output1.png](readme-images/s50-item1-solution.png)
![readme-images/sample-output1.png](readme-images/s50-item1-sample-output.png)

## S50 - Item (2/3) - React.js - Component-Driven Development - Component Creation

1. Solution

  - The "card" and "button" components must be imported from "react-bootstrap"
  - The different "card" components (e.g. Card.Title, Card.Subtitle, Card.text) must be used properly for each item in the card.

2. Sample Output

![readme-images/sample-output1.png](readme-images/s50-item1-solution.png)
![readme-images/sample-output1.png](readme-images/s50-item1-sample-output.png)

## S50 - Item (3/3) - React.js - Component-Driven Development - Rendering Components

1. Solution

  - The "CourseCard" component should be imported in the "Home" component.
  - The "CourseCard" component should be rendered properly as part of the "Home" component and as an individual component

2. Sample Output

  ![readme-images/sample-output1.png](readme-images/s50-item2-solution.png)
  ![readme-images/sample-output1.png](readme-images/s50-item2-sample-output.png)

[Back to top](#session-answer-keys)

<a name="s51"></a>
# S51 - React.js - Props and States
## Sample Output

![image-output](readme-images/s51-sample-output.png)
![image-output](readme-images/s51-sample-output2.png)

## S51 - Item (1/4) - React.js - Props and States - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s51-sample-output3.png)

## S51 - Item (2/4) - React.js - Props and States - States

   1. Solution
      - A separate state must be created for the seats per course as shown in the screenshot below

   2. Sample Output

      ![image-output](readme-images/s51-sample-output3.png)

## S51 - Item (3/4) - React.js - Props and States - enroll() Function

   1. Solution
      - An "enroll" function must be created separately that will increase the count of enrollees and reduce the amount of seats as shown in the screenshot below
      - A prompt will appear once the value of seats are reached

   2. Sample Output

      ![image-output](readme-images/s51-sample-output4.png)
      ![image-output](readme-images/s51-sample-output.png)
      ![image-output](readme-images/s51-sample-output2.png)

## S51 - Item (4/4) - React.js - Props and States - Props

   1. Solution
      - The function must be properly passed to each component via props

   2. Sample Output

      ![image-output](readme-images/s51-sample-output5.png)

[Back to top](#session-answer-keys)

<a name="s52"></a>
# S52 - React.js - Effects, Events and Forms
## Sample Output

![image-output](readme-images/s52-sample-output.png)
![image-output](readme-images/s52-sample-output2.png)
![image-output](readme-images/s52-sample-output3.png)

## S52 - Item (1/4) - React.js - Effects, Events and Forms - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s52-sample-output4.png)

## S52 - Item (2/4) - React.js - Effects, Events and Forms - Login Page

   1. Solution
      - A separate component must be created for the login page
      - State hooks should be defined for each input field and a state for conditionally rendering the button
      - The states must be binded correctly to the appropriate input fields

   2. Sample Output

      ![image-output](readme-images/s52-sample-output4.png)
      ![image-output](readme-images/s52-sample-output6.png)
      ![image-output](readme-images/s52-sample-output7.png)

## S52 - Item (3/4) - React.js - Effects, Events and Forms - Rendering Components

   1. Solution
      - The Login component must be successfully rendered with no errors
      - The button must be dynamically rendered based on the isActive state

   2. Sample Output

      ![image-output](readme-images/s52-sample-output5.png)
      ![image-output](readme-images/s52-sample-output9.png)
      ![image-output](readme-images/s52-sample-output.png)
      ![image-output](readme-images/s52-sample-output2.png)

## S52 - Item (4/4) - React.js - Effects, Events and Forms - useEffect and authenticate() function

   1. Solution
      - A useEffect must be used for validating the email and the password
      - A function must be created for authenticating the user input

   2. Sample Output

      ![image-output](readme-images/s52-sample-output8.png)
      ![image-output](readme-images/s52-sample-output10.png)

[Back to top](#session-answer-keys)

<a name="s53"></a>
# S53 - React.js - Routing and Conditional Rendering
## Sample Output

![image-output](readme-images/s53-sample-output.png)

## S53 - Item (1/4) - React.js - Routing and Conditional Rendering - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s53-sample-output2.png)

## S53 - Item (2/4) - React.js - Routing and Conditional Rendering - Error Page

   1. Solution
      - A separate component must be created for the error page

   2. Sample Output

      ![image-output](readme-images/s53-sample-output2.png)

## S53 - Item (3/4) - React.js - Routing and Conditional Rendering - Rendering Components

   1. Solution
      - The Error page must be successfully routed and rendered to all other routes not defined

   2. Sample Output

      ![image-output](readme-images/s53-sample-output3.png)
      ![image-output](readme-images/s53-sample-output.png)

## S53 - Item (4/4) - React.js - Routing and Conditional Rendering - Banner Component

   1. Solution
      - The banner component must be a refactored to become a reusable component as seen in the screenshots below

   2. Sample Output

      ![image-output](readme-images/s53-sample-output4.png)
      ![image-output](readme-images/s53-sample-output5.png)

[Back to top](#session-answer-keys)

<a name="s54"></a>
# S54 - React.js - App State Management
## Sample Output

![image-output](readme-images/s54-sample-output.png)
![image-output](readme-images/s54-sample-output2.png)

## S54 - Item (1/3) - React.js - App State Management - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s54-sample-output3.png)

## S54 - Item (2/3) - React.js - App State Management - Page Redirection

   1. Solution
      - The user must be redirected to the courses page if they are currently logged in as shown in the screenshots below

   2. Sample Output

      ![image-output](readme-images/s54-sample-output4.png)
      ![image-output](readme-images/s54-sample-output.png)
      ![image-output](readme-images/s54-sample-output2.png)

## S54 - Item (3/3) - React.js - App State Management - useContext

   1. Solution
      - The Context API must be used in order to retrieve the user details that will redirect the user

   2. Sample Output

      ![image-output](readme-images/s54-sample-output3.png)

[Back to top](#session-answer-keys)

<a name="s55"></a>
# S55 - React.js - API Integration with Fetch
## Sample Output

![image-output](readme-images/s55-sample-output.png)
![image-output](readme-images/s55-sample-output2.png)
![image-output](readme-images/s55-sample-output3.png)
![image-output](readme-images/s55-sample-output4.png)

## S55 - Item (1/3) - React.js - API Integration with Fetch - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s55-sample-output5.png)

## S55 - Item (2/3) - React.js - API Integration with Fetch - Form

   1. Solution
      - The Register page must be refactored to include additional form inputs and their respective states

   2. Sample Output

      ![image-output](readme-images/s55-sample-output5.png)
      ![image-output](readme-images/s55-sample-output6.png)
      ![image-output](readme-images/s55-sample-output.png)

## S55 - Item (3/3) - React.js - API Integration with Fetch - Form Handling

   1. Solution
      - A useEffect must be created to handle the validation of the form inputs
      - The registerUser() function must be refactored to interact with the backend database via a Fetch request to check if the user already exists and to register the user if no duplicate is found
      - An appropriate alert should be shown based on the output of the fetch requests as shown in the screenshots below

   2. Sample Output

      ![image-output](readme-images/s55-sample-output7.png)
      ![image-output](readme-images/s55-sample-output8.png)
      ![image-output](readme-images/s55-sample-output9.png)
      ![image-output](readme-images/s55-sample-output3.png)
      ![image-output](readme-images/s55-sample-output4.png)

[Back to top](#session-answer-keys)

<a name="s56"></a>
# S56 - Mock Technical Exam (Concepts and Theory + Function Coding)
## Sample Output

![image-output](readme-images/s56-sample-output.png)

## S56 - Item (1/3) - Mock Technical Exam (Concepts and Theory + Function Coding) - Quiz Form

   1. Solution
      - 11/15 questions of the Mock Technical Exams quiz form should be correct in order for the students' to gain a passing score.

   2. Sample Output

      - Refer to the [Google slide](https://docs.google.com/presentation/d/1RAIMvqq3fbTDm5IvVi98JUbP7DuKM5EDY0DCTs8DhU8/edit#slide=id.g53aad6d9a4_0_1443) presentation to refer to the solution set for the Mock Technical Exam

## S56 - Item (2/3) - Mock Technical Exam (Concepts and Theory + Function Coding) - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s56-sample-output.png)

## S56 - Item (3/3) - Mock Technical Exam (Concepts and Theory + Function Coding) - Coding solution

   1. Solution
      - 6/8 questions of the Mock Technical Exams should be correct in order for the students' to gain a passing score.
      - Both the quiz form and the functional coding exam should both have a passing score to be considered a grade of "P"

   2. Sample Output

      - Refer to the [Google slide](https://docs.google.com/presentation/d/1RAIMvqq3fbTDm5IvVi98JUbP7DuKM5EDY0DCTs8DhU8/edit#slide=id.g12ab51eba82_0_401) presentation to refer to the solution set for the Mock Technical Exam

[Back to top](#session-answer-keys)

<a name="s57"></a>
# S57 - Mock Technical Exam (Debugging & Code Tracing + Data Structures & Algorithms)
## Sample Output

![image-output](readme-images/s57-sample-output.png)
![image-output](readme-images/s57-sample-output2.png)

## S57 - Item (1/3) - Mock Technical Exam (Debugging & Code Tracing + Data Structures & Algorithms) - Debugging & Code Tracing - Quiz Form

   1. Solution
      - 6/8 questions of the Mock Technical Exams quiz form should be correct in order for the students' to gain a passing score.

   2. Sample Output

      - Refer to the [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5b/-/tree/master/fullstack/s57) to refer to the solution set for the Mock Technical Exam

## S57 - Item (2/3) - Mock Technical Exam (Debugging & Code Tracing + Data Structures & Algorithms) - Code Solution

   1. Solution
      - The students must use the "let"/"const" keyword for declaring variables and not the "var" keyword
      - Proper and camel cased names for variables must be used that's descriptive of the data stored in them
      - The values printed in the console must be coming from variables and not hard coded values
      - Semi-colons (;) must be properly placed. Ommission of a few semi-colons are acceptable.
      - Proper indentation must also be observed. A few misindented lines of code may be considered.

   2. Sample Output

      ![image-output](readme-images/s57-sample-output2.png)

## S57 - Item (3/3) - Mock Technical Exam (Concepts and Theory + Function Coding) - Data Structures & Algorithms - Function Coding

   1. Solution
      - Both "queue" and "dequeue" functions should work in order to pass the function coding exam
      - Both the quiz form and the functional coding exam should both have a passing score to be considered a grade of "P"

   2. Sample Output

      ![image-output](readme-images/s57-sample-output2.png)
      - Refer to the [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5b/-/tree/master/fullstack/s57) to refer to the solution set for the Mock Technical Exam

[Back to top](#session-answer-keys)

<a name="s58"></a>
# S58 - Wireframes, Mockups and Prototypes
## Sample Output

![image-output](readme-images/s58-sample-output.png)

## S58 - Item (1/3) - Wireframes, Mockups and Prototypes - Login Page

   1. Solution
      - All pages should have an associated image file pushed in the GitLab/GitHub repository
      - All pages should also have a mobile and desktop view wireframe available
      - The login page should contain a form for logging in the user
      - A wireframe, a mockup or a prototype could also be accepted as a valid output for this activity

   2. Sample Output

      ![image-output](readme-images/s58-sample-output.png)

## S58 - Item (2/3) - Wireframes, Mockups and Prototypes - Transactions Overview Page

   1. Solution
      - The Transactions Overview page should view all details regarding a transaction preferrably a table or a similar approach
      - The wireframe should also contain a page which shows viewing of individual transactions

   2. Sample Output

      ![image-output](readme-images/s58-sample-output.png)

## S58 - Item (3/3) - Wireframes, Mockups and Prototypes - Add income/expense page

   1. Solution
      - The income/expense page should show a wireframe that displays various user inputs for keeping track of income/expense transactions

   2. Sample Output

      ![image-output](readme-images/s58-sample-output.png)

[Back to top](#session-answer-keys)

<a name="s59-s64"></a>
# S59-S64 - Capstone 3 Development
## Sample Output

![image-output](readme-images/s59-s64-sample-output.png)
![image-output](readme-images/s59-s64-sample-output2.png)
![image-output](readme-images/s59-s64-sample-output3.png)
![image-output](readme-images/s59-s64-sample-output4.png)
![image-output](readme-images/s59-s64-sample-output5.png)

## S59-S64 - Item (1/5) - Capstone 3 Development - Register Page Requirements

   1. Solution
      - A Registration Page with proper form validation (all fields must be filled and passwords must match) that must redirect the user to the login page once successfully submitted.

   2. Sample Output

      ![image-output](readme-images/s59-s64-sample-output.png)

## S59-S64 - Item (2/5) - Capstone 3 Development - Login Page Requirements

   1. Solution
      - A Login Page that must redirect the user to the either the home page or the products catalog once successfully authenticated

   2. Sample Output

      ![image-output](readme-images/s59-s64-sample-output2.png)

## S59-S64 - Item (3/5) - Capstone 3 Development - Cart View Requirements

   1. Solution
      - A Cart View page with the following features:
         - Show all items the user has added to their cart (and their quantities)
         - Change product quantities
         - Remove products from cart
         - Subtotal for each item
         - Total price for all items
         - A working checkout button/functionality.
         - When the user checks their cart out, redirect them to either the homepage or the Order History page

   2. Sample Output

      ![image-output](readme-images/s59-s64-sample-output3.png)
      ![image-output](readme-images/s59-s64-sample-output4.png)

## S59-S64 - Item (4/5) - Capstone 3 Development - Admin Panel/Dashboard Requirements

   1. Solution
      - An Admin Panel/Dashboard with the following features:
         - Retrieve list of all products (available or unavailable)
         - Create new product
         - Update product information
         - Deactivate/reactivate product

   2. Sample Output

      ![image-output](readme-images/s59-s64-sample-output5.png)

## S59-S64 - Item (5/5) - Capstone 3 Development - Other Requirements

   1. Solution
      - A fully-functioning Navbar with proper dynamic rendering (Register/Login links for users not logged in, Logout link for users who are, etc)
      - App must be single-page and utilize proper routing (no navigating to another page/reloading)
      - Registration/Login pages must be inaccessible to users who are logged-in
      - Apart from users who are not logged-in, Admin must not be able to add products to their cart
      - Do not create a website other than the required e-commerce app
      - Do not use templates found in other sites or existing premade NPM packages that replicate a required feature

   2. Sample Output

[Back to top](#session-answer-keys)