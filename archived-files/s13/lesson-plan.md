# WDC028 - S11 - Bootstrap - Flex

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1B7yHJ5du_QzCaS3BNfRyXhXosDegBAPuxjrbF8NT7lI/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1B7yHJ5du_QzCaS3BNfRyXhXosDegBAPuxjrbF8NT7lI/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5/s10) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/frontend/s10/Manual.html) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                             | Link                                                         |
| --------------------------------- | ------------------------------------------------------------ |
| Bootstrap Text Utility Classes    | [Link](https://getbootstrap.com/docs/4.6/utilities/text/)    |
| Boostrap Color Utility Classes    | [Link](https://getbootstrap.com/docs/4.6/utilities/colors/)  |
| Bootstrap Spacing Utility Classes | [Link](https://getbootstrap.com/docs/4.6/utilities/spacing/) |
| CSS display Property              | [Link](https://www.w3schools.com/cssref/pr_class_display.asp) |
| CSS Flexbox                       | [Link](https://www.w3schools.com/css/css3_flexbox.asp)       |
| Bootstrap Flex Classes            | [Link](https://getbootstrap.com/docs/4.0/utilities/flex/)    |
| CSS justify-content Property      | [Link](https://www.w3schools.com/cssref/css3_pr_justify-content.asp) |
| CSS align-items Property          | [Link](https://www.w3schools.com/cssref/css3_pr_align-items.asp) |
| CSS flex-direction Property       | [Link](https://www.w3schools.com/cssref/css3_pr_flex-direction.asp) |
| Bootstrap CSS Framework           | [Link](https://getbootstrap.com/)                            |
| Introduction to Bootstrap 4.6     | [Link](https://getbootstrap.com/docs/4.6/getting-started/introduction/) |
| Bootstrap local vs. CDN           | [Link](https://www.belugacdn.com/bootstrap-local-vs-cdn/)    |
| Responsive Meta Tag               | [Link](https://css-tricks.com/snippets/html/responsive-meta-tag/) |
| Bootstrap Containers              | [Link](https://getbootstrap.com/docs/4.6/layout/overview/#containers) |
| Bootstrap Grid System classes     | [Link](https://getbootstrap.com/docs/4.0/layout/grid/)       |
| Bootstrap Image Utility Classes   | [Link](https://getbootstrap.com/docs/4.6/content/images/)    |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1. [10 mins] - CSS Flex Properties
	2. [1 hr 50 mins] - CSS and Bootstrap Flexbox
	3. [1 hr] - Bootstrap Flex Utilities
		- d-flex
		- justify-content
		- align-items
		- flex-column
		- flex-row
	4. [1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

​	

[Back to top](#table-of-contents)