# Session Objectives

At the end of the session, the students are expected to:

- quickly manage the one-directional layout of elements using flexbox utilities from Bootstrap.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5/s10)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1B7yHJ5du_QzCaS3BNfRyXhXosDegBAPuxjrbF8NT7lI)
- [Lesson Plan](lesson-plan.md)

## Supplemental Materials

- [Basic Concepts of Flexbox (MDN Web Docs)](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Basic_Concepts_of_Flexbox)
- [A Complete Guide to Flexbox (CSS Tricks)](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
- [Bootstrap 4.6 Flex](https://getbootstrap.com/docs/4.6/utilities/flex/)

# Lesson Proper

## Flexbox

It is a powerful one-directional layout model that allows modification to the layout and spacing of a group of elements.

This model is in contrast to the grid layout where columns and rows are managed together.

In a flexbox, elements can be laid out either horizontally (row) or vertically (column), also known as the two axes of flexbox.

![readme-images/Untitled.png](readme-images/Untitled.png)

Horizontal flexbox layout (from MDN Web Docs).

![readme-images/Untitled%201.png](readme-images/Untitled%201.png)

Vertical flexbox layout (from MDN Web Docs).

## Flex Utility Classes

The main difference of using Bootstrap's flex utility classes is that we will not need to declare our own styles by creating a CSS file. Instead, classes will be used and added to elements to apply certain flexbox styling.

# Code Discussion

## Folder and File Preparation

Create a folder named **s10**, a folder named **discussion** inside the **s10** folder, then a file named **index.html** inside the **discussion** folder.

## Preliminary Code

Add the following initial code to the **index.html** file:

```html
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"/>
        <title>Bootstrap Flex</title>
    </head>
    <body>
        <div class="container-fluid">
            <h1>Bootstrap Flex</h1>
            <p>Use the d-flex class to create a flex container and transform direct children into flex items.</p>
            <div class="d-flex bg-dark p-3">  
                <div class="p-2 bg-success">Flex Item A</div>
                <div class="p-2 bg-warning">Flex Item B</div>
                <div class="p-2 bg-danger">Flex Item C</div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    </body>
</html>
```

## Code Sample

Explore the flex classes as demonstrated in the Bootstrap [documentation](https://getbootstrap.com/docs/4.6/utilities/flex).

# GitLab Upload

After finishing the code discussion, demonstrate to the students how to create a GitLab project inside their subgroup and push the code discussion with a commit message of "Add discussion code".

# Activity

## Resources

[images.zip](readme-images/images.zip)

Instruct the students to put the images inside the activity folder.

## Instructions

- Create a Tools section.
- Add the provided images in a `div` container.
- Set the width attribute of the images to 80.
- Using flexbox classes, ensure that:
    - images are centered on the screen; and
    - when in small screens, wrap the images so that it will not horizontally overflow to the right side of the screen.

## Expected Output

![readme-images/Untitled%202.png](readme-images/Untitled%202.png)

Sample output in a large screen.

![readme-images/Untitled%203.png](readme-images/Untitled%203.png)

Sample output in a small screen (Pixel 2 XL).

## Solution

The solution may vary from student to student, but the code may look like this:

```html
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"/>
        <title>Activity: Bootstrap Introduction and Simple Styles</title>
    </head>
    <body>
        <div class="p-4 container-fluid">
            <div class="text-center mb-4">
                <img class="rounded-circle mb-3" width="200" src="https://i.guim.co.uk/img/media/3656ae6ea2209d4561caf04fa9f172a519908ca3/0_28_2318_1391/master/2318.jpg?width=1200&height=1200&quality=85&auto=format&fit=crop&s=afcc355f47876bc495cdc3c902639bae"/>
                <h2>Ada Lovelace</h2>
                <h4>Full-Stack Web Developer</h4>
            </div>
            <p class="text-justify">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            </p>
            <h3 class="text-center">Projects</h3>
            <div class="row mt-3 mb-4">
                <div class="col-sm-12 col-md-6 order-2 order-md-1">
                    <h5 class="text-center pt-3">Web API (Ecommerce)</h5>
                    <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                <div class="col-sm-12 col-md-6 order-1 order-md-2">
                    <div class="text-center">
                        <img class="img-fluid" src="https://www.cloudways.com/blog/wp-content/uploads/Rest-API-introduction.jpg"/>
                    </div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-sm-12 col-md-6 order-2 order-md-2">
                    <h5 class="text-center pt-3">React Frontend (Ecommerce)</h5>
                    <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                <div class="col-sm-12 col-md-6 order-1 order-md-1">
                    <div class="text-center">
                        <img class="img-fluid" src="https://d6vdma9166ldh.cloudfront.net/media/images/1455b5c8-4887-43a8-8214-de77543414c9.jpg"/>
                    </div>
                </div>
            </div>
            <h3 class="text-center">Tools</h3>
            <div class="d-flex flex-wrap justify-content-center mb-4">
                <img width="80" src="images/logo-html5.png"/>
                <img width="80" src="images/logo-css3.png"/>
                <img width="80" src="images/logo-javascript.png"/>
                <img width="80" src="images/logo-mongodb.png"/>
                <img width="80" src="images/logo-nodejs.png"/>
                <img width="80" src="images/logo-expressjs.png"/>
                <img width="80" src="images/logo-react.png"/>
                <img width="80" src="images/logo-heroku.png"/>
                <img width="80" src="images/logo-git.png"/>
                <img width="80" src="images/logo-sublime-text-3.png"/>
                <img width="80" src="images/logo-postman.png"/>
                <img width="80" src="images/logo-gitlab-ci-cd.png"/>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    </body>
</html>
```