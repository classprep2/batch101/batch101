# Session Objectives

At the end of the session, the students are expected to:

- Learn how to create arrays
- Learn how to access array items via its indices
- Learn how to get the total number of items in an array.
- Learn how to create and access multi-dimensional arrays.


# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5/s17)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1YTXxYplzWk1Bc58SIlNiTQuifNVyiO6cNrz5TZnvbGA/edit#slide=id.g1214a79fbb7_0_1)

## Supplemental Materials

- [JavaScript Arrays (w3schools)](https://www.w3schools.com/js/js_arrays.asp)
- [JavaScript Array (MDN Web Docs)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)

# Lesson Proper

## Introduction to Arrays

So far we have explored storing values on **individual** variables. However, modelling real-world information is a bit more **complicated** than that. 

Consider a scenario where we need to store values that are related to each other, for example a list of student numbers from a graduation class. To accomplish that with what we currently know, we are going to create a different variable to hold each student number.

It is not only **cumbersome** to write, it is also **hard to manage** these sets of values. To solve this problem, all programming languages have implemented what is called an **array**.

An array in programming is simply a list of data. Let's write the example earlier:

```jsx
let studentNumberA = '2020-1923';
let studentNumberB = '2020-1924';
let studentNumberC = '2020-1925';
let studentNumberD = '2020-1926';
let studentNumberE = '2020-1927';
```

Now, with an array, we can simply write the code above like this:

```jsx
let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'];
```

We need arrays because it lets us **store multiple values** into a **single variable**.

[https://avantutor.com/blog/arrays/](https://avantutor.com/blog/arrays/)

# Code Discussion

## Folder and File Preparation

Create a folder named **s21**, a folder named **discussion** inside the **s21** folder, then a file named **index.html** and **index.js** inside the **discussion** folder.

## Basic Array Structure

### Arrays

- Arrays are used to store multiple related values in a single variable
- They are declared using square brackets ([]) also known as "Array Literals"
- Commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks
- Arrays also provide access to a number of functions/methods that help in achieving this
- A method is another term for functions associated with an object and is used to execute statements that are relevant to a specific object
- Majority of methods are used to manipulate information stored within the same object
- Arrays are also objects which is another data type
- The main difference of arrays with an object is that it contains information in a form of a "list" unlike objects which uses "properties"
- Syntax
    let/const arrayName = [elementA, elementB, ElementC...]

```jsx
    let grades = [98.5, 94.3, 89.2, 90.1];
    let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];
```

Additionally, each data stored in an array is called an element.

It is **not recommended** to have an array with **different data types**.

```jsx
    let mixedArr = [12, 'Asus', null, undefined, {}]; // This is not recommended.
```

This is because an array, by definition, should be a collection of **related** data.

Each element of an array can also be written in a separate line.

```jsx
    let tasks = [
        'drink html',
        'eat javascript',
        'inhale css',
        'bake sass'
    ];
```
You can also store the values of separate variables in an array

```js
    //Creating an array with values from variables:
    let city1 = "Tokyo";
    let city2 = "Manila";
    let city3 = "Jakarta";

    let cities = [city1,city2,city3];

    console.log(myTasks);
    console.log(cities);
````

## Array length property

The .length property allows us to get and set the total number of items in an array.

```js
    console.log(myTasks.length); 
    console.log(cities.length);

    let blankArr = [];
    console.log(blankArr.length); //results to 0 because the array is empty.
````

length property can also be used with strings. Some array methods and properties can also be used with strings.

```js
    let fullname = "Jamie Noble";
    console.log(fullname.length);
```

length property on strings shows the number of characters in a string. Spaces are counted as characters in strings.

length property can also set the total number of items in an array, meaning we can actually delete the last item in the array or shorten the array by simply updating the length property of an array.

```js
    myTasks.length = myTasks.length-1;
    console.log(myTasks.length);
    console.log(myTasks);
```

To delete a specific item in an array we can employ array methods (which will be shown in the next session) or an algorithm (set of code to process tasks).

Another example using decrementation:
```js
    cities.length--;
    console.log(cities);
```
We can't do the same on strings, however.
```js
    username.length = username.length-1;
    console.log(username.length);
    username.length--;
    console.log(username);
```
If you can shorten the array by setting the length property, you can also lengthen it by adding a number into the length property. Since we lengthen the array forcibly, there will be another item in the array, however, it will be empty or undefined. Like adding another seat but having no one to sit on it.

```js
    let theBeatles = ["John","Paul","Ringo","George"];
    theBeatles.length++
    console.log(theBeatles);
```
## Accessing Elements of an Array

- Accessing array elements is one of the more common tasks that we do with an array
- This can be done through the use of array indexes
- Each element in an array is associated with it's own index/number
- In JavaScript, the first element is associated with the number 0 and increasing this number by 1 for every element
- The reason an array starts with 0 is due to how the language is designed
- Syntax
        arrayName[index];

![Array Visualization](readme-images/Untitled.png)

```js
    console.log(grades[0]);
    console.log(computerBrands[3]);
    // Accessing an array element that does not exist will return "undefined"
    console.log(grades[20]);

    let lakersLegends = ["Kobe","Shaq","LeBron","Magic","Kareem"];
    //Access the second item in the array:
    console.log(lakersLegends[1]);//Shaq
    //Access the fourth item in the array:
    console.log(lakersLegends[3]);//Magic
````

You can also save/store array items in another variable:
```js
    let currentLaker = lakersLegends[2];
    console.log(currentLaker);
````

You can also reassign array values using the items' indices
```js
    console.log('Array before reassignment');
    console.log(lakersLegends);
    lakersLegends[2] = "Pau Gasol";
    console.log('Array after reassignment');
    console.log(lakersLegends);
```
#### Accessing the last element of an array

Since the first element of an array starts at 0, subtracting 1 to the length of an array will offset the value by one allowing us to get the last element.

```js
    let bullsLegends = ["Jordan","Pippen","Rodman","Rose","Kukoc"];

    let lastElementIndex = bullsLegends.length - 1;

    console.log(bullsLegends[lastElementIndex]);

    //You can also add it directly:
    console.log(bullsLegends[bullsLegends.length-1]);
```

#### Adding Items into the Array

Using indices, you can also add items into the array.
```js
let newArr = [];
console.log(newArr[0]);
```
newArr[0] is undefined because we haven't yet defined the item/data for that index, we can update the index with a new value:
```js
newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Tifa Lockhart";
console.log(newArr);
```

For now, we can add items at the end of the array. Instead of adding it in the front to avoid the risk of replacing the first items in the array:
```js
//newArr[newArr.length-1] = "Aerith Gainsborough"; - will overwrite the last item instead.
newArr[newArr.length] = "Barrett Wallace";
console.log(newArr);
```
There is an array method which will be discussed in the next session which will allow us to add items at the start of the array.

#### Looping over an Array**
- You can use a for loop to iterate over all items in an array.
- Set the counter as the index and set a condition that as long as the current index iterated is less than the length of the array, we will run the condition. It is set this way because the index of an array starts at 0.

```js
    for(let index = 0; index < newArr.length; index++){

        //You can use the loop counter as index to be able to show each array items in a console log.
        console.log(newArr[index]);

    }
```
Given an array of numbers, you can also show if the following items in the array are divisible by 5 or not. You can mix in an if-else statement in the loop:

```js
    let numArr = [5,12,30,46,40];

    for(let index = 0; index < numArr.length; index++){

        if(numArr[index] % 5 === 0){

            console.log(numArr[index] + " is divisible by 5");

        } else {

            console.log(numArr[index] + " is not divisible by 5");

        }

    }
```
**Important Note**:
- Array indexes actually refer to an address/location in the device's memory and how the information is stored
- Example array location in memory
    Array address: 0x7ffe9472bad0
    Array[0] = 0x7ffe9472bad0
    Array[1] = 0x7ffe9472bad4
    Array[2] = 0x7ffe9472bad8
- In the example above, the first element and the array itself points to the same memory location and therefore is at 0 elements away from the location of the array itself.


## Multidimensional Arrays

The arrays that we were using earlier were **one-dimensional** arrays.

Sometimes, you need to store information that exceeds the form of a one-dimensional array. Some examples are chess boards and multiplication tables.

![readme-images/Untitled%202.png](readme-images/Untitled%202.png)

The code would then look like this:

```jsx
let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];
```

The only difference between a one and two-dimensional array is that each element in the array is also an array.

# Activity

**Note**: Copy the code from activity-template.js into the batch Boodle Notes so students can copy the template of the code for this activity.

## Instructions

1. In the S21 folder, create an activity folder, an index.html file inside of it and link the index.js file.
2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
3. Copy the activity code and instructions from your Boodle Notes into your index.js.

    - **Important note**: Don't pass the users array as an argument to the function. The functions must be able to manipulate the current users array.

4. Create a function which is able to receive a single argument and add the input at the end of the users array.
    - function should be able to receive a single argument.
    - add the input data at the end of the array.
    - The function should not be able to return data.
    - invoke and add an argument to be passed in the function.
    - log the users array in the console.
5. Create a function which is able to receive an index number as a single argument return the item accessed by its index.
    - function should be able to receive a single argument.
    - return the item accessed by the index.
    - Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        - log the itemFound variable in the console.
6. Create a function which is able to delete the last item in the array and return the deleted item.
    - create a function scoped variable to store the last item in the users array.
    - shorten the length of the array by at least 1 to delete the last item.
    - return the last item in the array which was stored in the function scoped variable.
    - create a global scoped variable outside of the function and store the result of the function.
    - log the global scoped variable in the console.
7. Create a function which is able to update a specific item in the array by its index.
    - function should be able to receive 2 arguments, the update and the index number.
    - first, access and locate the item by its index then re-assign the item with the update.
    - this function should not have a return.
    - invoke the function and add the update and index number as arguments.
    - outside of the function, log the users array in the console.
8. Create a function which is able to delete all items in the array.
    - you can modify/set the length of the array.
    - the function should not return anything.
    - invoke the function.
    - outside of the function, Log the users array in the console.
9. Create a function which is able to check if the array is empty.
    - add an if statement to check if the length of the users array is greater than 0.
    - if it is, return false.
    - else, return true.
    - create a global variable called outside of the function  called isUsersEmpty and store the returned value from the function.
        - log the isUsersEmpty variable in the console.
10. Update your local backend git repository and push to git with the commit message of Add activity code s21.
11. Add the link in Boodle for s21.

## Expected Output

![Expected Output](readme-images/solution.png)

## Solution

```jsx
let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);

/*
    1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -function should be able to receive a single argument.
        -add the input data at the end of the array.
        -The function should not be able to return data.
        -invoke and add an argument to be passed in the function.
        -log the users array in the console.

*/
function addItem(item){
    users[users.length] = item;
}

addItem("John Cena");
console.log(users)
/*
    2. Create function which is able to receive an index number as a single argument return the item accessed by its index.
        -function should be able to receive a single argument.
        -return the item accessed by the index.
        -Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        -log the itemFound variable in the console.

*/
function getItemByIndex(index){
    return users[index]
}

let itemFound = getItemByIndex(2);
console.log(itemFound);

/*
    3. Create function which is able to delete the last item in the array and return the deleted item.
        -Create a function scoped variable to store the last item in the users array.
        -Shorten the length of the array by at least 1 to delete the last item.
        -return the last item in the array which was stored in the function scoped variable.
        -Create a global scoped variable outside of the function and store the result of the function.
        -log the global scoped variable in the console.

*/
function deleteItem(){
    let deleted = users[users.length-1]
    users.length--;
    return deleted;
}

let deletedItem = deleteItem();
console.log(deletedItem);
console.log(users);


/*
    4. Create function which is able to update a specific item in the array by its index.
        -Function should be able to receive 2 arguments, the update and the index number.
        -First, access and locate the item by its index then re-assign the item with the update.
        -This function should not have a return.
        -Invoke the function and add the update and index number as arguments.
        -Outside of the function, Log the users array in the console.

*/

function updateItemByIndex(update,index){
    users[index] = update;
}

updateItemByIndex("Triple H",3);
console.log(users);

/*
    5. Create function which is able to delete all items in the array.
        -You can modify/set the length of the array.
        -The function should not return anything.
        -Invoke the function.
        -Outside of the function, Log the users array in the console.

*/

function deleteAll(){
    //users.length = 0;
    users = [];
}

deleteAll();
console.log(users)


/*
    6. Create a function which is able to check if the array is empty.
        -Add an if statement to check if the length of the users array is greater than 0.
            -If it is, return false.
        -Else, return true.
        -Create a global variable called outside of the function  called isUsersEmpty and store the returned value from the function.
        -log the isUsersEmpty variable in the console.

*/
function isEmpty(){
    if(users.length > 0){
        return false
    } else {
        return true
    }
}
let isUsersEmpty = isEmpty();
console.log(isUsersEmpty);

```