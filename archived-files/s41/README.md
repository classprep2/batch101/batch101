# S30 - Data Persistence via Mongoose ODM

## Topics
* What is an ODM?
* What is Mongoose?
* Schemas
* Models

## Presentation
[Google Slides](https://docs.google.com/presentation/d/1QWDh8mf_kcSBwmNSXCGuBqyWtgOyEQmLVwte_uLqSz0/edit?usp=sharing)

## Codealong
1. Create a new MongoDB collection named bXX_to-do (XX for your batch number).
![Step 1](./screenshots/step1.png)

2. Click the **Clusters** item on the left panel and click the **CONNECT** button on the right to get your database's connection string.
![Step 2](./screenshots/step2.png)

3. Select **Connect your application** from the following screen:
![Step 3](./screenshots/step3.png)

4. Copy the connection string provided in step 2 of the following screen:
![Step 4](./screenshots/step4.png)
* replace the password substring in the connection string with your actual password
* replace the myFirstDatabase substring in the connection string with the database name we just used. 
* if unsure of database user credentials, click **Database Access** under **SECURITY**, a list of users will be shown:
![Step 4a](./screenshots/step4a.png)
* click **EDIT** and you can **Autogenerate Secure Password** in the following screen:
![Step 4b](./screenshots/step4b.png)

5. Create an Express.js project and install mongoose in it via the terminal command:
> npm install mongoose

6. In the app entry point **index.js**, setup the dependencies as follows:
```javascript
const express = require("express"); 
const mongoose = require("mongoose");

const app = express(); 
const port = 3001;
```

7. Connect to the database by passing in the connection string obtained earlier as an argument to the connect() method of the mongoose object:
```javascript
//Connect to the database by passing in your connection string, remember to replace the password and database names with actual values
mongoose.connect("mongodb+srv://admin:actualPassword@learnmongo.9fdce.gcp.mongodb.net/bXX_to-do?retryWrites=true&w=majority", {useNewUrlParser : true});
```

8. Output corresponding notification messages in the console to reflect the status of the MongoDB connection:
```javascript
//set notifications for connection success or failure
let db = mongoose.connection; //connection to the database
//if a connection error occurred, output in the console
db.on("error", console.error.bind(console, "connection error"));

//if the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"));
```

9. Define the structure of a task document in our tasks collection via the use of a Mongoose Schema:
```javascript
//use the Schema() constructor of the mongoose module - create a new Schema object
const taskSchema = new mongoose.Schema({ //the new keyword creates a new Schema
	//define the fields with the corresponding datatype
	//for a task, it needs a task name and task status
	name : String, //there is a field called "name" and its data type is String
	status : { //there is a field called "status" that is a String and the default value is pending
		type : String,
		default : "pending" //default values are the predefined values for a field if we don't put any value
	}
})
```

10. After having defined a schema, we can now create a model out of it:
```javascript
//Models - uses schemas and are used to create objects that correspond to the schema
/*
Server                       Database
Schema (blueprint)	<->		 Collection
Models uses Schemas and they act as the middleman from the server (JS code) to our database (Schema)
*/
const Task = mongoose.model("Task", taskSchema); //The word Task can now used to run commands for databases
```

11. Enable our app to process request data:
```javascript
//Setup for allowing the server to handle data from requests
app.use(express.json()); //allows your app to read json data
app.use(express.urlencoded({extended:true})); //allows your app to read data from forms
```

12. Define a route for creating a task. The findOne() Mongoose model method will be used to find duplicates. If no duplicates found, the Task() constructor will be used to create a new Task object which will be saved as a MongoDB document in the matching collection when its save() method is invoked.
```javascript
app.post("/tasks", (req, res)=> {
	//create a new Task object with a field for the name and value = task
	//data will be from the request's body
	//add a functionality to check if there are duplicate tasks
		//wherein if the task already exists in the database, we return an error
		//if the task doesn't exist in the database, we write it in the database

	//Check if there are duplicate tasks
	//findOne is a mongoose function that acts similar to find() of MongoDB
	//findOne() finds the first document that matches the search criteria
	//if there are no matches, the value of result is null
	Task.findOne({name : req.body.name}, (err, result) => {
		if(result != null && result.name == req.body.name){
			//the if condition checks if the result is not empty and the name matches the task name
			return res.send("Duplicate task found");
		} else {
			//Create a new task and save it to the database
			let newTask = new Task({
				name : req.body.name
			});

			newTask.save((saveErr, savedTask) => {
				//if there are errors in saving
				if(saveErr){
					return console.error(saveErr);
				} else { //meaning there is no error
					return res.status(201).send("New Task created");
				}
			})
		}
	})
})
```

13. Serve our app on designated port:
```javascript
//listen to the port, meaning, if the port is accessed, we run the server
app.listen(port, () => console.log(`Server running at port ${port}`));
```

14. Start app server with terminal command:
> node index.js

15. Create a Postman request and test this endpoint:
![Step 14](./screenshots/step14.png)

16. Define a route that will get all tasks:
```javascript
app.get("/tasks", (req, res) => {
	//.find is a mongoose function that is similar to mongodb find(), and an empty {} means it gets all the documents and places the documents in the result
	Task.find({}, (err, result) => {
		if (err) {
			return console.log(err);
		} else {
			return res.status(200).json({ //status 200 means that everything is okay in terms of processing
				data : result			//.json sends a json format for the response
			})
		}
	})
})
```

17. Create a Postman request and test this endpoint:
![Step 16](./screenshots/step16.png)

## Activity
1. Have the students define a model representing the User. A user document will have a username and password that are both strings.
> Solution:
```javascript
const userSchema = new mongoose.Schema({
    username : String,
    password : String
})

const User = mongoose.model("User", userSchema);
```

2. Have the students define a route that will create a new user document when a POST request containing BOTH a username and password is received at the /signup endpoint. Make sure to check for duplicates before creating a new user.
> Solution:
```javascript
app.post("/signup", (req, res)=> {
	User.findOne({username : req.body.username}, (err, result) => {
		if(result != null && result.username == req.body.username){
			return res.send("Duplicate username found");
		} else {
			if(req.body.username !== '' && req.body.password !== ''){
                let newUser = new User({
                    username : req.body.username
                });
    
                newUser.save((saveErr, savedTask) => {
                    //if there are errors in saving
                    if(saveErr){
                        return console.error(saveErr);
                    } else { //meaning there is no error
                        return res.status(201).send("New user registered");
                    }
                })
            }else{
                return res.send("BOTH username and password must be provided.");
            }			
		}
	})
})
```
> Successful user signup:
![Act 1a](./screenshots/act1a.png)
> Duplicate username found
![Act 1b](./screenshots/act1b.png)
> Incomplete credentials provided
![Act 1c](./screenshots/act1c.png)
