# WDC028 - S35 - Express.js - Data Persistence via Mongoose ODM

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1QWDh8mf_kcSBwmNSXCGuBqyWtgOyEQmLVwte_uLqSz0/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1QWDh8mf_kcSBwmNSXCGuBqyWtgOyEQmLVwte_uLqSz0/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s30) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/backend/s30/Manual.js) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                         | Link                                                         |
| ----------------------------- | ------------------------------------------------------------ |
| npm init                      | [Link](https://docs.npmjs.com/cli/v7/commands/npm-init)      |
| MongoDB Atlas                 | [Link](https://cloud.mongodb.com/)                           |
| Get Started With Atlas        | [Link](https://docs.atlas.mongodb.com/getting-started/)      |
| Express JS                    | [Link](https://expressjs.com/)                               |
| What Is Mongoose              | [Link](https://mongoosejs.com/)                              |
| Mongoose Documentation        | [Link](https://mongoosejs.com/docs/)                         |
| What Are Drivers              | [Link](https://www.digitalcitizen.life/what-are-drivers-why-do-you-need-them/) |
| Mongoose Deprecation Warnings | [Link](https://mongoosejs.com/docs/deprecations.html)        |
| Mongoose Connections          | [Link](https://mongoosejs.com/docs/connections.html)         |
| MongoDB String URI Format     | [Link](https://docs.mongodb.com/manual/reference/connection-string/) |
| console.error.bind            | [Link](https://www.tjvantoll.com/2015/12/29/console-error-bind/) |
| Default MongoDB Port          | [Link](https://docs.mongodb.com/manual/reference/default-mongodb-port/#:~:text=27017,setting%20in%20a%20configuration%20file.) |
| Mongoose Schemas              | [Link](https://mongoosejs.com/docs/guide.html)               |
| Mongoose Models               | [Link](https://mongoosejs.com/docs/models.html)              |
| Mongoose findOne Method       | [Link](https://mongoosejs.com/docs/api.html#model_Model.findOne) |
| Mongoose save Method          | [Link](https://mongoosejs.com/docs/models.html#constructing-documents) |
| HTTP Status Codes             | [Link](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status) |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[30 mins] - Creation of MongoDB Database
	2.[5 mins] - Getting the connection string
	3.[10 mins] - Creation of node project
	4.[5 mins] - Installation of packages
		a. Express
		b. Mongoose
	5.[30 mins] - Creation of index file
	6.[10 mins] - Connecting the server to the database
	7.[30 mins] - Creation of Schema
	8.[30 mins] - Creation of Model
	9.[30 mins] - Creation of routes
		a. Create
		b. Read
	10.[1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

​	

[Back to top](#table-of-contents)
