# WDC028 - S19 - JavaScript - Selection Control Structures

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1SqunbIafOL61kcl3v1EPEkYPPHe_AsghwONBfvc_C90/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1SqunbIafOL61kcl3v1EPEkYPPHe_AsghwONBfvc_C90/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s15) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/backend/s15/Manual.js) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                | Link                                                         |
| ------------------------------------ | ------------------------------------------------------------ |
| Expressions and Operators            | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Expressions_and_Operators) |
| Arithmetic Operators                 | [Link](https://www.tutorialspoint.com/computer_programming/computer_programming_operators.htm) |
| Increment Operator                   | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Increment) |
| Decrement Operator                   | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Decrement) |
| Type Coercion                        | [Link](https://developer.mozilla.org/en-US/docs/Glossary/Type_Conversion) |
| Type Conversion                      | [Link](https://developer.mozilla.org/en-US/docs/Glossary/Type_Conversion) |
| Equality Operator                    | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Equality) |
| Inequality Operator                  | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Inequality) |
| Strict Equality Operator             | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Strict_equality) |
| Strict Inequality Operator           | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Strict_inequality) |
| Logical AND Operator                 | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Logical_AND) |
| Logical OR Operator                  | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Logical_OR_assignment) |
| Logical NOT Operator                 | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Logical_NOT) |
| If Else If Else Statement            | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/if...else) |
| Console Warn Method                  | [Link](https://developer.mozilla.org/en-US/docs/Web/API/console/warn) |
| Truthy                               | [Link](https://developer.mozilla.org/en-US/docs/Glossary/Truthy) |
| JavaScript Expressions and Statement | [Link](https://medium.com/launch-school/javascript-expressions-and-statements-4d32ac9c0e74) |
| Falsy                                | [Link](https://developer.mozilla.org/en-US/docs/Glossary/Falsy) |
| Conditional (ternary) Operator       | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Conditional_Operator) |
| Console Prompt Method                | [Link](https://developer.mozilla.org/en-US/docs/Web/API/Window/prompt) |
| Switch Statement                     | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/switch) |
| Break Statement                      | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/break) |
| Try Catch Finally Statement          | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/try...catch) |
| Error Object                         | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error) |
| typeof Operator                      | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof) |
| Error Handling                       | [Link](https://www.w3schools.com/js/js_errors.asp)           |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

```
1. [15 mins] - Assignment Operators
	- Basic Assignment Operator
	- Addition Assignment Operator
	- Subtraction Assignment Operator
	- Multiplication Assignment Operator
	- Division Assignment Operator
2. [15 mins] - Arithmetic Operators
	- Plus
	- Minus
	- Multiply
	- Divide
	- Modulo
	- Parenthesis
	- Increment
	- Decrement
3. [5 mins] - Type Coercion
4. [20 mins] - Comparison Operators
	- Equality Operators
	- Inequality Operators
	- Strict-Equality Operators
	- Strict-Inequality Operators
5. [20 mins] - Relational Comparison Operators
	- Greater than
	- Less than
	- Greater than or equal to
	- Less than or equal to
6. [20 mins] - Logical Operators
	- And Operators
	- Or Operator
	- Not Operator
7. [20 mins] - If-ElseIf-Else Statement
8. [20 mins] - Switch Statement
9. [20 mins] - Try-Catch-Finally Statement
10. [20 mins] - Functions
11. [1 hr] - Activity
```

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

```
1. Error Handling
```

[Back to top](#table-of-contents)