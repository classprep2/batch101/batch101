# S19 - JavaScript - Selection Control

# Session Objectives

At the end of the session, the students are expected to:

- learn how to create if-else statements.
- learn how to use if-else statements in functions.
- learn how to create a switch statement.
- learn how to use try-catch-finally statements.


# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5/s15)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1H9z-O9Q3xuAHIPmdYNxayWLJttyPIEHJr9RTDKkch3o/edit#slide=id.g53aad6d9a4_0_728)

## Supplemental Materials

- [JavaScript If-Else Statement (w3schools)](https://www.w3schools.com/js/js_if_else.asp)
- [JavaScript Switch Statement (w3schools)](https://www.w3schools.com/js/js_switch.asp)
- [Making Decisions In Your Code: Conditionals (MDN Web Docs)](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Building_blocks/conditionals)
- [JavaScript Conditionals (Pluralsight)](https://www.javascript.com/learn/conditionals)

# Lesson Proper

## What is a selection control structure?

A selection control structure allows us to select between two or more alternate paths. This is described as either two-way selection or multi-way selection.

## Conditional Statements

A conditional statement is one of the key features of a selection control structure in a programming language.
In our daily lives, we **decide** on a **daily** basis. From which to drink, to when we sleep.

Programming also needs to execute **decision-making tasks** that **depends on the input** from the user.

![readme-images/Untitled.png](readme-images/Untitled.png)

Flowchart for determining the actions of repairing a lamp.

Conditional statements, if written in words, would read like these:

- Is the container **full** or **not**?
- Is the temperature **higher than** or **equal to** 40 degrees Celsius?
- Does the title **contain** an ampersand (&) character?

With conditional statements, we can code **predetermined** actions based on **user input** and in turn make our code more **flexible**.

There are **three** types of conditional statements.

- **if-else** statement
- **switch** statement
- **try-catch-finally** statement

### If-Else if-Else Statement

if statement

Executes a stement if a specified condition is true 

```js
    let numA = -1;

    if(numA < 0) {
        console.log('Hello');
    }

    /*
        Syntax:

        if(condition){
            statement
        }

    */
```

The result of the expression added in the if's condition must result to true, else, the statement inside if() will not run.

You can also check the condition. The expression results to a boolean true because of the use of the less than operator.

```js
console.log(numA < 0)//results to true and so, the if statement was run.
````
Let's update the variable and run an if statement with the same condition:

```js
numA = 0

if(numA < 0){
    console.log('Hello again if numA is 0!')
}
```

It will not run because the expression now results to false:
```js
console.log(numA < 0)//results to false
```
Let's take a look at another example:
```js
let city = "New York"

if(city === "New York"){
    
    console.log("Welcome to New York City!");

} //should run because the if condition results to true.
```

else if statement

- Executes a statement if previous conditions are false and if the specified condition is true
- The "else if" clause is optional and can be added to capture additional conditions to change the flow of a program

```js
    let numH = 1;

    if(numA < 0) {
        console.log('Hello');
    } else if (numH > 0) {
        console.log('World');
    }
```
We will be able to run the else if() statement after we evaluate that the if condition has failed.
If the if() condition was passed and run, we will no longer evaluate to else if() and end the process there.

```js
    numA = 1

    if(numA > 0) {
        console.log('Hello');
    } else if (numH > 0) {
        console.log('World');
    }
````
else if() statement was no longer run because the if statement was able to run, the evaluation of the whole statement stops there.

Let's update the city variable and look at another example:
```js
    city = "Tokyo"

    if(city === "New York"){
        console.log("Welcome to New York City!")
    } else if(city === "Tokyo"){
        console.log("Welcome to Tokyo, Japan!")
    }
```
Since we failed the condition for the first if(), we went to the else if() and checked and instead passed that condition.

else Statement

- Executes a statement if all other conditions are false
- The "else" statement is optional and can be added to capture any other result to change the flow of a program

```js
    if(numA > 0) {
        console.log('Hello');
    } else if (numH === 0) {
        console.log('World');
    } else {
        console.log('Again');
    }
````
Since both the preceding if and else if conditions failed, the else statement was run instead.

Else statements should only be added if there is a preceding if condition. else statements by itself will not work, however, if statements will work even if there is no else statement.

```js
    else {
        console.log("Will not run without an if");
    }
```
Same goes for an else if, there should be a preceding if() first.
```js
    else if (numH === 0) {
        console.log('World');
    } else {
        console.log('Again');
    }
```

Note:

- You can add examples of independent or multiple if statements but beware of time.
- Check notes-mini.js for mini-activities

if, else if and else Statements with functions

- Most of the times we would like to use if, else if and else statements with functions to control the flow of our application
- By including them inside functions, we can decide when certain conditions will be checked instead of executing statements when the JavaScript loads
- The "return" statement can be utilized with conditional statements in combination with functions to change values to be used for other features of our application

```jsx
function determineTyphoonIntesity(windSpeed) {
    if (windSpeed < 30) {
        return 'Not a typhoon yet.';
    } 
    else if (windSpeed <= 61) {
        return 'Tropical depression detected.';
    }
    else if (windSpeed >= 62 && windSpeed <= 88) { // && means AND operator
        return 'Tropical storm detected.';
    }
    else if (windSpeed >= 89 || windSpeed <= 117) { // || means OR operator
        return 'Severe tropical storm detected.';
    }
    else {
        return 'Typhoon detected.';
    }
}

console.log(determineTyphoonIntesity(69));
```
Returns the string to the variable "message" that invoked it 
```js
    message = determineTyphoonIntensity(110);
    console.log(message);
````

- We can further control the flow of our program based on conditions and changing variables and results
- The initial value of "message" was "No message."
- Due to the conditional statements created in the function, we were able to reassign it's value and use it's new value to print a different output
- console.warn() is a good way to print warnings in our console that could help us developers act on certain output within our code

```js
if (message == 'Tropical storm detected.') {
    console.warn(message);
}
```
### Truthy and Falsy

- In JavaScript a "truthy" value is a value that is considered true when encountered in a Boolean context
- Values are considered true unless defined otherwise
- Falsy values/exceptions for truthy:
    1. false
    2. 0
    3. -0
    4. ""
    5. null
    6. undefined
    7. NaN

Truthy Examples

- If the result of an expression in a condition results to a truthy value, the condition returns true and the corresponding statements are executed
- Expressions are any unit of code that can be evaluated to a value

```js
    if (true) { 
        console.log('Truthy');
    }

    if (1) { 
        console.log('Truthy');
    }

    if ([]) { 
        console.log('Truthy');
    }

    // Falsy Examples
    if (false) { 
        console.log('Falsy');
    }

    if (0) { 
        console.log('Falsy');
    }

    if (undefined) { 
        console.log('Falsy');
    }

````

### Conditional (Ternary) Operator

- The Conditional (Ternary) Operator takes in three operands:
    1. condition
    2. expression to execute if the condition is truthy
    3. expression to execute if the condition is falsy
- Can be used as an alternative to an "if else" statement
- Ternary operators have an implicit "return" statement meaning that without the "return" keyword, the resulting expressions can be stored in a variable
- Commonly used for single statement execution where the result consists of only one line of code
- For multiple lines of code/code blocks, a function may be defined then used in a ternary operator
- Syntax
    (expression) ? ifTrue : ifFalse;

```jsx
let ternaryResult = (1 < 18) ? true : false;
console.log("Result of Ternary Operator: " + ternaryResult)
```

Multiple statement execution

Both functions perform two separate tasks which changes the value of the "name" variable and returns the result storing it in the "legalAge" variable

```js
    let name;

    function isOfLegalAge() {
        name = 'John';
        return 'You are of the legal age limit';
    }

    function isUnderAge() {
        name = 'Jane';
        return 'You are under the age limit';
    }
```
- The "prompt" function creates a pop-up message in the browser that can be used to gather user input
- Input received from the prompt function is returned as a string data type
- The "parseInt" function converts the input received into a number data type
- Pressing on the "cancel" button on a prompt will return a value of "null"
- Converting null to an integer/number will result to a NaN (Not a Number) value
- This can be useful for instances where a frontend application has not yet been created and can be used to test varying data

```js
    let age = parseInt(prompt("What is your age?"));
    console.log(age);
    let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
    console.log("Result of Ternary Operator in functions: " + legalAge + ', ' + name);
```

Note:

- Check notes-mini.js for mini-activities

### Switch Statement

-The switch statement evaluates an expression and matches the expression's value to a case clause. The switch will then execute the statements associated with that case, as well as statements in cases that follow the matching case.
- Can be used as an alternative to an if, "else if and else" statement where the data to be used in the condition is of an expected input
- The ".toLowerCase()" function/method will change the input received from the prompt into all lowercase letters ensuring a match with the switch case conditions if the user inputs capitalized or uppercased letters
- The "expression" is the information used to match the "value" provided in the switch cases
- Variables are commonly used as expressions to allow varying user input to be used when comparing with switch case values
- Switch cases are considered as "loops" meaning it will compare the "expression" with each of the case "values" until a match is found
- The "break" statement is used to terminate the current loop once a match has been found
- Removing the "break" statement will have the switch statement compare the expression with the values of succeeding cases even if a match was found
- Syntax
    switch (expression) {
        case value:
            statement;
            break;
        default:
            statement;
            break;
    }

```jsx

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day) {
    case 'monday': 
        console.log("The color of the day is red");
        break;
    case 'tuesday':
        console.log("The color of the day is orange");
        break;
    case 'wednesday':
        console.log("The color of the day is yellow");
        break;
    case 'thursday':
        console.log("The color of the day is green");
        break;
    case 'friday':
        console.log("The color of the day is blue");
        break;
    case 'saturday':
        console.log("The color of the day is indigo");
        break;
    case 'sunday':
        console.log("The color of the day is violet");
        break;
    default:
        console.log("Please input a valid day");
        break;
}
```
Note:

- Check notes-mini.js for mini-activities


## Try-Catch-Finally Statement

- "try catch" statements are commonly used for error handling
- There are instances when the application returns an error/warning that is not necessarily an error in the context of our code
- These errors are a result of an attempt of the programming language to help developers in creating efficient code
- They are used to specify a response whenever an exception/error is received
- It is also useful for debugging code because of the "error" object that can be "caught" when using the try catch statement
- In most programming languages, an "error" object is used to provide detailed information about an error and which also provides access to functions that can be used to handle/resolve errors to create "exceptions" within our code
- The "finally" block is used to specify a response/action that is used to handle/resolve errors

```jsx
function showIntensityAlert(windSpeed) {
    try {

        alerat(determineTyphoonIntensity(windSpeed));

    // error/err are commonly used variable names used by developers for storing errors
    } catch (error) {

        // The "typeof" operator is used to check the data type of a value/expression and returns a string value of what the data type is
        console.log(typeof error);

        // Catch errors within 'try' statement
        // In this case the error is an unknown function 'alerat' which does not exist in Javascript
        // The "alert" function is used similarly to a prompt to alert the user
        // "error.message" is used to access the information relating to an error object
        console.warn(error.message);

    } finally {

        // Continue execution of code regardless of success and failure of code execution in the 'try' block to handle/resolve errors
        alert('Intensity updates will show new alert.');

    }
}

showIntensityAlert(56);

```

Note:

- Check notes-mini.js for mini-activities



# Activity

**Note**: Copy the code from activity-template.js into the batch Boodle Notes so students can copy the template of the code for this activity.

## Instructions that can be provided to the students for reference:

1. In the S19 folder, create an activity folder, an index.html file inside of it and link the index.js file.
2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
3. Copy the activity code from your Boodle Notes. Paste the activity code from your Boodle Notes to your index.js file
4. Create a login function which is able to receive 3 parameters called username,password and role.

        -add an if statement to check if the the username is an empty string or undefined or if the password is an empty string or undefined or if the role is an empty string or undefined.

            -if it is, return a message in console to inform the user that their input should not be empty.

        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:

                -if the user's role is admin, return the following message:
                    "Welcome back to the class portal, admin!"

                -if the user's role is teacher,return the following message:
                    "Thank you for logging in, teacher!"

                -if the user's role is a rookie,return the following message:
                    "Welcome to the class portal, student!"

                -if the user's role does not fall under any of the cases, as a default, return a message:
                    "Role out of range."

5. Create a function which is able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
        -add parameters appropriate to describe the arguments.
        -create a new function scoped variable called average.
        -calculate the average of the 4 number inputs and store it in the variable average.
        -research the use of Math.round() and round off the value of the average variable.

            -update the average variable with the use of Math.round()
            -Do not use Math.floor()
            -console.log() the average variable to check if it is rounding off first.

        -add an if statement to check if the value of average is less than or equal to 74.

            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is F"

        -add an else if statement to check if the value of average is greater than or equal to 75 and if average is less than or equal to 79.

            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is D"
        -add an else if statement to check if the value of average is greater than or equal to 80 and if average is less than or equal to 84.

            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is C"

        -add an else if statement to check if the value of average is greater than or equal to 85 and if average is less than or equal to 89.

            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is B"

        -add an else if statement to check if the value of average is greater than or equal to 90 and if average is less than or equal to 95.

            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is A"

        -add an else if statement to check if the value of average is greater than 96.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is A+"

6. Update your local backend git repository and push to git with the commit message of Add activity code s19.
7. Add the link in Boodle for s19.

## Expected Output

![Expected Output](readme-images/solution.png)

## Solution

```jsx
/*
/*
    1. Create a login function which is able to receive 3 parameters called username,password and role.
        -add an if statement to check if the the username is an empty string or undefined or if the password is an empty string or undefined or if the role is an empty string or undefined.
            -if it is, return a message in console to inform the user that their input should not be empty.
        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
                -if the user's role is admin, return the following message:
                    "Welcome back to the class portal, admin!"
                -if the user's role is teacher,return the following message:
                    "Thank you for logging in, teacher!"
                -if the user's role is a rookie,return the following message:
                    "Welcome to the class portal, student!"
                -if the user's role does not fall under any of the cases, as a default, return a message:
                    "Role out of range."
*/
function login(username,password,role){

    if(username === "" || username === undefined || password === "" || password === undefined || role === "" || role === undefined){

        return "Inputs must not be empty";

    } else {
        switch (role){
            case "admin":
                return "Welcome back to the class portal, admin!";
                break;
            case "teacher":
                return "Thank you for logging in, teacher!";
                break;
            case "student":
                return "Welcome to the class portal, student!";
                break;
            default:
                return "Role out of range.";
                break;
        }
    }

}
    
login();

/*
    2. Create a function which is able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
        -add parameters appropriate to describe the arguments.
        -create a new function scoped variable called average.
        -calculate the average of the 4 number inputs and store it in the variable average.
        -research the use of Math.round() and round off the value of the average variable.
            -update the average variable with the use of Math.round()
            -Do not use Math.floor()
            -console.log() the average variable to check if it is rounding off first.
        -add an if statement to check if the value of average is less than or equal to 74.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is F"
        -add an else if statement to check if the value of average is greater than or equal to 75 and if average is less than or equal to 79.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is D"
        -add an else if statement to check if the value of average is greater than or equal to 80 and if average is less than or equal to 84.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is C"
        -add an else if statement to check if the value of average is greater than or equal to 85 and if average is less than or equal to 89.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is B"
        -add an else if statement to check if the value of average is greater than or equal to 90 and if average is less than or equal to 95.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is A"
        -add an else if statement to check if the value of average is greater than 96.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is A+"

        Invoke and add a number as argument using the browser console.
*/
function checkAverage(grade1,grade2,grade3,grade4){


    let average = (grade1+grade2+grade3+grade4)/4;
    average = Math.round(average);

    if(average <= 74){
        return "Hello, student, your average is: " + average + ". The letter equivalent is F";
    } else if(average >= 75 && average <= 79){
        return "Hello, student, your average is: " + average + ". The letter equivalent is D";
    } else if(average >= 80 && average <= 84){
        return "Hello, student, your average is: " + average + ". The letter equivalent is C";
    } else if(average >= 85 && average <= 89){
        return "Hello, student, your average is: " + average + ". The letter equivalent is B";
    } else if(average >= 90 && average <= 95){
        return "Hello, student, your average is: " + average + ". The letter equivalent is A";
    } else if(average >= 96){
        return "Hello, student, your average is: " + average + ". The letter equivalent is A+";
    }

}
```