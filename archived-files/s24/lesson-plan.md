# WDC028 - S20 - JavaScript - Repitition Control Structures

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1ML4BRgRGPGYtfF8IGHRlFqjAMajQ9gDVdDXclzoZfdc/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1ML4BRgRGPGYtfF8IGHRlFqjAMajQ9gDVdDXclzoZfdc/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s16) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/backend/s16/Manual.js) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                | Link                                                         |
| ------------------------------------ | ------------------------------------------------------------ |
| JavaScript Expressions and Statement | [Link](https://medium.com/launch-school/javascript-expressions-and-statements-4d32ac9c0e74) |
| Loops and Iteration                  | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration) |
| Statements                           | [Link](https://www.teamten.com/lawrence/programming/intro/intro2.html) |
| Iteration                            | [Link](https://teachcomputerscience.com/iterations/)         |
| JavaScript While Loop                | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/while) |
| Decrement Operator                   | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Decrement) |
| JavaScript Do While Loop             | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/do...while) |
| Number Function                      | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number) |
| JavaScript parseInt() Function       | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/parseInt) |
| JavaScript for Loop                  | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for) |
| Increment Operator                   | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Increment) |
| JavaScript String                    | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String) |
| JavaScript String .length Property   | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/length) |
| JavaScript .toLowerCase() Method     | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/toLowerCase) |
| Continue Statement                   | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/continue) |
| Break Statement                      | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/break) |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1. [30 mins] - Loops
	2. [30 mins] - While Loops
	3. [30 mins] - Do-While Loops
	4. [30 mins] - For Loops
	5. [30 mins] - Continue and Break Statements
	6. [1 hr 30 mins] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

[Back to top](#table-of-contents)

