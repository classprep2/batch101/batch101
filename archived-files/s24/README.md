# Session Objectives

At the end of the session, the students are expected to:

- execute code in a repetitive manner by using repetition control structures.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5/s16)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1ML4BRgRGPGYtfF8IGHRlFqjAMajQ9gDVdDXclzoZfdc)
- [Lesson Plan](lesson-plan.md)

## Supplemental Materials

- [JavaScript Loops and Iteration (MDN Web Docs)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration)
- [JavaScript Loops (Tutorials Republic)](https://www.tutorialrepublic.com/javascript-tutorial/javascript-loops.php)

# Lesson Proper

## Loops

Loops are **one** of the **most important** feature that a programming language must have.

It lets us execute code **repeatedly** in a pre-set number of time or maybe forever.

# Code Discussion

## Folder and File Preparation

Create a folder named **s16**, a folder named **discussion** inside the **s16** folder, then a file named **index.html** and **index.js** inside the **discussion** folder.

## While Loop

A while loop takes a single condition. If the condition evaluates to **true**, the code inside the block will run.

```jsx
function whileLoop() {
    let count = 5;

    while(count !== 0) {
        console.log(count);
        count--;
    }
}
```

## Do-While Loop

This type of loop works a lot like the while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once.

```jsx
function doWhileLoop() {
    let count = 20;

    do {
        console.log(count);
        count--;
    } while (count > 0);
}
```

The `do {...}` executes the code first. After `do {...}` finishes its execution, the while statement will evaluate whether to run the `do {...}` again based on given condition (count should be higher than 0).

This is how the do-while loops guarantee that the code will be executed at least once.

## For Loop

A for loop is more flexible than while and do-while loops. It consists of three parts:

- The **initial value** that will **track** the **progression** of the loop;
- The **condition** that will **evaluated** and will **determine** whether the loop will **run** one more time; and
- The **iteration** that **indicates how to advance** the loop.

Let's take a look at a sample code:

```jsx
function forLoop() {
    for (let count = 0; count <= 20; count++) {
        console.log(count);
    }
}
```

Based on the parts presented earlier, the loop has the following format:

`for (initial value; condition; iteration)`

The loop **starts** with a count of **zero**. Every start of a loop, it will **evaluate** whether the count is **equal** or **less than** to **20**. If it is, the code inside the loop will **execute** and the count will **increment** by 1 **after each loop has ended**.

## Continue and Break

Continue is a keyword that allows the code to **go to** the next loop **without finishing** the **current** code block.

Break on the other hand is a keyword that **ends the execution** of the current loop.

```jsx
function modifiedForLoop() {
    for (let count = 0; count <= 20; count++) {
        if (count % 2 === 0) {
            continue;
        }
        console.log(count);
        if (count > 10) {
            break;
        }
    }
}
```

If a number is divisible by 2 (or in other words, even), that number is **skipped** and the loop **iterates again without** going to the console.log and next if statement below.

Once we reach a count value that is **greater than** 10, the loop will **stop** even if the loop condition checks for count <= 20.

# Activity

## Instructions

- Using loops, print all numbers that are divisible by 5.
- Stop the loop when the loop reaches its 1000th iteration.

## Expected Output

![readme-images/Untitled.png](readme-images/Untitled.png)

## Solution

```jsx
function PrintDivisiblesOfFive() {
    for (let count = 0; count <= 1000; count++) {
        if (count % 5 === 0) { 
            console.log(count);
        }
    } 
}
```