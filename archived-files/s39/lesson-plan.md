# WDC028 - S33 - Introduction to Postman and REST

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1UhizSK9gnWNFmA1ddALIQdTc9ZeJVBHqnUSVGqJyG7M/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1UhizSK9gnWNFmA1ddALIQdTc9ZeJVBHqnUSVGqJyG7M/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s28) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/backend/s28/Manual.js) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                           | Link                                                         |
| ----------------------------------------------- | ------------------------------------------------------------ |
| JavaScript Synchronous and Asynchronous         | [Link](https://betterprogramming.pub/is-javascript-synchronous-or-asynchronous-what-the-hell-is-a-promise-7aa9dd8f3bfb) |
| Execution of Synchronous and Asynchronous codes | [Link](https://siddharthac6.medium.com/javascript-execution-of-synchronous-and-asynchronous-codes-40f3a199e687) |
| Programming Jokes API                           | [Link](https://official-joke-api.appspot.com/jokes/programming/random) |
| What Is An API?                                 | [Link](https://apifriends.com/api-management/what-is-an-api/) |
| JSON Placeholder                                | [Link](https://jsonplaceholder.typicode.com/guide/)          |
| Request Response Cycle                          | [Link](https://iq.opengenus.org/content/images/2019/09/Untitled-1-.png) |
| Fetch API - JavaScript Tutorial                 | [Link](https://www.javascripttutorial.net/javascript-fetch-api) |
| Fetch API - MDN Web Docs                        | [Link](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API) |
| Using Fetch                                     | [Link](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch) |
| Promise Diagram                                 | [Link](https://wtcindia.files.wordpress.com/2016/06/promises.png?w=605) |
| What is a Promise?                              | [Link](https://medium.com/javascript-scene/master-the-javascript-interview-what-is-a-promise-27fc71e77261) |
| Promise - MDN Web Docs                          | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) |
| Using Promise                                   | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises) |
| then Method                                     | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/then) |
| How To Use Fetch With Async Await               | [Link](https://dmitripavlutin.com/javascript-fetch-async-await/) |
| Async and Await                                 | [Link](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Asynchronous/Async_await) |
| REST API Diagram                                | [Link](https://usercontent.one/wp/www.kennethlange.com/wp-content/uploads/2020/04/customer_rest_api.png) |
| REST API - MDN Web Docs                         | [Link](https://developer.mozilla.org/en-US/docs/Glossary/REST) |
| What Is The Difference Between PUT and PATCH?   | [Link](https://rapidapi.com/blog/put-vs-patch/)              |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[10 mins] - Creation of index.js
	2.[30 mins] - API overview
	3.[30 mins] - Creation of fetch request
	4.[10 mins] - Checking of response
	5.[30 mins] - Async and await keywords
	6.[40 mins] - CRUD functionalities
		a. GET
		b. POST
		c. PUT / PATCH
		d. DELETE
	7.[30 mins] - Filtering posts
	8.[1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

​	

[Back to top](#table-of-contents)
