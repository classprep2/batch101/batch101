# S29 - Introduction to Express.js

## Topics
* What is an API?
* What is REST?
* Postman API Client

## Presentation
[Google Slides](https://docs.google.com/presentation/d/1GpL82WuYAuuy90OFNa3vtrgR9OEatt_KVlP8TxR5qQo/edit?usp=sharing)

## Codealong

1. Create project directory named S27.

2. Navigate to this directory and create a **package.json** file via the terminal command:
> npm init  
* You can just hit return on all prompts that will come up.
* think of the package.json file as a manifest that describes your project. It will be updated as we go to include packages that we will install for our application. This will be used by your application in rebuilding itself when you clone this project in a new machine.

3. Install the express package. This is a project dependency that contains the Express.js framework. Use the following terminal command to do so:
> npm install express
* if you take a look at package.json AFTER installation, you'll see express listed within the **dependencies** property. Your application will install all dependencies found here when the command **npm install** is run (usually done immediately after cloning this project).
* Notice also that a new directory named node_modules has been created, this contains the files needed by our installed packages to work.

4. Create a file named .gitignore. This file serves as a list of files to be excluded in your commits to git. It's best practice to put **node_modules** here.
* the node_modules directory should be left on the local machine to keep our git commits lightweight. Our application will know what packages to install when it's cloned somewhere else thanks to its package.json file.

5. Create a file named index.js. This will serve as our application entry point. Let's tell our API to expect a GET request at the "/hello" endpoint and to send a response comprised of a string "Hello from the /hello endpoint!"

```javascript
//This require is used to get the contents of the express package to be used by our application
const express = require("express");

//create an application using express
const app = express(); //this creates an express application and stores this in a constant called app.
//in layman's terms, app is our server.

//for our application server to run, we need a port to listen to
const port = 3000;

//Express has methods corresponding to each http method
//This route expects to receive a GET request at the base URI "/" and will in turn send a string response containing "Hello World"
app.get("/", (req, res) => {
	res.send("Hello World");
});

//This route is for GET /hello
app.get("/hello", (req, res) => {
	res.send("Hello from the /hello endpoint!");
});

//listen to the port - if the port is accessed, we run the server
app.listen(port, () => console.log(`Server running at port ${port}`));
```
* the req and res parameters are Express.js's abstractions of the Node.js implementation for processing a request and returning a response

6. Input the following terminal command to run our server:
> node index.js

7. Go to "localhost:3000" in our browser, we should get the Hello World response. Going to localhost:3000/hello will return its corresponding response as well.

8. Let's try sending a name in the JSON request body of a POST request to the same /hello endpoint. We'll have our route return a greeting with the received name in it. Add the following route to index.js:
```javascript
app.post("/hello", (req, res) => {
	//req.body contains the contents of the request body. All the properties defined in our Postman request will be accessible here as properties with the same names.
    res.send(`Hello there ${req.body.name}!`)
})
```

9. We also have to setup our Express.js app to parse JSON and form request bodies. Let's add the following statements before our routes:
```javascript
//Setup for allowing the server to handle data from requests
app.use(express.json()); //allows your app to read json data
app.use(express.urlencoded({extended:true})); //allows your app to read data from forms
```
* not doing this step will result in the req.body returning undefined

10. After adding the new route and setting up our app to handle request bodies, restart the app server by hitting CTRL+C in the terminal and repeating the terminal command:
> node index.js

11. Open Postman and send a POST request to the /hello endpoint as follows:
![step 11a](./screenshots/step11a.png)  
	The following response will be received:  
![step 11b](./screenshots/step11b.png)

## Activity
1. Have the students create a /home route that will return a string response "Welcome to the home page" upon receiving a GET request.
> Solution:
```javascript
app.get("/home", (req, res) => {
    res.send("Welcome to the home page")
})
```
> Expected Result:
![Activity 1](/screenshots/activity1.png)

2. Have the students create a /signup route that will add the user object sent via the request body of a POST request to a users array. Respond with a success notification if BOTH username and password are provided. Respond with a warning message if either is missing.
> Solution:
```javascript
let users = [];

app.post("/signup", (req, res) => {
    if(req.body.username !== '' && req.body.password !== ''){
        users.push(req.body)
        res.send(`User ${req.body.username} successfully registered!`)
    }else{
        res.send("Please input BOTH username and password.")
    }
})
```

> Expected Result:
![Activity 2](/screenshots/activity2.png)
![Activity 2b](/screenshots/activity2b.png)

3. Have the students create a /users endpoint that will return the users array upon receiving a GET request.
> Solution:
```javascript
app.get("/users", (req, res) => {
    res.send(users);
})
```

> Expected Result:
![Activity 3](/screenshots/activity3.png)
