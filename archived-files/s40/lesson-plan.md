# WDC028 - S34 - Express.js - Introduction

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1GpL82WuYAuuy90OFNa3vtrgR9OEatt_KVlP8TxR5qQo/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1GpL82WuYAuuy90OFNa3vtrgR9OEatt_KVlP8TxR5qQo/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s29) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/backend/s29/Manual.js) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                        | Link                                                         |
| ---------------------------- | ------------------------------------------------------------ |
| npm init documentation       | [Link](https://docs.npmjs.com/cli/v7/commands/npm-init)      |
| Express JS                   | [Link](https://expressjs.com/)                               |
| What Is A Middleware         | [Link](https://www.redhat.com/en/topics/middleware/what-is-middleware) |
| Express JS json Method       | [Link](http://expressjs.com/en/resources/middleware/body-parser.html#bodyparserjsonoptions) |
| Express JS urlencoded Method | [Link](http://expressjs.com/en/resources/middleware/body-parser.html#bodyparserurlencodedoptions) |
| Array splice Method          | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice) |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[10 mins] - npm initialization
	2.[10 mins] - Installation of Express
	3.[10 mins] - Creation of index file
	4.[2 hr 30 mins] - Creation of routes
		a. GET
		b. POST
		c. PUT
		d. DELETE
	5.[1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

​	

[Back to top](#table-of-contents)
