# Session Objectives

At the end of the session, the students are expected to:

- modify certain styling of a web page element by using the concept of CSS box model.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5/s07)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1Ix_xfpjwI591qizypGjtU3yKEpDy4XYHXSPNy3LCs_c)
- [Lesson Plan](lesson-plan.md)

## Supplemental Materials

- [CSS Box Model (w3schools)](https://www.w3schools.com/css/css_boxmodel.asp)
- [Introduction to the CSS Basic Box Model (MDN Web Docs)](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Box_Model/Introduction_to_the_CSS_box_model)
- [The CSS Box Model (CSS Tricks)](https://css-tricks.com/the-css-box-model/)
- [Inline Blocks (w3schools)](https://www.w3schools.com/css/css_inline-block.asp)

# Lesson Proper

## CSS Box Model

In CSS, each element in a webpage is considered a box. Look at Zuitt's website:

![readme-images/Untitled.png](readme-images/Untitled.png)

Everything in the webpage is considered a box, even if it does not look like it at first glance.

![readme-images/Untitled%201.png](readme-images/Untitled%201.png)

To further confirm this statement, let's open the Chrome Dev Tools by right-clicking the **Succeed With Zuitt** and clicking **Inspect**.

![readme-images/Untitled%202.png](readme-images/Untitled%202.png)

Look at the figure below:

![readme-images/Untitled%203.png](readme-images/Untitled%203.png)

The box model consists of four parts:

- **Content -** The text, image, or other media content in the element.
- **Padding -** The space between the box’s content and its border.
- **Border -** The line between the box’s padding and margin.
- **Margin -** The space between the box and surrounding boxes.

## Recall on Inline and Block Elements

Before continuing the lesson, we must remember the inline and block elements. 

- A **block** element will cause a line break and occupy the full width of the web page.
- An **inline** element on the other hand will not cause a line break and will only take the width necessary to contain its content.

### Examples

- Example A: Paragraphs (Block Element)

![readme-images/Untitled%204.png](readme-images/Untitled%204.png)

- Example B: Buttons (Inline Element)

![readme-images/Untitled%205.png](readme-images/Untitled%205.png)

Keeping this in mind will help us determine whether an element is an inline or block element and style the element according to its presentation format. Furthermore, in some cases we can change its display style by adding the following:

```css
element {
    display: block | inline;
}
```

# Code Discussion

## Folder and File Preparation

Create a folder named **s07**, a folder named **discussion** inside the **s07** folder, then files named **index.html** and **index.css** inside the **discussion** folder.

## Add Preliminary Code

Inside the **index.html** file, share the following code to the students:

```html
<!DOCTYPE html>
<html>
    <head>
        <title>CSS Box Model and Flexbox Layouting</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap">
        <link rel="stylesheet" href="./index.css">
    </head>
    <body>
        <header>
            <div class="nav-logo">
                <img src="https://d3ojsfl21hfxhc.cloudfront.net/assets/zuitt/zuittlogo.png">
            </div>
            <div class="nav">Home</div>
            <div class="nav">Web Dev Program</div>
            <div class="nav">Companies</div>
            <div class="nav">FAQ</div>
            <div class="nav">Blog</div>
        </header>
        <h1 class="header">What is Zuitt?</h1>
        <section id="intro">
            <p>Zuitt is the #1 Philippine-based startup offering web development coding bootcamps in Manila. Thanks to our rapid growth, we can now help over 1,000 Filipinos learn web development with our every year. We also equip them with the job hunting skills needed to get hired as Software Engineers.Our secret to success is our belief that Filipinos can do much more with affordable and quality education. If you invest in your education, you can be in the best position to achieve the life you’ve always wanted for yourself and your family.</p>
            <div>
                <button>Apply Now</button>
            </div>
        </section>
        <h1 class="header">Succeed With Zuitt</h1>
        <section id="succeed-info">
            <div>
                <h3>Beginner Friendly</h3>
                <p>Experienced instructors will guide you from the ultimate beginner’s lesson to your final dynamic website, via a live online instruction with passionate peers and one-on-one Q&A.</p>
            </div>
            <div>
                <h3>Study Now, Pay Later</h3>
                <p>We’re confident in your ability to get your dream career after the training. You don’t have to pay for the program while you’re still learning.</p>
            </div>
            <div>
                <h3>Just 4/6 Months</h3>
                <p>Join our 4 Month Day Class for completely immersed and dedicated learning, or our 6 Month Night Class so you can learn even while working.</p>
            </div>
        </section>
        <section id="pre-footer">
            <div>
                <h3>About Us</h3>
                <p>History</p>
                <p>Mission & Vision</p>
                <p>Founders</p>
                <p>Branches</p>
            </div>
            <div>
                <h3>Careers</h3>
                <p>Junior Developer</p>
                <p>Marketing Associate</p>
                <p>Legal Staff</p>
                <p>Junior Accountant</p>
            </div>
            <div>
                <h3>Follow Us</h3>
                <p>Facebook</p>
                <p>LinkedIn</p>
            </div>
            <div>
                <h3>Contact Us</h3>
                <p>(02) 8 282 9520</p>
                <p>0917 166 8714 (Globe), 0932 868 8713 (Smart)</p>
                <p>helpdesk@zuitt.co</p>
            </div>
        </section>
        <footer>
            <span>Terms of Service</span>
            <span>|</span>
            <span>Privacy Policy</span>
            <span>|</span>
            <span>&copy; 2017-2019 Zuitt. All Rights Reserved</span>
        </footer>
    </body>
</html>
```

After sharing the code, show the initial output of the HTML code to the browser. Right now, it will look something like this:

![readme-images/Untitled%206.png](readme-images/Untitled%206.png)

## CSS Reset

Show the output of each rule declaration to the browser whenever a new declaration is written so that the students become familiar to the CSS property and its effect.

Let's add the following styles:

```html
* {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
}
```

Browsers add default margin and padding values, making the measurement of a box a little bit inconsistent.

Because of this, we need to "reset" these styles.

Also, the box-sizing property was set to border-box because we want to have the following scenario:

- Border-box sizing computes the **total** width or height of a box **including** its margins and paddings; whereas
- Content-box sizing has a total width or height **plus** margins and paddings.

## Body and Header Styles

```css
body {
    font-family: 'Open Sans', sans-serif;
}

header {
    padding: 0.8rem;
    margin-bottom: 2rem;
    align-items: center;
    border-bottom: 1px solid gray;
}

header .nav, header .nav-logo {
    margin: 0.5rem;
    display: inline;
}
```

## Styles for Introductory Section

```css
#intro {
    padding-left: 10%;
    padding-right: 10%;
    padding-top: 2rem;
    padding-bottom: 3rem;
}

#intro h1, #intro p {
    margin-bottom: 1rem;
    text-align: center;
}

#intro div {
    text-align: center;
}

#intro button {
    color: white;
    font-weight: bold;
    background-color: #c2195b;
    border-radius: 25px;
    font-size: 1rem;
    padding: 1rem;
}
```

## Styles for Section Headers

```css
.header {
    text-align: center;
}
```

## Styles for Succeed Information

```css
#succeed-info {
    padding-left: 10%;
    padding-right: 10%;
    padding-top: 2rem;
    padding-bottom: 3rem;
}

#succeed-info > div > h3 {
    margin-bottom: 1rem;
}

#succeed-info div {
    display: inline;
    width: 33.33%;
    margin-left: 0.5rem;
    margin-right: 0.5rem;
}
```

## Styles for Footer

```css
footer {
    display: inline-block;
    padding: 0.7rem;
}

footer span {
    margin-left: 0.5rem;
    margin-right: 0.5rem;
}
```

# GitLab Upload

After finishing the code discussion, demonstrate to the students how to create a GitLab project inside their subgroup and push the code discussion with a commit message of "Add discussion code".

# Activity

## Instructions

Using the styling applied in the footer area, add styles to the pre-footer area so that the four areas (About Us, Careers, Follow Us, Contact Us) is displayed in the same line.

After finishing the activity, link the repository of your activity to your Boodle accounts.

Do not forget to remind your students to link the repository to their Boodle accounts.

## Expected Output

The heading of Follow Us section does not require to be aligned with the headings of other sections.

![readme-images/Untitled%207.png](readme-images/Untitled%207.png)

## Solution

```css
#pre-footer {
    padding: 1rem;
    background-color: #002540;
    color: white;
}

#pre-footer > div {
    display: inline-block;
    width: 20%;
}

#pre-footer > div > * {
    margin-top: 0.3rem;
    margin-bottom: 0.3rem;
}
```