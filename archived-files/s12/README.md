# Session Objectives

At the end of the session, the students are expected to:

- create web page layouts by using the grid system of Bootstrap.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5/s09)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1k1LbCbHsCr5UV-ajK38St4oeTI8cdwncSZW4RHznvH8)
- [Lesson Plan](lesson-plan.md)

## Supplemental Materials

- [Bootstrap 4.6 Grid System](https://getbootstrap.com/docs/4.6/layout/grid/)
- [Bootstrap Grid System (Tutorial Republic)](https://www.tutorialrepublic.com/twitter-bootstrap-tutorial/bootstrap-grid-system.php)

# Lesson Proper

Bootstrap’s grid system uses a series of containers, rows, and columns to layout and align content.

![readme-images/Untitled.png](readme-images/Untitled.png)

Diagram of the grid system structure.

![readme-images/Untitled%201.png](readme-images/Untitled%201.png)

Sample website with overlay of a grid system.

![readme-images/Untitled%202.png](readme-images/Untitled%202.png)

Sample website with overlay of a grid system.

# Code Discussion

## Folder and File Preparation

Create a folder named **s09**, a folder named **discussion** inside the **s09** folder, then a file named **index.html** inside the **discussion** folder.

## Preliminary Code

Add the following initial code to the **index.html** file:

```html
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"/>
        <title>Bootstrap Grid System</title>
    </head>
    <body>
        <div class="container-fluid">
            <h1>Bootstrap Grid System</h1>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    </body>
</html>
```

## Code Sample

Follow the discussion from this [link](https://www.tutorialrepublic.com/twitter-bootstrap-tutorial/bootstrap-grid-system.php) and add each code block to **index.html**.

# GitLab Upload

After finishing the code discussion, demonstrate to the students how to create a GitLab project inside their subgroup and push the code discussion with a commit message of "Add discussion code".

# Activity

## Resources

- [API Picture](https://www.cloudways.com/blog/wp-content/uploads/Rest-API-introduction.jpg)
- [React.js Picture](https://d6vdma9166ldh.cloudfront.net/media/images/1455b5c8-4887-43a8-8214-de77543414c9.jpg)

## Instructions

- Create a Projects section with two rows.
- Each row should have two columns.
- When in a large screen, the contents of the columns at each row should be alternating.
    - At first row, the left side contains the text and the right side contains the image.
    - At second row, the left side contains the image and the right side contains the image.
- When in a small screen, the image should be shown first.
- Look for the Bootstrap documentation regarding the grid system to achieve the required styling for this activity.

## Expected Output

![readme-images/Untitled%203.png](readme-images/Untitled%203.png)

Sample output in a large screen.

![readme-images/Untitled%204.png](readme-images/Untitled%204.png)

Sample in a small screen (Pixel 2 XL).

## Solution

The solution may vary from student to student, but the code may look like this:

```html
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"/>
        <title>Activity: Bootstrap Introduction and Simple Styles</title>
    </head>
    <body>
        <div class="p-4 container-fluid">
            <div class="text-center mb-4">
                <img class="rounded-circle mb-3" width="200" src="https://i.guim.co.uk/img/media/3656ae6ea2209d4561caf04fa9f172a519908ca3/0_28_2318_1391/master/2318.jpg?width=1200&height=1200&quality=85&auto=format&fit=crop&s=afcc355f47876bc495cdc3c902639bae"/>
                <h2>Ada Lovelace</h2>
                <h4>Full-Stack Web Developer</h4>
            </div>
            <p class="text-justify">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            </p>
            <h3 class="text-center">Projects</h3>
            <div class="row mt-3 mb-4">
                <div class="col-sm-12 col-md-6 order-2 order-md-1">
                    <h5 class="text-center pt-3">Web API (Ecommerce)</h5>
                    <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                <div class="col-sm-12 col-md-6 order-1 order-md-2">
                    <div class="text-center">
                        <img class="img-fluid" src="https://www.cloudways.com/blog/wp-content/uploads/Rest-API-introduction.jpg"/>
                    </div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-sm-12 col-md-6 order-2 order-md-2">
                    <h5 class="text-center pt-3">React Frontend (Ecommerce)</h5>
                    <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                <div class="col-sm-12 col-md-6 order-1 order-md-1">
                    <div class="text-center">
                        <img class="img-fluid" src="https://d6vdma9166ldh.cloudfront.net/media/images/1455b5c8-4887-43a8-8214-de77543414c9.jpg"/>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    </body>
</html>
```