# WDC028 - S12 - Bootstrap - Components

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1wkhdUllk3oss-KLwmHxtIgnUa0ktfAkUHd6CKu9qQk8/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1wkhdUllk3oss-KLwmHxtIgnUa0ktfAkUHd6CKu9qQk8/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/frontend/s11) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/frontend/s11/Manual.html) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                                  | Link                                                         |
| ------------------------------------------------------ | ------------------------------------------------------------ |
| Bootstrap CSS Framework                                | [Link](https://getbootstrap.com/)                            |
| Introduction to Bootstrap 4.6                          | [Link](https://getbootstrap.com/docs/4.6/getting-started/introduction/) |
| Bootstrap local vs. CDN                                | [Link](https://www.belugacdn.com/bootstrap-local-vs-cdn/)    |
| Responsive Meta Tag                                    | [Link](https://css-tricks.com/snippets/html/responsive-meta-tag/) |
| Bootstrap Navbar Component                             | [Link](https://getbootstrap.com/docs/4.6/components/navbar/) |
| Auto Indent On Sublime Text 3                          | [Link](https://stackoverflow.com/questions/44803547/autoindent-on-sublime-text) |
| How To Automatically Reindent Code With Sublime Text 3 | [Link](https://shibulijack.wordpress.com/2015/05/11/how-to-automatically-reindent-code-with-sublime-text-3/) |
| Boostrap Color Utility Classes                         | [Link](https://getbootstrap.com/docs/4.6/utilities/colors/)  |
| Bootstrap Spacing Utility Classes                      | [Link](https://getbootstrap.com/docs/4.6/utilities/spacing/) |
| Bootstrap Containers                                   | [Link](https://getbootstrap.com/docs/4.6/layout/overview/#containers) |
| Bootstrap Grid System classes                          | [Link](https://getbootstrap.com/docs/4.0/layout/grid/)       |
| Bootstrap Text Utility Classes                         | [Link](https://getbootstrap.com/docs/4.6/utilities/text/)    |
| Bootstrap Collapse Component                           | [Link](https://getbootstrap.com/docs/4.6/components/collapse/#accordion-example) |
| Bootstrap Display Utility Classes                      | [Link](https://getbootstrap.com/docs/4.0/utilities/display/) |
| Bootstrap Carousel Component                           | [Link](https://getbootstrap.com/docs/4.6/components/carousel/) |
| How Do You Decrease Carousel Size                      | [Link](https://forum.bootstrapstudio.io/t/how-do-you-decrease-carousel-size/5109) |
| Place Puppy                                            | [Link](https://place-puppy.com/)                             |
| Place Kitten                                           | [Link](https://placekitten.com/)                             |
| Place Bear                                             | [Link](https://placebear.com/)                               |
| Bootstrap Card Component                               | [Link](https://getbootstrap.com/docs/4.6/components/card/)   |
| CSS Flexbox                                            | [Link](https://www.w3schools.com/css/css3_flexbox.asp)       |
| Bootstrap Flex Classes                                 | [Link](https://getbootstrap.com/docs/4.0/utilities/flex/)    |
| Bootstrap Form Component                               | [Link](https://getbootstrap.com/docs/4.6/components/forms/)  |
| Bootstrap Modal Component                              | [Link](https://getbootstrap.com/docs/4.6/components/modal/)  |
| Rapid Prototyping                                      | [Link](https://devsquad.com/blog/what-is-rapid-prototyping-and-why-is-it-used-in-development/) |
| UI/UX Design                                           | [Link](https://careerfoundry.com/en/blog/ux-design/the-difference-between-ux-and-ui-design-a-laymans-guide/) |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1. [30 mins] - User Interface
	2. [2hr 30 mins] - Bootstrap Components
		- Navbar Component
		- Collapse Component
		- Carousel Component
		- Card Component
		- Form Component
		- Modal Component
	3. [1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

	1. Rapid Prototyping
	2. UI/UX Design

[Back to top](#table-of-contents)