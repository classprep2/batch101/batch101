# Session Objectives

At the end of the session, the students are expected to:

- learn how systems communicate with each other through the JavaScript Object Notation.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5/s20)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1wFgPDDrIuudyxqJ0oMYOrW1Zt6NKcpTSuUYQ06L5wQY)
- [Lesson Plan](lesson-plan.md)

## Supplemental Materials

- [JSON Intro (w3schools)](https://www.w3schools.com/js/js_json_intro.asp)
- [Core JSON (DZone)](https://dzone.com/refcardz/core-json)

# Lesson Proper

## Introduction

JSON, or JavaScript Object Notation, is a **data format** used by applications to **store** and **transport** data to one another.

Even though it has the "JavaScript" in its name, use of JSON is **not limited** to JavaScript-based applications and can also be used in other programming languages.

Files that store JSON data are saved with a file extension of `.json`.

## Why JSON?

Compared to other data formats, JSON is easier to read and its structure map to common programming concepts such as objects and arrays.

The code below is a sample of data in XML format:

```xml
<note>
		<to>Tove</to>
		<from>Jani</from>
		<heading>Reminder</heading>
		<body>Don't forget me this weekend!</body>
</note>
```

Showing the XML data format will allow the students to appreciate the benefits of using JSON instead of XML or other data formats.

Now, JSON expresses the data in the following format:

```jsx
{
    "to": "Tove",
    "from": "Jani",
    "heading": "Reminder",
    "body": "Don't forget me this weekend!"
}
```

The data formatted in JSON is easier to read than its XML counterpart and also requires less characters. Because of this, JSON also requires less bandwidth during data transfers because of its lightweight formatting.

# Code Discussion

## Folder and File Preparation

Create a folder named **s20**, a folder named **discussion** inside the **s20** folder, then a file named **index.html** and **index.js** inside the **discussion** folder.

## JSON Syntax

Writing JSON is **similar** to the object notation used in JavaScript, hence the name.

Although the JSON format is syntactically identical to JavaScript's object notation, there are some minor differences.

### Key/Value Pairs

Data in JSON is written just like the object properties in JavaScript.

```jsx
"firstName": "John"
```

The main difference is JSON key names **require double quotes**. JavaScript object property names do not.

### JSON Objects

Creating a JSON object does not differ much in the way we create objects in JavaScript.

```jsx
{
    "city": "Quezon City",
    "province": "Metro Manila",
    "country": "Philippines"
}
```

### JSON Arrays

JSON can also contain an array of objects.

```jsx
"cities": [
    { "city": "Quezon City", "province": "Metro Manila", "country": "Philippines" },
    { "city": "Manila City", "province": "Metro Manila", "country": "Philippines" },
    { "city": "Makati City", "province": "Metro Manila", "country": "Philippines" }
]
```

You will most likely get JSON arrays for requests that involve a list of records; JSON objects for requests on a single record.

## JSON in a File

Sometimes, JSON data can be contained in a file.

Create a file named **package.json** and add the following code.

```jsx
{
	  "name": "javascript-server",
	  "version": "1.0.0",
	  "description": "Boilerplate server application.",
	  "main": "index.js",
	  "scripts": {
		    "test": "echo \"Error: no test specified\" && exit 1",
	  },
	  "keywords": [],
	  "author": "John Smith",
	  "license": "ISC"
}
```

No in-depth discussion will be made regarding **package.json**. This section is meant for the students to familiarize how to read the demonstrated file.

## JSON Functions in JavaScript

When JSON data is sent or received, its format is **text-only** (string).

Look at the code below:

```jsx
let batchesArr = [{ batchName: 'Batch X' }, { batchName: 'Batch Y' }];
let batchesJSON = '[{ "batchName": "Batch X" }, { "batchName": "Batch Y" }]';
```

With an array of objects, the object's properties **do not need** to be enclosed in **double quotes**.

However, JSON text requires **double quotes** in the **properties**.

Having JSON data in text-only format will not be very usable.

To resolve this, JSON in JavaScript includes the functions **parse()** and **stringify()** to convert text-only JSON into JavaScript object and vice versa.

Before sending data, you can use **stringify** to convert an array or an object to its string equivalent.

```jsx
console.log(JSON.stringify(batchesArr));
```

Upon receiving data, the JSON text can be converted to a JavaScript object so that we can use it in our program.

```jsx
console.log(JSON.parse(batchesJSON));
```

Show the output of the log to the DevTools console.

# Activity

## Instructions

**Note**: Copy the code from activity-template.js into the batch Boodle Notes so students can copy the template of the code for this activity.

1. Given the code provided by your instructor, fix the JSON string by looking for errors when the code tries to parse the JSON string.
Note: The position mentioned in the given console error is the position of the error in the JSON string, not which line of code.

- Check and correct the JSON string if the following are missing:

  - double quotes
  - commas
  - colons
  - square/curly brackets
  - values

2. Create an object with the following properties:
  - Name (String)
  - Category (String)
  - Quantity (Number)
  - Model (String)

3. Then, stringify the object and save it in the given stringifiedProduct variable.

4. Update your local backend git repository and push to git with the commit message of Add activity code s22.
5. Add the link in Boodle for s25.

### Provided Code

**index.html**

```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>Activity: Introduction to JSON</title>
    </head>
    <body>
        <script src="./index.js"></script>
    </body>
</html>
```

**index.js**

```jsx
let users = `[
  {
    "id": ,
    "name": "Leanne Graham",
    "username": "Bret",
    "email": "Sincere@april.biz",
    "address": {
      "street": "Kulas Light",
      "suite": "Apt. 556",
      "city": "Gwenborough",
      "zipcode": "92998-3874",
      "geo": {
        "lat": "-37.3159",
        "lng": "81.1496"
      }
    },
    "phone": "1-770-736-8031 x56442",
    "website": hildegard.org",
    "company": {
      "name": "Romaguera-Crona",
      "catchPhrase": "Multi-layered client-server neural-net",
      "bs": "harness real-time e-markets"
    }
  },
  {
    "id": 2,
    "name": "Ervin Howell",
    "username": "Antonette",
    "email": "Shanna@melissa.tv",
    "address": {
      "street": "Victor Plains",
      "suite": "Suite 879",
      "city": "Wisokyburgh",
      "zipcode": "90566-7771",
      "geo": {
        "lat": "-43.9509",
        "lng": "-34.4618"
      }
    },
    "phone": "010-692-6593 x09125",
    "website": "anastasia.net",
    "company": {
      "name": "Deckow-Crist",
      "catchPhrase": "Proactive didactic contingency",
      "bs": synergize scalable supply-chains"
    }
  },
  {
    "id": 3,
    "name": "Clementine Bauch",
    "username": "Samantha",
    "email": "Nathan@yesenia.net",
    "address": {
      "street": "Douglas Extension",
      "suite": "Suite 847",
      "city": "McKenziehaven",
      "zipcode": "59590-4157",
      "geo": {
        "lat": "-68.6102",
        "lng": "-47.0653"
      }
    },
    "phone": "1-463-123-4447",
    "website": "ramiro.info",
    "company": 
      "name": "Romaguera-Jacobson",
      "catchPhrase": "Face to face bifurcated interface",
      "bs": "e-enable strategic applications"
    }
  },
  {
    "id": 4,
    "name": "Patricia Lebsack",
    "username": "Karianne",
    "email": "Julianne.OConner@kory.org",
    "address": {
      "street": "Hoeger Mall",
      "suite": "Apt. 692",
      "city": "South Elvis",
      "zipcode": "53919-4257",
      "geo": {
        "lat": "29.4572",
        "lng": "-164.2990"
      }
    },
    "phone": "493-170-9623 x156",
    "website": "kale.biz"
    "company": {
      "name": "Robel-Corkery",
      "catchPhrase": "Multi-tiered zero tolerance productivity",
      "bs": "transition cutting-edge web services"
    }
  },
  {
    "id": 5,
    "name": "Chelsey Dietrich",
    "username": "Kamren",
    "email": "Lucio_Hettinger@annie.ca",
    "address": {
      "street": "Skiles Walks",
      "suite": "Suite 351",
      "city": "Roscoeview",
      "zipcode": "33263",
      "geo": {
        "lat": "-31.8129",
        "lng": "62.5342"
      }
    },
    "phone": "(254)954-1289",
    "website": "demarco.info",
    "company": {
      "name": "Keebler LLC",
      "catchPhrase": "User-centric fault-tolerant solution",
      "bs": "revolutionize end-to-end systems"
    }
  },
  {
    "id": 6,
    "name": "Mrs. Dennis Schulist",
    "username": "Leopoldo_Corkery",
    "email": "Karley_Dach@jasper.info",
    "address": 
      "street": "Norberto Crossing",
      "suite": "Apt. 950",
      "city": "South Christy",
      "zipcode": "23505-1337",
      "geo": {
        "lat": "-71.4197",
        "lng": "71.7478"
      
    },
    "phone": "1-477-935-8478 x6430",
    "website": "ola.org",
    "company": {
      "name": "Considine-Lockman",
      "catchPhrase": "Synchronised bottom-line interface",
      "bs": "e-enable innovative applications"
    }
  },
  {
    "id": 7,
    "name": "Kurtis Weissnat",
    "username": "Elwyn.Skiles",
    "email": "Telly.Hoeger@billy.biz",
    "address": {
      "street": "Rex Trail",
      "suite": "Suite 280",
      "city": "Howemouth",
      "zipcode": "58804-1099",
      "geo": 
        "lat": "24.8918",
        "lng": "21.8984"
      
    },
    "phone": "210.067.6132",
    "website": "elvis.io",
    "company": {
      "name": "Johns Group",
      "catchPhrase": "Configurable multimedia task-force",
      "bs": "generate enterprise e-tailers"
    }
  },
  {
    "id": 8,
    "name": "Nicholas Runolfsdottir V",
    "username": "Maxime_Nienow",
    "email": "Sherwood@rosamond.me",
    "address": {
      "street": "Ellsworth Summit",
      "suite": "Suite 729",
      "city": "Aliyaview",
      "zipcode": "45169",
      "geo": {
        "lat": "-14.3990",
        "lng": "-120.7677"
      }
    }
    "phone": "586.493.6943 x140",
    "website": "jacynthe.com",
    "company": {
      "name": "Abernathy Group",
      "catchPhrase": "Implemented secondary concept",
      "bs": "e-enable extensible e-tailers"
    }
  },
  {
    "id": 9,
    "name": "Glenna Reichert",
    "username": "Delphine",
    "email": "Chaim_McDermott@dana.io",
    "address": {
      "street": "Dayna Park",
      "suite": "Suite 449",
      "city": "Bartholomebury",
      "zipcode": "76495-3109",
      "geo": {
        "lat": "24.6463"
        "lng": "-168.8889"
      }
    },
    "phone": "(775)976-6794 x41206",
    "website": "conrad.com",
    "company": {
      "name": "Yost and Sons",
      "catchPhrase": "Switchable contextually-based project",
      "bs": "aggregate real-time technologies"
    }
  },
  {
    "id": 10,
    "name": "Clementina DuBuque",
    "username": "Moriah.Stanton",
    "email": "Rey.Padberg@karina.biz",
    "address": {
      "street" "Kattie Turnpike",
      "suite": "Suite 198",
      "city": "Lebsackbury",
      "zipcode": "31428-2261",
      "geo": {
        "lat": "-38.2386",
        "lng": "57.2232"
      }
    },
    "phone": "024-648-3804",
    "website": "ambrose.net",
    "company": {
      "name": "Hoeger LLC",
      "catchPhrase": "Centralized empowering task-force",
      bs": "target end-to-end models"
    }
  }
]`;

console.log(JSON.parse(users));
```

## Expected Output

See the Solution section for the expected activity answers.

## Solution

JSON text without errors can be found [here](https://jsonplaceholder.typicode.com/users).

Lines with error:

- L003: missing value (ID: 1)
- L018: missing double quote
- L045: missing double quote
- L065: missing curly bracket (opening)
- L087: missing comma
- L122: missing curly bracket (opening)
- L130: missing curly bracket (closing)
- L150: missing curly bracket (opening)
- L150: missing curly bracket (closing)
- L177: missing comma
- L197: missing comma
- L215: missing colon
- L229: missing double quote