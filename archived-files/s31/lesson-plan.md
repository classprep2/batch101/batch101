# WDC028 - S25 - JavaScript - Introduction to JSON

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1wFgPDDrIuudyxqJ0oMYOrW1Zt6NKcpTSuUYQ06L5wQY/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1wFgPDDrIuudyxqJ0oMYOrW1Zt6NKcpTSuUYQ06L5wQY/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s20) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/backend/s20/Manual.js) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                         | Link                                                         |
| ----------------------------- | ------------------------------------------------------------ |
| JSON object                   | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON) |
| What Is Serialization         | [Link](https://hazelcast.com/glossary/serialization/)        |
| What Is A Byte                | [Link](https://searchstorage.techtarget.com/definition/byte) |
| What Are HTTP Request Methods | [Link](https://rapidapi.com/blog/api-glossary/http-request-methods/#:~:text=An%20HTTP%20request%20is%20an,is%20assigned%20a%20specific%20purpose.) |
| What Is A Database            | [Link](https://www.guru99.com/introduction-to-database-sql.html) |
| Node JS Documentation         | [Link](https://nodejs.org/en/about/)                         |
| What is Node JS               | [Link](https://effectussoftware.com/blog/node-js-a-framework/) |
| Express JS Documentation      | [Link](https://expressjs.com/)                               |
| What is Parse                 | [Link](https://www.techopedia.com/definition/3853/parse#:~:text=To%20parse%20is%20to%20break,each%20part's%20function%20or%20form.&text=Parsing%20is%20used%20in%20all,transformed%20into%20executable%20machine%20code.) |
| XML                           | [Link](https://www.w3schools.com/xml/xml_whatis.asp)         |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[30 mins] - JSON Objects
	2.[30 mins] - JSON Arrays
	3.[30 mins] - Stringified JSON
	4.[30 mins] - Stringify Methods with Variables
	5.[1 hr] - Converting Stringified JSON Into JavaScript Objects
	6.[1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

	1. XML

[Back to top](#table-of-contents)