/*
=====================================
S22 - JavaScript - Array Manipulation
=====================================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1n6Cl7jYEM93uxqRxnKWh_FboFfiBQdKPR3tCQc8SwO8/edit?pli=1#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s17

Other References:
	JavaScript Arrays
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array
		https://www.w3schools.com/js/js_arrays.asp
	/Method_definitions
	Why Do Arrays Start With Index 0
		https://albertkoz.com/why-does-array-start-with-index-0-65ffc07cbce8
	JavaScript Methods
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions
	Javascript Array Methods
		https://dev.to/ibrahima92/javascript-array-methods-mutator-vs-non-mutator-15e2
	Mutable Definition
		https://developer.mozilla.org/en-US/docs/Glossary/Mutable
	JavaScript Array push Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/push
	JavaScript Array pop Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/pop
	JavaScript Array unshift Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/unshift
	JavaScript Array shift Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/shift
	JavaScript Array splice Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice
	JavaScript Array sort Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort
	JavaScript Array reverse Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reverse
	Non-Mutator Array Methods In JavaScript
		http://www.discoversdk.com/blog/non-mutator-array-methods-in-javascript
	JavaScript Array indexOf Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf
	JavaScript Array lastIndexOf Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/lastIndexOf
	JavaScript Array slice Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/slice
	JavaScript Array toString Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/toString
	JavaScript Array concat Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/concat
	JavaScript Array join Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/join
	JavaScript Array forEach Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach
	JavaScript Array map Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map
	JavaScript Array every Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/every
	JavaScript Array some Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/some
	JavaScript Array filter Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
	JavaScript String includes Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/includes
	JavaScript Reduce Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce
	Creating Git Projects:
		GitLab
			https://gitlab.com/projects/new#blank_project
		GitHub
			https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
1. Create an "index.html" file.
	Batch Folder > S17  > Discussion > index.html
*/

		/*
		<!DOCTYPE HTML>
		<html>
		    <head>
		        <title>JavaScript Arrays</title>
		    </head>
		    <body>
		    </body>
		</html>
		*/

/*
2. Create an "script.js" file and to test if the script is properly linked to the HTML file.
	Application > script.js
*/

		console.log("Hello World!");

/*
3. Link the "script.js" file to our HTML file.
	Application > script.html
*/

		/*
		<!DOCTYPE HTML>
		<html>
		    <!-- ... -->
		    <body>
		    	<script src="./script.js"></script>
		    </body>
		</html>
		*/

		/*
		Important Note:
			- The "script" tag is commonly placed at the bottom of the HTML file right before the closing "body" tag.
			- The reason for this is because Javascript's main function in frontend development is to make our websites and applications interactive.
			- In order to achieve this, JavaScript selects/targets specific HTML elements in our application and performs a certain output.
			- It is added last to allow all HTML and CSS resources to load first before applying any JavaScript code to run.
			- Placing the "script" tag at the top the the file might result in errors because since the HTML elements have not yet been loaded when the JavaScript loads, it does not have any valid HTML elements to target/select.
		*/

/*
4. Add code to the "script.js" file to demonstrate and discuss Arrays and Indexes.
	Application > script.js
*/

		console.log("Hello World!");

		// [SECTION] Arrays and Indexes
		/*
		    - Arrays are used to store multiple related values in a single variable
		    - They are declared using square brackets ([]) also known as "Array Literals"
		    - Commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks
		    - Arrays also provide access to a number of functions/methods that help in achieving this
		    - A method is another term for functions associated with an object and is used to execute statements that are relevant to a specific object
		    - Majority of methods are used to manipulate information stored within the same object
		    - Arrays are also objects which is another data type
		    - The main difference of arrays with an object is that it contains information in a form of a "list" unlike objects which uses "properties"
		    - Syntax
		        let/const arrayName = [elementA, elementB, ElementC...]
		*/

		// Common examples of arrays
		let grades = [98.5, 94.3, 89.2, 90.1];
		let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];
		// Possible use of an array but is not recommended
		let mixedArr = [12, 'Asus', null, undefined, {}]; 

		// Alternative way to write arrays
		let myTasks = [
		    'drink html',
		    'eat javascript',
		    'inhale css',
		    'bake sass'
		];

		// Reassigning array values
		console.log('Array before reassignment');
		console.log(myTasks);
		myTasks[0] = 'hello world';
		console.log('Array after reassignment');
		console.log(myTasks);

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for JavaScript Arrays.
		*/

/*
5. Add more code to demonstrate and discuss Reading from Arrays.
	Application > script.js
*/

		/*...*/
		console.log(myTasks);

		// [SECTION] Reading from Arrays
		/*
		    - Accessing array elements is one of the more common tasks that we do with an array
		    - This can be done through the use of array indexes
		    - Each element in an array is associated with it's own index/number
		    - In JavaScript, the first element is associated with the number 0 and increasing this number by 1 for every element
		    - The reason an array starts with 0 is due to how the language is designed
		    - Array indexes actually refer to an address/location in the device's memory and how the information is stored
		    - Example array location in memory
		        Array address: 0x7ffe9472bad0
		        Array[0] = 0x7ffe9472bad0
		        Array[1] = 0x7ffe9472bad4
		        Array[2] = 0x7ffe9472bad8
		    - In the example above, the first element and the array itself points to the same memory location and therefore is at 0 elements away from the location of the array itself
		    - Syntax
		        arrayName[index];
		*/
		console.log(grades[0]);
		console.log(computerBrands[3]);
		// Accessing an array element that does not exist will return "undefined"
		console.log(grades[20]);

		// Getting the length of an array
		/*
		    - Arrays have access to the ".length" property similar to strings to get the number of elements present in an array
		    - This is useful for executing code that depends on the contents of an array
		*/
		console.log(computerBrands.length);

		if(computerBrands.length > 5){
		    console.log('We have too many suppliers. Please coordinate with the operations manager.');
		};

		// Accessing the last element of an array
		// Since the first element of an array starts at 0, subtracting 1 to the length of an array will offset the value by one allowing us to get the last element
		let lastElementIndex = computerBrands.length - 1;

		console.log(computerBrands[lastElementIndex]);

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for Why Do Arrays Start With Index 0.
		*/

/*
6. Add more code to demonstrate and discuss Array Methods and Array Mutator Methods.
	Application > script.js
*/

		/*...*/

		console.log(computerBrands[lastElementIndex]);

		// [SECTION] Array Methods


/*
9. Add more code to demonstrate and discuss Multidimensional arrays.
	Application > script.js
*/
		/*...*/
		console.log("Result of reduce method: " + reducedJoin);

		// [SECTION] Multidimensional Arrays
		/*
		    - Multidimensional arrays are useful for storing complex data structures
		    - A practical application of this is to help visualize/create real world objects
		    - Though useful in a number of cases, creating complex array structures is not always recommended especially since 
		*/

		let chessBoard = [
		    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
		    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
		    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
		    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
		    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
		    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
		    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
		    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
		];

		console.log(chessBoard);

		// Accessing elements of a multidimensional arrays
		console.log(chessBoard[1][4]);

		console.log("Pawn moves to: " + chessBoard[1][5]);

/*
========
Activity
========
*/

/*
1. Create an "activity" folder, an "index.html" file inside of it and link the "script.js" file.
	Batch Folder > S17 > Activity > index.html
*/

		/*
		<!DOCTYPE HTML>
		<html>
		    <head>
		        <title>Activity: JavaScript Arrays</title>
		    </head>
		    <body>
		    	<script src="./script.js"></script>
		    </body>
		</html>
		*/

/*
2. Create a "script.js" file and console log the message "Hello World" to ensure that the script file is properly associated with the html file.
	Application > script.js
*/

		console.log("Hello World");

/*
3. Create an empty "students" array which will store all the information to be manipulated in the activity.
	Application > script.js
*/

		// console.log("Hello World");

		let students = [];

/*
4. Create a function that will add a student to the "students" array.
	Application > script.js
*/

		/*...*/

		let students = [];

		function addStudent(name) {

		    students.push(name);

		    console.log(name + " was added to the student's list.");

		}

/*
5. Create a function that will print the number of students in the "students" array.
	Application > script.js
*/

		/*...*/

		function addStudent(name) {
			/*...*/
		}

		function countStudents() {

		    console.log('There are a total of ' + students.length + ' students enrolled.');

		}

/*
6. Create a function that will sort the "students" array and print all of the names of the students.
	Application > script.js
*/

		/*...*/

		function countStudents() {
			/*...*/
		}

		function printStudents() {

		    // Sorts the array elements in alphanumeric order
		    students.sort();

		    // Loops through all elements of the array printing all the names of the students
		    students.forEach(function(student) {
		        // Prints the name of the students one by one
		        console.log(student);
		    })

		}

/*
7. Create a function that will look for a student with a match to a specified keyword and then print the name of the student.
	Application > script.js
*/

		/*...*/

		function printStudents() {
			/*...*/
		}

		function findStudent(keyword) {

		    // Loops through all elements of the array to find the student name that matches the keyword provided
		    let match = students.filter(function(student) {

		        // If the student name includes the keyword provided
		        return student.toLowerCase().includes(keyword.toLowerCase());

		    })

		    // console.log(match);

		    // If a match was found the array's length will be 1
		    if (match.length == 1) {

		        console.log(match + ' is an Enrollee');

		    // If multiple elements are found
		    } else if (match.length > 1) {

		        console.log(match + ' are enrollees');

		    // If no match was found the array's length will be 0
		    } else {

		        console.log('No student found with the name ' + keyword);

		    }

		}

/*
8. Create an empty "sectionedStudents" array which will store the names of the students with their sections.
	Application > script.js
*/

		/*...*/
	
		let students = [];
		let sectionedStudents = [];

		function addStudent(name) {
			/*...*/
		}

		/*...*/

/*
9. Create a function that will add a section to all the students names.
	Application > script.js
*/

		/*...*/

		function findStudent(keyword) {
			/*...*/
		}

		function addSection(section) {

		    // Reassigns the value of the "map" method to the "sectionedStudents" array including the specified text and the section of the student
		    // Does not manipulate the original "students" array but creates a new one and stores it in a different variable
		    sectionedStudents = students.map(function(student){

		        // returns the name of the student with the added text and the section provided as a string
		        return student + ' - section ' + section; 

		    })

		    // Prints the array of sectionedStudents for verification
		    console.log(sectionedStudents);

		}

/*
10. Create a function that will look for the index of a given student in the "students" array and remove the name of the student.
	Application > script.js
*/

		/*...*/

		function addSection(section) {
			/*...*/
		}

		function removeStudent(name) {

		    // Converts the first letter of the name to uppercase
		    let firstLetter = name.slice(0, 1).toUpperCase();
		    // Retrieves all other letters of the name
		    let remainingLetters = name.slice(1, name.length);
		    // Combines the capitalized first letter and the remaining letters of the name
		    let capitalizedName = firstLetter + remainingLetters; 

		    // Used to retrieve the index of the name provided
		    let studentIndex = students.indexOf(capitalizedName);

		    // If a match is found, the index number would be 0 or greater
		    // The indexOf method will return a -1 value if the name is not found
		    if(studentIndex >= 0) {

		        // Manipulates the array to remove the element using the index of the name provided
		        students.splice(studentIndex, 1);
		        
		    }

		    console.log(name + " was removed from the student's list.");

		}