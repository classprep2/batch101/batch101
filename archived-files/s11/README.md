# Session Objectives

At the end of the session, the students are expected to:

- quickly apply styling to a web page by using a CSS framework called Bootstrap.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5/s08)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1GH5SjdamhHIaytmP8YYJYxKWleGmFVUNIun_iFgvzPQ)
- [Lesson Plan](lesson-plan.md)

## Supplemental Materials

- [A Short Bootstrap Tutorial on the What, Why, and How (Toptal)](https://www.toptal.com/front-end/what-is-bootstrap-a-short-tutorial-on-the-what-why-and-how)
- [Bootstrap CSS Framework](https://getbootstrap.com/)
- [Introduction to Bootstrap 4.6](https://getbootstrap.com/docs/4.6/getting-started/introduction/)

# Lesson Proper

## CSS Frameworks

In the era of multiple device screen sizes and rapid website development, CSS frameworks help developers create a responsive layout faster compared to using plain CSS.

The term **framework** can have various meanings, even in the software development field. In the context of this lesson, a **CSS framework** allows common web designs to be quickly incorporated into a website without writing extensive CSS code from scratch.

![readme-images/Untitled.png](readme-images/Untitled.png)

Sample site in desktop view.

![readme-images/Untitled%201.png](readme-images/Untitled%201.png)

Sample site in tablet view.

![readme-images/Untitled%202.png](readme-images/Untitled%202.png)

Sample site in mobile view.

## Bootstrap 4

Bootstrap is one of the most popular CSS frameworks. It was developed by **Mark Otto** and **Jacob Thornton** at Twitter. It was released as an open-source product in August 2011.

Open-source simply means that you can use a given technology or tool for free.

## Features of Bootstrap

- Easy Setup
    - Anyone with a knowledge of HTML and CSS can quickly start using Bootstrap.
    - It also has good documentation on how to use its features.
- Mobile-First Approach
    - It consists of mobile-first styling throughout its code that helps developers create websites that are friendly to the mobile screens (such as tablets and smartphones).

**Mobile-first approach** means that a website is developed for devices with small screen sizes first, then working the way up to the regular desktop screen sizes.

Mobile-first approach is preferred since most users nowadays accesses a website through a mobile device.

## Companies Using Bootstrap

- Spotify
- Snapchat
- Lyft
- Coursera
- Twitter
- LinkedIn

# Code Discussion

## Folder and File Preparation

Create a folder named **s08**, a folder named **discussion** inside the **s08** folder, then a file named **index.html** inside the **discussion** folder.

## Preliminary Code With Bootstrap

Inside the **index.html** file, add the following code:

```html
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"/>
        <title>Bootstrap Introduction and Simple Styles</title>
    </head>
    <body>
        <h1>Bootstrap Introduction and Simple Styles</h1>
        <table>
            <thead>
                <tr>
                    <th>Entity</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>&lt;</td>
                    <td>less than</td>
                </tr>
                <tr>
                    <td>&gt;</td>
                    <td>greater than</td>
                </tr>
                <tr>
                    <td>&#8369;</td>
                    <td>peso sign</td>
                </tr>
                <tr>
                    <td>&amp;</td>
                    <td>ampersand</td>
                </tr>
                <tr>
                    <td>&quot;</td>
                    <td>double quotation mark</td>
                </tr>
                <tr>
                    <td>&apos;</td>
                    <td>single quotation mark (apostrophe)</td>
                </tr>
                <tr>
                    <td>&ntilde;</td>
                    <td>Lower Tilde</td>
                </tr>
                <tr>
                    <td>&Ntilde;</td>
                    <td>Upper Tilde</td>
                </tr>
            </tbody>
        </table>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    </body>
</html>
```

We can immediately see that Bootstrap's styling has taken effect by looking at the font style applied to the text currently in the page.

The explanation for the `integrity` and `crossorigin` attributes found in the `link` and `script` tags can be found [here](https://stackoverflow.com/a/32042283).

A **Content Delivery Network** (or CDN) is a geographically distributed group of servers that work together to provide fast delivery of Internet content. 

It allows for the quick transfer of Internet content including HTML pages, JavaScript files, stylesheets, images, and most importantly videos.

![readme-images/Untitled%203.png](readme-images/Untitled%203.png)

## Table Classes

To use Bootstrap, all we need is to add a class name designed for a given element. In the case of tables, let's add the following class:

```html
<table class="table">
```

Once the page has refreshed, we can see that the table styling has changed.

![readme-images/Untitled%204.png](readme-images/Untitled%204.png)

You can show to the students the applied styles by inspecting the table element using the DevTools.

The applied styles in the table can be inspected through the browser DevTools.

After that, we add more classes to the table element.

```html
<table class="table table-dark table-bordered">
```

The table then becomes like this:

![readme-images/Untitled%205.png](readme-images/Untitled%205.png)

The complete reference for the table element classes can be found [here](https://getbootstrap.com/docs/4.6/content/tables/).

## Wrapping Content in Container

As you might have noticed, the elements are being displayed up to the edge of the browser window. To give it a little space, we wrap the heading and the table by adding a div element with the corresponding class name.

```html
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"/>
        <title>Bootstrap Introduction and Simple Styles</title>
    </head>
    <body>
        <div class="container-fluid">
            <h1>Bootstrap Introduction and Simple Styles</h1>
            <table class="table table-dark table-bordered">
                ...
            </table>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    </body>
</html>
```

The page should now look like this.

![readme-images/Untitled%206.png](readme-images/Untitled%206.png)

The `container` class can also be demonstrated to the students.

The `container` class has a fixed max-width value while the `container-fluid` class has a max-width of 100%.

## Bootstrap Colors

Bootstrap also has built-in color classes that can be used on certain elements.

Put the proceeding codes immediately after the table element.

### Text Colors

Add the following code and display the output.

```html
<p><a href="#" class="text-primary">Primary link</a></p>
<p><a href="#" class="text-secondary">Secondary link</a></p>
<p><a href="#" class="text-success">Success link</a></p>
<p><a href="#" class="text-danger">Danger link</a></p>
<p><a href="#" class="text-warning">Warning link</a></p>
<p><a href="#" class="text-info">Info link</a></p>
<p><a href="#" class="text-light bg-dark">Light link</a></p>
<p><a href="#" class="text-dark">Dark link</a></p>
<p><a href="#" class="text-muted">Muted link</a></p>
<p><a href="#" class="text-white bg-dark">White link</a></p>
```

As you might have noticed, some anchors had a `bg-` class. These are background color classes.

### Background Colors

Add the following code and display the output.

```html
<div class="p-3 mb-2 bg-primary text-white">.bg-primary</div>
<div class="p-3 mb-2 bg-secondary text-white">.bg-secondary</div>
<div class="p-3 mb-2 bg-success text-white">.bg-success</div>
<div class="p-3 mb-2 bg-danger text-white">.bg-danger</div>
<div class="p-3 mb-2 bg-warning text-dark">.bg-warning</div>
<div class="p-3 mb-2 bg-info text-white">.bg-info</div>
<div class="p-3 mb-2 bg-light text-dark">.bg-light</div>
<div class="p-3 mb-2 bg-dark text-white">.bg-dark</div>
<div class="p-3 mb-2 bg-white text-dark">.bg-white</div>
<div class="p-3 mb-2 bg-transparent text-dark">.bg-transparent</div>
```

Reference for Bootstrap colors can be found [here](https://getbootstrap.com/docs/4.6/utilities/colors).

Reference for Bootstrap margin and padding classes can be found [here](https://getbootstrap.com/docs/4.6/utilities/spacing).

## Text Classes

### Removing Text Decoration

```html
<a href="#" class="text-decoration-none">Non-underlined link</a>
```

### Text Transformation

```html
<p class="text-lowercase">Lowercased text.</p>
<p class="text-uppercase">Uppercased text.</p>
<p class="text-capitalize">CapiTaliZed text.</p>
```

Reference for Bootstrap text classes can be found [here](https://getbootstrap.com/docs/4.6/utilities/text).

Feel free to explore more of the Bootstrap [utility classes](https://getbootstrap.com/docs/4.6/utilities) for basic styling. 

# GitLab Upload

After finishing the code discussion, demonstrate to the students how to create a GitLab project inside their subgroup and push the code discussion with a commit message of "Add discussion code".

# Activity

The main purpose of this activity is to make the students get acquainted with using the documentation of Bootstrap.

This activity will be continuously improved by the students until they reach Capstone 1.

## Instructions

- Create a basic web page with Bootstrap and include the following:
    - Your professional photo
    - Your name (as title)
    - Your occupation (as subtitle, write "Full-Stack Web Developer")
    - Your self-description (as paragraph)
- By searching the Bootstrap [documentation](https://getbootstrap.com/docs/4.6/utilities), apply the following styling:
    - Make the photo, name and occupation center on the page.
    - Make the description justify its content.
    - Make the photo have rounded borders.
    - Spacing like margins and paddings, if necessary.
- Use the search function in the left side of Bootstrap documentation.

![readme-images/Untitled%207.png](readme-images/Untitled%207.png)

If a student is finished, let him/her explore Bootstrap more by adding more content to the page without 

## Expected Output

![readme-images/Untitled%208.png](readme-images/Untitled%208.png)

## Solution

The solution may vary from student to student, but the code may look like this:

```html
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"/>
        <title>Activity: Developer Portfolio</title>
    </head>
    <body>
        <div class="p-4 container-fluid">
            <div class="text-center mb-4">
                <img class="rounded-circle mb-3" width="200" src="https://i.guim.co.uk/img/media/3656ae6ea2209d4561caf04fa9f172a519908ca3/0_28_2318_1391/master/2318.jpg?width=1200&height=1200&quality=85&auto=format&fit=crop&s=afcc355f47876bc495cdc3c902639bae"/>
                <h2>Ada Lovelace</h2>
                <h4>Full-Stack Web Developer</h4>
            </div>
            <p class="text-justify">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            </p>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    </body>
</html>
```