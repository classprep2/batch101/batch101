# Topics
* Managing Routes Review
* HTTP methods
* Create & retrieve operations in Node.js

# Resources
## Instructional Materials
* [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s27)
* [Google Slide Presentation](https://docs.google.com/presentation/d/1vMbhXjIE2pSKSR39idsQiyNCuf80LN48YL74BCGz-uk/edit?usp=sharing)
* [Lesson Plan](lesson-plan.md)

