# WDC028 - S36 - Express.js - Modules and Parameterized Routes

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1_VUaNcoacbzdfSh-fYYFmoE8UeRLuFd85IVCzRCElj0/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1_VUaNcoacbzdfSh-fYYFmoE8UeRLuFd85IVCzRCElj0/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s31) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/backend/s31/Manual.js) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                | Link                                                         |
| ------------------------------------ | ------------------------------------------------------------ |
| npm init                             | [Link](https://docs.npmjs.com/cli/v7/commands/npm-init)      |
| MongoDB Atlas                        | [Link](https://cloud.mongodb.com/)                           |
| Get Started With Atlas               | [Link](https://docs.atlas.mongodb.com/getting-started/)      |
| Express JS                           | [Link](https://expressjs.com/)                               |
| What Is Mongoose                     | [Link](https://mongoosejs.com/)                              |
| Mongoose Documentation               | [Link](https://mongoosejs.com/docs/)                         |
| MVC Framework                        | [Link](https://www.tutorialspoint.com/mvc_framework/mvc_framework_introduction.htm) |
| Express JS Routing                   | [Link](https://expressjs.com/en/guide/routing.html)          |
| Mongoose find Method                 | [Link](https://mongoosejs.com/docs/api.html#model_Model.find) |
| Mongoose save Method                 | [Link](https://mongoosejs.com/docs/api.html#document_Document-save) |
| Express JS Static and Dynamic Routes | [Link](https://dev.to/reiallenramos/create-an-express-api-static-and-dynamic-routes-33lb) |
| Mongoose findByIdAndRemove           | [Link](https://mongoosejs.com/docs/api.html#model_Model.findOneAndRemove) |
| Mongoose findById                    | [Link](https://mongoosejs.com/docs/api.html#model_Model.findById) |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[10 mins] - Creation of node project
	2.[10 mins] - Installation of packages
		a. Express
		b. Mongoose
	3.[10 mins] - Creation of Server file
	4.[30 mins] - Creation of Model file
	5.[30 mins] - Creation of Controller file
	6.[30 mins] - Creation of Route file
	7.[30 mins] - Creation of controller functions and linking to routes
	8.[30 mins] - Testing with Postman
	9.[1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

​	

[Back to top](#table-of-contents)
