# S31 - Modules, Parameterized Routes

## Topics
* Separation of concerns
    * Models
    * Controllers
    * Routes
* JS Modules
* URL Parameterization
* Update Resource
* Delete Resource

## Presentation
[Google Slides](https://docs.google.com/presentation/d/1_VUaNcoacbzdfSh-fYYFmoE8UeRLuFd85IVCzRCElj0/edit?usp=sharing)

## Codealong
1. Create a directory named s31. Navigate to this directory.

2. Initialize NPM in this package via the terminal command:
> npm init -y
* -y option is passed to say yes to all prompts

3. Install Express.js and Mongoose via the terminal command:
> npm install express mongoose

4. Create a file named app.js. This will serve as our application's entry point. Code it as follows:
```javascript
//Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute");
//With this, we could use all the routes in taskRoute.js

//database connection
mongoose.connect("mongodb+srv://database-admin:admin_1234@sampledb.zxc7v.mongodb.net/b84_todo?retryWrites=true&w=majority", {useNewUrlParser:true});
mongoose.connection.once("open", ()=>console.log("Now connected to the database"));

//server setup
const app = express()
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//add the task route
app.use("/tasks", taskRoute); //By doing this, all the task routes would start with /tasks

//server listening
app.listen(port, () => console.log(`Now listening to port ${port}`));
```

5. We imported taskRoute in our app entry point via the require() directive. However, this does not exist yet. At the **root** of our project directory, create a **routes** folder. In it, create a **taskRoute.js** file. Let's define our routes as follows:
```javascript
//Routes - contain all the endpoints for our application
//Instead of putting the routes in app.js, 
//we separate the routes such that app.js only contains information on the server
//With this, we need to use express' Router() function
const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController");
//the taskController allows us to use the controller's functions

//Route to get all the tasks
router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
	//Note that there is no process that happens in the routes, it just calls the needed function from the controller. All the processing happens in the controller.
})

//Route to create a new task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
	//the createTask needs the data from the body, so we need to supply it to the function
})

//route to update a task based on task ID passed in as a URL parameter denoted by :id
router.put("/:id", (req, res) => {
	//:id is a wildcard where you can put any value, it then creates a link between id and the value
		//Ex. localhost:3000/tasks/1234 -> the value of 1234 is assigned to id.
	//URL parameter values are accessed via the req.params object. The property name of this object will match the given URL parameter name (id in our case)
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

//Delete a task
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})
```

6. You can see in the taskRoute.js file that we required a taskController module. This controller will contain all the logic for carrying out operations involving the task resource. At the project root directory, create a folder named controllers. In it, create a taskController.js file and define it as follows:
```javascript
//Controllers contain the functions of your application
//Meaning all the operations it can do will be placed in this folder

//Use the model file created earlier
const Task = require("../models/task"); //This says that it uses the contents of the task.js file in the model folder
//Meaning it uses the Task model

//Create the functions and export these functions
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}
//.then can be used to wait for the find() to finish before sending the result.

module.exports.createTask = (body) => {
	let newTask = new Task({
		name : body.name
	})

	return newTask.save().then((task, error) => {
		if (error) {
			console.log(error);
			return false; //save unsuccessful
		} else {
			return task; //save successful, return new task
		}
	})
	//.then waits until the saving is complete before running the function to return true or false

	//The return false / return task only says to the newTask.save() whether the save was sucessful or not.
	//However, newTask.save was not able to tell createTask whether the saving was successful
	//In the real world the analogy is this: The person who was in the kitchen was able to make your order and told it to your cashier, however, your cashier was not able to pick up the order and give it to you.
	//That's why, we need to add the return to newTask.save() so it can tell createTask that the saving was successful.
}

//taskId is the URL parameter id passed in from taskRoute.js
//newContent is the req.body passed in from taskRoute.js
module.exports.updateTask = (taskId, newContent) => {
	//1. get the task with the id using findById
	//2. replace the task's name with the name from the body
	//3. save the task
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(err);
			return false;
		}
		//results of the findById will be placed in result
		result.name = newContent.name;
		return result.save().then((updatedTask, saveErr) => {
			if (saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updatedTask;
			}
		})
	})
}

module.exports.deleteTask = (taskId) => {
	//1. look for the task with the corresponding id
	//2. use the remove() to delete the task from the database
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		}else{
			return removedTask;
		}
	})
}
```

7. Notice that the controller performs its CRUD actions via the **Task model object**. Let's create this now. Create a directory named **models** at the project's **root** directory. In this folder, create a file named **task.js**. Define it as follows:
```javascript
//Create the Schema, model and export the file
const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		default : "pending"
	}
});

module.exports = mongoose.model("Task", taskSchema);
//module.exports is a way for node to treat this value as a "package" that can be used by other files
```

8. At this point, all the parts of our API are in place. Run our API via the terminal command:
> node app.js

9. Import the collection of requests included here in Postman to demonstrate the request-response cycles involving the 4 CRUD endpoints.

## Activity
1. Have the students define the controller action and route for retrieving a **single** task based on its **ID** sent via **URL parameters**.
* Solution
> controller action
```javascript
module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		//Q: What do you think findById does? it looks for the document using the _id field
		//findById is the same as find({"_id" : value})
		if(error){
			console.log(error);
			return false;
		}else{
			return result;	
		}
	})
}
```

> route
```javascript
router.get("/:id", (req, res) => { 
	taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));	
})
```
* Expected Result
> see matching request in the activity subcollection of the provided Postman collection

2. Define the controller action and route for setting a task's status as **complete** when a **PUT** request is received at the **/:id/complete** endpoint. 
* Solution
> controller action
```javascript
module.exports.completeTask = (taskId) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err);
			return err;
		}
		result.status = "complete";
		return result.save().then((updatedTask, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return saveErr;
			} else {
				return updatedTask;
			}
		})
	})
}
```

> route
```javascript
//Update a task, change its status to "complete"
router.put("/:id/complete", (req, res) => {
	taskController.completeTask(req.params.id).then(resultFromController => res.send(resultFromController));
})
```

* Expected Result
> see matching request in the activity subcollection of the provided Postman collection