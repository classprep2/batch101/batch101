# WDC028 - S32 - Node.js - Routing w/ HTTP Methods

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1vMbhXjIE2pSKSR39idsQiyNCuf80LN48YL74BCGz-uk/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1vMbhXjIE2pSKSR39idsQiyNCuf80LN48YL74BCGz-uk/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s27) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/backend/s27/Manual.js) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                              | Link                                                         |
| ---------------------------------- | ------------------------------------------------------------ |
| What Does Module Mean              | [Link](https://www.techopedia.com/definition/3843/module)    |
| An overview of HTTP                | [Link](https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview) |
| Node JS Documentation createServer | [Link](https://nodejs.org/api/http.html#http_http_createserver_options_requestlistener) |
| Request Object                     | [Link](https://developer.mozilla.org/en-US/docs/Web/API/Request) |
| Response Object                    | [Link](https://developer.mozilla.org/en-US/docs/Web/API/Response) |
| What is a Computer Port?           | [Link](https://www.cloudflare.com/learning/network-layer/what-is-a-computer-port/) |
| HTTP Response Status Codes         | [Link](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status) |
| Rest API Table                     | [Link](https://miro.medium.com/max/1838/1*77_S1ANTdgFDmRBD5_Jf7w.jpeg) |
| Request Response Cycle             | [Link](https://iq.opengenus.org/content/images/2019/09/Untitled-1-.png) |
| Postman Download Link              | [Link](https://www.postman.com/downloads/)                   |
| JSON.stringify method              | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify) |
| What is a Stream?                  | [Link](https://study.com/academy/lesson/streams-in-computer-programming-definition-examples.html) |
| Node JS Stream Documentation       | [Link](https://nodejs.org/api/stream.html#stream_event_data) |
| JSON.parse method                  | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/parse) |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[5 mins] - Preparation of index.js
	2.[10 mins] - Creation of a server
	3.[5 mins] - Port Definition
	4.[30 mins] - Sending Responses
	5.[5 mins] - Running the server
	6.[10 mins] - Creation of test routes
	7.[20 mins] - Testing in Postman
	8.[10 mins] - Creation of CRUD file
	9.[30 mins] - Creation of user routes
		a. Getting entries
		b. Creating new entries
	10.[20 mins] - Testing of routes
	11.[10 mins] - Creation of other files
	12.[30 mins] - Creation of courses routes
		a. Create course
		b. Get courses
		c. Update courses
		d. Delete / Archive courses
	13.[1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

​	

[Back to top](#table-of-contents)
