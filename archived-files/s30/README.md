# Session Objectives

At the end of the session, the students are expected to:

- learn to refactor some codes discussed in the previous sessions by using the ES6 updates.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5/s19)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1vXGELPWYQ8K5P-WbCo6RCBiHRLNiIKIboKb19-Dq6v8)
- [Lesson Plan](lesson-plan.md)

## Supplemental Materials

- [ES2015+ Cheat Sheet (devhint)](https://devhints.io/es6)

# Lesson Proper

ES in ES6 means **ECMAScript**.

ECMAScript is the **standard** that is used to create the **implementations** of the language, one of which is JavaScript.

ES6 (also known as ECMAScript 2015) is an **update** to the **previous versions** of the ECMAScript.

This means that the ES6 update brings **new features** to JavaScript.

This course will **focus** on **basic** updates from ES6 (ECMAScript 2015) only since it will allow us to **write less** and **do more**.

The updates from ES7 (ECMAScript 2016) onwards can also benefit developers. However, it will currently have little value when learning JavaScript as beginners.

# Code Discussion

## Folder and File Preparation

Create a folder named **s19**, a folder named **discussion** inside the **s19** folder, then a file named **index.html** and **index.js** inside the **discussion** folder.

## Exponent Operator

An exponent operator has been added to simplify the calculation for the exponent of a given number.

```jsx
const squared = 8 ** 2; // Same as Math.pow(8, 2)
```

## Template Literals

```jsx
// Pre-Template Literal Multi-Line String

let message = 'Hello World!\n' + 
    'Welcome to programming!';

// Multi-Line String Using Template Literal

let message = `
    Hello World!\n
    Welcome to programming!
`;
```

## Array and Object Destructuring

The destructuring assignment syntax is a JavaScript expression that makes it **possible** to **unpack elements** from **arrays**, or **properties** from **objects**, into **distinct variables**.

### Array Destructuring

For example, we have an array that contains the name of a person. If we want to combine it to a full name, we will need to do it like this:

```jsx
let name = ["Juan", "Dela", "Cruz"];
let fullName = `${ name[0] } ${ name[1] } ${ name[2] }`;
```

The code above is not that readable at first glance. This is where array destructuring comes into play.

```jsx
let [firstName, middleName, lastName] = name;
let fullName = `${ firstName } ${ middleName } ${ lastName }`;
```

With array destructuring, we can **give names** to the **array elements** with pre-defined index orders instead of accessing array elements via their index numbers.

### Object Destructuring

Getting the properties of an object can be tedious, especially if the name of the object is long. Take a look at the code below:

```jsx
const person = {
    firstName: "Juan",
    middleName: "Dela",
    lastName: "Cruz"
};

const getFullName = (firstName, middleName, lastName) => {
    return `${ firstName } ${ middleName } ${ lastName }`;
}

getFullName(person.firstName, person.middleName, person.lastName);
```

With destructuring objects, we can pick which properties to get from the object and use it in our code:

```jsx
const person = {
    firstName: "Juan",
    middleName: "Dela",
    lastName: "Cruz"
};

const getFullName = ({ firstName, lastName }) => {
    return `${ firstName } ${ lastName }`;
}

getFullName(person);
```

## Arrow Functions

Before ES6, function expression is created through the following syntax:

```jsx
function printFullName (firstName, middleInitial, lastName) {
    return firstName + ' ' + middleInitial + '. ' + lastName;
}
```

In ES6, we can create functions using the arrow notation, known as function expression.

```jsx
const printFullName = (firstName, middleInitial, lastName) => {
    return firstName + ' ' + middleInitial + '. ' + lastName;
}
```

This is a function expression, where in we are assigning an **anonymous** function to a variable.

This is why it is **given as a value** to a **constant** to give it a **name**.

### Implicit Return Statement

Additionally, arrow functions can implicitly return a value when the curly brackets are omitted.

```jsx
const add = (x, y) => x + y;
```

If curly brackets exists in an arrow function, you will have to explicitly use the return keyword.

```jsx
const add = (x, y) => {
    return x + y;
}
```

## Default Function Argument Value

If a function does not receive an argument, we can now set the default value of a parameter.

```jsx
const greet = (name = 'User') => {
    return `Good morning, ${ name }!`;
}

greet();
greet('John');
```

# Activity

## Instructions

Given the following code:

```jsx
function getCube(num) {
    return Math.pow(num, 3);
}

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number) {
    return number * number;
});

let allValid = numbers.every(function(number) {
    return (number < 3);
});

let someValid = numbers.some(function(number) {
    return (number < 2);
});

let filterValid = numbers.filter(function(number) {
    return (number <  3);
});

let reduceNumber = numbers.reduce(function(x, y) {
    return x + y;
});
```

- Convert the given code using the ES6 updates discussed earlier.
- Add a function in the Address blueprint that will print the whole address (use the backticks to create the resulting string).

## Expected Output

See the Solution section for the expected output.

## Solution

```jsx
const getCube = (num) => num ** 3;

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map((number) => number * number);

let allValid = numbers.every((number) => number < 3);

let someValid = numbers.some((number) => number < 2);

let filterValid = numbers.filter((number) => number <  3);

let reduceNumber = numbers.reduce((x, y) => x + y);
```