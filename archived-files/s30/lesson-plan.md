# WDC028 - S24 - JavaScript - ES6 Updates

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1vXGELPWYQ8K5P-WbCo6RCBiHRLNiIKIboKb19-Dq6v8/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1vXGELPWYQ8K5P-WbCo6RCBiHRLNiIKIboKb19-Dq6v8/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s19) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s19/Manual.js) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                              | Link                                                         |
| -------------------------------------------------- | ------------------------------------------------------------ |
| JavaScript ES6                                     | [Link](https://www.w3schools.com/js/js_es6.asp)              |
| JavaScript Exponentation Operator                  | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Exponentiation) |
| JavaScript Template Literals                       | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals) |
| JavaScript Expressions and Statements              | [Link](https://medium.com/launch-school/javascript-expressions-and-statements-4d32ac9c0e74) |
| JavaScript Destructuring Assignment                | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment) |
| JavaScript Arrow Functions                         | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions) |
| JavaScript Default Parameters                      | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Default_parameters) |
| JavaScript Classes                                 | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes) |
| Object Oriented Programming (OOP) - Free Code Camp | [Link](https://www.freecodecamp.org/news/four-pillars-of-object-oriented-programming/) |
| Object Oriented Programming (OOP) - Educative      | [Link](https://www.educative.io/blog/object-oriented-programming) |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[10 mins] - ECMA Script 2015
	2.[10 mins] - Exponent Operator
	3.[20 mins] - Template Literals
	4.[20 mins] - Array Destructuring
	5.[30 mins] - Object Destructuring
	6.[30 mins] - Arrow Functions
	7.[10 mins] - Implicit Return Statement
	8.[10 mins] - Default Function Argument Value
	9.[40 mins] - JavaScript Classes
	10.[1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

	1. Object Oriented Programming (OOP)

[Back to top](#table-of-contents)