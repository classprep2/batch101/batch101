# WDC028 - S31 - Node.js - Introduction

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1H09Hsl0OVi74b3U-sYl9jvwWS2rrHSGbBrQmdH8EO_Q/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1H09Hsl0OVi74b3U-sYl9jvwWS2rrHSGbBrQmdH8EO_Q/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s26) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/backend/s26/Manual.js) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                              | Link                                                         |
| ---------------------------------- | ------------------------------------------------------------ |
| Bootcamp Installation Guides       | [Link](https://drive.google.com/drive/u/0/folders/1lHKIYXYis1rolnyVTiLnQPPS7gglS65y) |
| What Does Module Mean              | [Link](https://www.techopedia.com/definition/3843/module)    |
| An overview of HTTP                | [Link](https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview) |
| Node JS Documentation createServer | [Link](https://nodejs.org/api/http.html#http_http_createserver_options_requestlistener) |
| Request Object                     | [Link](https://developer.mozilla.org/en-US/docs/Web/API/Request) |
| Response Object                    | [Link](https://developer.mozilla.org/en-US/docs/Web/API/Response) |
| What is a Computer Port?           | [Link](https://www.cloudflare.com/learning/network-layer/what-is-a-computer-port/) |
| HTTP Response Status Codes         | [Link](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status) |
| Request Response Cycle             | [Link](https://iq.opengenus.org/content/images/2019/09/Untitled-1-.png) |
| Nodemon                            | [Link](https://www.npmjs.com/package/nodemon)                |
| npm install                        | [Link](https://docs.npmjs.com/cli/v7/commands/npm-install)   |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[5 mins] - Verify installation in NodeJS
	2.[30 mins] - Creation of a server
	3.[5 mins] - Port Definition
	4.[30 mins] - Sending Responses
	5.[5 mins] - Running the server
	6.[10 mins] - Checking of the server activity
	7.[30 mins] - Creation of route file
	8.[5 mins] - Nodemon installation
		a. Running of nodemon
	9.[30 mins] - Creation of routes
	10.[1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

​	

[Back to top](#table-of-contents)
