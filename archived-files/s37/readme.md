# S26 - Node.js Introduction

## Topics
* Client-server architecture
* What is Node.js?
* Basic server setup
* Route handling

## Presentation
[Google Slides](https://docs.google.com/presentation/d/1H09Hsl0OVi74b3U-sYl9jvwWS2rrHSGbBrQmdH8EO_Q/edit?usp=sharing)

## Codealong
1. Verify that both npm and node are installed and added to the students' local environment paths.
* if some students are non-compliant, refer them to the installation guide provided pre-bootcamp.

2. Create a folder named s26.

3. Navigate to s26 and create a file named index.js.

4. Create a simple server in index.js with the following code:
```javascript
//use the require directive to load Node.js modules
//the http module lets Node.js transfer data using the Hyper Text Transfer Protocol
//using this module's createServer() method, we can create an HTTP server that listens to requests on a specified port and gives responses back to the client
let http = require("http")

//the http module has a createServer() method that accepts a function as an argument
//the function argument has parameters for receiving requests and sending responses
http.createServer(function (request, response) {
   //use the writeHead() method to:
      //set a status code for the response - a 200 means OK
      //set the content-type of the response as plain text
   response.writeHead(200, {'Content-Type': 'text/plain'})
   
   //send the response with text content 'Hello World'
   response.end('Hello World')
}).listen(4000)//specify port where server will listen for incoming requests

// when server is running, console will print the message:
console.log('Server running at localhost:4000')
```

5. Run the server with the terminal command:
> node index.js
* upon doing so, you will see this in the terminal:
![step 5a](./screenshots/step5a.png)
* visiting localhost:4000 in the browser will yield the following result:
![step 5b](./screenshots/step5b.JPG)

6. As of now our simple server can handle requests at its base URL only. What if we want our server to return different responses when different endpoints are accessed (i.e. have a /greeting and /homepage endpoints that will return different responses)? Let's create a new file named **routes.js** in our project directory.

7. Implement route handling in routes.js with the following code:
```javascript
let http = require("http")
//the url module splits up a web address into readable parts
let url = require("url")

http.createServer(function (request, response) {

	//the url module's parse() method returns an object w/ each part of the address as properties
	//the pathname property of this object contains the endpoint of the URL (i.e. if URL is localhost:4000/greeting, reqUrl.pathname == "/greeting")
	let reqUrl =  url.parse(request.url, true)
	if(reqUrl.pathname == "/greeting"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Hello World')
	}

	if(reqUrl.pathname == "/homepage"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('This is the homepage')
	}
}).listen(4000)

// Console will print the message
console.log('Server running at localhost:4000')
```

8. **Stop** our previous server by hitting **CTRL+C** on our keyboard at the terminal. 
* attempting to run more than one server on the same port will result in the following error:
![step 8](./screenshots/step8.png)

9. Serve our new file with the terminal command:
> node routes.js

10. Visiting localhost:4000/homepage will show this on the browser:
![step 10](./screenshots/step10.png)

11. Visiting localhost:4000/greeting will show this on the browser:
![step 11](./screenshots/step11.png)

## Activity
### Quiz
1. What directive is used by Node.js in loading the modules it needs?
> require()

2. What Node.js module contains a method for server creation?
> http module

3. What is the method of the http object responsible for creating a server using Node.js?
> createServer()

4. What method of the response object allows us to set status codes and content types?
> writeHead()

5. Where will console.log() output its contents when run in Node.js?
> terminal

6. What Node.js module lets us split a given web address into readable parts?
> url

7. What method of the url module returns an object with the parts of the address as its properties?
> parse()

8. What property of the parsed URL object contains the address's endpoint?
> pathname
