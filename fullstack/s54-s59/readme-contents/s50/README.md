# Session Objectives

At the end of the session, the students are expected to:

- consume the API created earlier in the bootcamp by using Fetch inside the React.js frontend.

# Resources

## Instructional Materials

- GitLab Repository
- Google Slide Presentation

## Supplemental Materials

- [Using Fetch (MDN Web Docs)](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch)

# Lesson Proper

We already have an existing simulated login procedure inside the Login page component. We will update it so that whenever a login form is submitted, it will send the information to the server and wait for the response of the server.

The server that we are going to use is the one that was built during the 5-session discussion of Express.js API development (Course Booking API).

Before proceeding, make sure that the students have the code and the server is running properly on their local machines.

# Code Discussion

## Adding Fetch in Login Page

Go to **src/pages/Login.js** and add the highlighted code:

```jsx
// Base Imports
import React, { useState, useContext } from 'react';
import { Redirect } from 'react-router-dom';

// App Imports
import UserContext from 'UserContext';

// Bootstrap Components
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';

export default function Login() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const { user, setUser } = useContext(UserContext);

    function login(e) {
        e.preventDefault();

        fetch('http://localhost:4000/users/login', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ email: email, password: password })
        })
        .then((response) => response.json())
        .then((response) => {
            if (response.accessToken !== undefined) {
                localStorage.setItem('accessToken', response.accessToken);
                setUser({ accessToken: response.accessToken });
            } else {
                alert(response.error);
                setEmail('');
                setPassword('');
            }
        })
    }

    if (user.accessToken !== null) {
        return <Redirect to="/"/>;
    }

    return (...)
}
```

## Updating the App.js and AppNavbar.js

Go to **src/App.js** and update the highlighted code:

```jsx
// Base Imports
import React, { useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'index.css';

// App Imports
import UserContext from 'UserContext';

// App Components
import AppNavbar from 'components/AppNavbar';

// Page Components
import Home from 'pages/Home';
import Courses from 'pages/Courses';
import Register from 'pages/Register';
import Login from 'pages/Login';
import NotFound from 'pages/NotFound';

export default function App() {
    const [user, setUser] = useState({ accessToken: localStorage.getItem('accessToken') });

    const unsetUser = () => {
        localStorage.clear();
        setUser({ accessToken: null });
    }

    return (...)
}
```

Then, in the **src/components/AppNavbar.js**

```jsx
export default function AppNavbar() {
    ...

    let rightNav = (user.accessToken === null) ? (
        <Fragment>
            <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
            <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
        </Fragment>
    ) : (
        <Fragment>
            <Nav.Link onClick={logout}>Logout</Nav.Link>
        </Fragment>
    );

    return (...)
}
```

## Testing the Login Procedure with Fetch

Go to [localhost:3000/login](http://localhost:3000/login), open the Network tab in DevTools pane, then try logging in using an existing user credential.

If the login is successful, we must see the following server response from the tab.

![readme-images/Untitled.png](readme-images/Untitled.png)

Also, we can see the accessToken being sent back to the client by looking in the Preview tab of the response.

![readme-images/Untitled%201.png](readme-images/Untitled%201.png)

Also, look at the Application tab to confirm that the accessToken is in the localStorage.

![readme-images/Untitled%202.png](readme-images/Untitled%202.png)

Lastly, try logging out and see if the accessToken will be removed from the localStorage.

# Activity

## Instructions

Using the code discussed earlier, update the Register page component so that it sends the new user information to [localhost:4000/users](http://localhost:4000/users) via `POST` endpoint. 

After the successful registration, the page must redirect to the Login page. Refer to the AppNavbar component on how to redirect without using `<Redirect/>`.

Additionally, if there is a user currently logged in, the page must redirect to the home page.

## Expected Output

The new user information must be saved in the MongoDB Atlas database.

## Solution

```jsx
// Base Imports
import React, { useState, useEffect, useContext } from 'react';
import { Redirect, useHistory } from 'react-router-dom';

// App Imports
import UserContext from 'UserContext';

// Bootstrap Components
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';

export default function Register() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNumber, setMobileNumber] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passwordConfirm, setPasswordConfirm] = useState('');
    const [isDisabled, setIsDisabled] = useState(true);
    const { user } = useContext(UserContext);
    const history = useHistory();

    useEffect(() => {
        let isEmailNotEmpty = email !== '';
        let isPasswordNotEmpty = password !== '';
        let isPasswordConfirmNotEmpty = passwordConfirm !== '';
        let isPasswordMatched = password === passwordConfirm;

        // Determine if all conditions have been met.
        if (isEmailNotEmpty && isPasswordNotEmpty && isPasswordConfirmNotEmpty && isPasswordMatched) {
            setIsDisabled(false);
        } else {
            setIsDisabled(true);
        }
    }, [email, password, passwordConfirm]);
    
    function register(e) {
        e.preventDefault();

        fetch('http://localhost:4000/users', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 
                firstName: firstName, 
                lastName: lastName, 
                mobileNumber: mobileNumber, 
                email: email, 
                password: password 
            })
        })
        .then((response) => response.json())
        .then((response) => {
            if (response.id !== undefined) {
                alert('Registration successful, you may now log in.');
                history.push('/login');
            } else {
                alert(response.error);
            }
        })
    }    

    if (user.accessToken !== null) {
        return <Redirect to="/"/>;
    }

    return (
        <Container fluid>
            <h3>Register</h3>
            <Form onSubmit={register}>
                <Form.Group>
                    <Form.Label>First Name</Form.Label>
                    <Form.Control type="text" placeholder="First Name" value={firstName} onChange={(e) => setFirstName(e.target.value)} required/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control type="text" placeholder="Last Name" value={lastName} onChange={(e) => setLastName(e.target.value)} required/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control type="text" placeholder="Mobile Number" value={mobileNumber} onChange={(e) => setMobileNumber(e.target.value)} required/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Email Address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required/>
                    <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control type="password" placeholder="Verify Password" value={passwordConfirm} onChange={(e) => setPasswordConfirm(e.target.value)} required/>
                </Form.Group>
                <Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
            </Form>
        </Container>
    )
}
```