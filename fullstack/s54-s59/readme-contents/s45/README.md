# Session Objectives

At the end of the session, the students are expected to:

- learn how to make reusable UI components by using React.js.

# Resources

## Instructional Materials

- GitLab Repository
- Google Slide Presentation

## Supplemental Materials

- [Getting Started (React.js Docs)](https://reactjs.org/docs/getting-started.html)
- [Introducing JSX (React.js Docs)](https://reactjs.org/docs/introducing-jsx.html)

# Lesson Proper

## Introduction

This section is for the appreciation of the instructors only.

In the year 2011, Facebook started to encounter issues in their code maintenance due to their ever-increasing scope of services. Engineers could not keep up with simultaneous updates so they had to develop a new way of making their code more maintainable. Jordan Walke developed [FaxJS](https://github.com/jordwalke/FaxJs) to address Facebook’s issue and later on it became React.

## What is React.js

It is a JavaScript **library** for building **user interfaces**. In its [home page](https://reactjs.org/), it describes itself as the following:

1. **Declarative**. It makes you write code with the question of WHAT instead of HOW something is to be done. It also allows your code to be more predictable and easier to debug.
2. **Component-Based**. Allows you to write reusable, complex UI components in a quick and efficient manner.
3. **Learn Once, Write Anywhere**. React can also be used on the server using Node and power mobile apps using [React Native](https://facebook.github.io/react-native/).

React is backed by a lot of community support. This means that when a dev encounters a problem in their React-based project, millions of other devs can help out.

## What Problem Does It Solve?

Applications that require **frequent** and **rapid** **changes** in page **data** are **resource-intensive** and **complicated** to **code**. Also, updating an **element** on the page will require the **entire** DOM to **re-render.** 

Imagine tearing down a house and building it back up just to change a light bulb.

To recall, DOM means Document Object Model and is the concept that allows JavaScript to work with HTML elements.

DOM rendering refers to the browser processing the HTML code and then allocating a certain space to the screen based on the processed code.

For example, a Facebook news feed before React will require the entire DOM to render again on every like or comment. This action will drain the resources of the browser (it might even hang sometimes).

With React, only parts of the DOM will be updated and rendered, making the whole operation lighter compared to not using React.

## How React.js Solves The Problem

- Separate concerns into **components** rather than **technologies** instead of the conventional HTML for structure, CSS for styling, and JavaScript for interactivity.
- Apply rapid updates using **virtual DOM**. This only works on the differences between the current DOM state and the target DOM state.

Just change the light bulb without tearing down the house and building it back up.

- Store information in a component using **states**.

For example, a traffic light is a component and its states are **stop**, **wait** and **go**.

## Apps Built Using React

These are some of the apps you use built in React:

1. Facebook
2. Instagram
3. Netflix
4. Airbnb
5. Reddit

# Code Discussion

## Setting Up React.js Project

Create a React.js app by going to the folder where your projects are and executing the command below using your terminal:

```bash
npx create-react-app react-booking
```

After the command, you should see a message like this.

![readme-images/Untitled.png](readme-images/Untitled.png)

## Possible Setup Issues

### **Operation Not Permitted**

Original solution from [StackOverflow](https://stackoverflow.com/questions/58354348/windows-10-npx-create-react-app-myapp-command-does-not-work-because-of-whit).

If a student encounters the following below:

```
Error: EPERM: operation not permitted, mkdir 'C:\Users\Username'
command not found: create-react-app
```

Then it most likely means that the student has a space in between his/her first and last name (usually encountered by Windows users). 

To fix it, change where the NPM saves its cache by executing the following command:

```
npm config set cache C:\Node.js\Cache
```

After that, try the `npx` command again.

![readme-images/Untitled%201.png](readme-images/Untitled%201.png)

If you are seeing a view like this, wait for the command to finish.

## Viewing the React.js Project

Go inside the **react-booking** folder and execute the following command in the terminal.

```
npm start
```

If the command is successful, the terminal will contain an output like this.

![readme-images/Untitled%202.png](readme-images/Untitled%202.png)

Then in our browser, our page will look like this.

![readme-images/Untitled%203.png](readme-images/Untitled%203.png)

## Cleaning The Slate

Upon creating the React project, there are some ready-made files. We do not need some of these files so we can safely delete them. Inside the **src** folder, delete the following:

- App.css
- App.js
- App.test.js
- index.css
- logo.svg
- reportWebVitals.js
- setupTests.js

Also delete the **README.md** file in the root directory of the project.

## Initial React.js Code

Inside the **index.js** file, remove all of its contents and write the following:

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

ReactDOM.render(<h1>Hello World</h1>, document.getElementById('root'));
```

Upon saving the changes made, the web page should also update automatically.

![readme-images/Untitled%204.png](readme-images/Untitled%204.png)

### ReactDOM.render()

The render() function from the imported ReactDOM module is the one responsible for injecting the whole React.js project to the web page.

In the code above, the function adds the H1 element to the document element with an ID of "root".

The HTML file where the React app will be injected is located in **public/index.html**.

### JSX Syntax

JSX, or **JavaScript XML** is an extension to the syntax of JavaScript. It allows us to write HTML-like syntax within our React.js projects and it includes JavaScript features as well.

In the code above, the H1 element will be converted into the code below when it is compiled into plain JavaScript code.

```jsx
React.createElement("h1", null, "Hello World");
```

## Using Bootstrap In React.js

To add Bootstrap 4 in the React.js project, go to the project folder execute the following command in the terminal.

```
npm install bootstrap@4.6.0 react-bootstrap@1.5.2
```

The website for the said package can be found [here](https://react-bootstrap.github.io/).

The `bootstrap` package contains the actual styling while `react-bootstrap` contains the code for React components.

## Adding The AppNavbar

Add the highlighted code inside the **index.js** file.

```jsx
// Base Imports
import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

// Bootstrap Components
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

function AppNavbar() {
    return (
        <Navbar bg="light" expand="lg">
            <Navbar.Brand href="#home">Zuitt Booking</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                <Nav.Link href="#home">Home</Nav.Link>
                <Nav.Link href="#link">Courses</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

ReactDOM.render(<AppNavbar/>, document.getElementById('root'));
```

If the `bootstrap` package is not installed, importing the Bootstrap CSS will generate the error below.

![readme-images/Untitled%205.png](readme-images/Untitled%205.png)

The output in the browser should now look like this.

![readme-images/Untitled%206.png](readme-images/Untitled%206.png)

Components in React.js are declared by creating a function and named according to the purpose of the component (in this case, AppNavbar).

The name **AppNavbar** is used to avoid naming conflict with the pre-existing component named **Navbar** under the `react-bootstrap` package.

### Absolute Imports

To avoid using relative linking in the imported components later, we need to apply absolute imports  in the React.js project. To do that, add a file named **jsconfig.json** in the root directory then add the following code:

```jsx
{
    "compilerOptions": {
        "baseUrl": "src"
    },
    "include": ["src"]
}
```

Original solution from Create React App [documentation](https://create-react-app.dev/docs/importing-a-component/#absolute-imports).

### Using AppNavbar From Separate File

Since we are going to create more components later, we need to separate our AppNavbar component. To do that, we need to create a folder named **components** inside the **src** folder.

After that, add a file named **AppNavbar.js** inside the **components** folder and then add the following code inside the file:

```jsx
// Base Imports
import React from 'react';

// Bootstrap Components
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

export default function AppNavbar() {
    return (
        <Navbar bg="light" expand="lg">
            <Navbar.Brand href="#home">Zuitt Booking</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                <Nav.Link href="#home">Home</Nav.Link>
                <Nav.Link href="#link">Courses</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}
```

The name of the file must match the name of the component inside it (along with proper text casing).

The `export default` allows the function to be used in other files.

Then, in the **index.js** file at the root directory, update the code:

```jsx
// Base Imports
import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

// App Components
import AppNavbar from 'components/AppNavbar';

ReactDOM.render(<AppNavbar/>, document.getElementById('root'));
```

If the procedure from Absolute Imports section was not done, we would have needed to write the AppNavbar import into `'./components/AppNavbar'`.

The output in the browser should be the same.

## Adding the Banner Component

Create a file named **Banner.js** inside the **components** folder and add the following:

```jsx
// Base Imports
import React from 'react';

// Bootstrap Components
import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export default function Banner() {
    return (
        <Row>
            <Col>
                <Jumbotron>
                    <h1>Zuitt Coding Bootcamp</h1>
                    <p>Opportunities for everyone, everywhere.</p>
                    <Button variant="primary">Enroll now!</Button>
                </Jumbotron>
            </Col>
        </Row>
    )
}
```

## Adding The Highlights Component

Create a file named **Highlights.js** inside the **components** folder and add the following:

```jsx
// Base Imports
import React from 'react';

// Bootstrap Components
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';

export default function Highlights() {
    return (
        <Row>
            <Col xs={12} md={4}>
                <Card className="card-highlight">
                    <Card.Body>
                        <Card.Title>
                            <h2>Learn from Home</h2>
                        </Card.Title>
                        <Card.Text>
                            Pariatur adipisicing aute do amet dolore cupidatat. Eu labore aliqua eiusmod commodo occaecat mollit ullamco labore minim. Minim irure fugiat anim ea sint consequat fugiat laboris id. Lorem elit irure mollit officia incididunt ea ullamco laboris excepteur amet. Cillum pariatur consequat adipisicing aute ex.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="card-highlight">
                    <Card.Body>
                        <Card.Title>
                            <h2>Study Now, Pay Later</h2>
                        </Card.Title>
                        <Card.Text>
                            Ex Lorem cillum consequat ad. Consectetur enim sunt amet sit nulla dolor exercitation est pariatur aliquip minim. Commodo velit est in id anim deserunt ullamco sint aute amet. Adipisicing est Lorem aliquip anim occaecat consequat in magna nisi occaecat consequat et. Reprehenderit elit dolore sunt labore qui.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="card-highlight">
                    <Card.Body>
                        <Card.Title>
                            <h2>Be Part of Our Community</h2>
                        </Card.Title>
                        <Card.Text>
                            Minim nostrud dolore consequat ullamco minim aliqua tempor velit amet. Officia occaecat non cillum sit incididunt id pariatur. Mollit tempor laboris commodo anim mollit magna ea reprehenderit fugiat et reprehenderit tempor. Qui ea Lorem dolor in ad nisi anim. Culpa adipisicing enim et officia exercitation adipisicing.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}
```

## Including The Banner And Highlights Components

Back in the **index.js** file, add the following code:

```jsx
// Base Imports
import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

// App Components
import AppNavbar from 'components/AppNavbar';
import Banner from 'components/Banner';
import Highlights from 'components/Highlights';

ReactDOM.render(<AppNavbar/><Banner/><Highlights/>, document.getElementById('root'));
```

However, in the browser, the following error shows:

![readme-images/Untitled%207.png](readme-images/Untitled%207.png)

This is because when using React.js, all the other components must be enclosed in only one parent component.

To fix this, we must update the **index.js** file again:

```jsx
// Base Imports
import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

// App Components
import AppNavbar from 'components/AppNavbar';
import Banner from 'components/Banner';
import Highlights from 'components/Highlights';

ReactDOM.render(
    <Fragment>
        <AppNavbar/>
        <Banner/>
        <Highlights/>
    </Fragment>, 
    document.getElementById('root')
);
```

With the React fragment component, we can group multiple components and avoid adding extra nodes to the DOM ([documentation](https://reactjs.org/docs/fragments.html)).

`<Fragment>` is preferred over `<>` shortcut syntax because support for the shortcut is not universal and can cause problems in some other editors.

Additionally, `<>` does not support keys or attributes.

The output in the browser should look like this now.

![readme-images/Untitled%208.png](readme-images/Untitled%208.png)

## Adding The Container Component

Go to **index.js** and add the highlighted code.

```jsx
// Base Imports
import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

// Bootstrap Components
import Container from 'react-bootstrap/Container';

// App Components
import AppNavbar from 'components/AppNavbar';
import Banner from 'components/Banner';
import Highlights from 'components/Highlights';

ReactDOM.render(
    <Fragment>
        <AppNavbar/>
        <Container fluid>
            <Banner/>
            <Highlights/>
        </Container>
    </Fragment>, 
    document.getElementById('root')
);
```

The output should look like this.

![readme-images/Untitled%209.png](readme-images/Untitled%209.png)

## Adding Custom Styles To Highlight Cards

To add a custom style, create a file named **index.css** in the root directory and add the following styles.

```css
.card-highlight {
    min-height: 100%;
}
```

Then, in the **index.js**, import the CSS file.

```jsx
// Base Imports
import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'index.css';
```

The added style ensures that the height of the three cards are the same regardless of the varying contents of each card.

## Creating The Home Page Component

The project will consist of more than one page. Because of this, we need to create a **pages** folder in the root directory and create a file named **Home.js**. After that, add the following code.

```jsx
// Base Imports
import React from 'react';

// Bootstrap Components
import Container from 'react-bootstrap/Container';

// App Components
import Banner from 'components/Banner';
import Highlights from 'components/Highlights';

export default function Home() {
    return (
        <Container fluid>
            <Banner/>
            <Highlights/>
        </Container>
    )
}
```

If the procedure from Absolute Imports section was not done, we would have needed to write the Banner and Highlights import into `'../components/[Banner|Highlights]'`.

Then, in the **index.js** file, add the highlighted code.

```jsx
// Base Imports
import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'index.css';

// App Components
import AppNavbar from 'components/AppNavbar';

// Page Components
import Home from 'pages/Home';

ReactDOM.render(
    <Fragment>
        <AppNavbar/>
        <Home/>
    </Fragment>, 
    document.getElementById('root')
);
```

# Activity

## Pre-Activity

Send the React Bootstrap documentation for cards to the students.

[https://react-bootstrap.github.io/components/cards](https://react-bootstrap.github.io/components/cards/)

## Instruction

1. Have the students create a **Course** component showing a particular course with the name, description and price inside a Bootstrap Card:
    - The course name should be in the card title.
    - The description and price should be in the card body.
2. Render the component in the home page for checking.

## Expected Output

![readme-images/Untitled%2010.png](readme-images/Untitled%2010.png)

## Solution

**src/components/Course.js**

```jsx
// Base Imports
import React from 'react';

// Bootstrap Components
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

export default function Course() {
    return (
        <Card>
            <Card.Body>
                <Card.Title>Sample Course</Card.Title>
                <h6>Description</h6>
                <p>Pariatur aliquip nulla exercitation enim reprehenderit aliquip. Magna dolor voluptate labore veniam. Consectetur veniam commodo cillum id do ut cillum culpa. Nulla sit voluptate tempor est cupidatat amet nisi qui commodo sit reprehenderit. Sit enim anim anim officia commodo exercitation sit deserunt ipsum voluptate. Dolor ullamco do tempor irure.</p>
                <h6>Price</h6>
                <p>PhP 40,000</p>
                <Button variant="primary">Enroll</Button>
            </Card.Body>
        </Card>
    )
}
```

**src/pages/Home.js**

```jsx
// Base Imports
import React from 'react';

// Bootstrap Components
import Container from 'react-bootstrap/Container';

// App Components
import Banner from 'components/Banner';
import Highlights from 'components/Highlights';
import Course from 'components/Course';

export default function Home() {
    return (
        <Container fluid>
            <Banner/>
            <Highlights/>
            <Course/>
        </Container>
    )
}
```