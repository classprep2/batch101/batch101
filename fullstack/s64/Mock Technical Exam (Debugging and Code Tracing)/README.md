# Mock Technical Exam (Debugging and Code Tracing)

## Usage Instructions:
1. Duplicate the following [template](https://docs.google.com/forms/d/1FicWC2uzsTR4wawXxX6_AJ2N_uLAf2l_3_kPWXQpG-Q/edit).

2. Rename the duplicate as: **WDC028v1.5-S53 - Mock Technical Exam (Batch BatchNo.)**.

3. Open your duplicated form, go to the Responses tab and check that the **Accepting responses** toggle is set to enabled.
![Step 3](./screenshots/step3.png)
> screenshot taken from S51 but step to be followed is the same

4. Click the send button:
![Step 4](./screenshots/step4.png)
> screenshot taken from S51 but step to be followed is the same

5. In the popup, select the **link icon** in the "Send via" row and tick **Shorten URL** checkbox below the Link. Click the **Copy** button.
![Step 5](./screenshots/step5.png)

6. Share the copied link to your class.

7. Once your prescribed time is finished, go back to the **Responses** tab (see step 3) to toggle the **Accepting responses** to disabled.
![Step 7](./screenshots/step7.png)

8. Generate the results in a spreadsheet format by clicking the spreadsheet icon in the Responses tab:
![Step 8](./screenshots/step8.png)

9. The students' emails and scores as well as the timestamp of submission will be shown in the generated spreadsheet. Grade them accordingly in BooDLE.

## Session Objectives

At the end of this session, students must be able to:
* Demonstrate debugging and code tracing skills by correctly identifying bugs in code.

## Guide Questions

1. Why is the exam in multiple choice format?  
    - The causes for bugs in code are **clearly identifiable** and not open to different interpretations, hence the multiple choice format.

2. Why is debugging and code tracing important?  
    - **Debugging** is the process of **detecting** and **correcting** errors in code that could cause the program to behave unexpectedly or crash. 
    - **Code tracing** is the process of **reading** the code and running it line per line to **predict** the result of each statement.
    - Debugging and code tracing are important skills that any programmer should have as these skills are essential in ensuring applications run as **intended** all the time.

3. Why do we need to study these now?  
    - Debugging and code tracing skills are **hard to master**. As such, they should both be practiced **early** and **often**.

## Direct Benefits

What are the direct benefits of taking this mock technical exam?

**Exposure**

- Devs spend more time debugging existing code than writing new code. Programming is an iterative process. 
- Spotting errors in code that you did not write is difficult.
- Fixing errors in code that you did not write is even more difficult. 
- Familiarity gained through exposure makes these easier.

**Practice**

You get to practice your:
- attention to details;
- comprehension of code written by others;
- bug-fixing skills; and
- and ability to deduce or trace the cause of an error.

## Questions

#### Question 1

What would be the output?

```javascript
for (let i = 1; i < 5; i++) {
    console.log(i*i);
}
```

a. 1 2 3 4 5  
b. **1 4 9 16**  
c. 1 2 3 4  
d. 1 8 16 32  

*Explanation*<br/> 
The loop multiplies the loop counter to itself before incrementing. Note that the operation **does not** change the value of i. 

#### Question 2

What would be the problem in the code snippet?

```javascript
let students = ["John", "Paul", "George", "Ringo"];

console.log("Here are the graduating students:")
for (let count = 0; count <= students.length; count++) {
    console.log(students[count]);
}
```

a. There will be no output  
b. **There will be an extra *undefined* in the console**  
c. There will be missing students  
d. There will be no problems  

*Explanation*<br/> 
The count will reach index 4 as it is the length (number of elements) of the array, however the last index of the array is only 3. So `students[4]` will return *undefined*.

#### Question 3

What would be the output?

```javascript
function checkGift(day) {
    let gifts = ["partridge in a pear tree", "turtle doves", "french hens", "golden rings"];

    if(day > 0 && day < 4) {
        return `I was given ${day} ${gifts[day-1]}`;
    } else {
        return "No gifts were given";
    }
}

checkGift(3);
```

a. "I was given 3 french hens"  
b. "I was given 3 golden rings"  
c. "I was given 3 turtle doves"  
d. **No visible output**  

*Explanation*<br/> 
No output will be seen as the function returns a string. Said string can be assigned in a variable or directly put into the console for it to be seen.

#### Question 4

What would be the problem in the code snippet?

```javascript	
let items = [
    {
        id: 1,
        name: "Banana",
        description: "A yellow fruit",
        price: 15.00,
        category: 2 
    },
    {
        id: 2,
        name: "Pork Cutlet",
        description: "Japanese Kurobuta",
        price: 199.00,
        category: 1
    },
    {
        id: 1,
        name: "Sweet Potato",
        description: "Best when roasted",
        price: 20.00,
        category: 3
    }
];

for (let i = 0; i < items.length; i++) {
    console.log(`
        Name: ${items[i].name}
        Description: ${items[i].description}
        Price: ${items[i].price}
    `);
}
```

a. There will be no output  
b. There will be an *undefined* value at the end  
c. The objects' keys will be displayed  
d. **There will be no errors**  

*Explanation*<br/> 
Common source of errors with objects would be the missing commas in the definition and the dot and [] notations. In this code snippet, the code was written with care such that there are no errors to be seen.

#### Question 5

What would be the output?

```javascript
for (let row = 1; row < 3; row++) {
    for (let col = 1; col <= row; col++) {
        console.log(`Current row: ${row}, Current col: ${col}`);
    }
}
```

a.<br/>
**Current row: 1, Current col: 1**<br/>
**Current row: 2, Current col: 1**<br/>
**Current row: 2, Current col: 2**<br/>  
b.<br/>
Current row: 1, Current col: 1<br/>
Current row: 2, Current col: 2<br/> 
c.<br/>
Current row: 1, Current col: 1<br/>
Current row: 1, Current col: 2<br/>
Current row: 2, Current col: 1<br/>
Current row: 2, Current col: 2<br/> 
d.<br/>
No output  

*Explanation*<br/> 
The outer loop would run for row = 1 and 2. The inner loop will run depending on the value of the outer loop. it will only have values less than or equal to the current value of row

#### Question 6

What would be the problem in the code snippet?

```javascript
function checkLeapYear(year) {
    if (year % 4 = 0) {
        if (year % 100 = 0) {
            if (year % 400 = 0) {
                console.log("Leap year");
            } else {
                console.log("Not a leap year");
            }
        } else {
            console.log("Leap year");
        }
    } else {
        console.log("Not a leap year");
    }
}

checkLeapYear(1999)
```
a. The output will always be "Not a leap year"  
b. **There will be a syntax error**  
c. The output will always be "Leap year"  
d. There will be no problems  

*Explanation*<br/> 
The condition inside the if statements will have syntax errors (Invalid left-hand side in assignment) as the = symbol is the assignment operator. == should be used as it is the equality operator.

#### Question 7

Given the array below, how can the last student's English grade be displayed?

```javascript
let records = [
    {
        id: 1,
        name: 'Brandon',
        subjects: [
            { name: 'English', grade: 98 },
            { name: 'Math', grade: 66 },
            { name: 'Science', grade: 87 }
        ]
    },
    {
        id: 2,
        name: 'Jobert',
        subjects: [
            { name: 'English', grade: 87 },
            { name: 'Math', grade: 99 },
            { name: 'Science', grade: 74 }
        ]
    },
    {
        id: 3,
        name: 'Junson',
        subjects: [
            { name: 'English', grade: 60 },
            { name: 'Math', grade: 99 },
            { name: 'Science', grade: 87 }
        ]
    }
];
```
a. **`console.log(records[2].subjects[0].grade)`**  
b. `console.log(records[3].subjects[1].grade)`
c. `console.log(Junson.English.grade)`
d. `console.log(records[2].subjects[1].grade)`

*Explanation*<br/> 
Use the `[]` and `.` notations to call array elements and object properties respectively.

#### Question 8

What would be the problem in the code snippet?

```javascript
function checkDivisibility(dividend, divisor) {
    if (dividend % divisor == 0) {
        console.log(`${dividend} is divisible by ${divisor}`);
    } else {
        console.log(`${dividend} is not divisible by ${divisor}`);
    }
}

checkDivisibility(100, 0);
```

a. The output will always be `${dividend} is divisible by ${divisor}`  
b. The output will always be `${dividend} is not divisible by ${divisor}`  
c. **Division by 0 is not possible**  
d. There will be no problems  

*Explanation*<br/> 
It is important to include input validation to prevent runtime issues. The code may look correct but it will encounter runtime errors.

## Instructor Notes

Have the students do the validation. Here is a sample validation that can be done:

```javascript
function checkDivisibility(dividend, divisor) {
    if (divisor != 0) {
        if (dividend % divisor == 0) {
            console.log(`${dividend} is divisible by ${divisor}`);
        } else {
            console.log(`${dividend} is not divisible by ${divisor}`);
        }
    } else {
        console.log("Divisor cannot be equal to zero");
    }
}
```