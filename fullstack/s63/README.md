# S56 - Mock Technical Exam (Concepts and Theory)

## Usage Instructions:

1. Duplicate the following [template](https://docs.google.com/forms/d/1yZruPlfGD4vNENIYmFvnNU-RHxMp5N74FDJBXZ7vOgs/edit).

2. Rename the duplicate as: **WDC028v1.5b-S56 - Mock Technical Exam (Batch BatchNo.)**.

3. Open your duplicated form, go to the Responses tab and check that the **Accepting responses** toggle is set to enabled.
![Step 3](./screenshots/step3.png)

4. Click the send button:
![Step 4](./screenshots/step4.png)

5. In the popup, select the **link icon** in the "Send via" row and tick **Shorten URL** checkbox below the Link. Click the **Copy** button.
![Step 5](./screenshots/step5.png)

6. Share the copied link to your class.

7. Once your prescribed time is finished, go back to the **Responses** tab (see step 3) to toggle the **Accepting responses** to disabled.
![Step 7](./screenshots/step7.png)

8. Generate the results in a spreadsheet format by clicking the spreadsheet icon in the Responses tab:
![Step 8](./screenshots/step8.png)

9. The students' emails and scores as well as the timestamp of submission will be shown in the generated spreadsheet. Grade them accordingly in BooDLE.

## Session Objectives

At the end of this session, students must be able to:
* Demonstrate understanding of foundational programming concepts via a multiple choice exam.

## Guide Questions

1. Why is the exam in multiple choice format?  
    - The multiple choice format is best suited for checking facts that have **one clear, correct answer**. The other options presented to the student are distractors that distinguish those who understand the concept being tested from those who don't. 

2. Why is it important to know the solutions to these problems?  
    - The concepts being tested here are considered **foundational** and as such, knowledge of these is already **assumed** by prospective employers / clients once our students graduate.

3. Why do we need to study these now?  
    - Our students' progress in their developer careers will be dependent on their understanding of these concepts hence the need to focus on these first.

## Direct Benefits

What are the direct benefits of taking this mock technical exam?

- **Exposure**
    - Early exposure to distractors aids you in eliminating them.
    - Familiarity builds self-confidence.

- **Practice**
    - Reinforce your understanding of the fundamentals.
    - Train your deduction and comprehension skills in reading the questions.

## Questions

[1. Which of these is not a data type in JavaScript?](#which-of-these-is-not-a-data-type-in-javascript)

[2. True or False: Strings in JavaScript can use both double quotes ("") and single quotes ('')](#true-or-false-strings-in-javascript-can-use-both-double-quotes-and-single-quotes-)

[3. Which is true among the statements below:](#which-is-true-among-the-statements-below)

[4. True or False: An object's properties can be changed given the following code below:](#true-or-false-an-objects-properties-can-be-changed-given-the-following-code-below)

[5. What would be the output of the following code block:](#what-would-be-the-output-of-the-following-code-block)

[6. True or False: x++ and ++x increments +1 to variable x the same way.](#true-or-false-x-and-x-increments-1-to-variable-x-the-same-way)

[7. In the for loop below, what does the (i < 10) part of the code do?](#in-the-for-loop-below-what-does-the-i-10-part-of-the-code-do)

[8. What is the main difference between a while loop and a do-while loop.](#what-is-the-main-difference-between-a-while-loop-and-a-do-while-loop)

[9. True or false: if statements should always have an else statement.](#true-or-false-if-statements-should-always-have-an-else-statement)

[10. What statement can be used to exit out of an executing block of code?](#what-statement-can-be-used-to-exit-out-of-an-executing-block-of-code)

[11. True or False: Multiple conditions can be combined in a single IF statement.](#what-statement-can-be-used-to-exit-out-of-an-executing-block-of-code)

[12. Given the following variables, what would be the output?](#given-the-following-variables-what-would-be-the-output)

[13. Which of the following is not an arithmetic operator?](#which-of-the-following-is-not-an-arithmetic-operator)

[14. What is the index of the last element in an array?](#what-is-the-index-of-the-last-element-in-an-array)

[15. True or False: Objects can have nested objects inside them.](#true-or-false-objects-can-have-nested-objects-inside-them)

#### Which of these is not a data type in JavaScript?

- **Char** (correct answer)
- String
- Number
- Boolean

*Explanation*<br/>
Characters are strings in JavaScript (Hence the nondistinction between "" and ''). String, Number, and Boolean are all valid JavaScript data types.

#### True or False: Strings in JavaScript can use both double quotes ("") and single quotes ('')

- **True** (correct answer)
- False

*Explanation*<br/>
Characters are strings in JavaScript (Hence the nondistinction between "" and '')

#### Which is true among the statements below:

- The let keyword doesn't allow creation of object variables
- The let keyword cannot be used to make global scope variables
- **The const keyword can be used when the value assigned to the identifier shouldn't be changed** (correct answer)
- The const keyword can be interchanged with the let keyword in all cases

*Explanation*<br/>
`const` is used for values that should be fixed. However, objects' and functions' definitions can still be changed 

#### True or False: An object's properties can be changed given the following code below:

```javascript
const person = {
    name: "Brandon",
    age: 18
}
```

- **True** (correct answer)
- False

*Explanation*<br/>
`const` is used for values that should be fixed. However, objects' and functions' definitions can still be changed.

#### What would be the output of the following code block:

```javascript
let title = "The Lord of the Rings"
console.log(title[2])
```

- "h"
- "H"
- **"e"** (correct answer)
- error

*Explanation*<br/>
Strings can be considered as character arrays and arrays in JavaScript start with index 0.

#### True or False: x++ and ++x increments +1 to variable x the same way.

- True
- **False** (correct answer)

*Explanation*<br/>
`x++` evaluates the value of `x` **BEFORE** incrementing while `++x` evaluates the value of `x` **AFTER** incrementing.

#### In the for loop below, what does the (i < 10) part of the code do?

```javascript
for (let i = 0; i < 10; i++) {
    console.log(i)
}
```

- Increases the value of i
- **Sets the condition for the loop to stop/continue** (correct answer)
- Sets the value of i
- Resets the loop

*Explanation*<br/>
The second argument of a `for` loop specifies the condition for iteration.

#### What is the main difference between a while loop and a do-while loop

- A while loop can be used with ++ and --, a do-while can only use --
- **A while loop checks the condition before running the loop, a do while loop runs at least once.** (correct answer)
- do-while loops are harder to implement compared to while loops 
- There is no difference between a while and do-while loop

*Explanation*<br/>
Based on the design of a do-while loop, it executes the code block within the do part before checking the condition inside the while. Hence, the do-while loop can run at least once even if the while statement is not satisfied.

#### True or false: if statements should always have an else statement

- True
- **False** (correct answer)

*Explanation*<br/>
The else clause is optional if there is no action to be done if the statement is false.

#### What statement can be used to exit out of an executing block of code

- stop
- **break** (correct answer)
- continue
- wait

*Explanation*<br/>
The break statement is used to exit out of a control structure.

#### True or False: Multiple conditions can be combined in a single IF statement

- **True** (correct answer)
- False

*Explanation*<br/>
Using logical operators (AND, OR, NOT), multiple conditions can be combined in a single if statement

#### Given the following variables, what would be the output

```javascript
let conditionA = true
let conditionB = false
let conditionC = true
let result = !(conditionA && (conditionB || conditionC))

console.log(result)
```
- True
- **False** (correct answer)
- Undefined
- Null

*Explanation*<br/>
`conditionB` OR `conditionC` results in `True`. When combined with `conditionA` and the `AND` operator, it becomes `True`. The `NOT` operator then inverses the result into `False`.

#### Which of the following is not an arithmetic operator?

- +=
- %=
- **==** (correct answer)
- -=

*Explanation*<br/>
The "==" operator is the equality operator that compares two values if they are equal or not.

#### What is the index of the last element in an array?

- The value of the element
- The number of elements in the array
- **The number of elements in the array - 1** (correct answer)
- 0

*Explanation*<br/>
In JavaScript, arrays start at index 0 and ends in length - 1.

#### True or False: Objects can have nested objects inside them

- **True** (correct answer)
- False

*Explanation*<br/>
Objects can have properties that are objects as well.

## Instructor Notes (IF time allows)

Briefly discuss the following:
1. Difference between var, let and const
2. Various data types and operators
3. If-else statements and switch cases
4. Arrays and objects
5. Common errors
    - Lack of semicolon
    - Lack of commas
    - Missing parenthesis and curly braces


# S56 - Mock Technical Exam (Function Coding)

## Usage Instructions:

1. Create a new repo in your resources folder as S56-MockTech-FunctionCoding.
2. Push "Mock Technical Exam (Function Coding) - Template" into the repo.
3. Have students clone the S56-MockTech-FunctionCoding repo.
4. Let students use npm install first to re-install dependencies.
5. Run npm test to automatically test the functions in index.js

***Note***
    -  Do not modify test.js

## Session Objectives

At the end of this session, students must be able to:
* Demonstrate logic formulation and problem solving skills through the use of functions.

## Guide Questions

1. Why is the exam in function coding format?
    - Creating functions is the practical application of programming. A single problem may have multiple implementations of the same solution making this format the best fit.
2. Why is it important to know the solutions to these problems?
    - Developers are valued for their ability to create applications that solve real world problems. These applications are then made up of functions that each solve smaller problems.
3. Why do we need to study these now?
    - The ability to formulate solutions using logic and translating these solutions to code is a skill that needs constant practice.

## Direct Benefits

**Exposure**
    - This exam will help you familiarize with this common exam format.
    - You will have better confidence in taking this exam format through familiarity. 
**Practice**
    - Train your ability to comprehend the instruction or problem.
    - Practice formulating a solution and then translate the formulated solution into code.

## Solution
```js
    function countLetter(letter, sentence) {
        let result = 0;

        // Check first whether the letter is a single character.
        // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
        // If letter is invalid, return undefined.

        if (letter.length > 1) {
            return undefined;
        } else {
            for (let i = 0; i < sentence.length; i++) {
                if (sentence[i] === letter) {
                    result++;
                }
            }

            return result;
        }
    }

    function isIsogram(text) {
        // An isogram is a word where there are no repeating letters.
        // The function should disregard text casing before doing anything else.
        // If the function finds a repeating letter, return false. Otherwise, return true.

        text = text.toLowerCase();
        let letters = [];

        for (let i = 0; i < text.length; i++) {
            if (letters.indexOf(text[i]) !== -1) {
                return false;
            } else {
                letters.push(text[i]);
            }
        }

        return true;
    }

    function purchase(age, price) {
        // Return undefined for people aged below 13.
        // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
        // Return the rounded off price for people aged 22 to 64.
        // The returned value should be a string.

        if (age < 13) {
            return undefined;
        } else if ((age >= 13 && age <= 21) || age >= 65) {
            return (price * 0.8).toFixed(2);
        } else {
            return price.toFixed(2);
        }
    }

    function findHotCategories(items) {
        // Find categories that has no more stocks.
        // The hot categories must be unique; no repeating categories.

        // The passed items array from the test are the following:
        // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
        // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
        // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
        // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
        // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

        // The expected output after processing the items array is ['toiletries', 'gadgets'].
        // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

        let hotCategories = [];

        items.forEach(function(item) {
            if (item.stocks === 0) {
                if (hotCategories.indexOf(item.category) === -1) {
                    hotCategories.push(item.category);
                }
            }
        });

        return hotCategories;
    }

    function findFlyingVoters(candidateA, candidateB) {
        // Find voters who voted for both candidate A and candidate B.

        // The passed values from the test are the following:
        // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
        // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

        // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
        // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

        let flyingVoters = [];

        candidateA.forEach(function(voteA) {
            if (candidateB.indexOf(voteA) !== -1) {
                flyingVoters.push(voteA);
            }
        });

        return flyingVoters;
    }

    module.exports = {
        countLetter,
        isIsogram,
        purchase,
        findHotCategories,
        findFlyingVoters
    };
```
