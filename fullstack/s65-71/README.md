# Capstone Objective

At the end of the capstone period, the students are expected to:

- integrate the frontend (React.js) with the backend (Express.js API) then deploy the app to Heroku.

# Resources

## Instructional Materials

- [Frontend Template](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/fullstack/s56)
- [Backend Template](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/fullstack/s57)
- [CSP3 Solution](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/fullstack/csp3)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1F6hrZeApz3mQ_K4RP21F2Q7C8cMbKY3JhCWHL8PzQ1A)

# Capstone 3 Requirements

## Pre-capstone Preparation
- Create a copy of the final output of S56 and S57 in the class' [Zuitt Git subfolder](https://gitlab.com/tuitt/students) as another project
- This will act as a template for them to use during their capstone development
- Have the students clone a copy of the resources created in their [Zuitt subfolder](https://gitlab.com/tuitt/students).
- Refer to the links for the final output of the necessary materials: 
    - [S56 - Translating Mockups to Frontend Quickly](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/fullstack/s56) to use as the template for the CSP3 Frontend.
    - [S57 - Translating Backend Needs for Mockups](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/fullstack/s57) to use as the template for the CSP3 Backend.

## Specific Requirements

- Create the necessary backend integration using the fetch() function to the following pages:
    - Login
    - Register
    - Add Category
    - Add Entry
    - Get Expense Entries
    - Get Income Entries
- Write the necessary component states and form input bindings for every form found in the React.js app.
    - Login
    - Register
    - Add Category / Add Entry
- If the Home, AddCategory, AddEntry, ViewExpense and ViewIncome page components are accessed while there is no logged in user, the app must redirect back to the Login page component.
- If the Login and Register page components are accessed while there is a logged in user, the app must redirect to the Home page component.
- Both the backend and frontend must be deployed to Heroku.

## Code Snippets

These snippets are written in anticipation to student inquiries.

### useEffect Hooks

Use the `useEffect` for retrieval of information from the backend (e.g. get categories, income entries or expense entries).

```jsx
useEffect(getCategories, [type]);  // for AddEntry
useEffect(getIncomeEntries, []);   // for ViewIncome
useEffect(getExpenseEntries, []);  // for ViewExpense
```

### Successful Actions

For actions that adds new records to the database, the following condition can be used to determine if the adding of new record was successful.

```jsx
if (response._id !== undefined)
```

### Authorization Header

For the AddCategory, AddEntry, ViewExpense and ViewIncome page components, share the following highlighted code to the students so that they can send requests successfully to the server.

This procedure is equivalent to adding an access token to the requests created back in Postman testing.

```jsx
headers: { 
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${user.accessToken}`
}
```

## Heroku Deployment Procedure

Provide the following deployment procedure to the students (preferably through Boodle Notes).

### Pre-Deployment

Verify that Heroku CLI is installed by issuing the command `heroku -v` in the Terminal.

If the Heroku CLI is not yet installed, follow the installation guide found [here](https://devcenter.heroku.com/articles/heroku-cli#download-and-install).

For MacOS users, if the `brew` command is not found, execute the command below:

```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)" 
```

**Note:** if you have an Apple device with the M1 chip, run the command below, close terminal and open the terminal again

```bash
echo "export PATH=/opt/homebrew/bin:$PATH" >> ~/.zshrc
```

Source of the note above is from [StackOverflow](https://stackoverflow.com/a/65503883).

### Login

Execute the `heroku login` command from the Terminal. Then, press any key to continue logging in via a web browser.

![readme-images/Untitled.png](readme-images/Untitled.png)

Once the login process is finished, you can return to the Terminal.

![readme-images/Untitled%201.png](readme-images/Untitled%201.png)

### Backend

Open a Terminal and go to the root folder of the backend project. Then, issue the following command:

```bash
heroku create **lastname**-bt-api
```

Change the `lastname` with your actual last name.

It should provide the following output:

```bash
Creating ⬢ lastname-bt-api... done
https://lastname-bt-api.herokuapp.com/ | https://git.heroku.com/lastname-bt-api.git
```

To ensure that the creation of a Heroku app is successful, execute the following command:

```bash
git remote get-url heroku
```

The output should be:

```bash
https://git.heroku.com/lastname-bt-api.git
```

Then, add a file named `Procfile` (no filename extension) in the root directory with the following content:

```bash
web: node index.js
```

Execute the following commands in the Terminal:

```bash
git add .
git commit -m "Add a Procfile for Heroku deployment"
git push heroku master
```

The API should now be accessed using the following URL format:

```bash
http://lastname-bt-api.herokuapp.com
```

To verify if the deployment is properly executed, go to `http://lastname-bt-api.herokuapp.com` and verify if the output is `{"auth":"failed"}`.

### Frontend

Change all the fetch URLs from `[http://localhost:4000](http://localhost:4000)` to `http://lastname-bt-api.herokuapp.com`.

After the change, issue the following command in the Terminal.

```bash
heroku create **lastname**-bt-client -b https://github.com/mars/create-react-app-buildpack.git
```

It should provide the following output:

```bash
Creating ⬢ lastname-bt-client... done
Setting buildpack to https://github.com/mars/create-react-app-buildpack.git... done
https://lastname-bt-client.herokuapp.com/ | https://git.heroku.com/lastname-bt-client.git
```

To ensure that the creation of a Heroku app is successful, execute the following command:

```bash
git remote get-url heroku
```

The output should be:

```bash
https://git.heroku.com/lastname-bt-client.git
```

Execute the following commands in the Terminal:

```bash
git add .
git commit -m "Change fetch URL from localhost to Heroku-deployed API"
git push heroku master
```

The API should now be accessed using the following URL format:

```bash
http://lastname-bt-client.herokuapp.com
```

### Allowing Frontend to Access Backend API in Heroku

Go to the backend project and add the code in the declaration of `cors()` within **index.js** file:

```jsx
....

// Inject necessary middlewares.
app.use(cors(['http://localhost:3000', 'http://cahilog-bt-client.herokuapp.com']));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

....
```

Then, execute the following:

```bash
git add .
git commit -m "Add allowed URL in CORS"
git push heroku master
```

The frontend should now be accessed using the following URL format:

```bash
http://lastname-bt-client.herokuapp.com
```

# Instructor Notes

- #### Encourage students to ask questions.