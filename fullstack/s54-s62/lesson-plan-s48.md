# WDC028 - S48 - React.js - Routing and Conditional Rendering

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1w7_I6NdDcJ3W62y97wv4fRAw8o6okQ7sNE075aX-Ug8/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1w7_I6NdDcJ3W62y97wv4fRAw8o6okQ7sNE075aX-Ug8/edit#slide=id.g53aad6d9a4_0_745) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/fullstack/s45-s50) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/fullstack/s45-s50/Manual.js) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                   | Link                                                         |
| --------------------------------------- | ------------------------------------------------------------ |
| React Router DOM Documentation          | [Link](https://reactrouter.com/web/guides/quick-start)       |
| Mount vs Render                         | [Link](https://reacttraining.com/blog/mount-vs-render/)      |
| React Router DOM Primary Components     | [Link](https://reactrouter.com/web/guides/primary-components) |
| React Router DOM Switch Component       | [Link](https://reactrouter.com/web/api/Switch)               |
| React Router DOM Route Component        | [Link](https://reactrouter.com/web/api/Route)                |
| React Router DOM NavLink Component      | [Link](https://reactrouter.com/web/api/NavLink)              |
| JavaScript Web Storage API              | [Link](https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API) |
| JavaScript Window.localStorage Property | [Link](https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage) |
| React Router DOM Redirect Component     | [Link](https://reactrouter.com/web/api/Redirect)             |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[30 mins] - React Router DOM Primary Components
		- Router
		- Switch
		- Route
	2.[45 mins] - Rendering, Mounting, Rerendering and Unmounting
	3.[1 hr mins] - React Router DOM page navigation components
		- NavLink
		- Nav
	4.[30 mins] - JavaScript localStorage Object
	5.[15 mins] - React Router DOM Redirect Component
	6.[1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

​	

[Back to top](#table-of-contents)