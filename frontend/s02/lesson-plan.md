# WDC028 - S02 - Git Basics

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |

## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/17InJFJne-HDOCSBq1y5EEFqA_A4vsmvgSCcIhx1Rk9E/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/17InJFJne-HDOCSBq1y5EEFqA_A4vsmvgSCcIhx1Rk9E/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/frontend/s02) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/frontend/s02/Manual.txt) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                   | Link                                                         |
| --------------------------------------- | ------------------------------------------------------------ |
| About Version Control                   | [Link](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control) |
| A Visual Guide to Version Control       | [Link](https://betterexplained.com/articles/a-visual-guide-to-version-control/) |
| A Visual Git Reference                  | [Link](https://marklodato.github.io/visual-git-guide/index-en.html) |
| How to Write a Git Commit Message       | [Link](https://chris.beams.io/posts/git-commit)              |
| Setting Up SSH Keys                     | [Link](https://docs.google.com/presentation/d/1E4tBBwg5wla0u8sZEQhKNc3bKkXFWgLAHAD0MdF0e_I/edit#slide=id.gbf0b886339_0_2590) |
| Show hidden files and folders - Linux   | [Link](https://devconnected.com/how-to-show-hidden-files-on-linux/) |
| Show hidden files and folders - Mac     | [Link](https://www.macworld.co.uk/how-to/show-hidden-files-mac-3520878/#:~:text=How%20to%20see%20hidden%20files%20in%20macOS,folders%20just%20press%20Command%20%2B%20Shift%20%2B) |
| Show hidden files and folders - Windows | [Link](https://support.microsoft.com/en-us/windows/view-hidden-files-and-folders-in-windows-10-97fbc472-c603-9d90-91d0-1166d1d9f4b5) |
| Adding SSH Keys to Git - GitLab         | [Link](https://docs.google.com/presentation/d/1E4tBBwg5wla0u8sZEQhKNc3bKkXFWgLAHAD0MdF0e_I/edit#slide=id.gdef11eb712_0_0) |
| Adding SSH Keys to Git - GitHub         | [Link](https://docs.google.com/presentation/d/1E4tBBwg5wla0u8sZEQhKNc3bKkXFWgLAHAD0MdF0e_I/edit#slide=id.gdef11eb712_0_86) |
| Git Credentials - GitLab Email          | [Link](https://gitlab.com/-/profile)                         |
| Git Credentials - GitLab Username       | [Link](https://gitlab.com/-/profile/account)                 |
| Git Credentials - GitHub Email          | [Link](https://github.com/settings/emails)                   |
| Git Credentials - GitHub Username       | [Link](https://github.com/settings/profile)                  |
| Creating Git Projects - GitLab          | [Link](https://gitlab.com/projects/new#blank_project)        |
| Creating Git Projects - GitHub          | [Link](https://github.com/new)                               |
| Git Commit                              | [Link](https://www.atlassian.com/git/tutorials/saving-changes/git-commit) |
| Git Branch                              | [Link](https://www.atlassian.com/git/tutorials/using-branches#:~:text=The%20git%20branch%20command%20lets,checkout%20and%20git%20merge%20commands.) |
| Interactive Git Branching               | [Link](https://learngitbranching.js.org/)                    |
| CLI Commands                            | [Link](https://www.w3schools.com/whatis/whatis_cli.asp)      |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1. [30 mins] - Creating an SSH Key
	2. [5 mins] - Adding SSH keys to Git
	4. [10 mins] - Configuring Git account
	5. [1 hr] - Git Commands
	    - git config
	    - git init
	    - git status
	    - git add . / git add -A
	    - git commit
	    - git log
	6. [10 mins] - Creating a Git Repository
	7. [1 hr] - Other Git Commands
	    - git remote add
	    - git remote -v
	    - git remote get-url
	    - git remote set-url
	    - git remote remove
	    - git push
	    - git clone
	    - git pull
	8. [1 hr] Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

	1. CLI Commands
	2. Difference between CLI and Git commands
	3. Git Branching

[Back to top](#table-of-contents)