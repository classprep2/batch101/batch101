# Session Objectives

At the end of the session, the students are expected to:

- learn about managing changes in a file by using Git version control system.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5/s02)
- [Google Slide Presentation](https://docs.google.com/presentation/d/17InJFJne-HDOCSBq1y5EEFqA_A4vsmvgSCcIhx1Rk9E)
- [Lesson Plan](lesson-plan.md)

## Supplemental Materials

- [Getting Started - About Version Control](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control)
- [A Visual Git Reference](https://marklodato.github.io/visual-git-guide/index-en.html)
- [A Visual Guide to Version Control](https://betterexplained.com/articles/a-visual-guide-to-version-control/)
- [How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit)

# Lesson Proper

## Version Control

As a software/web developer, it is very critical for us to maintain a working version of the product (software) that we created collaboratively without breaking it by adding new features every time.

This is the problem that a version control system is trying to solve; to be able to simultaneously update the code base of our product without breaking it, such as by creating commits and branches. 

**Version control system** or **VCS** is a piece of software that manages (control) the revisions (changes) of a source code, document, and other collection of information in a folder (repository) and optimize collaboration of a team working on a single project or product.

## Git VCS

Git is an open source version control system that we will use to track changes in the files of our project. Git's [website](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control) has some diagrams to help us understand the concept of version control. To learn more regarding the benefits of using version control, you can read up [here](https://pantheon.io/blog/why-version-control-collaboration-accountability-security).

Open source simply means that we can use a system or software for free.

There are two types of Git repositories (or projects):

- **Local repositories** - these are repositories that are located in your own computer.
- **Remote repositories** - these are repositories that are located in the internet; specifically, they located on cloud services such as [GitLab](https://gitlab.com/) or [GitHub](https://github.com/).

## Git Commands

This section is for instructors only.

Overview of the basic Git commands that can be used to implement version control on a software project. Texts enclosed in square brackets `[]` signify a variable that must be replaced with an actual value.

### Basic Git Commands

`git init`

- Creates a git project on the current directory.

`git status`

- Lets you see which changes have been staged, which haven't, and which files aren't being tracked.

`git add [filename]`

- Prepares a file or set of files into the staging area, where the files are eventually added to the recorded history of changes.

`git commit -m "[message]"`

- Saves the changes added from `git add` to its recorded history of changes or *commits*.

`git config user.email "[email]"`

- Sets the user's email associated to the local Git repository.
- Note: adding `global` keyword like `git config --global user.email "[email]"` will set a given configuration as default.

`git config user.name "[name]"`

- Sets the user's real name associated to the local Git repository.

`git log`

- Displays the history of changes for the repository.
- Note: adding `oneline` keyword like `git log --oneline` will simplify the display output.

### Connecting to a Remote Repository

`git remote add origin [project-link]`

- Adds a reference to your GitLab project.
- Note: the word `origin` can be changed to developer's preferences.

`git remote get-url [remote-name]`

- Retrieves the URL on a given remote name (might be `origin` or any other remote name).

`git remote set-url origin [project-link]`

- Sets the URL on a given remote name.

`git push [remote-name] master`

- Uploads your local repository to your specified link (from `origin`).

`git pull [remote-name] master`

- Downloads your latest updates from remote GitLab repository.

`git clone [project-link]`

- Downloads your GitLab project on the current directory.

# Code Discussion

## Setting Up GitLab SSH Key

Before proceeding, we need to make sure that SSH keys are added to the GitLab accounts of the students.

SSH (or Secure Shell) key is a tool that we can use to avoid entering our username and password every time we are using certain git commands.

### Opening the Terminal

Prompt the students to open their terminal program.

- For Mac and Linux users, they have a built-in Terminal program.
- For Windows users, they can either use the Command Prompt or Windows Terminal.

For Windows users, executing the Git commands in the proceeding sessions is not limited via use of the Git Bash program. Command Prompt or Windows Terminal will work just fine.

Windows Terminal is preferred for Windows users because it can have multiple tabs in a single window (much like the Terminal for Linux users).

![readme-images/Untitled.png](readme-images/Untitled.png)

Interface of the Windows Terminal program with three tabs.

### Generating an SSH Key

To generate an SSH key, use the `ssh-keygen` command.

When the **Enter file in which to save key** is displayed, just press **Enter**.

When the **Enter passphrase** is displayed, you can either skip it by pressing **Enter** or enter your preferred passphrase before proceeding.

Enter the same passphrase then press **Enter**.

Upon completion, the terminal should look like this:

![readme-images/Untitled%201.png](readme-images/Untitled%201.png)

### Adding the Generated Key to GitLab

Go to the folder in which the key was saved (indicated in the Terminal earlier).

Open the **id_rsa.pub** via Notepad or any other text editor.

Copy the contents of the file.

Prompt the students to go to [https://gitlab.com/-/profile/keys](https://gitlab.com/-/profile/keys).

Paste the copied key into the big textarea and click the **Add key** button.

![readme-images/Untitled%202.png](readme-images/Untitled%202.png)

## Step 1. Set up folders.

In your preferred documents folder, create a folder named s**02** and add folders inside it named **discussion** and **activity**.

## Step 2. Set up discussion files.

Inside the **discussion** folder, create a file named **index.html**. 

## Step 3. Put initial file contents.

Inside the **index.html** file, add the following:

```html
<html>
    <head>
        <title>Git Demonstration</title>
    </head>
    <body>
        <h1>Hello World!</h1>
    </body>
</html>
```

When we open the file in Google Chrome, we will see something like this:

![readme-images/Untitled%203.png](readme-images/Untitled%203.png)

## Step 4. Set up local Git project.

Open a command line interface inside the **discussion-demo** folder and do the following in order:

- `git init` ****- to initialize a local Git repository.
- `git status` - to peek at the states of the files/folders in the repository.
- `git add .` - to prepare files for commit.
- `git config --global user.name "Student Name"` - set your global Git config for name.
- `git config --global user.email "Student Email"` - set your global Git config for email.
- `git config --global --list` - check whether the user information has been successfully added.
- `git commit -m "Add index.html"` - to save the changes to the history.
- `git log` - to check the version history of the repository.

For instructors who are undergoing this step again after their first batch, executing the `git config` again will just update your current git configuration.

## Step 5. Creating a project in GitLab.

Now that we have successfully saved our first recorded change in our local repository, we must upload our project to GitLab.

In your assigned student subgroup (denoted by **batch/lastname-firstname** naming), click the **New Project** button.

![readme-images/Untitled%204.png](readme-images/Untitled%204.png)

Then, select **Create blank project** option.

![readme-images/Untitled%205.png](readme-images/Untitled%205.png)

Name the project as **S02**, add the session's title (**Git Basics**) as the project description then click the **Create project** button.

![readme-images/Untitled%206.png](readme-images/Untitled%206.png)

After creating the project, we should see the following output below.

![readme-images/Untitled%207.png](readme-images/Untitled%207.png)

Adding a project description will help the students quickly remember what a specific session was all about.

![readme-images/Untitled%208.png](readme-images/Untitled%208.png)

## Step 6. Pushing the local repo to GitLab.

In the created project, copy the SSH link from the **Clone** button.

![readme-images/Untitled%209.png](readme-images/Untitled%209.png)

Then, in your terminal, do the following commands in order:

- `git remote add origin [ssh-link]` - to create a reference to the GitLab project.
- `git push origin master` - to actually upload your local project.

Once finished, the GitLab project should look something like this:

![readme-images/Untitled%2010.png](readme-images/Untitled%2010.png)

## Writing Good Git Commit Messages

Commit messages are an important part of Git. It provides developers with a high-level idea of the change that was done in a given commit. Because of this, writing good commit messages is essential. Here are some of the basic rules and suggestions for writing commit messages:

1. Capitalize the first letter in the message.
2. Limit the message to 50 characters or less.
3. Do not end the commit message with a period.
    - Trailing punctuation is unnecessary. Besides, space is precious when you’re trying to keep them to 50 characters or less.
4. Use the imperative mood in the subject line.
    - *Imperative mood* just means “spoken or written as if giving a command or instruction”. A few examples:
        - Clean your room
        - Close the door
        - Take out the trash

# Activity

## Instructions

1. Create a file named **myself.txt** inside the **activity** folder.
2. Add a line that contains your full name.
3. Save the changes by doing `git add` and `git commit`.
4. Add an empty line after the full name then a paragraph that describes your motivation in joining the bootcamp.
5. Save the changes by doing `git add` and `git commit`.
6. Add an empty line after the paragraph then add another paragraph that briefly narrates your work experience.
7. Save the changes by doing `git add` and `git commit`.
8. Upload your changes to the GitLab project by doing `git push origin master`.

## Expected Output

The project in GitLab must have the same amount of commits from the activity's local repository.

Look at the Solution section to see the specific expected outputs.

## Solution

The git log should look like this:

![readme-images/Untitled%2011.png](readme-images/Untitled%2011.png)

The number of commits in the GitLab project should indicate a total of four commits:

![readme-images/Untitled%2012.png](readme-images/Untitled%2012.png)

When the **4 Commits** text is clicked, the output should look like this:

![readme-images/Untitled%2013.png](readme-images/Untitled%2013.png)