# WDC028 - S05 - HTML - Forms, Tables and Semantic HTML

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/14-QXbHHZpXb9x23GnUQfTNDUsGujrAYqMRH7VdjpOw4/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/14-QXbHHZpXb9x23GnUQfTNDUsGujrAYqMRH7VdjpOw4/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/frontend/s05) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/frontend/s05/Manual.html) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                                     | Link                                                         |
| --------------------------------------------------------- | ------------------------------------------------------------ |
| HTML Entities                                             | [Link](https://www.w3schools.com/html/html_entities.asp)     |
| HTML Tables - w3schools                                   | [Link](https://www.w3schools.com/html/html_tables.asp)       |
| HTML Tables - MDN                                         | [Link](https://developer.mozilla.org/en-US/docs/Learn/HTML/Tables/Basics) |
| HTML Forms                                                | [Link](https://www.w3schools.com/html/html_forms.asp)        |
| HTML Semantic Tags                                        | [Link](https://www.freecodecamp.org/news/semantic-html5-elements/) |
| SEO Advantages of Machine-Readable HTML 5 Semantic Markup | [Link](https://searchengineland.com/seo-advantages-of-machine-readable-html5-semantic-markup-314455) |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1. [10 mins] - HTML Entities
	2. [1 hr] - HTML Tables
		- table
		- thead
		- tr
		- th
		- tbody
		- td
		- tfoot
	3. [1 hr 20 mins] - HTML Forms
		- form
		- button
		- label
		- input
		- select
		- option
		- textarea
	4. [30 mins] - HTML Semantic Tags / Div tags
		- header
		- main
		- section
	5. [1 hr] - Activity


[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics


[Back to top](#table-of-contents)
