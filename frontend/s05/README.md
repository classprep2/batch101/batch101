# Session Objectives

At the end of the session, the students are expected to:

- learn how information can be gathered from the users by using form input elements;
- present tabular information by creating a table element; and
- make HTML code more meaningful by using semantic HTML tags.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5/s05)
- [Google Slide Presentation](https://docs.google.com/presentation/d/14-QXbHHZpXb9x23GnUQfTNDUsGujrAYqMRH7VdjpOw4)
- [Lesson Plan](lesson-plan.md)

## Supplemental Materials

- [HTML Forms (w3schools)](https://www.w3schools.com/html/html_forms.asp)
- [HTML Tables (w3schools)](https://www.w3schools.com/html/html_tables.asp)
- [HTML Table Basics (MDN Web Docs)](https://developer.mozilla.org/en-US/docs/Learn/HTML/Tables/Basics)
- [Semantic HTML5 Elements Explained (freeCodeCamp)](https://www.freecodecamp.org/news/semantic-html5-elements/)

# Lesson Proper

## Tables in HTML

Table elements allow us to arrange data into rows and columns of cells. By using tables, we can present data in a tabular format instead of narrating it into paragraphs.

Tables should not be used to layout elements in a web page. It is only meant to structure data in tabular format. ([reference](https://developer.mozilla.org/en-US/docs/Learn/HTML/Tables/Basics#when_should_you_not_use_html_tables))

## Meaningful Code Through Semantic HTML

Semantic HTML are codes in HTML that clearly defines its meaning to both the computer and the developer. For example, the `<table>` element is both readable by the computer and the developer and does not have ambiguous meaning. On the other hand, the `<b>` element is readable by the computer but the developer might not immediately identify the purpose of the element.

With semantic HTML, we can replace the `<b>` element into the more meaningful element `<strong>`. This way, we immediately know the specific purpose of the element.

These are some of the semantic HTML tags that can be used (from w3schools):

[List of Semantic HTML Tags](https://www.notion.so/deaa33960d4b4c9dbda00bfa8a746fa4)

## Form Input Elements

Web pages are not used solely for displaying output. It can also be used to gather data input from a user. There are a variety of tags that can be used to gather input

[Input Tags](https://www.notion.so/e9cfc4f32e0142db9375da0fdafff93c)

## How Forms Work

![readme-images/Untitled.png](readme-images/Untitled.png)

# Code Discussion

## Folder and File Preparation

Create a folder named **s05**, a folder named **discussion** inside the **s05** folder, then a file named **index.html** inside the **discussion** folder.

## Instructor Notes

- Start doing the code discussion from **table.html** in the **discussion** folder.
- Do the **form.html** file next.
- Emphasize the existence of semantic HTML tags found in **form.html** file.
- Show the URL change when the form is submitted.
    - To hide the information, add the `method="POST"` attribute/value to the form.
    - Show the URL when the form is submitted with the added attribute.
    - Differentiate that by default, `method="GET"` is used by the form and if sensitive information is included in the form, `method="POST"` is to be used.

# GitLab Upload

After finishing the code discussion, demonstrate to the students how to create a GitLab project inside their subgroup and push the code discussion with a commit message of "Add discussion code".

# [Activity](https://docs.google.com/presentation/d/14-QXbHHZpXb9x23GnUQfTNDUsGujrAYqMRH7VdjpOw4/edit#slide=id.gea4dba3c3c_0_911)

## Instructions
Create a table of products and a contact form with semantic HTML tags included.

1. In the S05 folder, Create an activity folder and a contact.html file.
2. Add a header using semantic HTML.
3. Create a section.
4. Add a table to represent the products offered.
5. Add another section for the form.
6. Create a contact form with the different input fields.
7. The form input fields should be required.
8. Suggestions must populate when filling out the form.
9. Add a button to submit the form.
10. Add a button to reset the form.
11. Create a git repository named S05.
12. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
13. Add the link in Boodle.

## Expected Output

![s05_output.png](readme-images/s05_output.png)

## Solution

**activity/contact.html**

```html
<!DOCTYPE html>
<html>
    <head>
        <title>Contact Form</title>
    </head>
    <body>

        <header>
            <h1>Zuitt IT Solutions</h1>
        </header>
        
        <main>
            <section>
                <h2>Products Offered</h2>

                <table border="1">
                    <thead>
                        <tr>
                            <th>Package</th>
                            <th>Price</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Basic</td>
                            <td>&#8369; 500.00</td>
                            <td>Package includes a fully functional website with desired specifications.</td>
                        </tr>
                        <tr>
                            <td>Premium</td>
                            <td>&#8369; 1000.00</td>
                            <td>Enjoy all the benefits of the basic package and also includes site support and maintainance.</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="3">Additional features after initial project is inclusive of charges</th>
                        </tr>
                    </tfoot>
                </table>
            </section>

            <section>
                <h2>Contact Form</h2>

                <form action="http://google.com">

                    <label for="first_name">First Name:</label><br>
                    <input type="text" name="first_name" id="first_name"required autocomplete><br>

                    <label for="last_name">Last Name:</label><br>
                    <input type="text" name="last_name" id="last_name" required autocomplete><br>

                    <label for="email">Email:</label><br>
                    <input type="email" name="email" id="email" required autocomplete><br>

                    <label for="contact">Contact Number:</label><br>
                    <input type="number" name="contact" id="contact" required autocomplete><br>

                    <label for="service_type">Service Type:</label><br>
                    <select name="service_type" id="service_type" required>
                        <option value="frontend">Front End Programming</option>
                        <option value="backend">Back End Programming</option>
                        <option value="fullstack">Full Stack End Programming</option>
                    </select><br>

                    <label>Preferred Contact:</label><br>

                    <input type="checkbox" name="contact1" id="contact1" value="email">
                    <label for="contact1">Email</label><br>

                    <input type="checkbox" name="contact2" id="contact2" value="phone">
                    <label for="contact2">Phone</label>

                    <br>
                    
                    <label>Package:</label><br>

                    <input type="radio" name="package" id="package1" value="basic">
                    <label for="package1">Basic</label><br>

                    <input type="radio" name="package" id="package2" value="premium">
                    <label for="package2">Premium</label>

                    <br><br>

                    <label for="message">Message</label><br>
                    <textarea name="message" rows="5" id="message"></textarea><br>

                    <button type="submit">Send Inquiry</button><br>
                    <button type="reset">Reset</button>

                </form>
            </section>
        </main>

    </body>
</html>
```