# Session Objectives

At the end of the session, the students are expected to:

- undergo orientation regarding the various aspects of the bootcamp; and
- make sure that all the software needed in the bootcamp have been installed successfully.

# Resources

## Instructional Materials

- [Google Slide Presentation](https://docs.google.com/presentation/d/1-oV-sSYq0IipuZnlb3gp4QEP_MZIVDPScY-1VIswgPc)
- [Lesson Plan](lesson-plan.md)

# Instructor Notes

- Coordinate with Instructor SV/OICs regarding the orientation that the bootcampers will undergo during this session:
    - HR Orientation (conducted by career advisors)
    - Bootcamp Orientation (conducted by you, the instructor)
- Ensure that all bootcampers have successfully installed the software needed for the bootcamp.
    - Links to the installation guides can be found in your batch timeline's **Supplementary Documents** sheet.
    - Links to the actual guides are also available in the speaker notes of the linked Google Slide presentation.
- Putting dashes in naming certain things (such as files, folders, etc.) is recommended over underscores for a variety of reasons:
    - In SEO, URLs with underscores treats two or more words as one.
    - When highlighting a URL from a web browser, the highlight stops at a hyphen but does not stop in underscores.
    - To learn more, go to the [style guide](https://developers.google.com/style/filenames) of Google Developer Documentation for more information.

# Tools Used in the Bootcamp

## Sublime Text 3

Text editor used for writing codes.

Sublime Text 3 is preferred over Visual Studio Code for the following reasons:
- Less RAM consumption
- Active habit development to practice code indention

## Git

Program for managing versions of code.

## GitLab

Cloud service used for uploading and downloading codes.

## GitLab Pages

GitLab feature that allows hosting of basic website for CSP1 project.

## Postman

Tool used to test responses of a web server.

## Heroku

Cloud service used for hosting CSP2 and CSP3 projects.