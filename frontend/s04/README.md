# Session Objectives

At the end of the session, the students are expected to:

- modify the default behavior of HTML elements by changing an element's attributes; and
- navigate to other web pages by using hyperlinks.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5/s04)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1SvtqZupz-XcS1b0-8wDXK4IvV8cmunGwb4dn-o9K5Fo)
- [Lesson Plan](lesson-plan.md)

## Supplemental Materials

- [HTML Links (w3schools)](https://www.w3schools.com/html/html_links.asp)
- [HTML Attributes (w3schools)](https://www.w3schools.com/html/html_attributes.asp)
- [HTML Attributes (Tutorial Republic)](https://www.tutorialrepublic.com/html-tutorial/html-attributes.php)

# Lesson Proper

## HTML Attributes

Each HTML element has a default "behavior" when shown to a browser. For example, when we use the `<ol>` tag to create an ordered list, we automatically get a list that are numbered from 1 to n.

However, when we want to change the default ordering system used, for example from numbered list to lettered list, we must use an attribute. To use one, we must write the following syntax:

```html
<element attribute="value">content</element>
```

The attribute specifies which behavior of the element will be changed and the value indicates the specific changes to the element's behavior. To use the ordered list example:

```html
<ol type="a">content</ol>
```

The code above will then change the ordering system used from numbers to letters.

Additionally, multiple attributes can be written in a single tag, for example:

```html
<ol type="a" reversed>content</ol>
```

In the code above, we both have the **type** and **reserved** attributes set to the ordered list.

Note that the **reversed** attribute is a boolean (true or false) attribute. When it is written in the ordered list, it reverses the order from **a,b,c** to **c,b,a** when shown in the browser.

There are different attributes for different elements and therefore a single attribute is not applicable to all elements. For complete reference of the attributes that can be modified to a given element, you can look at Mozilla's MDN web docs [reference](https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes).

## Hyperlinks

Hyperlinks are a crucial element to web pages. It gives us the ability to go to other web pages and allows each page to truly interconnect with one another.

To create a hyperlink, the following code is used:

```html
<a href="https://www.google.com">Text that can be clicked</a>
```

The `<a>` tag stands for anchor and is usually accompanied by an **href** attribute. The attribute will tell the browser where to go when the anchor is clicked.

**Links** refer to a web page address (the location indicated in the href attribute) while **hyperlinks** are page elements that you can click to go to a specific web page address.

### Absolute Links

Any links that are outside of your current domain are considered absolute links. For example, you are on the website of **[zuitt.co](http://zuitt.co)** and you have a link going to **[google.com](http://google.com)**, then the link is considered an absolute link. 

**Domains** refer to the name of a website.

### Relative Links

These are links that targets pages within the same website and are relative to the current page's location in a given website. Let's consider the following hierarchy of pages:

- **[https://zuitt.co](https://zuitt.co)**
    - **web-dev-program** (folder)
        - **what-is-coding.html**
        - **how-it-works.html**
        - **program-details.html**
        - **instructors.html**
    - **companies** (folder)
        - **hire-a-graduate.html**
        - **corporate-training.html**

If for example you are currently on the **hire-a-graduate** page and you want to go to the **corporate-training** page, you must create the following link:

```html
<a href="./corporate-training.html">Corporate Training</a>
```

As you noticed, the full address of the website is not needed and is instead replaced by a prefix of a dot and slash. It means that the link will go to a page in the same folder.

How about from **corporate-training** to **what-is-coding** inside the **web-dev-program** folder? Look at the following code:

```html
<a href="../web-dev-program/what-is-coding.html">What is Coding?</a>
```

Now, there are two dots and a slash. It means that the link will go outside of its current folder and up one level, then look for the **web-dev-program** folder, go inside it, and finally look for the **what-is-coding** page.

# Code Discussion

## Folder and File Preparation

Create a folder named **s04**, a folder named **discussion** inside the **s04** folder, then a file named **index.html** inside the **discussion** folder.

## Instructor Notes

- Start doing the code discussion from **index.html** in the **discussion** folder.
- Do the **link.html** file next.
- Create a folder named **images**, create a file named **index.html** inside it and then create hyperlinks for the first two HTML files as well as a hyperlink to Google's website.

# GitLab Upload

After finishing the code discussion, demonstrate to the students how to create a GitLab project inside their subgroup and push the code discussion with a commit message of "Add discussion code".

# Activity

## Instructions

![readme-images/Untitled.png](readme-images/Untitled.png)

- Create the files and folders from the shown diagram.
- For each and every HTML file, it should have the necessary links going to the rest of the HTML files in the diagram.
- Additionally, each HTML file should have a heading that indicates its name and location.
    - E.g. ("You are at activity/movies/dvd/index page")

## Expected Output

Go to the provided GitLab repository and check the **activity** folder for the expected output and solution.