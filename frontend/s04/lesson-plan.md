# WDC028 - S04 - HTML - Attributes and Hyperlinks

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1SvtqZupz-XcS1b0-8wDXK4IvV8cmunGwb4dn-o9K5Fo/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1SvtqZupz-XcS1b0-8wDXK4IvV8cmunGwb4dn-o9K5Fo/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/frontend/s04) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/frontend/s04/Manual.html) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                 | Link                                                         |
| ------------------------------------- | ------------------------------------------------------------ |
| HTML Attributes - w3schools           | [Link](https://www.w3schools.com/html/html_attributes.asp)   |
| HTML Attributes - tutorial republic   | [Link](https://www.tutorialrepublic.com/html-tutorial/html-attributes.php) |
| HTML Entities                         | [Link](https://www.w3schools.com/html/html_entities.asp)     |
| Absolute vs Relative links            | [Link](https://www.w3schools.com/html/html_links.asp)        |
| Image tag and supported image formats | [Link](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img) |
| Place Puppy                           | [Link](https://place-puppy.com/)                             |
| Place Kitten                          | [Link](https://placekitten.com/)                             |
| Zuitt Website                         | [Link](https://zuitt.co/)                                    |
| Attribute Reference - w3schools       | [Link](https://www.w3schools.com/tags/ref_attributes.asp)    |
| Attribute Reference - MDN             | [Link](https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes) |
| Naming Conventions                    | [Link](https://devopedia.org/naming-conventions)             |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1. [30 mins] - HTML Attributes
		- align / HTML entities
		- src / image tags / absolute and relative linking
		- type
		- reversed
	2. [15 mins] - Anchor tag
	3. [15 mins] - Absolute linking
	4. [1hr 45 mins] - Relative linking
	5. [15 mins] - Other HTML attributes
		- id
		- target
	6. [1 hr] - Activity


[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

	1. File Naming Convention

[Back to top](#table-of-contents)
