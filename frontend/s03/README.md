# Session Objectives

At the end of the session, the students are expected to:

- create a simple web page by learning what is hypertext markup language and how to create HTML files.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5/s03)
- [Google Slide Presentation](https://docs.google.com/presentation/d/19KfZ_aG3nJm8nrH8QuCb7eGIhazEvwydOduUdBNMlSg)
- [Lesson Plan](lesson-plan.md)

## Supplemental Materials

- [Interneting Is Hard - Basic Web Pages](https://www.internetingishard.com/html-and-css/basic-web-pages/)
- [TutorialsPoint - HTML Blocks](https://www.tutorialspoint.com/html/html_blocks.htm)
- [HTML Element (MDN Web Docs)](https://developer.mozilla.org/en-US/docs/Glossary/Element)
- [International Society for Knowledge Organization - Hypertext](https://www.isko.org/cyclo/hypertext)
- [A History of HTML](https://www.w3.org/People/Raggett/book4/ch02.html)

# Lesson Proper

## What is HTML?

HTML, or **HyperText Markup Language**, is a language designed to create interconnected web pages. HTML defines the structure of the web page elements such as but are not limited to display of:

- headings
- tables
- multimedia (such as images and videos)
- paragraphs of text

## History of HTML

This section is only for the apprecation of the instructors and is not included in the Google Slide presentation.

- In 1989, Tim Berners-Lee, the inventor of World Wide Web, was working at CERN in Geneva, Switzerland. [[1]](https://www.notion.so/1-a87f2b439f094b0d8802bb63fbe968f8)
- Research at CERN often involved collaboration among institutes from all of the world.
- Lee suggested a way to link relevant files within texts to form a "web" of information, instead of just pooling large number of research documents to be downloaded into individual computers.
- Lee's suggestion was connected to the concept of **hypertext**.

![readme-images/Untitled%209.png](readme-images/Untitled%209.png)

Illustration of the hypertext concept, each document is connected within its text though hyperlinks.

- These links from texts allowed researchers to quickly display a part of another page that is relevant to the page they were viewing at a given time, removing the need to download individual files.
- This hypertext system was already developed by Lee named **Enquire** back in 1980 (which was the basis for the modern World Wide Web).
- Enquire later on evolved into **H**yper**T**ext **M**arkup **L**anguage or HTML in 1991. The latest version of HTML to date is HTML5.

## HTML Terminologies

- **Elements** - these are the components that you see on a web page such as images, videos, headers, etc.
- **Tags** - these are the codes that actually create the elements.

### Terminology Illustration

- Tag (the code)

    ```html
    <h1>Title</h1>
    <h2>Subtitle</h2>
    ```

- Element (what we see)

    ![readme-images/Untitled.png](readme-images/Untitled.png)

Inside the code, an **element** is the whole `<h1>Title</h1>` while a **tag** is the `<h1></h1>` only (without the content).

## Structure of HTML Tags

- Tags with opening and closing tags are commonly referred to as **paired** tags.

![readme-images/Untitled%201.png](readme-images/Untitled%201.png)

- Tags without opening/closing tags are called **single** tags. The tag above creates a horizontal rule (line).

![readme-images/Untitled%202.png](readme-images/Untitled%202.png)

## Basic HTML Elements

- `<html>` - tells the browser to read the contents of the file as HTML.
- `<head>` - contains the details of the HTML.
- `<body>` - contains the actual content of the HTML file.
- `<h1>` to `<h6>` - specifies page headings.
- `<p>` - creates paragraph elements.
- `<ul>`, `<ol>`, `<li>` - forms a bulleted (ul) or numbered (ol) lists.

## Inline and Block Elements

- A web page adheres to what is called a **document flow**. This flow determines how an element is to be presented.
- A **block** element will cause a line break and occupy the full width of the web page.
- An **inline** element on the other hand will not cause a line break and will only take the width necessary to contain its content.

### Examples

- Example A: Paragraphs (Block Element)

![readme-images/Untitled%203.png](readme-images/Untitled%203.png)

- Example B: Buttons (Inline Element)

![readme-images/Untitled%204.png](readme-images/Untitled%204.png)

# Code Discussion

## Folder and File Preparation

Create a folder named **s03**, a folder named **discussion** inside the **s03** folder, then a file named **index.html** inside the **discussion** folder.

## Basic HTML Code

Inside the **index.html** file, add the following:

```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>HTML Introduction</title>
    </head>
    <body>
    </body>
</html>
```

Let's discuss the tags that were added in the file:

- `<!DOCTYPE HTML>` simply tells the browser that the file contains a web page.
- `<html>` contains the code for all of the other HTML tags.
- `<head>` contains document information not visible in the browser.
- `<title>` sets the title of the tab shown in the browser.
- `<body>` contains elements that are visible to the browser.

For the title, we will see something like this in our browser tab:

![readme-images/Untitled%205.png](readme-images/Untitled%205.png)

## Add Headings

Add the following in the body of **index.html**.

```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>HTML Introduction</title>
    </head>
    <body>
        <h1>Zuitt Coding Bootcamp</h1>
        <h3>John Smith</h3>
    </body>
</html>
```

Show in the browser the result of the added headings to the students.

## Add Paragraph

Add the following in the body of **index.html**.

```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>HTML Introduction</title>
    </head>
    <body>
        <h1>Zuitt Coding Bootcamp</h1>
        <h3>John Smith</h3>
        <p>Welcome to Zuitt! In this online bootcamp course, you will learn a lot of things. Let's enumerate all the things that will make you a full-stack web developer.</p>
    </body>
</html>
```

Show in the browser the result of the added paragraph to the students.

## Add Unordered List

Add the following in the body of **index.html**.

```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>HTML Introduction</title>
    </head>
    <body>
        <h1>Zuitt Coding Bootcamp</h1>
        <h3>John Smith</h3>
        <p>Welcome to Zuitt! In this online bootcamp course, you will learn a lot of things. Let's enumerate all the things that will make you a full-stack web developer.</p>
        <ul>
            <li>Languages</li>
            <li>Frameworks & Libraries</li>
            <li>Tools</li>
        </ul>
    </body>
</html>
```

Show in the browser the result of the added unordered list to the students.

## Add Nested List

Add the following in the body of **index.html**.

```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>HTML Introduction</title>
    </head>
    <body>
        <h1>Zuitt Coding Bootcamp</h1>
        <h3>John Smith</h3>
        <p>Welcome to Zuitt! In this online bootcamp course, you will learn a lot of things. Let's enumerate all the things that will make you a full-stack web developer.</p>
        <ul>
            <li>Languages</li>
            <ul>
                <li>HTML</li>
                <li>CSS</li>
                <li>JavaScript</li>
            </ul>
            <li>Frameworks & Libraries</li>
            <ol>
                <li>Bootstrap</li>
                <li>Express.js</li>
                <li>React.js</li>
            </ol>
            <li>Tools</li>
            <ol>
                <li>Git</li>
                <li>GitLab</li>
                <li>Sublime Text 3</li>
                <li>Postman</li>
            </ol>
        </ul>
    </body>
</html>
```

Show in the browser the result of the added nested list to the students.

## Add More Nested List

Add the following in the body of **index.html**.

```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>HTML Introduction</title>
    </head>
    <body>
        <h1>Zuitt Coding Bootcamp</h1>
        <h3>John Smith</h3>
        <p>Welcome to Zuitt! In this online bootcamp course, you will learn a lot of things. Let's enumerate all the things that will make you a full-stack web developer.</p>
        <ul>
            <li>Languages</li>
            <ul>
                <li>HTML</li>
                <li>CSS</li>
                <li>JavaScript</li>
            </ul>
            <li>Frameworks & Libraries</li>
            <ol>
                <li>Bootstrap</li>
                <li>Express.js</li>
                <li>React.js</li>
            </ol>
            <li>Tools</li>
            <ol>
                <li>Git</li>
                <li>GitLab</li>
                <li>Sublime Text 3</li>
                <li>Postman</li>
                <li>Others</li>
                <ul>
                    <li>MongoDB</li>
                    <li>Robo3T</li>
                </ul>
            </ol>
        </ul>
    </body>
	</html>
```

Show in the browser the result of the added nested list to the students.

## Add Closing Paragraph

```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>HTML Introduction</title>
    </head>
    <body>
        <h1>Zuitt Coding Bootcamp</h1>
        <h3>John Smith</h3>
        <p>Welcome to Zuitt! In this online bootcamp course, you will learn a lot of things. Let's enumerate all the things that will make you a full-stack web developer.</p>
        <ul>
            ...
        </ul>
        <p>There is a lot to learn in this bootcamp. It may look overwhelming at the beginning, but it's all worth it when you graduate!</p>
    </body>
</html>
```

Show in the browser the result of the added closing paragraph to the students.

## End Result

![readme-images/Untitled%206.png](readme-images/Untitled%206.png)

# GitLab Upload

After finishing the code discussion, demonstrate to the students how to create a GitLab project inside their subgroup and push the code discussion with a commit message of "Add discussion code".

# Activity

## Instructions

- Create an **activity** folder.
- Create a file named **activity-a.html** and **activity-b.html** inside the **activity** folder.
- Mimic the contents presented in the expected output.
- Once finished, create a commit with message "Add activity code" and push to the repository.

The second activity can be a stretch goal for the students.

## Expected Output

### activity-a.html

![readme-images/Untitled%207.png](readme-images/Untitled%207.png)

### activity-b.html

![readme-images/Untitled%208.png](readme-images/Untitled%208.png)

## Solution

### activity-a.html

```html
<!DOCTYPE html>

<html>

    <head>

        <title>SE Code of Ethics</title>

    </head>

    <body>

        <em>Science and Engineering Ethics (2001) 7, 231-238</em>

        <hr>

        <h1>Software Engineering Code of Ethics and Professional Practice</h1>
        <h3>IEEE-CS/ACM Joint Task Force on Software Engineering Ethics and Professional Practices</h3>

        <hr>

        <p>
            <strong>Keywords</strong>: code of ethics, professional responsibility, software engineering
        </p>

        <h4>PREAMBLE</h4>

        <p>
            The short version of the code summarizes aspirations at a high level of abstraction. The
            clauses that are included in the full version give examples and details of how these
            aspirations change the way we act as software engineering professionals. Without the
            aspirations, the details can become legalistic and tedious; without the details, the
            aspirations can become high-sounding but empty; together, the aspirations and the
            details form a cohesive code.
        </p>

        <p>
            Software engineers shall commit themselves to making the analysis, specification,
            design, development, testing, and maintenance of software a beneficial and respected
            profession. In accordance with their commitment to the health, safety, and welfare of
            the public, software engineers shall adhere to the following eight Principles:
        </p>

        <h4>PRINCIPLES</h4>

        <ol>
            <li>
                <em>Public</em>. Software engineers shall act consistently with the public interest.
            </li>
            <li>
                <em>Client and employer</em>. Software engineers shall act in a manner that is in the best interests of their client and employer, consistent with the public interest.
            </li>
            <li>
                <em>Product</em>. Software engineers shall ensure that their products and related modifications meet the highest professional standards possible.
            </li>
            <li>
                <em>Judgment</em>. Software engineers shall maintain integrity and independence in their professional judgment.
            </li>
            <li>
                <em>Management</em>. Software engineering managers and leaders shall subscribe to and promote an ethical approach to the management of software development and maintenance.
            </li>
            <li>
                <em>Profession</em>. Software engineers shall advance the integrity and reputation of theprofession consistent with the public interest.
            </li>
            <li>
                <em>Colleagues</em>. Software engineers shall be fair to and supportive of their colleagues.
            </li>
            <li>
                <em>Self</em>. Software engineers shall participate in lifelong learning regarding the practice of their profession and shall promote an ethical approach to the practice of the profession.
            </li>
        </ol>

    </body>

</html>
```

### activity-b.html

```html
<!DOCTYPE html>

<html>

    <head>

        <title>IT Code of Ethics</title>

    </head>

    <body>

        <h1>Code of Ethics of the Filipino Computing and Information Technology Professional</h1>

        <p>
            For the purposes of this Code, the following terms are defined as follows:
        </p>

        <p>
            <strong>Information Technology</strong>
        </p>

        <p>
            The preparation, collection, creation, transport, retrieval, storage, access, 
            presentation and transformation of electronic information in all its forms including, 
            but not limited to, voice, graphics, text, video, data and image.
        </p>

        <p>
            <strong>Information Technology Professional</strong>
        </p>

        <p>
            One who develops or provides information technology products and/or services 
            to the public.
        </p>

        <h2>Preamble</h2>

        <p>
            I will use my special knowledge and skills for the benefit of the public. I will serve 
            employers and clients with integrity, subject to an overriding responsibility to the 
            public interest, and I will strive to enhance the competence and prestige of the 
            professional. By these, I mean:
        </p>

        <ul>
            <li>
                I will <strong>promote public knowledge, understanding and appreciation</strong> of information technology;
            </li>
            <li>
                I will <strong>consider the general welfare and public good</strong> in the performance of my work;
            </li>
            <li>
                I will advertise goods or professional services in a <strong>clear and truthful manner</strong>;
            </li>
            <li>
                I will <strong>comply and strictly abide by the intellectual property laws, patent laws and other 
                related laws</strong> in respect of information technology;
            </li>
            <li>
                I will <strong>accept full responsibility for the work undertaken</strong> and will utilize my skills 
                with <strong>competence and professionalism</strong>;
            </li>
            <li>
                I will make <strong>truthful statements</strong> on my areas of competence as well as the 
                capabilities and qualities of my products and services;
            </li>
            <li>
                I will not <strong>disclose or use any confidential information obtained</strong> in the course of 
                professional duties <strong>without the consent of the parties</strong> concerned, <strong>except when required by law</strong>;
            </li>
            <li>
                I will <strong>try to attain the highest quality</strong> in both the products and services I offer;
            </li>
            <li>
                I will <strong>not knowingly participate</strong> in the development of Information Technology Systems 
                that will promote the <strong>commission of fraud and other unlawful acts</strong>;
            </li>
            <li>
                I will <strong>uphold and improve</strong> the IT professional standards through <strong>continuing 
                professional development</strong> in order to enhance the IT profession.
            </li>
        </ul>

    </body>

</html>
```